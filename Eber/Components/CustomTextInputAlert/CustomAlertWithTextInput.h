//
//  CustomAlertWithTitle.h
//    Eber ClientProvider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CustomAlertWithTextInput;
@protocol CustomAlertWithTextInput <NSObject>
@required
- (void) onClickOkButton:(NSString*)inputTextData view:(CustomAlertWithTextInput*)view;
@end


@interface CustomAlertWithTextInput: UIView<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
-(instancetype)initWithTitle:(NSString *)title placeHolder:(NSString *)placeHolder delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle okButtonTitle:(NSString *)okButtonTitle;
@property (weak, nonatomic) IBOutlet UIView *alertView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtInput;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (nonatomic, assign) id<CustomAlertWithTextInput> delegate;
- (IBAction)onClickBtnOk:(id)sender;
- (IBAction)onClickBtnCancel:(id)sender;
@end
