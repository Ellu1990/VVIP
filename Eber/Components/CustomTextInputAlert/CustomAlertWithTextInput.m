//
//  CustomAlertWithTitle.m
//    Eber ClientProvider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "CustomAlertWithTextInput.h"
#import "UtilityClass.h"
#import "NSObject+Constants.h"
#import "AppDelegate.h"
#import "UIColor+Colors.h"
#import "UILabel+overrideLabel.h"
@implementation CustomAlertWithTextInput
- (instancetype)initWithTitle:(NSString*)title placeHolder:(nullable NSString *)placeHolder delegate:(nullable id)delegate cancelButtonTitle:(nullable NSString *)cancelButtonTitle okButtonTitle:(nullable NSString *)okButtonTitle
{
    self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
    if (self)
    {
        NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"CustomAlertWithTextInput" owner:nil options:nil];
        self = [nibContents lastObject];
        self.delegate = delegate;
        [_lblTitle setTextColor:[UIColor textColor]];
        self.frame=APPDELEGATE.window.frame;
        self.alertView.center=self.center;
        self.alertView=[[UtilityClass sharedObject]addShadow:self.alertView];
        [self setLocalization];
        [_lblTitle setText:[title capitalizedString] withColor:[UIColor textColor]];
        [_txtInput setPlaceholder:placeHolder];
        if ([placeHolder isEqualToString:NSLocalizedString(@"CURRENT_PASSWORD", nil)])
        {
            [_txtInput setSecureTextEntry:YES];
        }
        [_btnCancel setTitle:[cancelButtonTitle uppercaseString] forState:UIControlStateNormal];
        [_btnOk setTitle:[okButtonTitle uppercaseString] forState:UIControlStateNormal];
        
        [[UIApplication sharedApplication].keyWindow addSubview:self];
        [_txtInput becomeFirstResponder];
        return self;
    }
    return self;
}
- (IBAction)onClickBtnOk:(id)sender
{
    [self endEditing:YES];
    if (_txtInput.text.length>1)
    {
		[_delegate onClickOkButton:_txtInput.text view:self];
    }
    else
    {
        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_FILL_THE_REQUIRED_FIELD", nil)];
    }
}

- (IBAction)onClickBtnCancel:(id)sender
{
    [[UtilityClass sharedObject] animateHide:self];
}

-(void)setLocalization
{
    [_lblTitle setTextColor:[UIColor labelTextColor]];
    [_txtInput setTextColor:[UIColor textColor]];
    [_btnCancel setBackgroundColor:[UIColor buttonColor]];
    [_btnOk setBackgroundColor:[UIColor buttonColor]];
    
    [_btnCancel setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [_btnOk setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [self setBackgroundColor:[UIColor clearColor]];
    [_alertView setBackgroundColor:[UIColor whiteColor]];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_scrollView setContentOffset:CGPointMake(0, 0)];
    [textField resignFirstResponder];
    [self onClickBtnOk:nil];
     return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint pt;
    CGRect rc = [_alertView bounds];
    rc = [_alertView convertRect:rc toView:_scrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -=100;
    [_scrollView setContentOffset:pt animated:YES];
}
@end
