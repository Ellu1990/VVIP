//
//  MyCarouselView.h
//  scrollDemo
//
//  Created by My Mac on 7/21/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wobjc-missing-property-synthesis"


#import <Availability.h>
#undef weak_delegate
#undef __weak_delegate
#if __has_feature(objc_arc) && __has_feature(objc_arc_weak) && \
(!(defined __MAC_OS_X_VERSION_MIN_REQUIRED) || \
__MAC_OS_X_VERSION_MIN_REQUIRED >= __MAC_10_8)
#define weak_delegate weak
#else
#define weak_delegate unsafe_unretained
#endif
#import <QuartzCore/QuartzCore.h>

#define MYCAROUSELVIEW_IOS
#ifdef MYCAROUSELVIEW_IOS
#else
#import <Cocoa/Cocoa.h>
typedef NSView UIScrollView;
#endif
#import "CarouselView.h"

@protocol MyCarouselDelegate,MyCarouselDataSource;

@interface MyCarouselView : UIScrollView
@property (nonatomic, readonly) NSInteger numberOfItems;
@property (nonatomic, readonly) MyCarouselView *view;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, weak_delegate) IBOutlet id<MyCarouselDelegate> delegatex;
@property (nonatomic, weak_delegate) IBOutlet id<MyCarouselDataSource> datasource;
-(void)setUp;
-(void)reloadData;
-(void)reloadItemAtIndexes:(NSInteger)selectedIndex andCurrentSelectIndex:(NSInteger)currentIndex;
@property (nonatomic, strong) NSMutableSet *itemViewPool;
@property (nonatomic, strong) NSMutableDictionary *itemViews;
@end



@protocol MyCarouselDelegate <NSObject>
@optional
- (void)myView:(MyCarouselView *)myView didSelectItemAtIndex:(NSInteger)index;
- (void)myView:(MyCarouselView *)myView didLongPressedItemAtIndex:(NSInteger)index;
@end


@protocol MyCarouselDataSource <NSObject>
- (NSInteger)numberOfItemsInMyView:(MyCarouselView *)myView;
- (CarouselView *)myView:(MyCarouselView *)myView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view;
@end
