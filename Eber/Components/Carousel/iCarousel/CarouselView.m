//
//  CarouselView.m
//  scrollDemo
//
//  Created by My Mac on 7/22/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "CarouselView.h"
#import "UIColor+Colors.h"

@implementation CarouselView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#pragma mark - Methods

- (void)setCellData:(NSMutableDictionary *)dict
{
	dictCellData = dict;
    self.img.image=[UIImage imageNamed:@"car"];
    [_label setTextColor:[UIColor labelTitleColor]];
	self.label.text = [dictCellData objectForKey:@"name"];
    [self.selecter setBackgroundColor:[UIColor buttonColor]];
	if ([[dictCellData objectForKey:@"selected"] boolValue])
    {
			self.selecter.hidden = NO;
		} else {
			self.selecter.hidden = YES;
		}
}

-(UIImage *) getImageFromURL:(NSString *)fileURL {
	UIImage * result;
	
	NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
	result = [UIImage imageWithData:data];
	
	return result;
}

@end
