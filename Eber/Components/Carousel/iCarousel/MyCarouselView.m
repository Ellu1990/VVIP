//
//  MyCarouselView.m
//  scrollDemo
//
//  Created by My Mac on 7/21/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "MyCarouselView.h"
#import <objc/message.h>
#import <Availability.h>
#if !__has_feature(objc_arc)
#error This class requires automatic reference counting
#endif

#ifdef MYCAROUSELVIEW_MACOS
#else
#endif

@implementation NSObject (MyCarouselView)
- (void)didSelectItemAtIndex:(__unused NSInteger)index {}
@end
@implementation MyCarouselView
{
	
}
- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.contentMode=UIViewContentModeCenter;
        
    }
    return self;
}
-(void)setUp
{
	self.backgroundColor=[UIColor grayColor];
    
	_currentIndex=2;
}
- (void)updateItemSizeAndCount
{
_numberOfItems = [_datasource numberOfItemsInMyView:self];
}
-(void)reloadData
{
    self.contentMode=UIViewContentModeCenter;
	[[self subviews]
	 makeObjectsPerformSelector:@selector(removeFromSuperview)];
	[self updateItemSizeAndCount];
	 for (int i = 0; i < _numberOfItems; i++)
	 {
			CarouselView *view=[_datasource myView:self viewForItemAtIndex:i reusingView:[self dequeueItemView]];
			view.frame=CGRectMake((i*view.frame.size.width), 0, view.frame.size.width, 80);
			view.tag=i;
            UILongPressGestureRecognizer* longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(gestureLongPressHandler:)];
            [longPressGesture setMinimumPressDuration:1.0];
            [view addGestureRecognizer:longPressGesture];
         
			UITapGestureRecognizer *letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectCar:)];
			[view addGestureRecognizer:letterTapRecognizer];
			[self addSubview:view];
			if (i==_numberOfItems-1)
			{
                self.contentSize = CGSizeMake(view.frame.origin.x+view.frame.size.width+10,self.frame.size.height);
                self.backgroundColor = [UIColor clearColor];
                CGFloat newContentOffsetX = (self.contentSize.width - self.frame.size.width) / 2;
                self.contentOffset = CGPointMake(newContentOffsetX, 0);
            }
            view=nil;
	}
}
- (void)didSelectCar:(UITapGestureRecognizer*)sender
{
    UIView *view = sender.view;
	_currentIndex=view.tag;
	[_delegatex myView:self didSelectItemAtIndex:_currentIndex];
	
}
-(void)gestureLongPressHandler:(UILongPressGestureRecognizer*)sender
{
    UIView *view = sender.view;
    _currentIndex=view.tag;
    [_delegatex myView:self didLongPressedItemAtIndex:_currentIndex];
}
-(void)reloadItemAtIndexes:(NSInteger)selectedIndex andCurrentSelectIndex:(NSInteger)currentIndex
{
    CarouselView *selectedView=[[self subviews] objectAtIndex:selectedIndex];
    [selectedView.selecter setHidden:YES];
    selectedView.label.font = [selectedView.label.font fontWithSize:12.0f];
    selectedView=[[self subviews] objectAtIndex:currentIndex];
    [selectedView.selecter setHidden:NO];
    selectedView.label.font = [selectedView.label.font fontWithSize:14.0f];
}
#pragma mark -
#pragma mark View queing

- (void)queueItemView:(UIView *)view
{
	if (view)
	{
		[_itemViewPool addObject:view];
	}
}

- (UIView *)dequeueItemView
{
	UIView *view = [_itemViewPool anyObject];
	if (view)
	{
		[_itemViewPool removeObject:view];
	
	}
	return view;
}



@end
