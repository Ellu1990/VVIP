//
//  CarouselView.h
//  scrollDemo
//
//  Created by My Mac on 7/22/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarouselView : UIView
{
    NSMutableDictionary *dictCellData;
}

@property (strong, nonatomic) IBOutlet UIImageView *img;
@property (strong, nonatomic) IBOutlet UILabel *label;
@property (strong, nonatomic) IBOutlet UILabel *selecter;
- (void)setCellData:(NSMutableDictionary *)dict;

@end
