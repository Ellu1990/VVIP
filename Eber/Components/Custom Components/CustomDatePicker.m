//
//  CustomAlertWithTitle.m
//    Eber ClientProvider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "CustomDatePicker.h"
#import "UtilityClass.h"
#import "NSObject+Constants.h"
#import "AppDelegate.h"
#import "UIColor+Colors.h"
#import "UILabel+overrideLabel.h"
@implementation CustomDatePicker
@synthesize delegate;
UIView* coverView;
    
-(void)awakeFromNib
{
    [super awakeFromNib];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:3];
    NSDate *maxDate = [gregorian dateByAddingComponents:comps toDate:currentDate  options:0];
    currentDate=[NSDate dateWithTimeIntervalSinceNow:60*30];
    [_dateTimePicker setMinimumDate:currentDate];
    [_dateTimePicker setMaximumDate:maxDate];
    [_dateTimePicker setDatePickerMode:UIDatePickerModeDate];
}
- (IBAction)onClickBtnClose:(id)sender
{
    [[UtilityClass sharedObject] animateHide:self];
    [coverView removeFromSuperview];
    
}
-(void)setLocalization
{
    self.backgroundColor=[UIColor whiteColor];
    [_btnDone setBackgroundColor:[UIColor buttonColor]];
    [_btnDone setTitle:[NSLocalizedString(@"DONE", nil) uppercaseString] forState:UIControlStateNormal];
    [_btnDone setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
}

+(CustomDatePicker *)getCustomDatePickerwithParent:(id)parent {
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"CustomDatePicker" owner:nil options:nil];
    CustomDatePicker *view = [nibContents lastObject];
    view.parent = parent;
    
    view.center=APPDELEGATE.window.center;
    view=(CustomDatePicker*)[[UtilityClass sharedObject]addShadow:view];
    [view setLocalization];
    if([parent isKindOfClass:[UIViewController class]])
    {
        UIViewController *v=(UIViewController*)parent;
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        coverView = [[UIView alloc] initWithFrame:screenRect];
        coverView.backgroundColor = [UIColor clearColor] ;
        view.delegate=parent;
        [v.view addSubview:coverView];
    }
    [[UIApplication sharedApplication].keyWindow addSubview:view];
    
   
    return view;
}
- (IBAction)onClickBtnDone:(id)sender
{
    if (_dateTimePicker.datePickerMode==UIDatePickerModeTime)
    {
       
        NSDate *currentDate = [NSDate date];
        long currentTime=[self timeInterval:currentDate];
        long selectedTime=[self timeInterval:_dateTimePicker.date];
        long timeInMilisecond=selectedTime-currentTime;
        NSString *strTime=[NSString stringWithFormat:@"%ld",timeInMilisecond];
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:_dateTimePicker.date];
        NSInteger hour = [components hour];
        
        [delegate onDateSelected:strTime andHour:hour];
        [coverView removeFromSuperview];
        [[UtilityClass sharedObject] animateHide:self];
    }
    else
    {
        _dateTimePicker.datePickerMode=UIDatePickerModeTime;
        [_dateTimePicker reloadInputViews];
    }
}
- (long)timeInterval:(NSDate*)date
{
 
    long milliseconds = (long)(NSTimeInterval)([date timeIntervalSince1970]*1000);
  return milliseconds;
}
@end
