//
//  CustomAlertWithTitle.h
//    Eber ClientProvider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CustomDatePickerDelegate <NSObject>
@required
- (void) onDateSelected:(NSString*)SelectedDate andHour:(NSInteger)hours;
@end

@interface CustomDatePicker : UIView
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
@property (nonatomic, assign) id<CustomDatePickerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIDatePicker *dateTimePicker;
@property(strong,nonatomic)id parent;
+(CustomDatePicker *)getCustomDatePickerwithParent:(id)parent;



@end
