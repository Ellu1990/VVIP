//
//  CustomAlertWithTitle.h
//    Eber ClientProvider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
@protocol SelectImageDelegate <NSObject>
@required
- (void) setImage:(UIImage*)image;
@end
@interface SelectImage: UIView<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic, assign) id<SelectImageDelegate> delegate;
/*
 *  Setting default appearance for all WCAlertView's
 */
@property(strong,nonatomic)id parent;
+(SelectImage *)getSelectImageViewwithParent:(id)parent;
@property (weak, nonatomic) IBOutlet UIButton *btnOverLay;
@property (weak, nonatomic) IBOutlet UILabel *lblCamera;
@property (weak, nonatomic) IBOutlet UILabel *lblGallery;
- (IBAction)onClickBtnOverLay:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewForSelectImage;

@end
