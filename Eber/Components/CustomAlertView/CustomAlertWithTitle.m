//
//  CustomAlertWithTitle.m
//    Eber ClientProvider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "CustomAlertWithTitle.h"
#import "UtilityClass.h"
#import "NSObject+Constants.h"
#import "AppDelegate.h"
#import "UIColor+Colors.h"
#import "UILabel+overrideLabel.h"
@implementation CustomAlertWithTitle
{
DialogCompletionBlock dataBlock;
}
- (instancetype)initWithTitle:(NSString*)title message:(nullable NSString *)message delegate:(id<CustomAlertDelegate>) delegate cancelButtonTitle:(nullable NSString *)cancelButtonTitle otherButtonTitles:(nullable NSString *)otherButtonTitle
{
    self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
    if (self)
    {
        NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"CustomDialogWithTitle" owner:nil options:nil];
        self = [nibContents lastObject];
        self.delegate = delegate;
        [_lblTitle setTextColor:[UIColor textColor]];
        self.frame=APPDELEGATE.window.frame;
        self.alertView.center=self.center;
        self.alertView=[[UtilityClass sharedObject]addShadow:self.alertView];
        [self setLocalization];
        self.backgroundColor=[UIColor clearColor];
        [self.lblTitle setText:[title capitalizedString]];
        [self.lblMessage setText:message];
        [self.btnNo setTitle:[cancelButtonTitle uppercaseString] forState:UIControlStateNormal];
        [self.btnYes setTitle:[otherButtonTitle uppercaseString] forState:UIControlStateNormal];
        [[UIApplication sharedApplication].keyWindow addSubview:self];
           return self;
    }
    return self;
}
- (IBAction)onClickBtnNo:(id)sender
{
    [[UtilityClass sharedObject] animateHide:self];
    if ([self.delegate respondsToSelector:@selector(onClickClose:)])
    {
        [_delegate onClickClose:self];
    }
    
}
-(void)setLocalization
{
    
    [_lblTitle setTextColor:[UIColor textColor]];
    [_lblMessage setTextColor:[UIColor textColor]];
    [_btnNo setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [_btnYes setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [_btnNo setBackgroundColor:[UIColor buttonColor]];
    [_btnYes setBackgroundColor:[UIColor buttonColor]];
    [self setBackgroundColor:[UIColor clearColor]];
    [self.alertView setBackgroundColor:[UIColor whiteColor]];
    self.backgroundColor=[UIColor clearColor];
}
- (IBAction)onClickBtnYes:(id)sender
{
    [[UtilityClass sharedObject] animateHide:self];
    if (self.delegate==nil)
    {
       
        [self blockMethodBtnOkPressedWithBlock:dataBlock];
    }
    else
    {
    [self.delegate onClickCustomDialogOk:self];
    }
}
- (void)setColor:(UIColor *)titleColor message:(UIColor *)messageColor cancelButtonTitle:(UIColor *)cancelButtonColor DoneButtonTitle:(UIColor *)otherButtonColor BackGroundColor:(UIColor*)backGroundColor;
{
    self.lblTitle.textColor=titleColor;
    self.lblMessage.textColor=messageColor;
    self.btnNo.titleLabel.textColor=cancelButtonColor;
    self.btnYes.titleLabel.textColor=otherButtonColor;
}
-(void)blockMethodBtnOkPressedWithBlock:(DialogCompletionBlock)block
{
    
}
-(void)setTitle:(NSString*)title andMessage:(NSString*)message
{
    [_lblTitle setText:[title capitalizedString]];
    [_lblMessage setFontForLabel:_lblMessage withMaximumFontSize:35 andMaximumLines:1];
    [_lblMessage setText:message];
}

@end
