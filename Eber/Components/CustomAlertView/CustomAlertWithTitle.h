//
//  CustomAlertWithTitle.h
//    Eber ClientProvider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CustomAlertWithTitle;
@protocol CustomAlertDelegate <NSObject>
@optional
- (void) onClickClose:(CustomAlertWithTitle*)view;
@required
- (void) onClickCustomDialogOk:(CustomAlertWithTitle*)view;
@end
typedef void (^DialogCompletionBlock)(NSString *str);

@interface CustomAlertWithTitle : UIView

-(instancetype)initWithTitle:(NSString*)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitle;
@property (nonatomic, assign) id<CustomAlertDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *btnYes;
@property (weak, nonatomic) IBOutlet UIButton *btnNo;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *alertView;
@property (nonatomic,strong) UIColor *buttonTextColor;
@property (nonatomic,strong) UIFont  *buttonFont;
@property (nonatomic,strong) UIColor *buttonShadowColor;
@property (nonatomic,assign) CGSize   buttonShadowOffset;
@property (nonatomic,assign) CGFloat  buttonShadowBlur;
@property (nonatomic,assign) CGFloat cornerRadius;
-(void)setTitle:(NSString*)title andMessage:(NSString*)message;
@property(strong,nonatomic)id parent;
-(void)blockMethodBtnOkPressedWithBlock:(DialogCompletionBlock)block;
-(void)setColor:(UIColor *)titleColor message:(UIColor *)messageColor cancelButtonTitle:(UIColor *)cancelButtonColor DoneButtonTitle:(UIColor *)otherButtonColor BackGroundColor:(UIColor*)backGroundColor;
@end
