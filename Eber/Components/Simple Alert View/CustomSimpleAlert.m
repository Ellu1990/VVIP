//
//  CustomAlertWithTitle.m
//    Eber ClientProvider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "CustomSimpleAlert.h"
#import "UtilityClass.h"
#import "NSObject+Constants.h"
#import "AppDelegate.h"
#import "UIColor+Colors.h"
#import "UILabel+overrideLabel.h"
@implementation CustomSimpleAlert
-(instancetype)initWithTitle:(NSString*)title message:(NSString *)message delegate:(id)delegate okButtonTitle:(NSString *)okButtonTitle
{
    self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
    if (self)
    {
        NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"CustomSimpleAlert" owner:nil options:nil];
        self = [nibContents lastObject];
        self.delegate = delegate;
        
        [_lblTitle setTextColor:[UIColor textColor]];
        self.frame=APPDELEGATE.window.frame;
        
        self.alertView.center=self.center;
        self.alertView=[[UtilityClass sharedObject]addShadow:self.alertView];
        [self setLocalization];
        self.backgroundColor=[UIColor clearColor];
        [self.lblTitle setText:[title capitalizedString]];
        [self.lblMessage setText:message];
        [self.btnOk setTitle:[okButtonTitle uppercaseString] forState:UIControlStateNormal];
        [[UIApplication sharedApplication].keyWindow addSubview:self];
        return self;
    }
    return self;
}
- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle
{
    self.lblTitle.text=title;
    self.lblMessage.text=message;
    [_btnOk setTitle:cancelButtonTitle forState:UIControlStateNormal];
    [_lblTitle setFontForLabel:_lblTitle withMaximumFontSize:30 andMaximumLines:2];
    [_lblMessage   setFontForLabel:_lblMessage  withMaximumFontSize:16 andMaximumLines:3];
    
 }
- (IBAction)onClickBtnOk:(id)sender
{
    [[UtilityClass sharedObject] animateHide:self];
    [_delegate onClickCustomDialogOk:self];
}

- (void)setColor:(UIColor *)titleColor message:(UIColor *)messageColor cancelButtonTitle:(UIColor *)cancelButtonColor DoneButtonTitle:(UIColor *)otherButtonColor BackGroundColor:(UIColor*)backGroundColor;
{
    self.lblTitle.textColor=titleColor;
    self.lblMessage.textColor=messageColor;
    self.btnOk.titleLabel.textColor=cancelButtonColor;
    
}
-(void)setLocalization
{
    self.lblTitle.textColor=[UIColor labelTextColor];
    self.lblMessage.textColor=[UIColor textColor];
    self.btnOk.titleLabel.textColor=[UIColor buttonTextColor];
    self.btnOk.backgroundColor=[UIColor buttonColor];
    self.backgroundColor=[UIColor clearColor];
    self.alertView.backgroundColor=[UIColor whiteColor];
}
@end
