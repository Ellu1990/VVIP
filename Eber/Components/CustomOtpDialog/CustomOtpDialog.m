//
//  CustomAlertWithTitle.m
//    Eber ClientProvider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "CustomOtpDialog.h"
#import "UtilityClass.h"
#import "NSObject+Constants.h"
#import "AppDelegate.h"
#import "UIColor+Colors.h"
#import "UILabel+overrideLabel.h"
#import "PreferenceHelper.h"
#import "AFNHelper.h"

@implementation CustomOtpDialog
@synthesize isSmsOtpOn,isEmailOtpOn;
-(instancetype)initWithOtpEmail:(NSString*)otpEmail optSms:(NSString *)otpSms emailOtpOn:(BOOL)isEmailOtp smsOtpOn:(BOOL)isSmsOtp delegate:(id)delegate
{
    self = [super initWithFrame:[[UIScreen mainScreen] bounds]];
    if (self)
    {
        NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"CustomOtpDialog" owner:nil options:nil];
        self = [nibContents lastObject];
        self.delegate = delegate;
        [_lblTitle setTextColor:[UIColor textColor]];
        self.frame=APPDELEGATE.window.frame;
        self.alertView.center=self.center;
        self.alertView=[[UtilityClass sharedObject]addShadow:self.alertView];
        [self setLocalization];
        strSmsOtp=otpSms;
        strEmailOtp=otpEmail;
        isEmailOtpOn=isEmailOtp;
        isSmsOtpOn=isSmsOtp;
        [self adjustFrames];
        [[UIApplication sharedApplication].keyWindow addSubview:self];
        return self;
    }
    return self;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
   [self onClickBtnOk:nil];
    return true;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    CGPoint pt;
    CGRect rc = [_alertView bounds];
    rc = [_alertView convertRect:rc toView:_scrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -=30;
    [_scrollView setContentOffset:pt animated:YES];
}
-(void)adjustFrames
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelay:1.0];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    self.autoresizesSubviews = NO;
    self.alertView.autoresizesSubviews = NO;
   
    if (isEmailOtpOn && !isSmsOtpOn)
    {
        CGRect frame= _alertView.frame;
        frame.size.height-=_txtEmailOtp.frame.size.height;
        [_txtSmsOtp setHidden:YES];
        _alertView.frame=frame;
        frame=_btnOk.frame;
        frame.origin.y-=_txtEmailOtp.frame.size.height;
        _btnOk.frame=frame;
        frame=_btnCancel.frame;
        frame.origin.y-=_txtEmailOtp.frame.size.height;
        _btnCancel.frame=frame;
        
    }
    else if(!isEmailOtpOn && isSmsOtpOn)
    {
        CGRect frame= _alertView.frame;
        _txtSmsOtp.frame=_txtEmailOtp.frame;
        frame.size.height-=_txtSmsOtp.frame.size.height;
        [_txtEmailOtp setHidden:YES];
        _alertView.frame=frame;
        frame=_btnOk.frame;
        frame.origin.y-=_txtEmailOtp.frame.size.height;
        _btnOk.frame=frame;
        frame=_btnCancel.frame;
        frame.origin.y-=_txtEmailOtp.frame.size.height;
        _btnCancel.frame=frame;
        
    }
    self.autoresizesSubviews = YES;
    self.alertView.autoresizesSubviews = YES;
    [UIView commitAnimations];
}
- (IBAction)onClickBtnOk:(id)sender
{
    if (isEmailOtpOn && !isSmsOtpOn)
    {
        if ([_txtEmailOtp.text isEqualToString:strEmailOtp])
        {
            [[UtilityClass sharedObject] animateHide:self];
            [_delegate onClickCustomDialogOtpOk:self];
        }
        else
        {
            [_txtEmailOtp resignFirstResponder];
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_OTP", nil)];
            
        }
    }
    else if(!isEmailOtpOn && isSmsOtpOn)
    {
        if ([_txtSmsOtp.text isEqualToString:strSmsOtp])
        {
            [[UtilityClass sharedObject] animateHide:self];
            [_delegate onClickCustomDialogOtpOk:self];
        }
        else
        {
            [_txtSmsOtp resignFirstResponder];
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_OTP", nil)];
            
        }
    }
    else
    {
        if ([_txtEmailOtp.text isEqualToString:strEmailOtp])
        {
            if ([_txtSmsOtp.text isEqualToString:strSmsOtp])
            {
                [[UtilityClass sharedObject] animateHide:self];
                [_delegate onClickCustomDialogOtpOk:self];
            }
            else
            {
                [_txtSmsOtp resignFirstResponder];
                [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_OTP", nil)];
                
            }
            
        }
        else
        {
            [_txtEmailOtp resignFirstResponder];
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_OTP", nil)];
        }
    }
}

- (IBAction)onClickBtnCancel:(id)sender
{
	[self removeFromSuperview];
}
- (void)setColor:(UIColor *)titleColor message:(UIColor *)messageColor cancelButtonTitle:(UIColor *)cancelButtonColor DoneButtonTitle:(UIColor *)otherButtonColor BackGroundColor:(UIColor*)backGroundColor;
{
    self.lblTitle.textColor=titleColor;
    self.lblMessage.textColor=messageColor;
    self.btnOk.titleLabel.textColor=cancelButtonColor;
    
}
-(void)setLocalization
{
    
    [_btnCancel setTitle:[NSLocalizedString(@"ALERT_BTN_CANCEL", nil) uppercaseString] forState:UIControlStateNormal];
    [_btnCancel setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [_btnCancel setBackgroundColor:[UIColor buttonColor]];
    [_txtSmsOtp setTextColor:[UIColor textColor]];
    [_txtEmailOtp setTextColor:[UIColor textColor]];
    [_btnOk setTitle:[NSLocalizedString(@"ALERT_BTN_OK", nil) uppercaseString] forState:UIControlStateNormal];
    [_btnOk setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [_btnOk setBackgroundColor:[UIColor buttonColor]];

    self.lblTitle.textColor=[UIColor labelTextColor];
    self.lblMessage.textColor=[UIColor textColor];
    self.alertView.backgroundColor=[UIColor whiteColor];
}
@end
