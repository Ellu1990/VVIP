//
//  CustomOtpDialog.h
//    Eber ClientProvider
//
//  Created by Elluminati Mini Mac 5 on 13/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CustomOtpDialog;
@protocol CustomOtpDialogDelegate <NSObject>
@optional
- (void) onClickCustomDialogOtpOk:(CustomOtpDialog*)view;
@end

@interface CustomOtpDialog: UIView<UITextFieldDelegate,UIScrollViewDelegate>
{
    NSString *strEmailOtp,*strSmsOtp;
    BOOL isVerify;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
-(instancetype)initWithOtpEmail:(NSString*)otpEmail optSms:(NSString *)otpSms emailOtpOn:(BOOL)isEmailOtp smsOtpOn:(BOOL)isSmsOtp delegate:(id)delegate;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailOtp;
@property (weak, nonatomic) IBOutlet UITextField *txtSmsOtp;
@property (nonatomic, assign) id<CustomOtpDialogDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *alertView;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;
@property(assign,nonatomic) BOOL isEmailOtpOn;
@property(assign,nonatomic) BOOL isSmsOtpOn;
-(IBAction)onClickBtnOk:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
- (IBAction)onClickBtnCancel:(id)sender;
-(void)setColor:(UIColor *)titleColor message:(UIColor *)messageColor cancelButtonTitle:(UIColor *)cancelButtonColor DoneButtonTitle:(UIColor *)otherButtonColor BackGroundColor:(UIColor*)backGroundColor;
@end
