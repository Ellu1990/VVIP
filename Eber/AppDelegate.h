//
//  AppDelegate.h
//    Eber Client
//
//  Created by My Mac on 6/14/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <GoogleSignIn/GoogleSignIn.h>
@import Firebase;
@interface AppDelegate : UIResponder
<UIApplicationDelegate,GIDSignInDelegate>
{

    UIView *view;
    
}
@property (strong, nonatomic) UIWindow *window;
+(AppDelegate *)sharedAppDelegate;

-(void)showLoadingWithTitle:(NSString *)title;
-(void)hideLoadingView;
-(BOOL)connected;
- (void)goToMain;
-(void)goToMap;
- (NSURL *)applicationDocumentsDirectory;
- (NSString *)applicationCacheDirectoryString;
@end

