//
//  UIColor+Colors.h
//  TexyAnyTimeAnywhere
//
//  Created by My Mac on 6/13/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Colors)

#pragma mark Label Colore
+(UIColor *)labelHintColor;
+(UIColor *)labelSelectColor;
#pragma mark-ThemeColor
+(UIColor *)GreenColor;
+(UIColor *)lightGreenColor;
+(UIColor *)DarkGreenColor;
+(UIColor *)backGroundColor;
+(UIColor *)transparentGreenColor;
#pragma mark Label Color
+(UIColor *)borderColor;
+(UIColor *)textColor;
+(UIColor *)textPlaceHolderColor;
+(UIColor *)textBackColor;
+(UIColor *)dividerColor;
#pragma mark Label Color
+(UIColor *)labelTextColor;
+(UIColor *)labelTitleColor;
#pragma mark- Button Color
+(UIColor *)buttonColor;
+(UIColor *)buttonTextColor;
#pragma mark- Gradient Color
+(UIColor *) gradientColorUp;
+(UIColor *) gradientColorDown;
#pragma mark-PathColor
+(UIColor *) googlePathColor;
+(UIColor *) currentPathColor;


@end
