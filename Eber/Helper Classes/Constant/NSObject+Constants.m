//
//  NSObject+Constants.m
//  TexyAnyTimeAnywhere
//
//  Created by My Mac on 6/13/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "NSObject+Constants.h"


@implementation NSObject (Constants)

/*APPLICATION DETAILS*/

NSString *const APPLICATION_NAME=@"Taxi AnyTime AnyWhere";
NSString *const COPYRIGHTS_NOTE=@"COPYRIGHTS_NOTE";

#pragma mark -
#pragma mark -WS METHODS

NSString *const WS_SEND_SMS_TO_EMERGENCY_CONTACT=@"send_sms_to_emergency_contact";
NSString *const WS_GET_PROVIDER_DETAIL=@"get_provider_info";
NSString *const WS_GET_REFERELL_CREDIT=@"getuser_referalcredit";
NSString *const WS_USER_REGISTER=@"userregister";
NSString *const WS_USER_FORGET_PASSWORD=@"forgotpassword";
NSString *const WS_USER_GET_CARDS=@"cards";
NSString *const WS_USER_UPADTE_PROFILE=@"userupdate/";
NSString *const WS_GET_PROVIDER_LATLONG=@"getproviderlatlong";
NSString *const WS_GET_NEARBY_PROVIDER=@"getnearbyprovider";
NSString *const WS_CANCEL_FUTURE_TRIP=@"cancelscheduledtrip/";
NSString *const WS_GET_APP_KEYS=@"getappkeys";
NSString *const WS_GET_SETTING_DETAILS=@"getsettingdetail";
NSString *const WS_GET_VERIFICATION_OTP=@"verification";
NSString *const WS_GET_GOOGLE_MAP_PATH=@"getgooglemappath";
NSString *const WS_ADD_WALLET_AMOUNT=@"add_wallet_amount";
NSString *const WS_CHANGE_WALLET_STATUS=@"change_user_wallet_status";
/*contact List Web-Service Methods*/
NSString *const WS_ADD_EMERGENCY_CONTACT=@"add_emergency_contact";
NSString *const WS_DELETE_EMERGENCY_CONTACT=@"delete_emergency_contact/";
NSString *const WS_UPDATE_EMERGENCY_CONTACT=@"update_emergency_contact/";
NSString *const WS_GET_EMERGENCY_CONTACT_LIST=@"get_emergency_contact_list";
NSString *const WS_GET_FAV_DRIVER=@"getfavouriteid";
NSString *const WS_DELETE_FAV_DRIVER=@"delete_favourite_provider";
/*Ongoing Trip Web-Service Methods*/
NSString *const USER_UPDATE_DESTINATION_ADDRESS=@"usersetdestination";
NSString *const USER_LOGIN=@"userslogin";
NSString *const USER_ADD_CARD=@"addcard";
NSString *const USER_DELETECARD=@"deletecard/";
NSString *const USER_SELECTCARD=@"card_selection";
NSString *const USER_DESELECTCARD=@"card_deselect";
NSString *const USER_APPLY_REFERRAL=@"apply_referral_code/";
NSString *const USER_LOGOUT=@"logout";
NSString *const USER_CREATE_REQUEST=@"createtrip/";
NSString *const USER_GET_FARE_ESTIMATE=@"getfareestimate";
NSString *const USER_GET_CAR_TYPE=@"typelist_selectedcountrycity";
NSString *const USER_TRIP_DETAIL=@"usertripdetail";
NSString *const USER_HISTORY =@"userhistory";
NSString *const USER_GET_TRIP_STATUS=@"usergettripstatus/";
NSString *const USER_APPLY_PROMO=@"apply_promo_code/";
NSString *const USER_GET_INVOICE=@"getuserinvoice";
NSString *const WS_CANCEL_TRIP=@"canceltrip/";
NSString *const USER_RATE_PROVIDER=@"usergiverating/";
NSString *const USER_CHANGE_PAYMENT_MODE=@"userchangepaymenttype";
NSString *const USER_REFRESH_TOKEN=@"updateuserdevicetoken/";
NSString *const USER_FUTURE_TRIP=@"createfuturetrip";
NSString *const USER_GET_FUTURE_TRIP=@"getfuturetrip";
#pragma mark -
#pragma mark - SEGUE-IDENTIFIERES
NSString *const SEGUE_TO_DIRCET_REGI=@"seguetoDriectRegister";
NSString *const SEGUE_TO_REFERRAL_CODE=@"segueToReferralCode";
NSString *const SEGUE_TO_PAYMENT=@"segueToPayment";
NSString *const SEGUE_PROFILE=@"segueToProfile";
NSString *const SEGUE_TO_DIRCET_LOGIN=@"segueToDirectLogin";//Used in Home VC
NSString *const SEGUE_TO_HISTORY=@"segueToHistory";
NSString *const SEGUE_TO_ACCEPT=@"segueToAccept";
NSString *const SEGUE_TO_HISTORY_DETAIL=@"segueToHistoryDetail";
NSString *const SEGUE_TO_MY_BOOKING=@"segueToMyBooking";
NSString *const SEGUE_TO_FEEDBACK=@"segueToFeedback";
NSString *const SEGUE_TO_HOME=@"segueToHome";
NSString *const SEGUE_TO_SETTINGS=@"segueToSettings";
NSString *const SEGUE_TO_INVOICE=@"segueToInvoice";
NSString *const SEGUE_TO_CONTACT=@"segueToContact";

#pragma mark -
#pragma mark - Prefences key

NSString *const PREF_DEVICE_TOKEN=@"deviceToken";
NSString *const PREF_USER_TOKEN=@"usertoken";
NSString *const PREF_IS_LOGIN=@"islogin";
NSString *const PREF_LOGIN_OBJECT=@"loginobject";
NSString *const PREF_REFERRAL_CODE=@"referral_code";
NSString *const PREF_PASSWORD=@"password";
NSString *const PREF_IS_REFEREE=@"is_referee";
NSString *const PREF_TRIP_ID=@"trip_id";
NSString *const PREF_STRIPE_KEY=@"stripe_publishable_key";
NSString *const PARAM_GOOGLE_SERVER_KEY=@"ios_user_app_google_key";

#pragma mark-CountryCodeJson File
NSString *const PHONE_CODE_JSON_PARAMETER=@"phone-code";
NSString *const COUNTRY_NAME_JSON_PARAMETER=@"name";

#pragma mark -
#pragma mark - PARAMETER NAME
NSString *const PARAM_DEVICE_UNIQUE_CODE=@"device_unique_code";
NSString *const PARAM_PICTURE_DATA=@"pictureData";
/*LOGIN REGISTER AND PROFILE PARAMETER */
NSString *const PARAM_USER_APPROVED=@"is_approved";
NSString *const PARAM_ID=@"_id";
NSString *const PARAM_EMAIL=@"email";
NSString *const PARAM_PASSWORD=@"password";
NSString *const PARAM_FIRST_NAME=@"first_name";
NSString *const PARAM_LAST_NAME=@"last_name";
NSString *const PARAM_PHONE=@"phone";
NSString *const PARAM_PICTURE=@"picture";
NSString *const PARAM_DEVICE_TOKEN=@"device_token";
NSString *const PARAM_DEVICE_TYPE=@"device_type";
NSString *const PARAM_BIO=@"bio";
NSString *const PARAM_ADDRESS=@"address";
NSString *const PARAM_COUNTRY_CODE=@"country_phone_code";
NSString *const PARAM_COUNTRY=@"country";
NSString *const PARAM_CITY=@"city";
NSString *const PARAM_ZIPCODE=@"zipcode";
NSString *const PARAM_LOGIN_BY=@"login_by";
NSString *const PARAM_SOCIAL_UNIQUE_ID=@"social_unique_id";
NSString *const PARAM_TYPE=@"type";
NSString *const PARAM_TOKEN=@"token";
NSString *const PARAM_LAST_FOUR=@"last_four";
NSString *const PARAM_REFERRAL_SKIP=@"is_skip";
NSString *const PARAM_OLD_PASSWORD=@"old_password";
NSString *const PARAM_NEW_PASSWORD=@"new_password";
NSString *const PARAM_REFERRAL_CODE=@"referral_code";
NSString *const PARAM_USER_ID=@"user_id";
NSString *const PARAM_PROMO_CODE=@"promocode";
NSString *const PARAM_DEVICE_TIMEZONE=@"device_timezone";
NSString *const PARAM_DISTANCE_UNIT=@"unit";
NSString *const PARAM_EMAIL_VERIFICATION_ON=@"userEmailVerification";
NSString *const PARAM_SMS_VERIFICATION_ON=@"userSms";
NSString *const PARAM_SMS_OTP=@"otpForSMS";
NSString *const PARAM_EMAIL_OTP=@"otpForEmail";
NSString *const PARAM_USER_CREATE_TIME=@"user_create_time";
NSString *const PARAM_SCHEDULED_REQUEST_PRE_START_TIME=@"scheduledRequestPreStartMinute";
NSString *const PARAM_CONTACT_EMAIL=@"contactUsEmail";
NSString *const PARAM_USER_PATH_DRAW=@"userPath";
NSString *const PARAM_TYPE_NOTE=@"note";
NSString *const PARAM_COMPANY_ID=@"company_id";

#pragma mark - CONTACTLIST
NSString *const PARAM_IS_ALWAYS_SHARE_DETAIL=@"is_always_share_ride_detail";
NSString *const PARAM_EMERGENCY_CONTACT_DATA=@"emergency_contact_data";
NSString *const PARAM_EMERGENCY_CONTACT_NAME=@"name";
NSString *const PARAM_NAME=@"name";
NSString *const PARAM_EMERGENCY_CONTACT_ID=@"emergency_contact_detail_id";
#pragma mark-Payment Parameters
NSString *const PARAM_WALLET=@"wallet";
NSString *const PARAM_IS_USE_WALLET=@"is_use_wallet";
NSString *const PARAM_WALLET_CURRENCY_CODE=@"wallet_currency_code";
NSString *const PARAM_IS_PAYMENT_MODE_CASH=@"is_payment_mode_cash";
NSString *const PARAM_IS_PAYMENT_MODE_CARD=@"is_payment_mode_card";
NSString *const PARAM_IS_PROMO_APPLY_FOR_CASH =@"isPromoApplyForCash";
NSString *const PARAM_IS_PROMO_APPLY_FOR_CARD=@"isPromoApplyForCard";
NSString *const PARAM_PAYMENT_GATEWAY=@"payment_gateway";
NSString *const PARAM_PAYMENT_GATEWAY_ID=@"id";

NSString *const PARAM_PAYMENT_TOKEN=@"payment_token";
NSString *const PARAM_CARD_ID=@"card_id";
NSString *const PARAM_CARD_TYPE=@"card_type";
NSString *const PARAM_CARD=@"card";
NSString *const PARAM_PAYMENT_MODE=@"payment_mode";
NSString *const PARAM_PAYMENT_TYPE=@"payment_type";
NSString *const PARAM_CREATED_AT=@"created_at";
NSString *const PARAM_UPDATED_AT=@"updated_at";
NSString *const PARAM_CUSTOMER_ID=@"customer_id";
NSString *const PARAM_IS_DEFAULT=@"is_default";
NSString *const PARAM_CITY_TYPE=@"citytypes";
NSString *const PARAM_CITY_DETAIL=@"city_detail";
NSString *const PARAM_TYPE_NAME=@"typename";
NSString *const PARAM_BILL=@"bill";
NSString *const PARAM_TYPE_IMAGE_URL=@"type_image_url";
#pragma mark-GET PROVIDER DETAIL Parameters
NSString *const PARAM_TYPE_DETAILS=@"type_details";
NSString *const PARAM_DESCRIPTION=@"description";
NSString *const PARAM_PROVIDER_ID=@"provider_id";
NSString *const PARAM_CAR_MODEL=@"car_model";
NSString *const PARAM_CAR_NUMBER=@"car_number";
NSString *const PARAM_BEARING=@"bearing";
NSString *const PARAM_PROVIDERS=@"providers";
NSString *const PARAM_PROVIDER=@"provider";
NSString *const PARAM_TYPE_ID=@"typeid";
NSString *const PARAM_SURGE_MULTIPLIEER=@"surge_multiplier";
NSString *const PARAM_SURGE_START_HOUR=@"surge_start_hour";
NSString *const PARAM_SURGE_END_HOUR=@"surge_end_hour";
NSString *const PARAM_IS_SURGE_HOUR=@"is_surge_hours";
NSString *const PARAM_WAITING_TIME_COST=@"waiting_time_cost";
NSString *const PARAM_IS_FAVOURITE=@"is_favourite";


#pragma mark-GET PROVIDER DETAIL Parameters

#pragma mark-Trip Status Parameters
NSString *const PARAM_IS_CANCELLATION_FEE=@"is_cancellation_fee";
NSString *const PARAM_TRIP_NUMBER=@"unique_id";
NSString *const PARAM_INVOICE_NUMBER=@"invoice_number";
NSString *const PARAM_IS_TRIP_COMPLETED=@"is_trip_completed";
NSString *const PARAM_IS_TRIP_CANCELLED_BY_USER=@"is_trip_cancelled_by_user";
NSString *const PARAM_TRIPS=@"trips";
NSString *const PARAM_TRIP=@"trip";
NSString *const PARAM_GET_GOOGLE_MAP_PATH_START_LOCATION_TO_PICKUP_LOCATION=@"googlePathStartLocationToPickUpLocation";
NSString *const PARAM_GET_GOOGLE_MAP_PATH_PICKUP_LOCATION_TO_DESTINATION_LOCATION=@"googlePickUpLocationToDestinationLocation";
NSString *const PARAM_TRIP_LOCATION=@"triplocation";
NSString *const PARAM_START_TRIP_TO_END_TRIP_LOCATION=@"startTripToEndTripLocations";
NSString *const PARAM_SERVER_TIME=@"server_time";
NSString *const PARAM_CURRENT_PROVIDER=@"current_provider";
NSString *const PARAM_DEST_LATITUDE=@"d_latitude";
NSString *const PARAM_DEST_LONGITUDE=@"d_longitude";
NSString *const PARAM_LATITUDE=@"latitude";
NSString *const PARAM_LONGITUDE=@"longitude";
NSString *const PARM_SUB_ADMIN_CITY=@"subAdminCity";
NSString *const PARAM_TRIP_ID=@"trip_id";
NSString *const PARAM_TRIP_TIME=@"total_time";
NSString *const PARAM_TRIP_CREATE_TIME=@"user_create_time";
NSString *const PARAM_TRIP_DISTANCE=@"total_distance";
NSString *const PARAM_TRIP_COST=@"total";
NSString *const PARAM_TRIP_SOURCE_ADDRESS=@"source_address";
NSString *const PARAM_TRIP_DESTINATION_ADDRESS=@"destination_address";
NSString *const PARAM_IS_PROVIDER_ACCEPTED=@"is_provider_accepted";
NSString *const PARAM_PROVIDER_STATUS=@"is_provider_status";
NSString *const PARAM_IS_PROVIDER_RATED=@"is_provider_rated";
NSString *const PARAM_SOURCE_LOCATION=@"sourceLocation";
NSString *const PARAM_DESTINATION_LOCATION=@"destinationLocation";
NSString *const PARAM_PROVIDER_LOCATION=@"providerLocation";
NSString *const PARAM_TIME_ZONE=@"timezone";
NSString *const PARAM_TOTAL_TIME=@"total_time";
NSString *const PARAM_TOTAL_DISTANCE=@"total_distance";
NSString *const PARAM_IS_PROMO_USED=@"isPromoUsed";
NSString *const PARAM_MAP_IMAGE=@"map";
//Future Trip
NSString *const PARAM_START_TIME=@"start_time";
NSString *const PARAM_SCHEDULE_TRIP=@"scheduledtrip";
NSString *const PARAM_SERVER_START_TIME=@"server_start_time";
NSString *const PARAM_SCHEDULE_TRIP_ID=@"scheduledtrip_id";
NSString *const PARAM_CANCEL_TRIP_REASON=@"cancelReason";
#pragma mark -
#pragma mark - INVOICE PARAM

NSString *const PARAM_BASEPRICE=@"base_price";
NSString *const PARAM_TRIP_SERVICE=@"tripservice";
NSString *const PARAM_TOTAL=@"total";
NSString *const PARAM_DISTANCE_COST=@"distance_cost";
NSString *const PARAM_TIME_COST=@"time_cost";
NSString *const PARAM_PROMO_BONOUS=@"promo_bonus";
NSString *const PARAM_REFFERAL_BONOUS=@"referral_bonus";
NSString *const PARAM_PRICE_FOR_TOTAL_TIME=@"price_for_total_time";
NSString *const PARAM_PRICE_PER_UNIT_TIME=@"price_per_unit_time";
NSString *const PARAM_PRICE_PER_UNIT_DISTANCE=@"price_per_unit_distance";
NSString *const PARAM_DISTANCE=@"distance";
NSString *const PARAM_TIME=@"time";
NSString *const PARAM_FILE=@"type_image";
NSString *const PARAM_MIN_FARE=@"min_fare";
NSString *const PARAM_TAX=@"tax";
NSString *const PARAM_TAX_FEE=@"tax_fee";
NSString *const PARAM_SURGE_FEE=@"surge_fee";
NSString *const PARAM_TOTAL_WAITING_TIME=@"total_waiting_time";
NSString *const PARAM_TOTAL_WAIT_TIME=@"total_wait_time";
NSString *const PARAM_FOR_WAITING_TIME=@"price_for_waiting_time";
NSString *const PARAM_WALLET_PAYMENT=@"wallet_payment";
NSString *const PARAM_REMAINING_PAYMENT=@"remaining_payment";
NSString *const PARAM_CARD_PAYMENT=@"card_payment";
NSString *const PARAM_CASH_PAYMENT=@"cash_payment";
NSString *const PARAM_SERVICE_TYPE_ID=@"service_type";


#pragma mark - FEEDBACK PARAM

NSString *const PARAM_RATING=@"rating";
NSString *const PARAM_REVIEW=@"review";
#pragma mark-Fare Estimate
NSString *const PARAM_BASE_PRICE_DISTANCE=@"base_price_distance";
NSString *const PARAM_CANCELLATION_FEE=@"cancellation_fee";
NSString *const PARAM_MAX_SPACE=@"max_space";
NSString *const PARAM_CURRENCY=@"currency";
NSString *const PARAM_SERVICE_TYPE=@"service_type_id";
NSString *const PARAM_REFERRAL_CREDIT=@"referral_credit";
NSString *const PARAM_ESTIMATE_FARE=@"estimated_fare";
#pragma mark-LOADING DIALOG
NSString *const  LOADING_ALREADY_LOGIN=@"USER ALREADY LOGGED IN";
#pragma mark-PARAM-VALUES
NSString *const MANUAL=@"manual";
NSString *const  DEVICE_TYPE=@"ios";
NSString *const  SUCCESS=@"success";
NSString *const ERROR_CODE=@"error_code";
NSString *const MESSAGE_CODE=@"message";
NSString *strForCurLatitude;
NSString *strForCurLongitude;
NSString *strUserId;
NSString *strUserToken;
#pragma mark-Measurement Units
NSString *CurrencySign=@"$";
NSString *TIME_SUFFIX=@"min";
NSString *DISTANCE_SUFFIX=@"km";
NSString *DEVICE_TOKEN=@"";
NSString *GoogleServerKey= @"";
NSString *StripePublishableKey = @"";
// TODO: replace nil with your own value
#pragma mark-checkTripStatus
/*Global Boolean Variable To Check Weather Any Trip Is Running Or Not..*/
BOOL IS_TRIP_EXSIST=NO;
/*Global Boolean Variable To Check Weather Current Running Trip Is ACCEPTED BY DRIVER  Or Not..*/
BOOL IS_TRIP_ACCEPTED=NO;
/*Global Boolean Variable To Check Weather User Is Already Register or Not..*/
BOOL IS_PARENT_IS_REGISTER=NO;

#pragma FACEBOOK_PARAMETERS
NSString *const FACEBOOK=@"facebook";
NSString *const PARAM_FB_PICTURE=@"picture";
NSString *const PARAM_FB_URL=@"url";
NSString *const PARAM_FB_DATA=@"data";
NSString *const PARAM_FB_PUBLIC_PROFILE=@"public_profile";
NSString *const PARAM_FB_NAME=@"name";
NSString *const PARAM_FB_EMAIL=@"email";
NSString *const PARAM_FB_USER_FRIENDS=@"user_friends";
NSString *const PARAM_FB_FIELDS=@"fields";
NSString *const PARAM_FB_ID=@"id";
NSString *const PARAM_FB_REQUIRED_FIELDS=@"first_name, last_name, picture.type(large), email, name, id, gender";
#pragma mark -
#pragma mark - GOOGLE-PARAMS
NSString *const GOOGLE=@"google";
NSString *const GOOGLE_PARAM_ELEMENTS=@"elements";
NSString *const GOOGLE_PARAM_VALUES=@"value";
NSString *const GOOGLE_PARAM_STATUS=@"status";
NSString *const GOOGLE_PARAM_ROWS=@"rows";
NSString *const GOOGLE_PARAM_STATUS_OK=@"OK";
NSString *const GOOGLE_PARAM_DISTANCE=@"distance";
NSString *const GOOGLE_PARAM_DURATION=@"duration";
NSString *const GOOGLE_PARAM_RESULTS=@"results";
NSString *const GOOGLE_PARAM_AUTOCOMLETE_KEY=@"key";
NSString *const GOOGLE_PARAM_AUTOCOMLETE_INPUT=@"input";
NSString *const GOOGLE_PARAM_AUTOCOMLETE_SENSOR=@"sensor";
NSString *const GOOGLE_PARAM_GEOMETRY=@"geometry";
NSString *const GOOGLE_PARAM_LOCATION=@"location";
NSString *const GOOGLE_PARAM_AUTOCOMLETE_DESCRIPTION=@"description";
NSString *const GOOGLE_PARAM_AUTOCOMLETE_PREDICTION=@"predictions";
NSString *const GOOGLE_PARAM_AUTOCOMLETE_PLACE_NAME=@"name";
NSString *const GOOGLE_PARAM_AUTOCOMLETE_PLACE_LATITUDE=@"lat";
NSString *const GOOGLE_PARAM_AUTOCOMLETE_PLACE_LONGITUDE=@"lng";
NSString *const GOOGLE_PARAM_AUTOCOMLETE_FORMATTED_ADDRESS=@"formatted_address";

/*server given keys*/
NSString *HotlineAppKey= @"";
NSString *HotlineAppId= @"";
NSString *const PARAM_HOTLINE_KEY=@"hotline_app_key";
NSString *const PARAM_HOTLINE_APP_ID=@"hotline_app_id";

#pragma mark -PUSH Param

Boolean checkApprove=false;
Boolean checkDecline=false;
NSInteger WAITING_SECONDS_REMAIN=0;
@end
