//
//  NSObject+Constants.h
//  TexyAnyTimeAnywhere
//
//  Created by My Mac on 6/13/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define PREF [PreferenceHelper sharedObject]
#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])
#define GoogleKey @"AIzaSyD-X64z_0xXdiwbeuamah1Elkl2FkgEZVg"
#define Card_Length 19
#define Card_Month 2
#define Card_Year 2
#define Card_CVC_CVV 3
#define USER_TYPE 1
#define invalidTokenId @"451"
#define animateCameraDelay 3.00f
#import <Foundation/Foundation.h>

@interface NSObject (Constants)
typedef NS_ENUM(NSInteger,ProviderStatus)
{
    ProviderStatusComing=2,
    ProviderStatusArrived=4,
    ProviderStatusTripStarted=6,
    ProviderStatusTripEnd=8,
    ProviderStatusTripCompleted=9,
};

/* WEB SERVICE Live URL*/

#define BASE_URL @"https://www.vvip.biz/"
///#define BASE_URL @"http://192.168.0.115:5000/"
///#define BASE_URL @"http://staging.elluminatiinc.com/"

/* METHOD USED TO POST WEB SERVICE DATA */
#define POST @"POST"
#define PUT @"PUT"
#define DELETE @"DELETE"
#define TYPE_USER 1

/* METHOD USED FOR CALL WEB SERVICE */

#pragma mark -
#pragma mark - WS METHODS
extern NSString *const WS_USER_REGISTER;
extern NSString *const WS_GET_GOOGLE_MAP_PATH;
extern NSString *const USER_LOGIN;
extern NSString *const WS_USER_FORGET_PASSWORD;
extern NSString *const WS_USER_UPADTE_PROFILE;
extern NSString *const WS_CANCEL_FUTURE_TRIP;
extern NSString *const WS_CANCEL_TRIP;
extern NSString *const WS_GET_APP_KEYS;
extern NSString *const WS_GET_SETTING_DETAILS;
extern NSString *const WS_SEND_SMS_TO_EMERGENCY_CONTACT;
extern NSString *const WS_ADD_EMERGENCY_CONTACT;
extern NSString *const WS_DELETE_EMERGENCY_CONTACT;
extern NSString *const WS_UPDATE_EMERGENCY_CONTACT;
extern NSString *const WS_GET_EMERGENCY_CONTACT_LIST;
extern NSString *const WS_CHANGE_WALLET_STATUS;
extern NSString *const WS_DELETE_FAV_DRIVER;
extern NSString *const WS_ADD_WALLET_AMOUNT;

/*ongoing Trip Methods*/
extern NSString *const USER_GET_TRIP_STATUS;
extern NSString *const WS_GET_PROVIDER_DETAIL;
extern NSString *const WS_GET_NEARBY_PROVIDER;
extern NSString *const USER_UPDATE_DESTINATION_ADDRESS;
extern NSString *const USER_CHANGE_PAYMENT_MODE;
extern NSString *const USER_APPLY_PROMO;
extern NSString *const USER_GET_INVOICE;
extern NSString *const USER_RATE_PROVIDER;
extern NSString *const WS_GET_PROVIDER_LATLONG;
extern NSString *const WS_GET_VERIFICATION_OTP;
extern NSString *const WS_GET_FAV_DRIVER;
/*Payment Card Methods*/
extern NSString *const USER_ADD_CARD;
extern NSString *const USER_SELECTCARD;
extern NSString *const USER_DESELECTCARD;
extern NSString *const USER_DELETECARD;
extern NSString *const WS_USER_GET_CARDS;
/*referel Methods*/
extern NSString *const USER_APPLY_REFERRAL;
extern NSString *const WS_GET_REFERELL_CREDIT;
/*Trip Methods*/
extern NSString *const USER_FUTURE_TRIP;
extern NSString *const USER_GET_FUTURE_TRIP;
extern NSString *const USER_CREATE_REQUEST;

extern NSString *const USER_LOGOUT;
extern NSString *const USER_GET_CAR_TYPE;
extern NSString *const USER_TRIP_DETAIL;
extern NSString *const USER_HISTORY;

extern NSString *const USER_GET_FARE_ESTIMATE;
extern NSString *const USER_REFRESH_TOKEN;

/*ALL Segue Identifier Used in Application*/

extern NSString *const SEGUE_TO_DIRCET_REGI;
extern NSString *const SEGUE_TO_REFERRAL_CODE;
extern NSString *const SEGUE_TO_PAYMENT;
extern NSString *const SEGUE_PROFILE;
extern NSString *const SEGUE_TO_DIRCET_LOGIN;
extern NSString *const SEGUE_TO_HISTORY;
extern NSString *const SEGUE_TO_ACCEPT;
extern NSString *const SEGUE_TO_HISTORY_DETAIL;
extern NSString *const SEGUE_TO_MY_BOOKING;
extern NSString *const SEGUE_TO_FEEDBACK;
extern NSString *const SEGUE_TO_HOME;
extern NSString *const SEGUE_TO_SETTINGS;
extern NSString *const SEGUE_TO_INVOICE;
extern NSString *const SEGUE_TO_CONTACT;

/*APP Information*/
extern NSString *const APPLICATION_NAME;

extern NSString *const COPYRIGHTS_NOTE;

#pragma mark -
#pragma mark - Prefences key

extern NSString *const PREF_DEVICE_TOKEN;
extern NSString *const PREF_USER_TOKEN;
extern NSString *const PREF_USER_ID;
extern NSString *const PREF_IS_LOGIN;
extern NSString *const PREF_IS_LOGOUT;
extern NSString *const PREF_REFERRAL_CODE;
extern NSString *const PREF_LOGIN_OBJECT;
extern NSString *const PREF_PASSWORD;
extern NSString *const PREF_IS_REFEREE;
extern NSString *const PREF_TRIP_ID;
extern NSString *const PREF_STRIPE_KEY;
extern NSString *const PARAM_GOOGLE_SERVER_KEY;
extern NSString *const PARAM_SCHEDULED_REQUEST_PRE_START_TIME;
extern NSString *const PARAM_USER_CREATE_TIME;
extern NSString *const PARAM_COMPANY_ID;
#pragma mark-CountryCodeJson File
extern NSString *const PHONE_CODE_JSON_PARAMETER;
extern NSString *const COUNTRY_NAME_JSON_PARAMETER;

#pragma mark -
#pragma mark - COMMON-PARAMETER NAME

extern NSString *const PARAM_PICTURE_DATA;
extern NSString *const PARAM_PASSWORD;
extern NSString *const PARAM_EMAIL;
extern NSString *const PARAM_SOCIAL_UNIQUE_ID;
extern NSString *const PARAM_DEVICE_TOKEN;
extern NSString *const PARAM_DEVICE_TYPE;
extern NSString *const PARAM_TOKEN;
extern NSString *const PARAM_USER_ID;
extern NSString *const PARAM_ID;
extern NSString *const PARAM_TRIPS;
extern NSString *const PARAM_CITY_TYPE;
extern NSString *const PARM_SUB_ADMIN_CITY;
extern NSString *const PARAM_TYPE_NAME;
extern NSString *const PARAM_BILL;
extern NSString *const PARAM_DISTANCE_UNIT;
extern NSString *const PARAM_MIN_FARE;
extern NSString *const PARAM_EMAIL_VERIFICATION_ON;
extern NSString *const PARAM_SMS_VERIFICATION_ON;
extern NSString *const PARAM_SMS_OTP;
extern NSString *const PARAM_EMAIL_OTP;
extern NSString *const PARAM_CONTACT_EMAIL;
extern NSString *const PARAM_USER_PATH_DRAW;
extern NSString *const PARAM_TYPE_NOTE;
extern NSString *const PARAM_IS_FAVOURITE;
extern NSString *const PARAM_SERVICE_TYPE_ID;
#pragma mark -

#pragma mark -
#pragma mark - LOGIN-REGISTER AND PROFILE-PARAMS
extern NSString *const PARAM_USER_APPROVED;
extern NSString *const PARAM_DEVICE_UNIQUE_CODE;
extern NSString *const PARAM_LOGIN_BY;
extern NSString *const PARAM_FIRST_NAME;
extern NSString *const PARAM_LAST_NAME;
extern NSString *const PARAM_PHONE;
extern NSString *const PARAM_PICTURE;
extern NSString *const PARAM_BIO;
extern NSString *const PARAM_ADDRESS;
extern NSString *const PARAM_COUNTRY;
extern NSString *const PARAM_ZIPCODE;
extern NSString *const PARAM_OLD_PASSWORD;
extern NSString *const PARAM_NEW_PASSWORD;
extern NSString *const PARAM_REFERRAL_SKIP;
extern NSString *const PARAM_CITY;
extern NSString *const PARAM_TYPE;
extern NSString *const PARAM_TOKEN;
extern NSString *const PARAM_COUNTRY_CODE;
extern NSString *const PARAM_DEVICE_TIMEZONE;



#pragma mark-PROVIDER DETAIL Parameters
extern NSString *const PARAM_TYPE_IMAGE_URL;
extern NSString *const PARAM_TYPE_DETAILS;
extern NSString *const PARAM_DESCRIPTION;
extern NSString *const PARAM_BEARING;
extern NSString *const PARAM_PROVIDER_ID;
extern NSString *const PARAM_CAR_MODEL;
extern NSString *const PARAM_CAR_NUMBER;
extern NSString *const PARAM_TAX;
extern NSString *const PARAM_PROVIDERS;
extern NSString *const PARAM_PROVIDER;
extern NSString *const PARAM_TYPE_ID;
extern NSString *const PARAM_SURGE_MULTIPLIEER;
extern NSString *const PARAM_SURGE_START_HOUR;
extern NSString *const PARAM_SURGE_END_HOUR;
extern NSString *const PARAM_IS_SURGE_HOUR;

#pragma mark-Wallet Parameters
extern NSString *const PARAM_WALLET;
extern NSString *const PARAM_WALLET_CURRENCY_CODE;
extern NSString *const PARAM_IS_USE_WALLET;

#pragma mark-Trip Status Parameters
extern NSString *const PARAM_TRIP;
extern NSString *const PARAM_TOTAL_WAIT_TIME;
extern NSString *const PARAM_IS_CANCELLATION_FEE;
extern NSString *const PARAM_TRIP_NUMBER;
extern NSString *const PARAM_IS_TRIP_COMPLETED;
extern NSString *const PARAM_INVOICE_NUMBER;
extern NSString *const PARAM_GET_GOOGLE_MAP_PATH_START_LOCATION_TO_PICKUP_LOCATION;
extern NSString *const PARAM_GET_GOOGLE_MAP_PATH_PICKUP_LOCATION_TO_DESTINATION_LOCATION;
extern NSString *const PARAM_TRIP_LOCATION;
extern NSString *const PARAM_START_TRIP_TO_END_TRIP_LOCATION;
extern NSString *const PARAM_IS_TRIP_CANCELLED_BY_USER;
extern NSString *const PARAM_CURRENT_PROVIDER;
extern NSString *const PARAM_TRIP_ID;
extern NSString *const PARAM_SERVER_TIME;
extern NSString *const PARAM_TRIP_TIME;
extern NSString *const PARAM_TRIP_CREATE_TIME;
extern NSString *const PARAM_TRIP_DISTANCE;
extern NSString *const PARAM_TRIP_COST;
extern NSString *const PARAM_TRIP_SOURCE_ADDRESS;
extern NSString *const PARAM_TRIP_DESTINATION_ADDRESS;
extern NSString *const PARAM_PAYMENT_TYPE;
extern NSString *const PARAM_IS_PROVIDER_ACCEPTED;
extern NSString *const PARAM_PROVIDER_STATUS;
extern NSString *const PARAM_IS_PROVIDER_RATED;
extern NSString *const PARAM_SOURCE_LOCATION;
extern NSString *const PARAM_DESTINATION_LOCATION;
extern NSString *const PARAM_PROVIDER_LOCATION;
extern NSString *const PARAM_LATITUDE;
extern NSString *const PARAM_LONGITUDE;
extern NSString *const PARAM_DEST_LATITUDE;
extern NSString *const PARAM_DEST_LONGITUDE;
extern NSString *const PARAM_REFERRAL_CODE;
extern NSString *const PARAM_REFERRAL_CREDIT;
extern NSString *const PARAM_TIME_ZONE;
extern NSString *const PARAM_TOTAL_TIME;
extern NSString *const PARAM_TOTAL_DISTANCE;
extern NSString *const PARAM_PRICE_FOR_TOTAL_TIME;
extern NSString *const PARAM_IS_PROMO_USED;
extern NSString *const PARAM_MAP_IMAGE;
extern NSString *const PARAM_PROMO_CODE;
//Future Trip
extern NSString *const PARAM_SCHEDULE_TRIP;
extern NSString *const PARAM_START_TIME;
extern NSString *const PARAM_SERVER_START_TIME;
extern NSString *const PARAM_CANCEL_TRIP_REASON;
extern NSString *const PARAM_SCHEDULE_TRIP_ID;

/*ESTIMATE*/
#pragma mark-Fare Estimate
extern NSString *const PARAM_BASE_PRICE_DISTANCE;
extern NSString *const PARAM_CANCELLATION_FEE;
extern NSString *const PARAM_MAX_SPACE;
extern NSString *const PARAM_CURRENCY;
extern NSString *const PARAM_CITY_DETAIL;
extern NSString *const PARAM_SERVICE_TYPE;
extern NSString *const PARAM_ESTIMATE_FARE;



/* PROVIDER Parameters*/


#pragma mark-Payment Parameter
extern NSString *const PARAM_CARD;
extern NSString *const PARAM_CARD_ID;
extern NSString *const PARAM_CARD_TYPE;
extern NSString *const PARAM_PAYMENT_TOKEN;
extern NSString *const PARAM_PAYMENT_MODE;
extern NSString *const PARAM_LAST_FOUR;
extern NSString *const PARAM_CREATED_AT;
extern NSString *const PARAM_UPDATED_AT;
extern NSString *const PARAM_CUSTOMER_ID;
extern NSString *const PARAM_IS_DEFAULT;
extern NSString *const PARAM_IS_PAYMENT_MODE_CASH;
extern NSString *const PARAM_IS_PAYMENT_MODE_CARD;
extern NSString *const PARAM_IS_PROMO_APPLY_FOR_CASH;
extern NSString *const PARAM_IS_PROMO_APPLY_FOR_CARD;
extern NSString *const PARAM_PAYMENT_GATEWAY;
extern NSString *const PARAM_PAYMENT_GATEWAY_ID;
extern NSString *const PARAM_NAME;

#pragma mark -
#pragma mark - CONTACTLIST
extern NSString *const PARAM_IS_ALWAYS_SHARE_DETAIL;
extern NSString *const PARAM_EMERGENCY_CONTACT_DATA;
extern NSString *const PARAM_EMERGENCY_CONTACT_NAME;
extern NSString *const PARAM_EMERGENCY_CONTACT_ID;
#pragma mark -INVOICE Param
extern NSString *const PARAM_TRIP_SERVICE;
extern NSString *const PARAM_BASEPRICE;
extern NSString *const PARAM_TOTAL;
extern NSString *const PARAM_DISTANCE_COST;
extern NSString *const PARAM_TIME_COST;
extern NSString *const PARAM_PROMO_BONOUS;
extern NSString *const PARAM_REFFERAL_BONOUS;
extern NSString *const PARAM_PRICE_PER_UNIT_TIME;
extern NSString *const PARAM_PRICE_PER_UNIT_DISTANCE;
extern NSString *const PARAM_DISTANCE;
extern NSString *const PARAM_TIME;
extern NSString *const PARAM_FILE;
extern NSString *const PARAM_TAX_FEE;
extern NSString *const PARAM_SURGE_FEE;
extern NSString *const PARAM_TOTAL_WAITING_TIME;
extern NSString *const PARAM_FOR_WAITING_TIME;
extern NSString *const PARAM_WAITING_TIME_COST;
extern NSString *const PARAM_WALLET_PAYMENT;
extern NSString *const PARAM_REMAINING_PAYMENT;
extern NSString *const PARAM_CARD_PAYMENT;
extern NSString *const PARAM_CASH_PAYMENT;
#pragma mark - FEEDBACK PARAM

extern NSString *const PARAM_RATING;
extern NSString *const PARAM_REVIEW;

#pragma mark -
#pragma mark - MAP Attributes

extern NSString *strForCurLatitude;
extern NSString *strForCurLongitude;


#pragma mark -
#pragma mark - LOADING DIALOGS
extern NSString *const  LOADING_ALREADY_LOGIN;

#pragma mark -
#pragma mark - PARAM-VALUES

/**Login By*/
extern NSString *const MANUAL;
extern NSString *const GOOGLE;

/**Device Detail*/
extern NSString *DEVICE_TOKEN;
extern NSString *const  DEVICE_TYPE;
/**WEB SERVICE RESPONSE*/

extern NSString *const  SUCCESS;
extern NSString *const ERROR_CODE;
extern NSString *const MESSAGE_CODE;

#pragma mark -
#pragma mark - GOOGLE-PARAMS
/*GOOGLE Keys*/

extern NSString *GoogleServerKey;
extern NSString *StripePublishableKey;
/*GOOGLE URLS*/
#define AutoComplete_URL @"https://maps.googleapis.com/maps/api/place/autocomplete/json?"
/*GOOGLE PARAMS*/
extern NSString *CurrencySign;
extern NSString *strUserId;
extern NSString *strUserToken;
#pragma mark - Check Trip Status
/*Global Boolean Variable To Check Weather Any Trip Is Running Or Not..*/
extern BOOL IS_TRIP_EXSIST;
/*Global Boolean Variable To Check Weather Current Running Trip Is ACCEPTED BY DRIVER  Or Not..*/
extern BOOL IS_TRIP_ACCEPTED;
extern BOOL IS_PARENT_IS_REGISTER;
#pragma mark-Measurement Units
extern NSString *TIME_SUFFIX;
extern NSString *DISTANCE_SUFFIX;

#pragma FACEBOOK_PARAMETERS
extern NSString *const FACEBOOK;
extern NSString *const PARAM_FB_NAME;
extern NSString *const PARAM_FB_PICTURE;
extern NSString *const PARAM_FB_URL;
extern NSString *const PARAM_FB_DATA;
extern NSString *const PARAM_FB_ID;
extern NSString *const PARAM_FB_PUBLIC_PROFILE;
extern NSString *const PARAM_FB_EMAIL;
extern NSString *const PARAM_FB_USER_FRIENDS;
extern NSString *const PARAM_FB_FIELDS;
extern NSString *const PARAM_FB_REQUIRED_FIELDS;
#pragma GOOGLE_PARAMS
extern NSString *const GOOGLE_PARAM_ELEMENTS;
extern NSString *const GOOGLE_PARAM_VALUES;
extern NSString *const GOOGLE_PARAM_STATUS;
extern NSString *const GOOGLE_PARAM_STATUS_OK;
extern NSString *const GOOGLE_PARAM_ROWS;
extern NSString *const GOOGLE_PARAM_DISTANCE;
extern NSString *const GOOGLE_PARAM_DURATION;
extern NSString *const GOOGLE_PARAM_RESULTS;
extern NSString *const GOOGLE_PARAM_GEOMETRY;
extern NSString *const GOOGLE_PARAM_LOCATION;

extern NSString *const GOOGLE_PARAM_AUTOCOMLETE_KEY;
extern NSString *const GOOGLE_PARAM_AUTOCOMLETE_INPUT;
extern NSString *const GOOGLE_PARAM_AUTOCOMLETE_SENSOR;
extern NSString *const GOOGLE_PARAM_AUTOCOMLETE_DESCRIPTION;
extern NSString *const GOOGLE_PARAM_AUTOCOMLETE_PREDICTION;
extern NSString *const GOOGLE_PARAM_AUTOCOMLETE_PLACE_NAME;
extern NSString *const GOOGLE_PARAM_AUTOCOMLETE_PLACE_LATITUDE;
extern NSString *const GOOGLE_PARAM_AUTOCOMLETE_PLACE_LONGITUDE;
extern NSString *const GOOGLE_PARAM_AUTOCOMLETE_FORMATTED_ADDRESS;

/*Server given keys*/
extern NSString *HotlineAppKey;
extern NSString *HotlineAppId;
extern NSString *const PARAM_HOTLINE_KEY;
extern NSString *const PARAM_HOTLINE_APP_ID;

#pragma mark -PUSH Param

extern Boolean checkApprove;
extern Boolean checkDecline;
extern NSInteger WAITING_SECONDS_REMAIN;

@end
