//
//  ProviderDetail.h
//  Eber Client
//  Created by My Mac on 7/8/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface CurrentTrip : NSObject

@property (nonatomic,strong) NSString * tripNumber;
@property (nonatomic,strong) NSString * trip_id;
@property (nonatomic,strong) NSString * providerFirstName;
@property (nonatomic,strong) NSString * providerLastName;
@property (nonatomic,strong) NSString * providerCarNumber;
@property (nonatomic,strong) NSString * providerCarModal;
@property (nonatomic,strong) NSString * rating;
@property (nonatomic,strong) NSString * providerProfileImage;
@property (nonatomic,strong) NSString * providerLatitude;
@property (nonatomic,strong) NSString * providerLongitude;
@property (nonatomic,strong) NSString * providerId;
@property (nonatomic,strong) NSString * srcAddress;
@property (nonatomic,strong) NSString * destAddress;
@property (nonatomic,strong) NSString * providerStatus;
@property (nonatomic,strong) NSString * isProviderAccepted;
@property (nonatomic,strong) NSString * isProviderRated;
@property (nonatomic,strong) NSString * srcLatitude;
@property (nonatomic,strong) NSString * srcLongitude;
@property (nonatomic,strong) NSString * destLatitude;
@property (nonatomic,strong) NSString * destLongitude;
@property (nonatomic,strong) NSString * providerPhone;
@property (nonatomic,strong) NSString * estimateFareTime;
@property (nonatomic,strong) NSString * estimateFareDistance;
@property (nonatomic,strong) NSString * estimateFareTotal;
@property (nonatomic,strong) NSString * cancelTripCharge;
@property (nonatomic,strong) NSString * phoneCountryCode;
@property (nonatomic,strong) NSString * serverTime;
@property (nonatomic,strong) NSString * timeZone;
@property (nonatomic,strong) NSString * totalDistance;
@property (nonatomic,strong) NSString * totalTime;
@property (nonatomic,assign) double   waitingPrice;
@property (nonatomic,strong) NSString  * cityDetailID;
@property (nonatomic,strong) NSString  * serviceTypeID;



@property (nonatomic,assign) BOOL  paymentMode;
@property (nonatomic,assign) BOOL  isPromoUsed;
+(CurrentTrip *) sharedObject;
-(void)resetObject;



@end
