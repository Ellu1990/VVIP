//
//  FavouriteDriver.h
//  Eber
//
//  Created by Elluminati on 25/02/17.
//  Copyright © 2017 Jaydeep. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FavouriteDriver : NSObject

@property (nonatomic,strong) NSArray * arrFavDriver;
@property (nonatomic,strong) NSString * defaultRadious;

+(FavouriteDriver *) sharedObject;
-(void)resetObject;
@end
