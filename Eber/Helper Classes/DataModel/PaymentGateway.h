//
//  card.h
//  Eber Client
//  Created by Elluminati Mini Mac 5 on 16/09/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentGateway : NSObject
@property(nonatomic,assign) NSInteger paymentGatewayId;
@property(nonatomic,copy) NSString * paymentGatewayName;
@end
