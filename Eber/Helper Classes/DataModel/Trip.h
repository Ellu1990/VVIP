 //
//  Trip.h
//    Eber ClientProvider
//
//  Created by Elluminati Mini Mac 5 on 20/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Trip : NSObject
@property(nonatomic,copy) NSString *srcAddress;
@property(nonatomic,copy) NSString *destAddress;
@property(nonatomic,copy) NSString *tripDistance;
@property(nonatomic,copy) NSString *tripTotalCost;
@property(nonatomic,copy) NSDate *tripCreateDate;
@property(nonatomic,copy) NSString *tripCreateTime;
@property(nonatomic,copy) NSString *tripTime;
@property(nonatomic,copy) NSString *tripId;
@property(nonatomic,copy) NSString *userFirstName;
@property(nonatomic,copy) NSString *userLastName;
@property(nonatomic,copy) NSString *userRating;
@property(nonatomic,copy) NSString *userImage;
@property(nonatomic,copy) NSString *userContact;
@property(nonatomic,copy) NSString *distanceUnit;
@property(nonatomic,copy) NSString *currency;
@property(nonatomic,copy) NSString *map;
@property(nonatomic,assign) BOOL isTripCancelled;
@end
