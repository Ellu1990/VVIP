//
//  card.h
//  Eber Client
//  Created by Elluminati Mini Mac 5 on 16/09/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject
@property(nonatomic,copy) NSString * cardType;
@property(nonatomic,copy) NSString * customerId;
@property(nonatomic,copy) NSString * paymentToken;
@property(nonatomic,copy) NSString * lastFour;
@property(nonatomic,copy) NSString * updatedAt;
@property(nonatomic,copy) NSString * createdAt;
@property(nonatomic,copy) NSString * cardId;
@property (nonatomic,assign)BOOL isDefault;
@end
