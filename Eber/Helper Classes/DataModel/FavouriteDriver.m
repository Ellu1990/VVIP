//
//  FavouriteDriver.m
//  Eber
//
//  Created by Elluminati on 25/02/17.
//  Copyright © 2017 Jaydeep. All rights reserved.
//

#import "FavouriteDriver.h"

@implementation FavouriteDriver
@synthesize arrFavDriver,defaultRadious;

#pragma mark - SingleTon Methods
+(FavouriteDriver *) sharedObject
{
	static FavouriteDriver *objFavDriver = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		objFavDriver = [[FavouriteDriver alloc] init];
	});
	return objFavDriver;
}
-(void)resetObject
{
}
@end
