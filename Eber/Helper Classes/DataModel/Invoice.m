//
//  Invoice.m
//  
//
//  Created by Elluminati Mini Mac 5 on 13/09/16.
//
//

#import "Invoice.h"

@implementation Invoice
@synthesize waitingTimeCost,surgeMultiplier,basePriceDistance,tax,total,time,timeCost,distance,distanceUnit,distanceCost,promoBonus,paymentMode,basePrice,referralBonus,taxCost,pricePerUnitTime,pricePerUnitDistance,currency,surgeTimeFee,totalWaitingTime,pricePerWaitingTime,invoiceNumber,walletPayment,cardPayment,remainingPayment,cashPayment;
+ (Invoice *)sharedObject
{
    static Invoice *objinvoice = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        objinvoice = [[Invoice alloc] init];
    });
    return objinvoice;
}
- (instancetype)init
{
    self = [super init];
    if (self)
    {
        total=@"0";
        time=@"0";
        timeCost=@"0";
        distance=@"0";
        distanceCost=@"0";
        promoBonus=@"0.00";
        paymentMode=@"0";
        basePrice=@"0";
        referralBonus=@"0.00";
        taxCost=@"0.00";
        tax=@"0.00";
        basePriceDistance=@"0.00";
        currency=@"$";
        invoiceNumber=@"0";
        walletPayment=@"0.00";
        remainingPayment=@"0.00";
        cardPayment=@"0.00";
        cashPayment=@"0.00";
        distanceUnit=NSLocalizedString(@"KILOMETER", nil);
    }
    return self;
}


@end
