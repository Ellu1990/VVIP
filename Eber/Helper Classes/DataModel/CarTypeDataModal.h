//
//  CarTypeDataModal.h
//  UberforXOwner
//
//  Created by Deep Gami on 14/11/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarTypeDataModal : NSObject
@property (nonatomic,strong) NSString *id_;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *base_price_distance;
@property (nonatomic,strong) NSString *base_price;
@property (nonatomic,strong) NSString *price_for_waiting_time;
@property (nonatomic,strong) NSString *price_for_total_time;
@property (nonatomic,strong) NSString *price_per_unit_distance;
@property (nonatomic,strong) NSString *tax;
@property (nonatomic,strong) NSString *max_space;
@property (nonatomic,strong) NSString *cancellation_charge;
@property (nonatomic,strong) NSString *picture;
@property (nonatomic,strong) NSString *minFare;
@property (nonatomic,strong) NSString *sugrgeMultiplier;
@property (nonatomic,strong) NSString *surgeStartHour;
@property (nonatomic,strong) NSString *surgeEndHour;
@property (nonatomic,strong) NSString *description;
@property (nonatomic,assign) BOOL isSurgeHour;
@property (nonatomic,assign)BOOL isSelected;
@end
