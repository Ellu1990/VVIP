//
//  Invoice.h
//  
//
//  Created by Elluminati Mini Mac 5 on 13/09/16.
//
//

#import <Foundation/Foundation.h>

@interface Invoice : NSObject
+ (Invoice *)sharedObject;
@property(nonatomic,copy) NSString* basePrice;
@property(nonatomic,copy) NSString* invoiceNumber;
@property(nonatomic,copy) NSString* distance;
@property(nonatomic,copy) NSString* time;
@property(nonatomic,copy) NSString* distanceCost;
@property(nonatomic,copy) NSString* timeCost;
@property(nonatomic,copy) NSString* paymentMode;
@property(nonatomic,copy) NSString* referralBonus;
@property(nonatomic,copy) NSString* promoBonus;
@property(nonatomic,copy) NSString* total;
@property(nonatomic,copy) NSString* taxCost;
@property(nonatomic,copy) NSString* pricePerUnitTime;
@property(nonatomic,copy) NSString* pricePerUnitDistance;
@property(nonatomic,copy) NSString* currency;
@property(nonatomic,copy) NSString* distanceUnit;
@property(nonatomic,copy) NSString* surgeTimeFee;
@property(nonatomic,copy) NSString* surgeMultiplier;
@property(nonatomic,copy) NSString* basePriceDistance;
@property(nonatomic,copy) NSString* tax;
@property(nonatomic,copy) NSString* totalWaitingTime;
@property(nonatomic,copy) NSString* pricePerWaitingTime;
@property(nonatomic,copy) NSString* waitingTimeCost;
@property(nonatomic,copy) NSString* walletPayment;
@property(nonatomic,copy) NSString* remainingPayment;
@property(nonatomic,copy) NSString* cardPayment;
@property(nonatomic,copy) NSString* cashPayment;




@end
