 //
//  Trip.h
//    Eber ClientProvider
//
//  Created by Elluminati Mini Mac 5 on 20/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject
@property(nonatomic,copy) NSString *contactId;
@property(nonatomic,copy) NSString *contactName;
@property(nonatomic,copy) NSString *contactNumber;
@property(nonatomic,assign)BOOL isAlwaysShareRideDetail;
@end
