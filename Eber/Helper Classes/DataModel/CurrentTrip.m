//
//  ProviderDetail.m
//  Eber Client
//  Created by My Mac on 7/8/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "CurrentTrip.h"
#import "NSObject+Constants.h"



@implementation CurrentTrip
@synthesize
 trip_id,
 providerFirstName,
 providerLastName,
 providerCarNumber,
 providerCarModal,
 rating,
 providerProfileImage,
 providerLatitude,
 providerLongitude,
 providerId,
 srcAddress,
 destAddress,
 providerStatus,
 isProviderAccepted,
 isProviderRated,
 srcLatitude,
 srcLongitude,
 destLatitude,
 destLongitude,
 providerPhone,
 paymentMode,
 estimateFareTime,
 estimateFareDistance,
 estimateFareTotal,
 cancelTripCharge,
 phoneCountryCode,
 serverTime,
 timeZone,
 waitingPrice,
 tripNumber,
 isPromoUsed,
cityDetailID,
serviceTypeID;
- (id)init {
    if (self = [super init])
    {
        trip_id = @"";
        tripNumber = @"";
        estimateFareTime= @"";
        estimateFareDistance= @"";
        estimateFareTotal= @"";
        providerFirstName = @"";
        providerLastName = @"";
        providerCarNumber = @"";
        providerCarModal = @"";
        rating = @"";
        providerProfileImage = @"";
        providerLatitude = @"";
        providerLongitude = @"";
        providerId = @"";
        srcAddress = @"";
        destAddress = @"";
        providerStatus = @"";
        isProviderAccepted = @"";
        isProviderRated = @"";
        srcLatitude = @"";
        srcLongitude = @"";
        destLatitude = @"";
        destLongitude = @"";
        providerPhone = @"";
        paymentMode= 0;
        isPromoUsed=0;
        cancelTripCharge=@"0";
        phoneCountryCode=@"0";
        waitingPrice=0.0;
		cityDetailID=@"0";
		serviceTypeID=@"0";

	
    }
    return self;
}
#pragma mark - SingleTon Methods
+(CurrentTrip *) sharedObject
{
    static CurrentTrip *objTrip = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        objTrip = [[CurrentTrip alloc] init];
    });
    return objTrip;
}
-(void)resetObject
{
    
    trip_id = @"";
    providerFirstName = @"";
    providerLastName = @"";
    providerCarNumber = @"";
    providerCarModal = @"";
    rating = @"";
    providerProfileImage = @"";
    providerLatitude = @"";
    providerLongitude = @"";
    providerId = @"";
    srcAddress = @"";
    destAddress = @"";
    providerStatus = @"";
    isProviderAccepted = @"";
    isProviderRated = @"";
    srcLatitude = @"";
    srcLongitude = @"";
    destLatitude = @"";
    destLongitude = @"";
    providerPhone = @"";
    paymentMode=NO;
    estimateFareTime= @"";
    estimateFareDistance= @"";
    estimateFareTotal= @"";
    cancelTripCharge=@"0";
    phoneCountryCode=@"0";
    isPromoUsed=0;
    tripNumber = @"";
    waitingPrice=0.0;
	cityDetailID=@"0";
}

@end
