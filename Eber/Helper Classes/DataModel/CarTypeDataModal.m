//
//  CarTypeDataModal.m
//  UberforXOwner
//
//  Created by Deep Gami on 14/11/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "CarTypeDataModal.h"

@implementation CarTypeDataModal
@synthesize
 id_,
 name,
 base_price_distance,
 base_price,
 price_for_waiting_time,
 price_for_total_time,
 price_per_unit_distance,
 tax,
 max_space,
 cancellation_charge,
 picture,
 minFare,
 isSurgeHour,
 sugrgeMultiplier,
 surgeStartHour,
 surgeEndHour,
 isSelected,
  description;
@end
