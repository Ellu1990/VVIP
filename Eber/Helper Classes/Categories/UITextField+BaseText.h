//
//  UITextField+BaseText.h
//  Eber Client
//  Created by My Mac on 7/15/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (BaseText)
-(void)setBorder;
-(void)setText:(NSString*)strText withColor:(UIColor*)Color;
@end
