//
//  UITextField+BaseText.m
//  Eber Client
//  Created by My Mac on 7/15/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "UITextField+BaseText.h"
#import "UIColor+Colors.h"
#import "Aspects.h"

@implementation UITextField (BaseText)

+ (void)load
{
    [[self class]aspect_hookSelector:@selector(awakeFromNib) withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo> aspectInfo) {
        
        UITextField* instance = [aspectInfo instance];
        instance.textColor=[UIColor textColor];
        for (UIView *subView in instance.subviews) {
            if ([subView isKindOfClass:[UIButton class]]) {
                UIButton *button = (UIButton *)subView;
                [button setImage:[[button imageForState:UIControlStateNormal] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]
                        forState:UIControlStateNormal];
                button.tintColor = instance.tintColor;
            }
        }
    }error:nil];
    
}

-(void) setLeftPadding:(int) paddingValue
{
	UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, paddingValue, self.frame.size.height)];
	self.leftView = paddingView;
	self.leftViewMode = UITextFieldViewModeAlways;
}

-(void) setRightPadding:(int) paddingValue
{
	UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, paddingValue, self.frame.size.height)];
	self.rightView = paddingView;
	self.rightViewMode = UITextFieldViewModeAlways;
}
-(void)setBorder
{
	self.layer.borderWidth=1.0f;
	self.layer.borderColor=[UIColor borderColor].CGColor;
	[self setLeftPadding:10];
	[self setRightPadding:10];
	
}
-(void)setText:(NSString*)strText withColor:(UIColor*)Color
{
    
}
@end
