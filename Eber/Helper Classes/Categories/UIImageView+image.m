//
//  UIImageView+image.m
//  
//
//  Created by Elluminati Mini Mac 5 on 06/08/16.
//
//

#import "UIImageView+image.h"
#import "AppDelegate.h"
@implementation UIImageView (image)

-(void)setProfileImage:(UIImage *)imageToResize
{
    
    
    CGFloat width = imageToResize.size.width;
    CGFloat height = imageToResize.size.height;
    
    float scaleFactor;
    if(width > height)
    {
        
        scaleFactor = self.frame.size.height / height;
    }
    else
    {
        scaleFactor = self.frame.size.width / width;
    }
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(width * scaleFactor, height * scaleFactor), NO, 0.0);
    [imageToResize drawInRect:CGRectMake(0, 0, width * scaleFactor, height * scaleFactor)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.contentMode = UIViewContentModeCenter;
    if (self.bounds.size.width > resizedImage.size.width && self.bounds.size.height > resizedImage.size.height) {
        self.contentMode = UIViewContentModeScaleAspectFit;
    }
    self.image=resizedImage;
    
    [self setImageToCenter:self];

}
- (void)setImageToCenter:(UIImageView *)imageView
{
    CGSize imageSize = imageView.image.size;
    [imageView sizeThatFits:imageSize];
    CGPoint imageViewCenter = imageView.center;
    imageViewCenter.x = CGRectGetMidX(self.frame);
    [imageView setCenter:imageViewCenter];
    
}
-(void)downLoadImageFromUrl:(NSString*)url withPlaceHolder:(NSString*)placeHolderImg
{
    [self loadFromURL:[NSURL URLWithString:url] callback:^(UIImage *image)
    {
            if (image)
            {
                self.image=image;
            }
            else
            {
               self.image=[UIImage imageNamed:placeHolderImg];
            }
     }];
}
- (void) loadFromURL: (NSURL*) url callback:(void (^)(UIImage *image))callback
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        NSData * imageData = [NSData dataWithContentsOfURL:url];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *image = [UIImage imageWithData:imageData];
            callback(image);
        });
    });
}
-(void)downloadFromURL:(NSString *)url withPlaceholder:(UIImage *)placehold
{
    if (placehold)
    {
        [self setImage:placehold];
    }
    if (url)
    {
        UIActivityIndicatorView *ai=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake((self.frame.size.width-37)/2, (self.frame.size.height-37)/2, 37, 37)];
        [ai setHidesWhenStopped:YES];
        ai.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
        [ai setTintColor:[UIColor redColor]];
        [self addSubview:ai];
        [ai startAnimating];
        if ([url rangeOfString:@"/Library/"].location != NSNotFound)
        {
            [ai stopAnimating];
            ai = nil;
            NSData *imageData=[NSData dataWithContentsOfFile:url];
            UIImage* image = [[UIImage alloc] initWithData:imageData];
            if (image) {
                [self setImage:image];
                [self setNeedsLayout];
                imageData = nil;
            }
            return;
        }
        
        NSString *strImgName = [[[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] componentsSeparatedByString:@"/"] lastObject];
        AppDelegate *app=(AppDelegate*)[[UIApplication sharedApplication]delegate];
        
        [app applicationCacheDirectoryString];
        
        // NSString *imagePath = [NSString stringWithFormat:@"%@%@",[[AppDelegate sharedObject]applicationCacheDirectoryString],strImgName];
        
        
        NSString *imagePath = [NSString stringWithFormat:@"%@%@",[app applicationCacheDirectoryString],strImgName];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSString *aURL=[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        if([strImgName isEqualToString:@"picture?type=large"])
        {
            [fileManager removeItemAtPath:imagePath error:nil];
        }
        if ([fileManager fileExistsAtPath:imagePath]==NO)
        {
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
            dispatch_async(queue, ^(void)
            {
                NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:aURL]];
                //[imageData writeToFile:imagePath atomically:YES];
                UIImage* imgUpload = [[UIImage alloc] initWithData:imageData];
                NSData *dataS = UIImagePNGRepresentation(imgUpload);
                [dataS writeToFile:imagePath atomically:YES];
                
                imageData = nil;
                if (imgUpload) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [ai stopAnimating];
                        [self setImage:imgUpload];
                        [self setNeedsLayout];
                    });
                }
                else
                {
                    [ai stopAnimating];
                }
            });
        }
        else{
            [ai stopAnimating];
            ai = nil;
            NSData *imageData=[NSData dataWithContentsOfFile:imagePath];
            UIImage* image = [[UIImage alloc] initWithData:imageData];
            if (image) {
                [self setImage:image];
                [self setNeedsLayout];
            }
            imageData = nil;
        }
    }
}

-(void)setRoundImageWithColor:(UIColor*)borderColor
{
    CGRect frame=self.frame;
    frame.size.height=MIN(self.frame.size.width, self.frame.size.height) ;
    frame.size.width=frame.size.height;
    self.frame=frame;
    self.layer.cornerRadius=self.layer.frame.size.width/2;
    self.clipsToBounds = YES;
    self.layer.borderWidth = 2.0f;
    self.layer.borderColor = borderColor.CGColor;
    
}
@end
