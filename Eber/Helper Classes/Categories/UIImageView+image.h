//
//  UIImageView+image.h
//  
//
//  Created by Elluminati Mini Mac 5 on 06/08/16.
//
//

#import <UIKit/UIKit.h>
#import "Aspects.h"
#import "UtilityClass.h"
@interface UIImageView (image) <UIApplicationDelegate>
- (void)setProfileImage:(UIImage *)imageToResize;
-(void)downLoadImageFromUrl:(NSString*)url withPlaceHolder:(NSString*)placeHolderImg;
-(void)downloadFromURL:(NSString *)url withPlaceholder:(UIImage *)placehold;
-(void)setRoundImageWithColor:(UIColor*)borderColor;
@end
