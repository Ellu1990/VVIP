//
//  UILabel+overrideLabel.m
//  
//
//  Created by Elluminati Mini Mac 5 on 27/07/16.
//
//
#import "UILabel+overrideLabel.h"
#import "Aspects.h"
#import "UIColor+Colors.h"
@implementation UILabel (overrideLabel)

+ (void)load{[[self class]aspect_hookSelector:@selector(awakeFromNib) withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo> aspectInfo) {
        
        UILabel* instance = [aspectInfo instance];
        /*UIFont* font = [UIFont fontWithName:@"Roboto-Regular_0" size:instance.font.pointSize];
        instance.font = font;
        instance.adjustsFontSizeToFitWidth=YES;
        [instance setMinimumScaleFactor:10.0/[UIFont labelFontSize]];*/
        instance.textColor=[UIColor labelTextColor];
    }error:nil];}
-(void)setFontForLabel:(UILabel *)label withMaximumFontSize:(float)maxFontSize andMaximumLines:(int)maxLines {
    int numLines = 1;
    float fontSize = maxFontSize;
    CGSize textSize; // The size of the text
    CGSize frameSize; // The size of the frame of the label
    CGSize unrestrictedFrameSize; // The size the text would be if it were not restricted by the label height
    CGRect originalLabelFrame = label.frame;
    
    frameSize = label.frame.size;
    textSize = [label.text sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize: fontSize]}];
    
    // Work out the number of lines that will need to fit the text in snug
    while (((textSize.width / numLines) / (textSize.height * numLines) > frameSize.width / frameSize.height) && (numLines < maxLines)) {
        numLines++;
    }
    
    label.numberOfLines = numLines;
    
    // Get the current text size
    label.font = [UIFont systemFontOfSize:fontSize];
    textSize = [label.text boundingRectWithSize:CGSizeMake(frameSize.width, CGFLOAT_MAX)
                                        options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                     attributes:@{NSFontAttributeName : label.font}
                                        context:nil].size;
    
    // Adjust the frame size so that it can fit text on more lines
    // so that we do not end up with truncated text
    label.frame = CGRectMake(label.frame.origin.x, label.frame.origin.y, label.frame.size.width, label.frame.size.width);
    
    // Get the size of the text as it would fit into the extended label size
    unrestrictedFrameSize = [label textRectForBounds:CGRectMake(0, 0, label.bounds.size.width, CGFLOAT_MAX) limitedToNumberOfLines:numLines].size;
    
    // Keep reducing the font size until it fits
    while (textSize.width > unrestrictedFrameSize.width || textSize.height > frameSize.height) {
        if (fontSize==12.0)
        {
            break;
        }
        else
        {fontSize--;}
        
        label.font = [UIFont systemFontOfSize:fontSize];
        textSize = [label.text boundingRectWithSize:CGSizeMake(frameSize.width, CGFLOAT_MAX)
                                            options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                         attributes:@{NSFontAttributeName : label.font}
                                            context:nil].size;
        unrestrictedFrameSize = [label textRectForBounds:CGRectMake(0, 0, label.bounds.size.width, CGFLOAT_MAX) limitedToNumberOfLines:numLines].size;
    }
    
    // Set the label frame size back to original
    label.frame = originalLabelFrame;
}
-(void)setText:(NSString *)title withColor:(UIColor*)color{
    [self setText:title];
    [self setTextColor:color];
}
@end
