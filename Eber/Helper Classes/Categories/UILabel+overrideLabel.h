//
//  UILabel+overrideLabel.h
//  
//
//  Created by Elluminati Mini Mac 5 on 27/07/16.
//
//

#import <UIKit/UIKit.h>
#import "Aspects.h"
@interface UILabel (overrideLabel)
-(void)setFontForLabel:(UILabel *)label withMaximumFontSize:(float)maxFontSize andMaximumLines:(int)maxLines;
-(void)setText:(NSString *)title withColor:(UIColor*)color;
@end
