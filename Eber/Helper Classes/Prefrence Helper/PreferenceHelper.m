//
//  UserDefaults.m
//  FixatiSPS
//
//  Created by Disha Ladani on 24/12/15.
//  Copyright © 2015 Elluminati. All rights reserved.
//


#import "PreferenceHelper.h"
#import "NSObject+Constants.h"
#define PREFERENCE [NSUserDefaults standardUserDefaults]

/*USER DETAIL*/
static NSString * const keyUserID=@"userID";
static NSString * const keyUserToken=@"userToken";
static NSString * const keyUserFirstName=@"userFirstName";
static NSString * const keyUserLastName=@"userLastName";
static NSString * const keyUserPhone=@"userPhone";
static NSString * const keyUserEmail=@"userEmail";
static NSString * const keyUserPicture=@"userPictureUrl";
static NSString * const keyUserBio=@"userBio";
static NSString * const keyUserAddress=@"userAddress";
static NSString * const keyUserZipcode=@"userZipcode";
static NSString * const keyUserCompanyId=@"userCompanyId";
static NSString * const keyIsLogin=@"userIsLogin";
static NSString * const keyUserLoginBy=@"userLoginBy";
static NSString * const keyUserSocialId=@"userSocialID";
static NSString * const keyUserCountryCode=@"UserCountryCode";
static NSString * const keyTripTime=@"preTripTime";
static NSString * const keyHotlineId=@"hotlineId";
static NSString * const keyIsUseApproved=@"userApproved";
static NSString * const keyHotlineKey=@"hotlineKey";
static NSString * const keyContactEmail=@"contactEmail";
/*Trip Details*/
static NSString * const keyDeviceToken=@"deviceToken";
static NSString * const keyIsPathDraw=@"isPathDraw";
static NSString * const keyIsSoundOff=@"isSoundOff";
static NSString * const keyIsDriverArrivedSoundOff=@"isDriverArrivedSoundOff";
static NSString * const keyTripId=@"tripId";
static NSString * const keyIsPaymentModeCashAvailable=@"isPaymentModeCash";
static NSString * const keyIsPaymentModeCardAvailable=@"isPaymentModeCard";
static NSString * const keyIsPromoForCash=@"isPromoForCash";
static NSString * const keyIsPromoForCard=@"isPromoForCard";

static NSString * const keyRefferalCode=@"refferalCode";
static NSString * const keyHomeAddress=@"homeAddress";
static NSString * const keyWorkAddress=@"workAddress";
/*Google Keys and Settings*/
static NSString * const keyPublicStripeKey=@"publicStripeKey";
static NSString * const keyServerKey=@"googleServerKey";
static NSString * const keyIsEmailOtpOn=@"emailOtpOn";
static NSString * const keyIsSmsOtpOn=@"smsOtpOn";
@implementation PreferenceHelper
{
    
}

+ (PreferenceHelper *)sharedObject
{
    static PreferenceHelper *objPreference = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        objPreference = [[PreferenceHelper alloc] init];
    });
    return objPreference;
}

#pragma Common Preference
-(void)setUserId:(NSString *)userId
{
   [PREFERENCE setObject:userId forKey:keyUserID];
   [PREFERENCE synchronize];
}
-(NSString*)userId
{  return [PREFERENCE objectForKey:keyUserID];}
-(void)setUserToken:(NSString *)userToken
{
    [PREFERENCE setObject:userToken forKey:keyUserToken];
    [PREFERENCE synchronize];
}
-(NSString*)userToken
{  return [PREFERENCE objectForKey:keyUserToken];}

#pragma Register Preference
-(void)setUserFirstName:(NSString *)userFirstName
{
    [PREFERENCE setObject:userFirstName forKey:keyUserFirstName];
    [PREFERENCE synchronize];
}
-(NSString*)userFirstName
{  return [PREFERENCE objectForKey:keyUserFirstName];}
-(void)setUserLastName:(NSString *)userLastName
{
    [PREFERENCE setObject:userLastName forKey:keyUserLastName];
    [PREFERENCE synchronize];
}
-(NSString*)userLastName
{  return [PREFERENCE objectForKey:keyUserLastName];}
-(void)setUserEmail:(NSString *)userEmail
{
    [PREFERENCE setObject:userEmail forKey:keyUserEmail];
    [PREFERENCE synchronize];
}
-(NSString*)userEmail
{  return [PREFERENCE objectForKey:keyUserEmail];}
-(void)setUserPicture:(NSString *)userPicture
{
    [PREFERENCE setObject:userPicture forKey:keyUserPicture];
    [PREFERENCE synchronize];
}
-(NSString*)userPicture
{  return [PREFERENCE objectForKey:keyUserPicture];}
-(void)setUserZipcode:(NSString *)userZipcode
{
    [PREFERENCE setObject:userZipcode forKey:keyUserZipcode];
    [PREFERENCE synchronize];
}
-(NSString*)userZipcode
{  return [PREFERENCE objectForKey:keyUserZipcode];
}
-(void)setUserCompanyId:(NSString *)userCompanyId
{
	[PREFERENCE setObject:userCompanyId forKey:keyUserCompanyId];
	[PREFERENCE synchronize];
}

-(NSString*)userCompanyId
{  return [PREFERENCE objectForKey:keyUserCompanyId];
}
-(void)setUserAddress:(NSString *)userAddress
{
    [PREFERENCE setObject:userAddress forKey:keyUserAddress];
    [PREFERENCE synchronize];
}
-(NSString*)userAddress
{  return [PREFERENCE objectForKey:keyUserAddress];}
-(void)setUserBio:(NSString *)userBio
{
    [PREFERENCE setObject:userBio forKey:keyUserBio];
    [PREFERENCE synchronize];
}
-(NSString*)userBio
{  return [PREFERENCE objectForKey:keyUserBio];}
-(void)setUserPhone:(NSString *)userPhone
{
    [PREFERENCE setObject:userPhone forKey:keyUserPhone];
    [PREFERENCE synchronize];
}
-(NSString*)userPhone
{  return [PREFERENCE objectForKey:keyUserPhone];}
-(void)setUserCountryCode:(NSString *)userCountryCode
{
    [PREFERENCE setObject:userCountryCode  forKey:keyUserCountryCode];
    [PREFERENCE synchronize];
}
-(NSString*)userCountryCode
{  return [PREFERENCE objectForKey:keyUserCountryCode];}


-(void)setUserLoginBy:(NSString *)userLoginBy
{
    [PREFERENCE setObject:userLoginBy  forKey:keyUserLoginBy];
    [PREFERENCE synchronize];
}
-(NSString*)userLoginBy
{  return [PREFERENCE objectForKey:keyUserLoginBy];}

-(void)setUserSocialId:(NSString *)UserSocialId
{
    [PREFERENCE setObject:UserSocialId  forKey:keyUserSocialId];
    [PREFERENCE synchronize];
}
-(NSString*)userSocialId
{  return [PREFERENCE objectForKey:keyUserSocialId];}

-(void)setUserLogin:(BOOL) UserLogin
{
    [PREFERENCE setBool:UserLogin forKey:keyIsLogin];
    [PREFERENCE synchronize];
}
-(BOOL)isUserLogin
{  return [PREFERENCE boolForKey:keyIsLogin];}
- (void) clearPreference
{   [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
    [[NSUserDefaults standardUserDefaults] synchronize];}
-(void)setTripId:(NSString *)tripId
{
    [PREFERENCE setObject:tripId forKey:keyTripId];
    [PREFERENCE synchronize];
}
-(NSString*)tripId
{
 return [PREFERENCE objectForKey:keyTripId];
}
-(NSString*)deviceToken
{  return [PREFERENCE objectForKey:keyDeviceToken];}
-(void)setDeviceToken:(NSString*)deviceToken
{   [PREFERENCE setObject:deviceToken forKey:keyDeviceToken];
    [PREFERENCE synchronize];}


-(NSString*)stripeKey
{  return [PREFERENCE objectForKey:keyPublicStripeKey];}
-(void)setStripeKey:(NSString*)stripeKey
{   [PREFERENCE setObject:stripeKey forKey:keyPublicStripeKey];
    [PREFERENCE synchronize];
}


-(NSString*)serverKey
{  return [PREFERENCE objectForKey:keyServerKey];}
-(void)setServerKey:(NSString*)serverKey
{   [PREFERENCE setObject:serverKey forKey:keyServerKey];
    [PREFERENCE synchronize];
}

-(NSString*)refferalCode
{  return [PREFERENCE objectForKey:keyRefferalCode];}
-(void)setRefferalCode:(NSString*)refferalCode
{   [PREFERENCE setObject:refferalCode forKey:keyRefferalCode];
    [PREFERENCE synchronize];
}

-(NSString*)homeAddress
{  return [PREFERENCE objectForKey:keyHomeAddress];}
-(void)setHomeAddress:(NSString*)homeAddress
{   [PREFERENCE setObject:homeAddress forKey:keyHomeAddress];
    [PREFERENCE synchronize];
}

-(NSString*)workAddress
{  return [PREFERENCE objectForKey:keyWorkAddress];}
-(void)setWorkAddress:(NSString*)workAddress
{   [PREFERENCE setObject:workAddress forKey:keyWorkAddress];
    [PREFERENCE synchronize];
}

-(void)setSoundOff:(BOOL)soundOff
{
    [PREFERENCE setBool:soundOff forKey:keyIsSoundOff];
    [PREFERENCE synchronize];
}
-(BOOL)isSoundOff
{
   
    return [PREFERENCE boolForKey:keyIsSoundOff];
}


-(void)setEmailOtpOn:(BOOL)isEmailOtp
{
    [PREFERENCE setBool:isEmailOtp forKey:keyIsEmailOtpOn];
    [PREFERENCE synchronize];
}
-(BOOL)isEmailOtpOn
{  return [PREFERENCE boolForKey:keyIsEmailOtpOn];}

-(void)setSmsOtpOn:(BOOL)isSmsOtp
{
    [PREFERENCE setBool:isSmsOtp forKey:keyIsSmsOtpOn];
    [PREFERENCE synchronize];
}
-(BOOL)isSmsOtpOn
{ return [PREFERENCE boolForKey:keyIsSmsOtpOn];}
-(void)setPreTripTime:(NSString*)tripTime
{
    [PREFERENCE setObject:tripTime forKey:keyTripTime];
    [PREFERENCE synchronize];
}
-(NSString*)preTripTime
{ return [PREFERENCE objectForKey:keyTripTime];}




-(void)setCardAvailable:(BOOL)cardAvailable
{
    [PREFERENCE setBool:cardAvailable forKey:keyIsPaymentModeCardAvailable];
    [PREFERENCE synchronize];

}
-(BOOL)isCardAvailable
{  return [PREFERENCE boolForKey:keyIsPaymentModeCardAvailable];}
-(void)setCashAvailable:(BOOL)cashAvailable
{
    [PREFERENCE setBool:cashAvailable forKey:keyIsPaymentModeCashAvailable];
    [PREFERENCE synchronize];
}
-(BOOL)isCashAvailable
{  return [PREFERENCE boolForKey:keyIsPaymentModeCashAvailable];}


-(NSString*)hotLineAppKey
{  return [PREFERENCE objectForKey:keyServerKey];}
-(void)setHotlineAppKey:(NSString*)hotlineKey
{
    [PREFERENCE setObject:hotlineKey forKey:keyHotlineKey];
    [PREFERENCE synchronize];
}


-(void)setUserApproved:(BOOL) userApproved
{
    [PREFERENCE setBool:userApproved forKey:keyIsUseApproved];
    [PREFERENCE synchronize];
}
-(BOOL)UserApproved
{  return [PREFERENCE boolForKey:keyIsUseApproved];}



-(NSString*)hotLineAppId
{  return [PREFERENCE objectForKey:keyHotlineId];}
-(void)setHotlineAppId:(NSString*)hotlineId
{
    [PREFERENCE setObject:hotlineId forKey:keyHotlineId];
    [PREFERENCE synchronize];
}


-(void)setContactEmail:(NSString*)contactMail
{
    [PREFERENCE setObject:contactMail forKey:keyContactEmail];
    [PREFERENCE synchronize];
}
-(NSString*)contactEmail
{
    return [PREFERENCE objectForKey:keyContactEmail];
}
    
-(void)setIsPathDraw:(BOOL) isPathDraw
{
        [PREFERENCE setBool:isPathDraw forKey:keyIsPathDraw];
        [PREFERENCE synchronize];
        
}
-(BOOL)isPathDraw
{
        return [PREFERENCE boolForKey:keyIsPathDraw];
}
-(void)setDriverArrivedSoundOff:(BOOL)soundOff
{
    [PREFERENCE setBool:soundOff forKey:keyIsDriverArrivedSoundOff];
    [PREFERENCE synchronize];
}
-(BOOL)isDriverArrivedSoundOff
{
    
    return [PREFERENCE boolForKey:keyIsDriverArrivedSoundOff];
}

-(BOOL)isPromoForCard
{
    return [PREFERENCE boolForKey:keyIsPromoForCard];
}
-(void)setPromoForCard:(BOOL)cardPromoAvailable
{
    [PREFERENCE setBool:cardPromoAvailable forKey:keyIsPromoForCard];
    [PREFERENCE synchronize];

}
-(BOOL)isPromoForCash
{
    return [PREFERENCE boolForKey:keyIsPromoForCash];
}
-(void)setPromoForCash:(BOOL)cashPromoAvailable
{
    [PREFERENCE setBool:cashPromoAvailable forKey:keyIsPromoForCash];
    [PREFERENCE synchronize];

}

@end
