//
//  UserDefaults.h
//  FixatiSPS
//
//  Created by Disha Ladani on 24/12/15.
//  Copyright © 2015 Elluminati. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface PreferenceHelper : NSObject
+ (PreferenceHelper *)sharedObject;

-(void)setUserId:(NSString *)userId;
-(NSString*)userId;
-(void)setUserToken:(NSString *)userToken;
-(NSString*)userToken;
#pragma Register Preference
-(void)setUserFirstName:(NSString *)userFirstName;
-(NSString*)userFirstName;
-(void)setUserLastName:(NSString *)userLastName;
-(NSString*)userLastName;
-(void)setUserEmail:(NSString *)userEmail;
-(NSString*)userEmail;
-(void)setUserPicture:(NSString *)userPicture;
-(NSString*)userPicture;
-(void)setUserZipcode:(NSString *)userZipcode;
-(NSString*)userZipcode;
-(void)setUserAddress:(NSString *)userAddress;
-(NSString*)userAddress;
-(void)setUserBio:(NSString *)userBio;
-(NSString*)userBio;
-(void)setUserPhone:(NSString *)userPhone;
-(NSString*)userPhone;
-(void)setUserCountryCode:(NSString *)userCountryCode;
-(NSString*)userCountryCode;
-(void)setUserLoginBy:(NSString *)UserLoginBy;
-(NSString*)userLoginBy;
-(void)setUserSocialId:(NSString *)UserSocialId;
-(NSString*)userSocialId;
-(void)setRefferalCode:(NSString*)refferalCode;
-(NSString*)refferalCode;
-(void)setTripId:(NSString *)tripId;
-(NSString*)tripId;
-(void)setDeviceToken:(NSString*)deviceToken;
-(NSString*)deviceToken;
-(void)setStripeKey:(NSString*)stripeKey;
-(NSString*)stripeKey;
-(void)setServerKey:(NSString*)serverKey;
-(NSString*)serverKey;
-(void)setHomeAddress:(NSString*)homeAddress;
-(NSString*)homeAddress;
-(void)setUserCompanyId:(NSString *)userCompanyId;
-(NSString*)userCompanyId;
-(void)setWorkAddress:(NSString*)workAddress;
-(NSString*)workAddress;
-(BOOL)isCardAvailable;
-(void)setCardAvailable:(BOOL)cardAvailable;
-(BOOL)isCashAvailable;
-(void)setCashAvailable:(BOOL)cashAvailable;

-(BOOL)isPromoForCard;
-(void)setPromoForCard:(BOOL)cardPromoAvailable;
-(BOOL)isPromoForCash;
-(void)setPromoForCash:(BOOL)cashPromoAvailable;



-(BOOL)isSoundOff;
-(void)setSoundOff:(BOOL)soundOff;
-(BOOL)isUserLogin;
-(void)setUserLogin:(BOOL) UserLogin;
-(void)setEmailOtpOn:(BOOL)isEmailOtp;
-(BOOL)isEmailOtpOn;
-(void)setSmsOtpOn:(BOOL)isSmsOtp;
-(BOOL)isSmsOtpOn;
-(void)setPreTripTime:(NSString*)tripTime;
-(NSString*)preTripTime;
-(void) clearPreference;
-(void)setUserApproved:(BOOL) userApproved;
-(BOOL)UserApproved;


-(void)setContactEmail:(NSString*)contactMail;
-(NSString*)contactEmail;
-(NSString*)hotLineAppKey;
-(void)setHotlineAppKey:(NSString*)hotlineKey;
-(NSString*)hotLineAppId;
-(void)setHotlineAppId:(NSString*)hotlineId;
-(void)setIsPathDraw:(BOOL) isPathDraw;
-(BOOL)isPathDraw;
-(void)setDriverArrivedSoundOff:(BOOL)soundOff;
-(BOOL)isDriverArrivedSoundOff;

@end
