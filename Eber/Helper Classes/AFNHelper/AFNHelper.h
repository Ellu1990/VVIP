//
//  AFNHelper.h
//  Tinder
//
//  Created by Elluminati - macbook on 04/04/14.
//  Copyright (c) 2014 AppDupe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomAlertWithTitle.h"
#define POST_METHOD @"POST"
#define GET_METHOD  @"GET"
#define PUT_METHOD  @"PUT"
#define DELETE_METHOD  @"DELETE"
#import "AppDelegate.h"

typedef void (^RequestCompletionBlock)(id response, NSError *error);

@interface AFNHelper : NSObject<CustomAlertDelegate,UIViewControllerPreviewingDelegate>
{
        RequestCompletionBlock dataBlock;
}
@property(nonatomic,copy)NSString *strReqMethod;
-(id)initWithRequestMethod:(NSString *)method;
-(void)getDataFromPath:(NSString *)path withParamData:(NSMutableDictionary *)dictParam withBlock:(RequestCompletionBlock)block;
-(void)getAddressFromGooglewAutoCompletewithParamData:(NSMutableDictionary *)dictParam withBlock:(RequestCompletionBlock)block;
-(NSString *)getErrorMessage:(NSString *)str;
@property(nonatomic,strong) CustomAlertWithTitle *netWorkDialog;
@end
