//
//  AFNHelper.m
//  Tinder
//
//  Created by Elluminati - macbook on 04/04/14.
//  Copyright (c) 2014 AppDupe. All rights reserved.
//

#import "AFNHelper.h"
#import "AFNetworking.h"
#import "NSObject+Constants.h"
#import "AFHTTPSessionManager.h"
#import "AppDelegate.h"
#import "UtilityClass.h"
#import "Parser.h"

@implementation AFNHelper
{
    
}
@synthesize strReqMethod,netWorkDialog;

#pragma mark -
#pragma mark - Init

-(id)initWithRequestMethod:(NSString *)method
{
    if ((self = [super init]))
    {
        self.strReqMethod=method;
    }
	return self;
}
#pragma mark -
#pragma mark - Methods

-(void)getDataFromPath:(NSString *)path withParamData:(NSMutableDictionary *)dictParam withBlock:(RequestCompletionBlock)block
{
    if ([APPDELEGATE connected])
    {
        if (block)
        {
            dataBlock=[block copy];
        }
        if ([self.strReqMethod isEqualToString:POST_METHOD] || [self.strReqMethod isEqualToString:PUT_METHOD]||[self.strReqMethod isEqualToString:DELETE_METHOD])
        {
            NSString *url=[NSString stringWithFormat:@"%@%@",BASE_URL,path];
            NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
            [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictParam options:NSJSONWritingPrettyPrinted error:&error];
            NSLog(@"post data:%@",dictParam);
            [urlRequest setHTTPMethod:strReqMethod];
            [urlRequest setHTTPBody:jsonData];
            
            NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
            [urlRequest setHTTPBody:jsonData];
            NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                                  {
                                                      
                                                      if(response!=nil)
                                                      {
                                                          NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                          
                                                          if ([httpResponse statusCode]==200)
                                                          {
                                                              if (error==nil)
                                                              {
                                                                  if (dataBlock)
                                                                  {
                                                                      if(data!=nil)
                                                                      {
                                                                          NSString *responseStr = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
                                                                          
                                                                          if ([[Parser sharedObject]parseSessionToken:responseStr])
                                                                          {
                                                                              dataBlock(responseStr,nil);
                                                                          }
                                                                      }
                                                                      else
                                                                      {
                                                                          NSLog(@"Data Response is Null");
                                                                      }
                                                                  }
                                                                  else
                                                                  {
                                                                      NSLog(@"DataBlock Not Initiate");
                                                                  }
                                                              }
                                                              else
                                                              {
                                                                  
                                                                  if (dataBlock)
                                                                      dataBlock(nil,error);
                                                                  else
                                                                      NSLog(@"DataBlock Not Initiate");
                                                              }
                                                              
                                                          }
                                                          else
                                                          {
                                                              [APPDELEGATE hideLoadingView];
                                                              switch ([httpResponse statusCode])
                                                              {
                                                                  case 404:
                                                                      
                                                                      [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_404", nil)];
                                                                      
                                                                      break;
                                                                  case 408:
                                                                      
                                                                      [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_408", nil)];
                                                                      
                                                                      break;
                                                                  case 413:
                                                                      
                                                                      [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_413", nil)];
                                                                      break;
                                                                  case 500:
                                                                      
                                                                      [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_500", nil)];
                                                                  case 502:
                                                                      
                                                                      [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_502", nil)];
                                                                     break;
                                                                  case 503:
                                                                      
                                                                      [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_503", nil)];
                                                                      break;
                                                                      
                                                                  case 504:
                                                                      
                                                                      [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_504", nil)];
                                                                      break;
                                                                      break;
                                                                   default:
                                                                      break;
                                                              }
                                                          }
                                                          
                                                      }
                                                      else
                                                      {
                                                          NSLog(@"Response Is Null -- %@",error );
                                                          [APPDELEGATE hideLoadingView];
                                                          [[UtilityClass sharedObject]showToast:NSLocalizedString(@"Could not connect to the server", nil)];
                                                      }
                                                      
                                                  }];
            [postDataTask resume];
        }
    }
    else
    {
        [APPDELEGATE hideLoadingView];
        if (netWorkDialog == nil)
        {
                                   netWorkDialog=[[CustomAlertWithTitle alloc]
                                       initWithTitle:NSLocalizedString(@"ALERT_TITLE_NETWORK_STATUS", nil)
                                       message:NSLocalizedString(@"ALERT_MSG_NO_INTERNET", nil)
                                       delegate:APPDELEGATE
                                       cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_NO", nil)
                                       otherButtonTitles:NSLocalizedString(@"ALERT_BTN_YES", nil)];
        }
        
    }
}
-(void)getAddressFromGooglewAutoCompletewithParamData:(NSMutableDictionary *)dictParam withBlock:(RequestCompletionBlock)block
{
    if ([APPDELEGATE connected])
    {
        if (block)
        {
            dataBlock=[block copy];
        }
        NSString *url=[NSString stringWithFormat:@"%@input=%@&key=%@",AutoComplete_URL,[dictParam valueForKey:GOOGLE_PARAM_AUTOCOMLETE_INPUT],[dictParam valueForKey:GOOGLE_PARAM_AUTOCOMLETE_KEY]];
        url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
      //  NSLog(@"%@",url);
       // NSLog(@"%@",dictParam);
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
         {
             if(responseObject!=nil)
             {
                 dataBlock(responseObject,nil);
             }
             else
             {
                 dataBlock(nil,nil);
             }
         }
             failure:^(NSURLSessionTask *operation, NSError *error) {
                 NSLog(@"Error: %@", error);
             }];

    }
    else
    {
        [APPDELEGATE hideLoadingView];
        if (netWorkDialog == nil)
        {
            netWorkDialog=[[CustomAlertWithTitle alloc]
                           initWithTitle:NSLocalizedString(@"ALERT_TITLE_NETWORK_STATUS", nil)
                           message:NSLocalizedString(@"ALERT_MSG_NO_INTERNET", nil)
                           delegate:APPDELEGATE
                           cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_NO", nil)
                           otherButtonTitles:NSLocalizedString(@"ALERT_BTN_YES", nil)];
            
        }
      
    }
}

#pragma mark - get Error Messsage :

-(NSString *)getErrorMessage:(NSString *)str
{
	NSString *strs=[NSString stringWithFormat:@"%@",NSLocalizedString(str, nil)];
	return strs;
	
}
-(void)onClickClose:(CustomAlertWithTitle *)view
{
    [[UtilityClass sharedObject]animateHide:netWorkDialog];
  }
-(void)onClickCustomDialogOk:(CustomAlertWithTitle *)view
{
    if (view==netWorkDialog)
    {
        [[UtilityClass sharedObject] animateHide:netWorkDialog];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }

}





@end
