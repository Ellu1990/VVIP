
#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"
 /*Date Formats*/
#define DATE_TIME_FORMAT_WEB  @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
#define DATE_TIME_FORMAT @"yyyy-MM-dd HH:mm:ss"
#define TIME_FORMAT @"HH:mm:ss"
#define DATE_FORMAT @"yyyy-MM-dd"
#define DATE_FORMAT_MONTH @"MMMM yyyy"
#define DAY @"d"
#define TIME_FORMAT_AM @"h:mm a"
@interface UtilityClass : NSObject
+ (UtilityClass *)sharedObject;
//Show And Hide Animation
- (void)animateShow:(UIView*) view;
- (void)animateHide:(UIView*) view;
//Distance convertion methods
-(double)meterToKilometer:(double)meter;
-(NSString *)secondToTime:(int)totalSeconds;
//Directory Path Methods
- (NSString *)applicationDocumentDirectoryString;
- (NSString *)applicationCacheDirectoryString;
- (NSURL *)applicationDocumentsDirectoryURL;
//Email Validation
-(BOOL)isValidEmailAddress:(NSString *)email;
//Show Alert
-(void)displayAlertWithTitle:(NSString *)title andMessage:(NSString *)message;
-(void) showToast:(NSString *)message;
-(UIView*)addShadow:(UIView*)view;
#pragma mark-Date Releted Funtions
-(void)setTimeZone:(NSString*)timeZone;
-(NSString*)getTimeZone;
-(NSDate *)stringToDate:(NSString *)dateString;
-(NSDate *)stringToDate:(NSString *)dateString withFormate:(NSString *)format;
-(NSString *)DateToString:(NSDate *)date withFormateSufix:(NSString *)format;
-(NSString *)DateToString:(NSDate *)date;
-(NSString *)DateFormate:(NSString*)date;
-(NSString *)DateToString:(NSDate *)date withFormate:(NSString *)format;
//Download image from url
- (void) loadFromURL: (NSURL*) url callback:(void (^)(UIImage *image))callback;
#pragma mark-String Releted Funtions
+(NSString *)formatAddress:(NSString *)address;
+(BOOL)isEmpty:(NSString *)str;
@end
