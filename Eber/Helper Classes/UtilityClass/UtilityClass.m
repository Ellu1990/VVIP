
#import "UtilityClass.h"
#import "UILabel+overrideLabel.h"
#import "UIColor+Colors.h"
@implementation UtilityClass
{
    NSDateFormatter *dateFormatter;
}

#pragma mark -
#pragma mark - Init And Shared Object

-(id) init{
    if((self = [super init]))
    {
     dateFormatter = [[NSDateFormatter alloc] init];
   }
    return self;
}

+ (UtilityClass *)sharedObject
{
    static UtilityClass *objUtility = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        objUtility = [[UtilityClass alloc] init];
    });
    return objUtility;
}

#pragma mark -
#pragma mark - Distance convertion methods

-(double)meterToKilometer:(double)meter
{
    double kilometer=meter/1000;
    return kilometer;
}
#pragma mark -
#pragma mark - Directory Path Methods
- (NSString *)applicationDocumentDirectoryString
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}
- (NSString *)applicationCacheDirectoryString
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cacheDirectory = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return cacheDirectory;
}
- (NSURL *)applicationDocumentsDirectoryURL
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}
#pragma mark -
#pragma mark - datetime helper
- (NSDate *)stringToDate:(NSString *)dateString
{
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
	NSDate * date = [dateFormatter dateFromString:dateString];
    return date;
}
- (NSDate *)stringToDate:(NSString *)dateString withFormate:(NSString *)format
{
    
    [dateFormatter setDateFormat:format];
    NSDate *date = [dateFormatter dateFromString: dateString];
    return date;
}
- (NSString *)DateToString:(NSDate *)date
{
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//2013-07-15:10:00:00
    NSString * strdate = [dateFormatter stringFromDate:date];
    return strdate;
}
-(NSString *)DateToString:(NSDate *)date withFormate:(NSString *)format
{
    
    [dateFormatter setDateFormat:format];//2013-07-15:10:00:00
    NSString * strdate = [dateFormatter stringFromDate:date];
    return strdate;
}
-(NSString *)DateToString:(NSDate *)date withFormateSufix:(NSString *)format
{
    
    NSDateFormatter *prefixDateFormatter = [[NSDateFormatter alloc] init];
    [prefixDateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [prefixDateFormatter setDateFormat:format];
    NSString * prefixDateString = [prefixDateFormatter stringFromDate:date];
    NSDateFormatter *monthDayFormatter = [[NSDateFormatter alloc] init];
    [monthDayFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [monthDayFormatter setDateFormat:@"d"];
    int date_day = [[monthDayFormatter stringFromDate:date] intValue];
    NSString *suffix;
    if (date_day >= 11 && date_day <= 13)
    {
        suffix=@"th ";
    }
    else
    {
        switch (date_day % 10) {
        case 1:
            suffix=@"st ";
        case 2:
            suffix=@"nd ";
        case 3:
            suffix=@"rd ";
        default:
            suffix=@"th ";
        }
    }
    prefixDateString = [prefixDateString stringByReplacingOccurrencesOfString:@" " withString:suffix options:NSCaseInsensitiveSearch range:NSMakeRange(0, 3)];
    return prefixDateString;
}
- (NSString *)secondToTime:(int)totalSeconds{
	
     float minut= totalSeconds * 0.0166667;
    int x=round(minut);
   
    return [NSString stringWithFormat:@"%i",x];
}
#pragma mark -
#pragma mark - Show Alert
-(void) displayAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    @try
    {
        [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    }
    @catch (NSException *exception)
    {
    }
	UIAlertController * alert=   [UIAlertController
							alertControllerWithTitle:title
							message:message
							preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction* ok = [UIAlertAction
					 actionWithTitle:NSLocalizedString(@"ALERT_BTN_OK", nil)
					 style:UIAlertActionStyleDefault
					 handler:^(UIAlertAction * action)
					 {
						 [alert dismissViewControllerAnimated:YES completion:nil];
						 
					 }];
	
	[alert addAction:ok];
    id rootViewController=[UIApplication sharedApplication].delegate.window.rootViewController;
	if([rootViewController isKindOfClass:[UINavigationController class]])
	{
		rootViewController=[((UINavigationController *)rootViewController).viewControllers objectAtIndex:0];
	}
	[rootViewController presentViewController:alert animated:YES completion:nil];
}


-(void)showToast:(NSString *)message
{
    @try
    {
        [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    }
    @catch (NSException *exception)
    {
    }
    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
        UIWindow * keyWindow = [[UIApplication sharedApplication] keyWindow];
        UILabel *toastView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 20)];
        
        toastView.text = message;
        toastView.textColor = [UIColor whiteColor];
        toastView.backgroundColor = [UIColor blackColor];
        toastView.textAlignment = NSTextAlignmentCenter;
        [toastView setNumberOfLines:2];
        [toastView sizeToFit];
        toastView.frame = CGRectMake(25, keyWindow.frame.size.height-toastView.frame.size.height-200, toastView.frame.size.width+50, toastView.frame.size.height+10);
        toastView.layer.cornerRadius = toastView.frame.size.height/2;
        toastView.layer.masksToBounds = YES;
        toastView.center =CGPointMake(keyWindow.center.x, keyWindow.frame.size.height-50-toastView.frame.size.height);
        [toastView setFontForLabel:toastView withMaximumFontSize:12 andMaximumLines:2];
        [keyWindow addSubview:toastView];
        [UIView animateWithDuration: 2.0
                              delay: 0.0
                            options: UIViewAnimationOptionCurveEaseOut
                         animations: ^{
                             toastView.alpha = 0.0;
                         }
                         completion: ^(BOOL finished) {
                             [toastView removeFromSuperview];
                         }
         ];
    }];
}
-(NSString *)DateFormate:(NSString*)date
{
    [date stringByAppendingString:@":00"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *dateToFormate = [dateFormatter dateFromString:date];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *formattedDate = [dateFormatter stringFromDate:dateToFormate];
    return  formattedDate;
}
- (void)animateShow:(UIView *)view
{
	CAKeyframeAnimation *animation = [CAKeyframeAnimation
							    animationWithKeyPath:@"transform"];
	
	CATransform3D scale1 = CATransform3DMakeScale(0.5, 0.5, 1);
	CATransform3D scale2 = CATransform3DMakeScale(1.2, 1.2, 1);
	CATransform3D scale3 = CATransform3DMakeScale(0.9, 0.9, 1);
	CATransform3D scale4 = CATransform3DMakeScale(1.0, 1.0, 1);
	
	NSArray *frameValues = [NSArray arrayWithObjects:
					    [NSValue valueWithCATransform3D:scale1],
					    [NSValue valueWithCATransform3D:scale2],
					    [NSValue valueWithCATransform3D:scale3],
					    [NSValue valueWithCATransform3D:scale4],
					    nil];
	[animation setValues:frameValues];
	
	NSArray *frameTimes = [NSArray arrayWithObjects:
					   [NSNumber numberWithFloat:0.0],
					   [NSNumber numberWithFloat:0.5],
					   [NSNumber numberWithFloat:0.8],
					   [NSNumber numberWithFloat:1.5],
					   nil];
	[animation setKeyTimes:frameTimes];
	
	animation.fillMode = kCAFillModeForwards;
	animation.removedOnCompletion = NO;
	animation.duration = 0.2;
	[view.layer addAnimation:animation forKey:@"show"];
}
-(void)animateHide:(UIView *) view
{
	CAKeyframeAnimation *animation = [CAKeyframeAnimation
							    animationWithKeyPath:@"transform"];
	
	CATransform3D scale1 = CATransform3DMakeScale(1.0, 1.0, 1);
	CATransform3D scale2 = CATransform3DMakeScale(0.5, 0.5, 1);
	CATransform3D scale3 = CATransform3DMakeScale(0.0, 0.0, 1);
	
	NSArray *frameValues = [NSArray arrayWithObjects:
					    [NSValue valueWithCATransform3D:scale1],
					    [NSValue valueWithCATransform3D:scale2],
					    [NSValue valueWithCATransform3D:scale3],
					    nil];
	[animation setValues:frameValues];
	
	NSArray *frameTimes = [NSArray arrayWithObjects:
					   [NSNumber numberWithFloat:0.0],
					   [NSNumber numberWithFloat:0.5],
					   [NSNumber numberWithFloat:0.8],
					   [NSNumber numberWithFloat:1.5],
					   nil];
	[animation setKeyTimes:frameTimes];
	
	animation.fillMode = kCAFillModeForwards;
	animation.removedOnCompletion = NO;
	animation.duration = 0.5;
	
	[view.layer addAnimation:animation forKey:@"hide"];
	
	[view performSelector:@selector(removeFromSuperview) withObject:view afterDelay:0.10];
	
}

#pragma mark-Convert String JsonString
-(UIView*)addShadow:(UIView*)view
{
    view.layer.shadowOpacity=0.6;
    view.layer.shadowOffset= CGSizeMake(0, 3.0f);
    view.layer.shadowColor = [UIColor textColor].CGColor;
    view.layer.shadowRadius=10.0;
    view.layer.masksToBounds=NO;
    view.clipsToBounds = NO;
    return view;
}
- (void) loadFromURL: (NSURL*) url callback:(void (^)(UIImage *image))callback
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        NSData * imageData = [NSData dataWithContentsOfURL:url];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *image = [UIImage imageWithData:imageData];
            callback(image);
        });
    });
}
#pragma mark -
#pragma mark - String Utillity Functions
+(NSString *)formatAddress:(NSString *)address
{
    NSString *myAddress;
    NSArray* myArray = [address  componentsSeparatedByString:@","];
    if (myArray.count)
    {
        NSString* firstString = [myArray objectAtIndex:0];
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        BOOL isDecimal = [nf numberFromString:firstString] != nil;
        if (isDecimal)
        {
            switch (myArray.count)
            {
                case 1:
                case 2:
                    myAddress=address;
                    break;
                default:
                    myAddress=[NSString stringWithFormat:@"%@,%@",myArray[0],myArray[1]];
                    break;
            }
        }
        else
        {
            switch (myArray.count)
            {
                case 1:
                    myAddress=address;
                case 2:
                    myAddress=myArray[0];
                    break;
                default:
                    myAddress=[NSString stringWithFormat:@"%@,%@",myArray[0],myArray[1]];
                    break;
            }
        }
    }
    return myAddress;
}
+(BOOL)isEmpty:(NSString *)str
{
    @try {
        if([str  isKindOfClass:NULL]||str.length==0 || [str isKindOfClass:[NSNull class]] || [str isEqualToString:@""]||[str isEqualToString:@"(null)"]||str==nil || [str isEqualToString:@"<null>"])
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *exception) {
        return YES;
    }
}
#pragma mark -
#pragma mark - Validation Utillity Functions
-(BOOL)isValidEmailAddress:(NSString *)email
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString:laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
-(void)setTimeZone:(NSString*)timeZone
{
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:timeZone]];
}
-(NSString*)getTimeZone
{
    return [[dateFormatter timeZone] name];
}
@end
