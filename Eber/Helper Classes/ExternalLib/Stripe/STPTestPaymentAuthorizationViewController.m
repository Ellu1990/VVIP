//
//  STPTestPaymentAuthorizationViewController.m
//  StripeExample
//
//  Created by Jack Flintermann on 9/8/14.
//  Copyright (c) 2014 Stripe. All rights reserved.
//

//#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000

#import "STPTestPaymentAuthorizationViewController.h"
#import "PKPayment+STPTestKeys.h"

@interface STPTestPaymentAuthorizationViewController()<UIActionSheetDelegate>
@property(weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

@implementation STPTestPaymentAuthorizationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator = activityIndicator;
    self.activityIndicator.center = self.view.center;
    self.activityIndicator.hidesWhenStopped = YES;
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
	
	UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Test Card Picker" message:@"Using the alert controller" preferredStyle:UIAlertControllerStyleActionSheet];
	
	[actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
		
		  [self.delegate paymentAuthorizationViewControllerDidFinish:(PKPaymentAuthorizationViewController *)self];
		[self dismissViewControllerAnimated:YES completion:^{
			
		}];
	}]];
	
	[actionSheet addAction:[UIAlertAction actionWithTitle:@"Select Test Working Card" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		   [self makePaymentWithCardNumber:STPSuccessfulChargeCardNumber];
			[self dismissViewControllerAnimated:YES completion:^{
		}];
	}]];
	
	[actionSheet addAction:[UIAlertAction actionWithTitle: @"Select Test Failing Card" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		
		// OK button tapped.
		
		[self dismissViewControllerAnimated:YES completion:^{
		}];
	}]];
	
	// Present action sheet.
	[self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)makePaymentWithCardNumber:(NSString *)cardNumber {
    [self.activityIndicator startAnimating];
    PKPayment *payment = [PKPayment new];
    payment.stp_testCardNumber = cardNumber;
    
    PKPaymentAuthorizationViewController *auth = (PKPaymentAuthorizationViewController *)self;
    
    [self.activityIndicator startAnimating];
    [self.delegate paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)auth
                                  didAuthorizePayment:payment
                                           completion:^(PKPaymentAuthorizationStatus status) {
                                               [self.activityIndicator stopAnimating];
                                               [self.delegate paymentAuthorizationViewControllerDidFinish:auth];
                                           }];
}


@end

//#endif
