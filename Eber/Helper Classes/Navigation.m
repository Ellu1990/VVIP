//  Created by Elluminati - macbook on 19/05/14.
//  Copyright (c) 2014 Elluminati MacBook Pro 1. All rights reserved.
//

#import "Navigation.h"

@interface Navigation ()
@end

@implementation Navigation

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    animPop=YES;
}

#pragma mark -
#pragma mark - Utility Methods


//set NavigationBar title from String;

-(void)setNavBarTitle:(NSString *)title
{
    UILabel *lbl=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 150, 30)];
    lbl.textAlignment=NSTextAlignmentCenter;
    lbl.backgroundColor=[UIColor clearColor];
    lbl.font=[UIFont systemFontOfSize:16.0];
    lbl.textColor=[UIColor whiteColor];
    lbl.text=title;
}

//Add back button to the Navigation
-(void)setBackBarItem
{
    self.navigationItem.hidesBackButton = YES;
       UIButton *btnLeft=[UIButton buttonWithType:UIButtonTypeCustom];
    btnLeft.frame=CGRectMake(0, 0, 25, 25);
    [btnLeft addTarget:self action:@selector(onClickBackBarItem:) forControlEvents:UIControlEventTouchUpInside];
    [btnLeft setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnLeft];
}

-(void)setBackBarItemPresent
{
    self.navigationItem.hidesBackButton = YES;
    //self.navigationItem.backBarButtonItem = nil;
    UIButton *btnLeft=[UIButton buttonWithType:UIButtonTypeCustom];
    btnLeft.frame=CGRectMake(0, 0, 25, 25);
       [btnLeft setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnLeft];
}

-(void)setBackBarItem:(BOOL)animated
{
    animPop=animated;
    [self setBackBarItem];
}


-(void)onClickBackBarItem:(id)sender
{
    [self.navigationController popViewControllerAnimated:animPop];
}

#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end


/*

 #pragma mark -
 #pragma mark - KeyBord Methods
 
 -(void) keyboardWillShow:(NSNotification *)note{
 // get keyboard size and loctaion
 CGRect keyboardBounds;
 [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
 NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
 NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
 
 // Need to translate the bounds to account for rotation.
 keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
 //for get keybord height
 //CGFloat kbHeight = [[note objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
 
 CGRect containerFrame = self.bottomView.frame;
 containerFrame.origin.y=self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
 [self.view bringSubviewToFront:self.bottomView];
 
 // animations settings
 [UIView beginAnimations:nil context:NULL];
 [UIView setAnimationBeginsFromCurrentState:YES];
 [UIView setAnimationDuration:[duration doubleValue]];
 [UIView setAnimationCurve:[curve intValue]];
 // set views with new info
 self.bottomView.frame=containerFrame;
 // commit animations
 [UIView commitAnimations];
 }
 
 -(void) keyboardWillHide:(NSNotification *)note{
 NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
 NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
 
 CGRect containerFrame = self.bottomView.frame;
 containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
 
 [UIView beginAnimations:nil context:NULL];
 [UIView setAnimationBeginsFromCurrentState:YES];
 [UIView setAnimationDuration:[duration doubleValue]];
 [UIView setAnimationCurve:[curve intValue]];
 
 // set views with new info
 self.bottomView.frame = containerFrame;
 // commit animations
 [UIView commitAnimations];
 }
 */

