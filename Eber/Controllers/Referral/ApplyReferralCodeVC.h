//
//  ApplyReferralCodeVC.h
//  UberforXOwner
//
//  Created by Deep Gami on 22/11/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DisplayCardVC.h"

@interface ApplyReferralCodeVC : UIViewController
<UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UITextField *txtCode;
- (IBAction)codeBtnPressed:(id)sender;
- (IBAction)ContinueBtnPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnContinue;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UILabel *lblMsg;
@property (weak, nonatomic) IBOutlet UIView *viewForReferralError;
@property (weak, nonatomic) IBOutlet UILabel *lblReferralErrorMsg;

@end
