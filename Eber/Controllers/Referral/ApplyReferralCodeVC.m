//
//  ApplyReferralCodeVC.m
//  UberforXOwner
//
//  Created by Deep Gami on 22/11/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "ApplyReferralCodeVC.h"
#import "NSObject+Constants.h"
#import "AppDelegate.h"
#import "UtilityClass.h"
#import "AFNHelper.h"
#import "UIColor+Colors.h"
#import "PreferenceHelper.h"

@interface ApplyReferralCodeVC ()
{
   BOOL isSkip;
    
}
@end

@implementation ApplyReferralCodeVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.hidesBackButton=YES;
    [self.navigationController setNavigationBarHidden:NO];
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor labelTitleColor],NSForegroundColorAttributeName,
                                    [UIColor labelTitleColor],NSBackgroundColorAttributeName,nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.title = [NSLocalizedString(@"TITLE_REFFERAL_CODE", nil) capitalizedString];
    
    [self SetLocalization];
     isSkip=YES;
}
-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    self.viewForReferralError.hidden=YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    IS_PARENT_IS_REGISTER=YES;
}
- (IBAction)codeBtnPressed:(id)sender
{
    isSkip=NO;
    [self referelService];
    }

- (IBAction)ContinueBtnPressed:(id)sender
{
    isSkip=YES;
    [self referelService];
    return;
}

-(void)referelService
{
    self.viewForReferralError.hidden=YES;
	NSString *strForUserId=PREF.userId;
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
    
	    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
	    [dictParam setObject:strForUserId forKey:PARAM_USER_ID];
	    [dictParam setObject:[NSNumber numberWithBool:isSkip ] forKey:PARAM_REFERRAL_SKIP];
        
	    if (!isSkip)
	    {
		    [dictParam setObject:self.txtCode.text forKey:PARAM_REFERRAL_CODE];
		}
	    else
	    {
		    [dictParam setObject:PREF.refferalCode forKey:PARAM_REFERRAL_CODE];
	    }
	    NSString *url=[NSString stringWithFormat:@"%@%@",USER_APPLY_REFERRAL,strForUserId];
		   
	    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:PUT];
		   [afn getDataFromPath:url withParamData:dictParam withBlock:^(id response, NSError *error)
		    {
			    if (response)
			    {
				    dispatch_async(dispatch_get_main_queue(), ^
							    {
								    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
								    NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
																					options:kNilOptions
																					  error:nil];
								    
								    if([[jsonResponse valueForKey:SUCCESS] boolValue])
								    {
									     //NSlog(@"%@",jsonResponse);
									    if(!isSkip)
									    {
                                            PREF.refferalCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_REFERRAL_CODE] ];
                                            NSString *messageCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:MESSAGE_CODE]];
                                            [[UtilityClass sharedObject] showToast:NSLocalizedString(messageCode, nil)];
										    [self performSegueWithIdentifier:SEGUE_TO_PAYMENT sender:self];
									    }
									    else
									    {
										    [self performSegueWithIdentifier:SEGUE_TO_PAYMENT sender:self];
										    
									    }

									    
								    }
								    else
								    {
									    NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
									    self.txtCode.text=@"";
									    self.viewForReferralError.hidden=NO;
									    self.lblReferralErrorMsg.text=NSLocalizedString(errorCode, nil);
									    self.lblReferralErrorMsg.textColor=[UIColor colorWithRed:205.0/255.0 green:0.0/255.0 blue:15.0/255.0 alpha:1];
                                       

								    }
							       [[AppDelegate sharedAppDelegate]hideLoadingView];
							    });
			    }
			    
		    }];
	    
    
    

}
#pragma mark-
#pragma mark- TextField Delegate
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtCode resignFirstResponder];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.viewForReferralError.hidden=YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self.view endEditing:YES];
	return YES;
}
-(void)SetLocalization
{
	[self.txtCode setPlaceholder:NSLocalizedString(@"Enter Referral Code", nil)];
    [_txtCode setTextColor:[UIColor textColor]];
    [self.lblMsg setText:NSLocalizedString(@"Referral_Msg", nil)];
	[self.btnContinue setTitle:[NSLocalizedString(@"SKIP", nil) uppercaseString] forState:UIControlStateNormal];
	[self.btnContinue setTitle:[NSLocalizedString(@"SKIP", nil) uppercaseString] forState:UIControlStateSelected];
	[self.btnSubmit setTitle:[NSLocalizedString(@"SUBMIT", nil) uppercaseString] forState:UIControlStateNormal];
	[self.btnSubmit setTitle:[NSLocalizedString(@"SUBMIT", nil) uppercaseString] forState:UIControlStateSelected];
    [self.btnContinue setBackgroundColor:[UIColor buttonColor]];
    [self.btnSubmit setBackgroundColor:[UIColor buttonColor]];
}
@end
