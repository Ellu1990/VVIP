//
//  ReferralVc.m
//  Eber Client
//  Created by My Mac on 6/28/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "ReferralVc.h"
#import "UIColor+Colors.h"
#import "NSObject+Constants.h"
#import "AppDelegate.h"
#import "UtilityClass.h"
#import "AFNHelper.h"
#import "PreferenceHelper.h"

@interface ReferralVc ()
{
    
}
@end

@implementation ReferralVc

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self SetLocalization];
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_REFFEREL_DETAIL", nil)];
    [self wsGetReferel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}
-(void)viewWillAppear:(BOOL)animated
{
    [_btnBack setTitle:[NSLocalizedString(@"TITLE_REFFERAL_CODE", nil) capitalizedString] forState:UIControlStateNormal];
    [_btnBack setTitleColor:[UIColor labelTitleColor] forState:UIControlStateNormal];
}
-(void)wsGetReferel
{
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:PREF.userId forKey:PARAM_USER_ID];
    [dictParam setObject:PREF.userToken forKey:PARAM_TOKEN];
   
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
        [afn getDataFromPath:WS_GET_REFERELL_CREDIT withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                                NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                                             options:kNilOptions
                                                                                               error:nil];
                                
                                if([[jsonResponse valueForKey:SUCCESS] boolValue])
                                {
                                    dispatch_async(dispatch_get_main_queue()
                                                   , ^{
                                                       if (![UtilityClass isEmpty:[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_REFERRAL_CREDIT] ]])
                                                       {
                                                           [_lblCredit setText:[NSString stringWithFormat:@"%@ %@",CurrencySign,[jsonResponse valueForKey:PARAM_REFERRAL_CREDIT]]];
                                                       }
                                                       if (PREF.refferalCode)
                                                       {
                                                           [_lblRefCode setText:[NSString stringWithFormat:@"%@",PREF.refferalCode]];
                                                           
                                                       }
                                                       else
                                                       {
                                                       [_lblRefCode setText:NSLocalizedString(@"REFFERAL_CODE_NOT_AVAILABLE", nil)];
                                                       }
                                                       [[AppDelegate sharedAppDelegate] hideLoadingView];
                                                       
                                                   });
                                }
                                else
                                {
                                    [[AppDelegate sharedAppDelegate] hideLoadingView];
                                    
                                }
                                
                                
                            });
         }];
    
   

}
- (IBAction)onClickBtnBack:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}
-(void)SetLocalization
{
    self.lblRefferal.text=NSLocalizedString(@"REFFERAL_CODE_TITLE", nil);
    self.lblRefferalCredit.text=NSLocalizedString(@"REFFERAL_CREDIT", nil);
    [self.btnShare setTitle:[NSLocalizedString(@"SHARE_REFFERAL_CODE", nil) uppercaseString] forState:UIControlStateNormal];
   [_btnShare setBackgroundColor:[UIColor buttonColor]];
    [_lblRefCode setTextColor:[UIColor textColor]];
    [_lblCredit setTextColor:[UIColor textColor]];
    [_lblCredit setText:[NSString stringWithFormat:@"%@ 0",CurrencySign]];
    [_lblRefCode setText:NSLocalizedString(@"REFFERAL_CODE_NOT_AVAILABLE", nil)];
    
    _viewForRefferal.layer.shadowOpacity=0.8;
    _viewForRefferal.layer.shadowOffset= CGSizeMake(0, 3.0f);
    _viewForRefferal.layer.shadowColor = [UIColor blackColor].CGColor;
    _viewForRefferal.layer.masksToBounds=NO;
    _viewForRefferal.clipsToBounds = NO;
    [self.view setBackgroundColor:[UIColor backGroundColor]];
    [_viewForRefferal setBackgroundColor:[UIColor whiteColor]];
}
- (IBAction)onClickBtnShare:(id)sender
{
    NSString *texttoshare = [NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"MY_REFFEREL_CODE_IS", Nil),_lblRefCode.text]; //this is your text string to share
    //UIImage *imagetoshare = @""; //this is your image to share
    NSArray *activityItems = @[texttoshare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
    [self presentViewController:activityVC animated:TRUE completion:nil];
}
-(void)shareMail
{
    if(_lblRefCode.text)
    {
        
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailer=[[MFMailComposeViewController alloc]init ];
            mailer.mailComposeDelegate=self;
            NSArray *toRecipients=[[NSArray alloc]initWithObjects:@"",nil];
            NSString *msg=[NSString stringWithFormat:@"Sign up for Uber For X with my referral code %@, and get the first ride worth $x free!",_lblRefCode.text];
            [mailer setSubject:@"SHARE REFERRAL CODE"];
            [mailer setMessageBody:msg isHTML:NO];
            [mailer setToRecipients:toRecipients];
            
            // NSData *dataObj = UIImageJPEGRepresentation(shareImage, 1);
            //[mailer addAttachmentData:dataObj mimeType:@"image/jpeg" fileName:@"iBusinessCard.jpg"];
            
            [mailer setDefinesPresentationContext:YES];
            [mailer setEditing:YES];
            [mailer setModalInPopover:YES];
            [mailer setNavigationBarHidden:NO animated:YES];
            
            [self presentViewController:mailer animated:YES completion:nil];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                            message:@"Your device doesn't support the composer sheet"
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_OK", nil)                                                  otherButtonTitles: nil];
            [alert show];
            
        }
    }
    else
        
    {
        NSLog(@"Referel not Share");
    }
    
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail send canceled");
            [self.tabBarController setSelectedIndex:0];
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSent:
            [self showAlert:@"Mail sent successfully." message:@"Success"];
            [self dismissViewControllerAnimated:YES completion:NULL];
            
            NSLog(@"Mail send successfully");
            break;
        case MFMailComposeResultSaved:
            [self showAlert:@"Mail saved to drafts successfully." message:@"Mail saved"];
            NSLog(@"Mail Saved");
            break;
        case MFMailComposeResultFailed:
            [self showAlert:[NSString stringWithFormat:@"Error:%@.", [error localizedDescription]] message:@"Failed to send mail"];
            NSLog(@"Mail send error : %@",[error localizedDescription]);
            break;
        default:
            break;
    }
}

- (void)showAlert:(NSString *)title message:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}


@end
