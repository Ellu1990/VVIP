//
//  ReferralVc.h
//  Eber Client
//  Created by My Mac on 6/28/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>

@interface ReferralVc : UIViewController
<MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UILabel *lblRefCode;
@property (weak, nonatomic) IBOutlet UILabel *lblCredit;
- (IBAction)onClickBtnShare:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewForRefferal;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lblRefferal;
@property (weak, nonatomic) IBOutlet UILabel *lblRefferalCredit;

@end
