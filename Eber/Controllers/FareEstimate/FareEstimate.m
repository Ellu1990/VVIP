//
//  FareEstimate.m
//  Eber Client
//  Created by My Mac on 7/23/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "FareEstimate.h"
#import "FareEstimate2.h"
#import "NSObject+Constants.h"
#import "UIColor+Colors.h"
#import "UtilityClass.h"
#import "UIImageView+image.h"
#import "CurrentTrip.h"
#import "UILabel+overrideLabel.h"
#import "AppDelegate.h"
@implementation FareEstimate

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    // if labelView is not set userInteractionEnabled, you must do so
    
 
}
-(void) setDataForEstimateView:(CarTypeDataModal *)car isSurge:(int)isSurge
{
    MapVc *vc;
    if ([self.parent isKindOfClass:[MapVc class]])
    {
        vc = (MapVc*) _parent;
       
    }
    [_imgcar downloadFromURL:car.picture withPlaceholder:[UIImage imageNamed:@"car"]];
    [_lblCarName setText:[NSString stringWithFormat:@"%@",car.name]];
    
    if ([car.base_price_distance isEqualToString:@"1.00"])
    {
      [_lblMinFare setText:[NSString stringWithFormat:@"%@ %@/%@",CurrencySign,car.base_price,DISTANCE_SUFFIX]] ;
    }
    else
    {
    [_lblMinFare setText:[NSString stringWithFormat:@"%@ %@/%@ %@",CurrencySign,car.base_price,car.base_price_distance,DISTANCE_SUFFIX]];
    }
    [_lblPerKm setText:[NSString stringWithFormat:@"%@ %@/%@",CurrencySign,car.price_per_unit_distance,DISTANCE_SUFFIX]];
    [_lblTimeCost setText:[NSString stringWithFormat:@"%@ %@/%@",CurrencySign,car.price_for_total_time,TIME_SUFFIX]];
    [_lblMaxSize setText:[NSString stringWithFormat:@"%@ Person",car.max_space]];
    [_lblCurrentLocation setText:[NSString stringWithFormat:@"%@",vc.txtPickupAddress.text]];
    [_lblDestinationLocation setText:[NSString stringWithFormat:@"%@",vc.txtDestinationAddress.text]];
    [_lblTaxValue setText:[NSString stringWithFormat:@"%@ %%",car.tax]];
    [_lblCacellationFeeValue setText:[NSString stringWithFormat:@"%@ %@",CurrencySign,car.cancellation_charge]];
    if ([car.tax isEqualToString:@"0.00"])
    {
        [_lblTax setHidden:YES];
        [_lblTaxValue setHidden:YES];
    }
    if ([car.cancellation_charge isEqualToString:@"0.00"] )
    {
        [_lblCancellationFee setHidden:YES];
        [_lblCacellationFeeValue setHidden:YES];
        _lblTax.frame=_lblCancellationFee.frame;
        _lblTaxValue.frame=_lblCacellationFeeValue.frame;
    }
    
    [_lblFare setText:[NSString stringWithFormat:@"%@ %@",CurrencySign,car.minFare]];
    [_lblEta setText:[NSString stringWithFormat:@"0 %@",TIME_SUFFIX]];
    [_lblDistanceValue setText:[NSString stringWithFormat:@"0.0 %@",DISTANCE_SUFFIX]];
    
    currentLatLong=vc.source_coordinate;
    if (![UtilityClass isEmpty:_lblDestinationLocation.text])
    {
        destinationLatLong=vc.dest_coordinate;
        [_lblEta setText:[NSString stringWithFormat:@"%@",[CurrentTrip sharedObject].estimateFareTime]];
        [_lblDistanceValue setText:[NSString stringWithFormat:@"%@ %@",[CurrentTrip sharedObject].estimateFareDistance,DISTANCE_SUFFIX]];
        [_lblFare setText:[NSString stringWithFormat:@"%@ %@",CurrencySign,[CurrentTrip sharedObject].estimateFareTotal]];
        gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickDestinationAddress:)];
        [_lblDestinationLocation setUserInteractionEnabled:YES];
        [_lblDestinationLocation addGestureRecognizer:gesture];
        [_btnGetEstimate setTitle:NSLocalizedString(@"GET_RIDE", nil) forState:UIControlStateNormal];
        [_lblCurrentLocation setFontForLabel:_lblCurrentLocation withMaximumFontSize:14 andMaximumLines:2];
        [_lblDestinationLocation setFontForLabel:_lblDestinationLocation withMaximumFontSize:14 andMaximumLines:2];
        CGFloat pointSize=MIN(_lblCurrentLocation.font.pointSize, _lblDestinationLocation.font.pointSize);
        UIFont *font=[UIFont fontWithName:_lblCurrentLocation.font.fontName size:pointSize];
        _lblCurrentLocation.font=_lblDestinationLocation.font=font;
        [_lblCurrentLocation sizeToFit];
        [_lblDestinationLocation sizeToFit];
        
    }
    else
    {
    }
    if (isSurge)
    {
        [_viewForMinFare setHidden:NO];
        [_lblMinFareView setText:_lblsFare.text];
        [_lblMinFareViewValue setText:_lblFare.text];
        [_lblSurgePriceValue setText:[NSString stringWithFormat:@"%@x",car.sugrgeMultiplier ]];
    }
    else
    {
        [_viewForMinFare setHidden:YES];
    }
        
    
}

#pragma mark-Actions
-(IBAction)onClickBtnGetEstimate:(id)sender
{
    if ([[_btnGetEstimate titleForState:UIControlStateNormal] isEqualToString:[NSLocalizedString(@"CONTINUE", nil) uppercaseString] ])
    {
        [self removeFromSuperview];
        if ([self.parent isKindOfClass:[MapVc class]])
        {
            MapVc *vc = (MapVc*) _parent;
            vc.dest_address=[_lblDestinationLocation text];
            vc.dest_coordinate=destinationLatLong;
            [vc.txtDestinationAddress setText:vc.dest_address];
            [vc.txtPickupAddress setEnabled:NO];
            [vc onClickBtnPickUpMe:nil];
            [vc focusMapToShowAllMarkers:vc.source_coordinate andDestinationLatLong:vc.dest_coordinate];
            
        }
        
    }
    else
    {
        if ([_parent isKindOfClass:[UIViewController class]])
        {
        FareEstimate2 *view= [FareEstimate2 getViewFareEstimatewithParent:_parent];
        view.delegate=self;
        view.srcLatLong=currentLatLong;
        [APPDELEGATE.window addSubview:view];
        }
    
    }
    
    
}
- (IBAction)onClickCloseView:(id)sender
{
    [self removeFromSuperview];
    
}

+(FareEstimate *)getViewFareEstimatewithParent:(id)parent
{
    
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"FareEstimate" owner:nil options:nil];
	FareEstimate *view = [nibContents lastObject];
	view.parent = parent;
    view.frame=APPDELEGATE.window.frame;
    [view setLocaliztion];
	if ([parent isKindOfClass:[MapVc class]])
    {
		MapVc *vc = (MapVc*) parent;
		view.viewFareEstimate.frame =CGRectMake(10, 5, vc.view.frame.size.width-20, view.frame.size.height-10);
        view.viewFareEstimate=[[UtilityClass sharedObject]addShadow:view.viewFareEstimate];
        view.viewFareEstimate.center=view.center;
        
    }
	return view;
}

-(void)dataBack:(FareEstimate2*)f2
{
    gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickDestinationAddress:)];
    [_lblDestinationLocation setUserInteractionEnabled:YES];
    [_lblDestinationLocation addGestureRecognizer:gesture];
    [_btnGetEstimate setTitle:[NSLocalizedString(@"CONTINUE", nil) uppercaseString] forState:UIControlStateNormal];
     [_lblEta setText:[NSString stringWithFormat:@"%@ Min",f2.strEta]];
    [_lblFare setText:[NSString stringWithFormat:@"%@ %@",CurrencySign,f2.strTotal]];
    [_lblDestinationLocation setText:[NSString stringWithFormat:@"%@",f2.strDaddress]];
    [_lblDistanceValue setText:[NSString stringWithFormat:@"%@",f2.strDistance]];
    destinationLatLong.latitude=[f2.strForDestLatitude doubleValue];
    destinationLatLong.longitude=[f2.strForDestLongitude doubleValue];
    [_lblMinFareView setText:_lblsFare.text];
    [_lblMinFareViewValue setText:_lblFare.text];
    
    
    
    [_lblCurrentLocation setFontForLabel:_lblCurrentLocation withMaximumFontSize:14 andMaximumLines:2];
    [_lblDestinationLocation setFontForLabel:_lblDestinationLocation withMaximumFontSize:14 andMaximumLines:2];
    CGFloat pointSize=MIN(_lblCurrentLocation.font.pointSize, _lblDestinationLocation.font.pointSize);
    UIFont *font=[UIFont fontWithName:_lblCurrentLocation.font.fontName size:pointSize];
    _lblCurrentLocation.font=_lblDestinationLocation.font=font;
    [_lblCurrentLocation sizeToFit];
    [_lblDestinationLocation sizeToFit];
    
    
    
}
-(void)setLocaliztion
{
    
    [_lblsEta setText:NSLocalizedString(@"ETA", nil)];
    [_lblsMinFare setText:NSLocalizedString(@"BASE_FARE", nil)];
    [_lblsPerKm setText:NSLocalizedString(@"DISTANCE_FARE", nil)];
    [_lblsTimeCost setText:NSLocalizedString(@"TIME_FARE", nil)];
    [_lblsMaxSize setText:NSLocalizedString(@"MAX_SIZE", nil)];
    [_lblsFrom setText:[NSLocalizedString(@"FROM", nil) capitalizedString]];
    [_lblsTo setText:[NSLocalizedString(@"TO", nil) capitalizedString]];
    [_lblsFare setText:NSLocalizedString(@"MIN_FARE", nil)];
    [_lblsEstimateMessage setText:NSLocalizedString(@"Estimate_Message", nil)];
    [_lblSurgePrice setText:NSLocalizedString(@"SURGE_COST", nil)];
    [_lblSurgePriceValue setText:@"0"];
    [_lblMinFareView setText:_lblsFare.text];
    [_lblMinFareViewValue setText:_lblFare.text];
    /*change Label Color*/
    [_viewFareEstimate setBackgroundColor:[UIColor backGroundColor]];
    [_viewForMinFare setBackgroundColor:[UIColor backGroundColor]];
    [_lblSurgePriceValue setTextColor:[UIColor textColor]];
    [_lblSurgePrice setTextColor:[UIColor textColor]];
    [_lblMinFareViewValue setTextColor:[UIColor textColor]];
    [_lblMinFareView setTextColor:[UIColor textColor]];
     [_lblTaxValue setTextColor:[UIColor textColor]];
    [_lblEta setTextColor:[UIColor textColor]];
    [_lblTimeCost setTextColor:[UIColor textColor]];
    [_lblMinFare setTextColor:[UIColor textColor]];
    [_lblMaxSize setTextColor:[UIColor textColor]];
    [_lblFare setTextColor:[UIColor textColor]];
    [_lblDistanceValue setTextColor:[UIColor textColor]];
    [_lblDestinationLocation setTextColor:[UIColor textColor]];
    [_lblCurrentLocation setTextColor:[UIColor textColor]];
    [_lblPerKm setTextColor:[UIColor textColor]];
    [_lblCarName setTextColor:[UIColor textColor]];
    [_lblCacellationFeeValue setTextColor:[UIColor textColor]];
    /*button setUp*/
    [_btnGetEstimate setTitle:[NSLocalizedString(@"SET_DESTINATION", nil) uppercaseString] forState:UIControlStateNormal];
    [_btnGetEstimate setBackgroundColor:[UIColor buttonColor]];
    [_btnGetEstimate setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
}
#pragma mark-Delocation Memory
-(void)dealloc
{
    _lblDestinationLocation=nil;_lblFare=nil;  _lblMaxSize=nil;     _lblTimeCost=nil;    _lblEta=nil;     _lblPerKm=nil;     _lblCarName=nil;_imgcar=nil;
}
- (void)onClickDestinationAddress:(UIGestureRecognizer*)gestureRecognizer;
{
    if ([_parent isKindOfClass:[UIViewController class]])
    {
        FareEstimate2 *view= [FareEstimate2 getViewFareEstimatewithParent:_parent];
        view.delegate=self;
        view.srcLatLong=currentLatLong;
        [APPDELEGATE.window addSubview:view];
    }
}
/**/
@end


