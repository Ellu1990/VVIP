//
//  FareEstimate2.m
//  Eber Client
//  Created by Elluminati Mini Mac 5 on 23/07/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "FareEstimate2.h"
#import "NSObject+Constants.h"
#import "AFNHelper.h"
#import <CoreLocation/CoreLocation.h>
#import "UtilityClass.h"
#import "AppDelegate.h"
#import "UIColor+Colors.h"
#import "PreferenceHelper.h"

@implementation FareEstimate2
{
    NSInteger index;
    
    
}
@synthesize arrForPredictedAddresses,dictPlace,Places,Location,flag,strForDestLongitude,strForDestLatitude,strUserId,strUserToken,strCarTypeId,delegate,strDaddress;

-(void)awakeFromNib
{
    [super awakeFromNib];
    _tblAutoComplete.hidden=YES;
    arrForPredictedAddresses=[[NSMutableArray alloc]init];
    Location=[[NSMutableArray alloc]init];
    Places=[[NSMutableArray alloc]init];
    [_fareEstimate2View setBackgroundColor:[UIColor backGroundColor]];
    [_lblHome setText:NSLocalizedString(@"HOME", nil) ];
    [_lblWork setText:NSLocalizedString(@"WORK", nil) ];
    [_lblHome setTextColor:[UIColor labelTextColor]];
    [_lblWork setTextColor:[UIColor labelTextColor]];
    /*Text setUp*/
    [_txtDestinationAddress setPlaceholder:NSLocalizedString(@"PH_SEARCH_DESTINATION_ADDRESS",nil) ];
    [self getPlaces];
    [_txtWork setPlaceholder:NSLocalizedString(@"PH_WORK_ADDRESS",nil)];
    [_txtHome setPlaceholder:NSLocalizedString(@"PH_HOME_ADDRESS",nil)];
    [_txtWork setText:[UtilityClass formatAddress:_strWorkAddress]];
    [_txtHome setText:[UtilityClass formatAddress:_strHomeAddress]];
    [_txtHome setTextColor:[UIColor textColor]];
    [_txtWork setTextColor:[UIColor textColor]];
    
    _strHomeAddress=PREF.homeAddress;
    _strWorkAddress=PREF.workAddress;
    _tblNearByPlaces.hidden=NO;
    [_tblNearByPlaces setBackgroundColor:[UIColor clearColor]];
}

+(FareEstimate2 *)getViewFareEstimatewithParent:(id)parent
{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"FareEstimate2" owner:nil options:nil];
    FareEstimate2 *view = [nibContents lastObject];
    view.frame=APPDELEGATE.window.frame;
    view.parent = parent;
      if ([parent isKindOfClass:[UIViewController class]])
      {
        UIViewController *vc = (UIViewController*) parent;
        view.fareEstimate2View.frame =CGRectMake(10, 5, vc.view.frame.size.width-20, view.frame.size.height-10);
        view.fareEstimate2View.center=view.center;
        view.fareEstimate2View=[[UtilityClass sharedObject]addShadow:view.fareEstimate2View];
      }
    return view;
}
- (IBAction)onClickBtnHome:(id)sender
{
    @try
    {
       [self getLocationFromAddressString:_strHomeAddress];
    }
    @catch(NSException *theException) {
    [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_LOCATION",nil)];
     }
    @finally
    {
        NSLog(@"Executing finally block");
    }
    
  }
- (IBAction)onClickBtnWork:(id)sender{
    
    @try
    {
       [self getLocationFromAddressString:_strWorkAddress];
    }
    @catch(NSException *theException)
    {
        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_LOCATION",nil)];
    }
    @finally
    {
        NSLog(@"Executing finally block");
    }

}
- (IBAction)onClickBtnCloseView:(id)sender
{
    [self removeFromSuperview];
}
#pragma mark-TableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==self.tblAutoComplete)
    {
        return arrForPredictedAddresses.count;
    }
    if (tableView==self.tblNearByPlaces)
    {
        return Places.count;
    }
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(cell == nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
     [cell setBackgroundColor:[UIColor clearColor]];
    if(tableView == self.tblAutoComplete)
    {
        if(arrForPredictedAddresses.count >0)
        {
           NSString *formatedAddress=[[arrForPredictedAddresses objectAtIndex:indexPath.row] valueForKey:GOOGLE_PARAM_AUTOCOMLETE_DESCRIPTION]; // AUTOCOMPLETE API
            
            cell.textLabel.text=formatedAddress;
            cell.textLabel.font=[UIFont fontWithName:@"Arial" size:15];
            cell.textLabel.textColor=[UIColor blackColor];
            cell.textLabel.alpha=0.7;
        }
    }
    if(tableView==self.tblNearByPlaces)
    {
        cell.textLabel.font=[UIFont fontWithName:@"Arial" size:13];
        cell.textLabel.text=[Places objectAtIndex:indexPath.row];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView == self.tblAutoComplete)
    {
        index=indexPath.row;
        self.tblAutoComplete.hidden=YES;
        dictPlace=[arrForPredictedAddresses objectAtIndex:index];
        NSString *strAddress=[dictPlace valueForKey:GOOGLE_PARAM_AUTOCOMLETE_DESCRIPTION];
        [self setNewPlaceData:strAddress];
        
    }
    else if (tableView==self.tblNearByPlaces)
    {
        [self.tblNearByPlaces deselectRowAtIndexPath:indexPath animated:YES];
        NSMutableDictionary *dict=[Location objectAtIndex:indexPath.row];
        strForDestLatitude=[dict valueForKey:GOOGLE_PARAM_AUTOCOMLETE_PLACE_LATITUDE];
        strForDestLongitude=[dict valueForKey:GOOGLE_PARAM_AUTOCOMLETE_PLACE_LONGITUDE];
         CLLocationCoordinate2D dcoor= CLLocationCoordinate2DMake([strForDestLatitude doubleValue], [strForDestLongitude doubleValue]);
        NSArray *TempArr=[self getTimeAndDistance:_srcLatLong destLocation:dcoor];
        [self getFareEstimate:TempArr];
        strDaddress=[Places objectAtIndex:indexPath.row];
     
    }
   
    
}

-(void)setNewPlaceData:(NSString *)address
{
    NSString *myAddress=[UtilityClass formatAddress:address];
    if (flag==0)
    {
        _strHomeAddress=address;
        PREF.HomeAddress=address;
        [self.txtHome setText:myAddress];
        [self textFieldShouldReturn:self.txtHome];
         
    }
    else if (flag==1)
    {
        _strWorkAddress=address;
        PREF.WorkAddress=address;
        [_txtWork setText:myAddress];
        [self textFieldShouldReturn:self.txtWork];
    }
    else if(flag==2)
    {
        [_txtDestinationAddress setText:myAddress];
        [self textFieldShouldReturn:self.txtDestinationAddress];
        @try
        {
            [self getLocationFromAddressString:address];
        }
        @catch(NSException *theException)
        {
         [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_LOCATION",nil)];
            [[AppDelegate sharedAppDelegate]hideLoadingView];
        }
        @finally
        {
            NSLog(@"Executing finally block");
        }

    }
}
#pragma mark-TextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)Searching:(id)sender
{
    [arrForPredictedAddresses removeAllObjects];
    self.tblAutoComplete.hidden=YES;
    NSString *str;
    if(flag==0)
    {
        str=self.txtHome.text;
    }
    else if (flag==1)
    {
        str=self.txtWork.text;
    }
    else if(flag==2)
    {
        str=self.txtDestinationAddress.text;
    }
    
    if(str.length > 2)
    {
        self.tblAutoComplete.hidden=YES;
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:str forKey:GOOGLE_PARAM_AUTOCOMLETE_INPUT]; // AUTOCOMPLETE API
        [dictParam setObject:GOOGLE_PARAM_AUTOCOMLETE_SENSOR forKey:@"false"]; // AUTOCOMPLETE API
        [dictParam setObject:GoogleServerKey forKey:GOOGLE_PARAM_AUTOCOMLETE_KEY];
        
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:@"GET"];
            [afn getAddressFromGooglewAutoCompletewithParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 
                 dispatch_async(dispatch_get_main_queue(), ^
                                {
                                    NSMutableArray *arrAddress=[response valueForKey:GOOGLE_PARAM_AUTOCOMLETE_PREDICTION];
                                    [arrForPredictedAddresses removeAllObjects];
                                    if ([arrAddress count] > 1)
                                    {
                                        [arrForPredictedAddresses addObjectsFromArray:arrAddress];
                                        [self.tblAutoComplete reloadData];
                                        self.tblAutoComplete.hidden=NO;
                                        
                                    }
                                    else
                                    {
                                        self.tblAutoComplete.hidden=YES;
                                    }
                                    
                                    
                                    
                                });
                 
             }];
            
        

        
    }
    
}



/*Get Near By Places And Set To The Table*/
-(void)getPlaces
{
    [Places removeAllObjects];
    [Location removeAllObjects];
     NSString *url=[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?sensor=true&key=%@&location=%f,%f&radius=5000",GoogleServerKey,[strForCurLatitude floatValue],[strForCurLongitude floatValue]];
    
    NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
    
    NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [str dataUsingEncoding:NSUTF8StringEncoding]options: NSJSONReadingMutableContainers error: nil];
    
    NSMutableArray *result = [JSON valueForKey:GOOGLE_PARAM_RESULTS];
    for (NSMutableDictionary *dict in result)
    {
        NSMutableDictionary *tempHolder=[dict valueForKey:GOOGLE_PARAM_GEOMETRY];
    [Location addObject:[tempHolder valueForKey:GOOGLE_PARAM_LOCATION]];
    [Places addObject:[dict valueForKey:GOOGLE_PARAM_AUTOCOMLETE_PLACE_NAME]];
    }
    
    [self.tblNearByPlaces reloadData];
}
-(NSArray*) getTimeAndDistance:(CLLocationCoordinate2D) src_location destLocation:(CLLocationCoordinate2D)dest_location

{
    NSArray *arrayForTimeAndDistance;
    NSString *src_lat=[NSString stringWithFormat:@"%f",src_location.latitude];
    NSString *src_long=[NSString stringWithFormat:@"%f",src_location.longitude];
    NSString *dest_lat=[NSString stringWithFormat:@"%f",dest_location.latitude];
    NSString *dest_long=[NSString stringWithFormat:@"%f",dest_location.longitude];
    
    
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%@,%@&destinations=%@,%@&key=%@",src_lat,  src_long, dest_lat, dest_long, GoogleServerKey];
    
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSData *jsonData = [NSData dataWithContentsOfURL:url];
    if(jsonData != nil)
    {
        
        NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
        NSString *status=[[[[[dict valueForKey:GOOGLE_PARAM_ROWS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_ELEMENTS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_STATUS];
        
        
        if ([status isEqualToString:GOOGLE_PARAM_STATUS_OK])
        {
            NSString *distance=[[[[[[dict valueForKey:GOOGLE_PARAM_ROWS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_ELEMENTS] objectAtIndex:0] objectForKey:GOOGLE_PARAM_DISTANCE] valueForKey:GOOGLE_PARAM_VALUES];
            NSString *second=[[[[[[dict valueForKey:GOOGLE_PARAM_ROWS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_ELEMENTS] objectAtIndex:0] objectForKey:GOOGLE_PARAM_DURATION] valueForKey:GOOGLE_PARAM_VALUES];
            NSString *time=[[UtilityClass sharedObject]secondToTime:[second intValue]];
            arrayForTimeAndDistance=[[NSArray alloc]initWithObjects:distance, time,second, nil];
        }
        else
        {
            
        }
       
    }
    else
    {
        NSLog(@"API NOT CALLED");
    }
    return arrayForTimeAndDistance;
    
}
-(void)getFareEstimate:(NSArray *)time
{
    MapVc *p=nil;
    if ([_parent isKindOfClass:[MapVc class]])
    {
        p = (MapVc*) _parent;
        NSDictionary *dict=[p getData];
        strUserToken=[dict valueForKey:@"strUserToken"];
        strUserId=[dict valueForKey:@"strUserId"];
        strCarTypeId=[dict valueForKey:@"strCarTypeId"];
     }

        
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOADING_FAREESTIMATE", nil)];
        //NSlog(@"dict for request :%@",dictParam);
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:[time objectAtIndex:0] forKey:PARAM_DISTANCE];
        [dictParam setObject:[time objectAtIndex:2] forKey:PARAM_TIME];
        [dictParam setObject:strUserId forKey:PARAM_USER_ID];
        [dictParam setObject:strUserToken forKey:PARAM_TOKEN];
        [dictParam setObject:strCarTypeId forKey:PARAM_SERVICE_TYPE];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
        [afn getDataFromPath:USER_GET_FARE_ESTIMATE withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                                NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                                             options:kNilOptions
                                                                                              error:nil];
                               
                                if([[jsonResponse valueForKey:SUCCESS] boolValue])
                                {
                                    
                                  _strTotal=[NSString stringWithFormat:@"%.2f",[[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_ESTIMATE_FARE]] doubleValue]];
                                  _strEta=[NSString stringWithFormat:@"%.2f",[[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_PRICE_PER_UNIT_TIME]]doubleValue]];
                                  _strDistance=[NSString stringWithFormat:@"%.2f",[[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_DISTANCE]]doubleValue]];
                                  [delegate dataBack:self];
                                    [self removeFromSuperview];
                                    
                                }
                                else
                                {
                                    NSString *strErrorCode=[jsonResponse valueForKey:ERROR_CODE];
                                    [[UtilityClass sharedObject]showToast:NSLocalizedString(strErrorCode, nil)];
                                    
                                }
                                [[AppDelegate sharedAppDelegate]hideLoadingView];
                            });
             
         }];
    
}
-(void) callWebService
{
    CLLocationCoordinate2D dcoor= CLLocationCoordinate2DMake([strForDestLatitude doubleValue], [strForDestLongitude doubleValue]);
    NSArray *TempArr=[self getTimeAndDistance:_srcLatLong destLocation:dcoor];
    [self getFareEstimate:TempArr];
    [self.txtHome resignFirstResponder];
}
#pragma mark
#pragma mark - UITextfield Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField==self.txtHome)
    {
        flag=0;
        _tblAutoComplete.frame=CGRectMake(
                                          _txtHome.frame.origin.x,
                                          _txtHome.frame.origin.y+38.0f,
                                          _txtHome.frame.size.width,
                                          _tblAutoComplete.frame.size.height);
        [self bringSubviewToFront:_tblAutoComplete];
        
    }
    else if (textField==self.txtWork)
    {
        flag=1;
        _tblAutoComplete.frame=CGRectMake(
        _txtWork.frame.origin.x,
        _txtWork.frame.origin.y+38.0f,
        _txtWork.frame.size.width,
        _tblAutoComplete.frame.size.height);
        [self bringSubviewToFront:_tblAutoComplete];
       
    }
    else if(self.txtDestinationAddress==textField)
    {
        flag=2;
        _tblAutoComplete.frame=CGRectMake(
        _txtHome.frame.origin.x,
        _txtDestinationAddress.frame.origin.y+38.0f,
        _txtHome.frame.size.width,
        _tblAutoComplete.frame.size.height);
        [self bringSubviewToFront:_tblAutoComplete];
    }
    else
    {
        _tblAutoComplete.hidden=YES;
    }
}
/*Get LatLong From Address*/
-(void) getLocationFromAddressString: (NSString*) addressStr
{
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result)
    {
        
        NSData *data = [result dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        NSDictionary *dict=[[NSDictionary alloc]init];
        if([[jsonResponse valueForKey:GOOGLE_PARAM_STATUS]isEqualToString:GOOGLE_PARAM_STATUS_OK])
        {
            dict=[[[[jsonResponse valueForKey:GOOGLE_PARAM_RESULTS]objectAtIndex:0] objectForKey:GOOGLE_PARAM_GEOMETRY]objectForKey:GOOGLE_PARAM_LOCATION];
            latitude=[[dict valueForKey:GOOGLE_PARAM_AUTOCOMLETE_PLACE_LATITUDE]doubleValue ];
            longitude=[[dict valueForKey:GOOGLE_PARAM_AUTOCOMLETE_PLACE_LONGITUDE]doubleValue ];
            strForDestLatitude =[NSString stringWithFormat:@"%f",latitude ];
            strForDestLongitude =[NSString stringWithFormat:@"%f",longitude ];
            strDaddress=addressStr;
            [self callWebService];
        }
        else
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_LOCATION",nil)];
        }
        
    }
    else
    {
        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_LOCATION",nil)];
    }
    
}

@end
