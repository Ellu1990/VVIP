//
//  FareEstimate.h
//  Eber Client
//  Created by My Mac on 7/23/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapVc.h"
#import "FareEstimate2.h"
#import "CarTypeDataModal.h"

@protocol FareEstimate2Delegate;
@interface FareEstimate : UIView <FareEstimate2Delegate>
{
    CLLocationCoordinate2D currentLatLong,destinationLatLong;
    UITapGestureRecognizer* gesture;
    NSString *strSrcLocation,*strDestLocation;
}

/*Variable OutLets*/
@property (weak, nonatomic) IBOutlet UIImageView *imgcar;
@property (weak, nonatomic) IBOutlet UILabel *lblCarName;
@property (weak, nonatomic) IBOutlet UILabel *lblMinFare;
@property (weak, nonatomic) IBOutlet UILabel *lblPerKm;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeCost;
@property (weak, nonatomic) IBOutlet UILabel *lblMaxSize;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblDestinationLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblEta;
@property (weak, nonatomic) IBOutlet UILabel *lblFare;
@property (weak, nonatomic) IBOutlet UILabel *lblCancellationFee;
@property (weak, nonatomic) IBOutlet UILabel *lblCacellationFeeValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTax;
@property (weak, nonatomic) IBOutlet UILabel *lblTaxValue;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceValue;
#pragma mark-Actions
@property (weak, nonatomic) IBOutlet UIButton *btnGetEstimate;
- (IBAction)onClickBtnGetEstimate:(id)sender;
@property(strong,nonatomic)id parent;
/*Methods*/
+(FareEstimate *)getViewFareEstimatewithParent:(id)parent;
//- (void) setDataForEstimateView:(NSDictionary*)dict;

- (void) setDataForEstimateView:(CarTypeDataModal *)car isSurge:(int )isSurge;
@property (weak, nonatomic) IBOutlet UIView *viewFareEstimate;

/*Static outlets*/
@property (weak, nonatomic) IBOutlet UILabel *lblsTo;
@property (weak, nonatomic) IBOutlet UILabel *lblsEstimateMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblsFare;
@property (weak, nonatomic) IBOutlet UILabel *lblsEta;
@property (weak, nonatomic) IBOutlet UILabel *lblsFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblsMaxSize;
@property (weak, nonatomic) IBOutlet UILabel *lblsTimeCost;
@property (weak, nonatomic) IBOutlet UILabel *lblsMinFare;
@property (weak, nonatomic) IBOutlet UIView *viewForMinFare;
@property (weak, nonatomic) IBOutlet UILabel *lblsPerKm;


@property (weak, nonatomic) IBOutlet UILabel *lblSurgePriceValue;
@property (weak, nonatomic) IBOutlet UILabel *lblSurgePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblMinFareView;
@property (weak, nonatomic) IBOutlet UILabel *lblMinFareViewValue;

@end
