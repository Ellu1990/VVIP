//
//  FareEstimate2.h
//  Eber Client
//  Created by Elluminati Mini Mac 5 on 23/07/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapVc.h"
@protocol FareEstimate2Delegate;

@interface FareEstimate2 : UIView
<UITableViewDataSource,
UITableViewDataSource>
{
    id<FareEstimate2Delegate>_delegate;
}
@property (nonatomic, assign) id<FareEstimate2Delegate> delegate;
@property(strong,nonatomic)id parent;

/*OutLets*/
@property (weak, nonatomic) IBOutlet UIView *fareEstimate2View;
- (IBAction)onClickBtnHome:(id)sender;
- (IBAction)onClickBtnCloseView:(id)sender;
- (IBAction)onClickBtnWork:(id)sender;
@property (assign,nonatomic)CLLocationCoordinate2D srcLatLong;
@property (weak, nonatomic) IBOutlet UITextField *txtDestinationAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtWork;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@property (weak, nonatomic) IBOutlet UITextField *txtHome;
@property (weak, nonatomic) IBOutlet UILabel *lblHome;
@property (weak, nonatomic) IBOutlet UIButton *btnWork;
/*Table For AutoComplete In Searching*/
@property (weak, nonatomic) IBOutlet UILabel *lblWork;
@property (weak, nonatomic) IBOutlet UITableView *tblAutoComplete;
@property (weak, nonatomic) IBOutlet UITableView *tblNearByPlaces;
/*Prperties*/
@property(copy,nonatomic) 	NSMutableArray *arrForPredictedAddresses,*Places,*Location;
@property(copy,nonatomic) NSDictionary *dictPlace;
@property(assign,nonatomic)int flag;
@property(copy,nonatomic) NSString
*strForDestLatitude,
*strForDestLongitude,
*strUserId,
*strUserToken,
*strCarTypeId,
*strDaddress,
*strTotal,
*strEta,
*strHomeAddress,
*strWorkAddress,
*strDistance,
*strDistanceUnit;

/*Static Methods*/
+(FareEstimate2 *)getViewFareEstimatewithParent:(id)parent;
@end

@protocol FareEstimate2Delegate
-(void)dataBack:(FareEstimate2*)f2;
@end
