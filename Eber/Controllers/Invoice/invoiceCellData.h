//
//  invoiceCellData.h
//  Eber Client
//
//  Created by Elluminati Mini Mac 5 on 20/09/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface invoiceCellData : NSObject
@property (nonatomic,copy)NSString  *cost_title;
@property (copy, nonatomic)NSString *cost_value;
-(id)initWithCostTitle:(NSString*)costTitle andCostValue:(NSString*)costValue;
@end
