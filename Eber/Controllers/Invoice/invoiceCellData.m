//
//  invoiceCellData.m
//  Eber client
//
//  Created by Elluminati Mini Mac 5 on 20/09/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "invoiceCellData.h"

@implementation invoiceCellData
@synthesize cost_title,cost_value;
-(id)initWithCostTitle:(NSString*)costTitle andCostValue:(NSString*)costValue
{
    cost_title=costTitle;
    cost_value=costValue;
    return self;
}
@end
