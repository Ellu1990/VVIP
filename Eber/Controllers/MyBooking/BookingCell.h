//
//  HistoryCell.h
//  UberforX Provider
//
//  Created by My Mac on 11/15/14.
//  Copyright (c) 2014 Deep Gami. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Trip.h"

@interface BookingCell : UITableViewCell
-(void)setDataForCell:(Trip*)t;
@property (weak, nonatomic) IBOutlet UIView *viewForCell;
@property (weak, nonatomic) IBOutlet UIImageView *img1;
@property (weak, nonatomic) IBOutlet UIImageView *img2;
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;
@property (weak, nonatomic) IBOutlet UIButton *btnRemove;

@end
