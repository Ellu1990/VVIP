//
//  HistoryVc.h
//  Eber Client
//  Created by My Mac on 7/2/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCancelTripDialog.h"

@interface MyBookingVc : UIViewController
<UITableViewDelegate,
UITableViewDataSource,
CustomCancelTripDialogDelegate>
/*View For Header*/
@property (weak, nonatomic) IBOutlet UIView *viewForOngoingHeader;
@property (weak, nonatomic) IBOutlet UIView *viewForUpcomingHeader;
@property (weak, nonatomic) IBOutlet UILabel *lblOngoingTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblUpcompingTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgSection;
/*view For Ongoing Trip*/
@property (weak, nonatomic) IBOutlet UIView *viewForOngoingTrip;
@property (weak, nonatomic) IBOutlet UILabel *lblTripStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblSrcAddress;
@property (weak, nonatomic) IBOutlet UITableView *tblForBooking;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lblNoItemToDisplay;

- (IBAction)onClickBtnBack:(id)sender;


@end
