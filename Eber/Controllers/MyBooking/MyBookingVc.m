//
//  HistoryVc.m
//  Eber Client
//  Created by My Mac on 7/2/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "MyBookingVc.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "UtilityClass.h"
#import "NSObject+Constants.h"
#import "Trip.h"
#import "BookingCell.h"
#import "UIColor+Colors.h"
#import "UILabel+overrideLabel.h"
#import "Parser.h"
#import "PreferenceHelper.h"
#import "CurrentTrip.h"

@interface MyBookingVc ()
{
    NSMutableArray *arrForBooking;
    NSString *strForUserId,*tripID;
    NSInteger cancelTripIndex;
}
@end
@implementation MyBookingVc
#pragma mark-View LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    arrForBooking=[[NSMutableArray alloc]init];
    strForUserId=PREF.userId ;
    [self setLocalization];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setLocalization
{
    [self.view setBackgroundColor:[UIColor backGroundColor]];
    [_lblNoItemToDisplay setText:NSLocalizedString(@"NO_ITEM_TO_DISPLAY", nil) withColor:[UIColor textColor]];
    [_lblNoItemToDisplay setContentMode:UIViewContentModeCenter];
    [_lblOngoingTitle setText:NSLocalizedString(@"ON_GOING_TRIP", nil) withColor:[UIColor labelTitleColor]];
    [_lblUpcompingTitle setText:NSLocalizedString(@"UP_COMING_TRIP", nil) withColor:[UIColor labelTitleColor]];
    
    /*view setUp*/
    [_viewForUpcomingHeader setBackgroundColor:[UIColor GreenColor]];
    [_viewForOngoingHeader setBackgroundColor:[UIColor GreenColor]];
    [_viewForOngoingTrip setBackgroundColor:[UIColor whiteColor]];
    _viewForOngoingTrip=[self addShadow:_viewForOngoingTrip];
    _viewForUpcomingHeader.layer.cornerRadius=5.0;
    _viewForUpcomingHeader.layer.masksToBounds=YES;
    _viewForOngoingHeader.layer.cornerRadius=5.0;
    _viewForOngoingHeader.layer.masksToBounds=YES;
     if ([[CurrentTrip sharedObject].isProviderAccepted isEqualToString:@"1"])
    {
        [_viewForOngoingHeader setHidden:NO];
        [_viewForOngoingTrip setHidden:NO];
        [self.view bringSubviewToFront:_viewForOngoingTrip];
        [_lblSrcAddress setText:[CurrentTrip sharedObject].srcAddress];
        NSInteger providerStatus=[[CurrentTrip sharedObject].providerStatus integerValue];
        switch (providerStatus)
        {
                
            case ProviderStatusComing:
                [_lblTripStatus setText:NSLocalizedString(@"TEXT_DRIVER_COMING", nil)];
                break;
            case ProviderStatusArrived:
                [_lblTripStatus setText:NSLocalizedString(@"TEXT_DRIVER_ARRIVED", nil)];
                
                break;
            case ProviderStatusTripStarted:
                    self.lblTripStatus.text=NSLocalizedString(@"TEXT_DRIVER_TRIP_STARTED", nil);
                break;
            default:
                _viewForUpcomingHeader.frame=_viewForOngoingHeader.frame;
                [_viewForOngoingHeader setHidden:YES];
                [_lblOngoingTitle setHidden:YES];
                _tblForBooking.frame=_viewForOngoingTrip.frame;
                [_viewForOngoingTrip setHidden:YES];

                
        }
}
    else
    {
        _viewForUpcomingHeader.frame=_viewForOngoingHeader.frame;
        [_viewForOngoingHeader setHidden:YES];
        [_lblOngoingTitle setHidden:YES];
        _tblForBooking.frame=_viewForOngoingTrip.frame;
        [_viewForOngoingTrip setHidden:YES];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING_BOOKING", nil)];
    [self performSelectorInBackground:@selector(wsFutureTrips) withObject:nil];
    [_btnBack setTitle:[NSLocalizedString(@"TITLE_BOOKING", nil)capitalizedString ] forState:UIControlStateNormal];
    [_btnBack setTitleColor:[UIColor labelTitleColor] forState:UIControlStateNormal];
}
#pragma mark-WebServiceCalls
#pragma mark-Get History
-(IBAction)cancelBookedTrip:(UIButton*)sender
{
    cancelTripIndex = sender.tag;
    Trip *trip=[arrForBooking objectAtIndex:cancelTripIndex];
    tripID=trip.tripId;
    CustomCancelTripDialog *viewForAlert=[[CustomCancelTripDialog alloc]initInView:self];
    [self.view bringSubviewToFront:viewForAlert];
}
-(void)wsFutureTrips
{
	
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
		NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
		[dictParam setObject:strForUserId forKey:PARAM_USER_ID];
		
    
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
		[afn getDataFromPath:USER_GET_FUTURE_TRIP withParamData:dictParam withBlock:^(id response, NSError *error)
		 {
            
                 
                 [arrForBooking removeAllObjects];
                 if ([[Parser sharedObject]parseBooking:response toArray:&arrForBooking])
                 { dispatch_async(dispatch_get_main_queue(), ^
                                  {
                                      [APPDELEGATE hideLoadingView];
                                      [self.tblForBooking reloadData];
                                      [self adjustHeightOfTableview];});
                 }
                 else
                 {
                     dispatch_async(dispatch_get_main_queue(), ^
                                    {
                                        [APPDELEGATE hideLoadingView];
                     _lblNoItemToDisplay.hidden=NO;
                     _tblForBooking.hidden=YES;
                     [self.view bringSubviewToFront:_lblNoItemToDisplay];
                     });
                 }
		}];
	}

#pragma mark-TABLE VIEW METHODS
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return   arrForBooking.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"bookingcell";
    BookingCell *cell = [self.tblForBooking dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if (cell==nil)
    {
        cell=[[BookingCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    Trip *t=(Trip*)[arrForBooking objectAtIndex:[indexPath row]];
    [cell setDataForCell:t];
    cell.contentView.backgroundColor=[UIColor backGroundColor];
    CGRect frame=cell.contentView.frame;
    frame.origin.x+=2;
    frame.origin.y+=2;
    frame.size.height-=4;
    frame.size.width-=4;
    cell.viewForCell.frame=frame;
    [cell.viewForCell setBackgroundColor:[UIColor whiteColor]];
    [cell.btnRemove setTag:indexPath.row];
    [cell.btnRemove addTarget:self action:@selector(cancelBookedTrip:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70.0f;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}
#pragma mark-User Define Method
- (void)adjustHeightOfTableview
{
    CGFloat height = self.tblForBooking.contentSize.height;
    CGFloat maxHeight = self.tblForBooking.superview.frame.size.height - self.tblForBooking.frame.origin.y;
    
    // if the height of the content is greater than the maxHeight of
    // total space on the screen, limit the height to the size of the
    // superview.
    
    if (height > maxHeight)
        height = maxHeight;
    
    // now set the frame accordingly
    
    [UIView animateWithDuration:0.25 animations:^{
        CGRect frame = self.tblForBooking.frame;
        frame.size.height = height;
        self.tblForBooking.frame = frame;
        
        // if you have other controls that should be resized/moved to accommodate
        // the resized tableview, do that here, too
    }];
	
	if (arrForBooking.count)
    {
        [_tblForBooking setHidden:NO];
        [_lblNoItemToDisplay setHidden:YES];
    }else
    {
        [_tblForBooking setHidden:YES];
        [_lblNoItemToDisplay setHidden:NO];
        [self.view bringSubviewToFront:_lblNoItemToDisplay];
    }
    
}
-(UIView*)addShadow:(UIView*)view
{
    view.layer.shadowOpacity=0.4;
    view.layer.shadowOffset= CGSizeMake(0, 2.0f);
    view.layer.shadowColor = [UIColor textColor].CGColor;
    view.layer.masksToBounds=NO;
    view.clipsToBounds = NO;
    return view;
}
-(void)onClickCancelTripDialogWithReason:(NSString *)strCancelTripReason
{
    [self wsCancelFutureTrip:strCancelTripReason];
}
-(void)wsCancelFutureTrip:(NSString*)cancelReason
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:strForUserId forKey:PARAM_USER_ID];
    [dictParam setObject:tripID forKey:PARAM_SCHEDULE_TRIP_ID];
    [dictParam setObject:cancelReason forKey:PARAM_CANCEL_TRIP_REASON];
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        NSString *strUrl=[NSString stringWithFormat:@"%@%@",WS_CANCEL_FUTURE_TRIP,tripID];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:PUT];
        [afn getDataFromPath:strUrl withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                NSDictionary *dictionary;
                                if ([[Parser sharedObject]stringToJson:response To:&dictionary])
                                {
                                    if ([[dictionary valueForKey:SUCCESS]boolValue])
                                    {
                                        [arrForBooking removeObjectAtIndex:cancelTripIndex];
                                        [_tblForBooking reloadData];
                                        [self adjustHeightOfTableview];
                                        
                                    }
                                    else
                                    {
                                        NSString *errorCode=[NSString stringWithFormat:@"%@",[dictionary valueForKey:ERROR_CODE]];
                                        [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                                    }
                                }
                               [APPDELEGATE hideLoadingView];
                                
                            });
			}];
}

- (IBAction)onClickBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
