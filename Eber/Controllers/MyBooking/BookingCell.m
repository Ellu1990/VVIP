//
//  HistoryCell.m
//  UberforX Provider
//
//  Created by My Mac on 11/15/14.
//  Copyright (c) 2014 Deep Gami. All rights reserved.
//

#import "BookingCell.h"
#import "Trip.h"
#import "UIColor+Colors.h"
#import "NSObject+Constants.h"
#import "UILabel+overrideLabel.h"
#import "UtilityClass.h"
@implementation BookingCell
@synthesize imageView;

- (void)awakeFromNib
{
    [super awakeFromNib];
    _viewForCell=[[UtilityClass sharedObject] addShadow:_viewForCell];
    [_viewForCell setBackgroundColor:[UIColor whiteColor]];
    [_lbl1 setTextColor:[UIColor textColor]];
    [_lbl2 setTextColor:[UIColor textColor]];
}
-(void)setDataForCell:(Trip *)t
{
    NSString *strDate=[[UtilityClass sharedObject]DateToString:t.tripCreateDate withFormate:@"dd MMMM yyyy"];
    NSString *strDateToSet=[NSString stringWithFormat:@"%@,%@",strDate,t.tripTime];
    [_lbl1 setText:strDateToSet];
    [_lbl1 setFontForLabel:_lbl1 withMaximumFontSize:14 andMaximumLines:1];
    [_lbl2 setText:[NSString stringWithFormat:@"%@",t.srcAddress ]];
    [_lbl2 setFontForLabel:_lbl2 withMaximumFontSize:14 andMaximumLines:1];
    
}
@end

