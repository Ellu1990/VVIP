//
//  SplashVC.h
//  Eber Client
//  Created by Elluminati Mini Mac 5 on 10/10/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PreferenceHelper.h"
#import "NSObject+Constants.h"


@interface SplashVC : UIViewController

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;

@end
