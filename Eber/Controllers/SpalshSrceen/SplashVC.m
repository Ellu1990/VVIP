//
//  SplashVC.m
//  Eber Client
//  Created by Elluminati Mini Mac 5 on 10/10/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "SplashVC.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "Parser.h"
#import "CurrentTrip.h"
#import "UtilityClass.h"
#import "Hotline.h"

@interface SplashVC ()
{
    
}
@end

@implementation SplashVC

- (void)viewDidLoad
{
    [super viewDidLoad];
     [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appplicationIsActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}
- (void)appplicationIsActive:(NSNotification *)notification {
    [self doWebServiceCall];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


-(void)doWebServiceCall
{
  [self wsGetApiKeys];
}
-(void)wsAppSetting
{
    
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:@"NOT_REQUIRED" forKey:@"NOT_REQUIRED"];
        [afn getDataFromPath:WS_GET_SETTING_DETAILS withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if ([[Parser sharedObject]parseVerificationTypes:response])
             {
                 dispatch_async(dispatch_get_main_queue()
                                , ^{
                                    [self checkLogin];
                                          });
                 
 
             }
         }];
}
-(void)checkLogin
{
    if(PREF.isUserLogin)
    {
        strUserId=PREF.userId;
        strUserToken=PREF.userToken;
        if ([UtilityClass isEmpty:strUserId] ||[UtilityClass isEmpty:strUserToken])
        {
            
        }
        else
        {
            [self wsCheckInCompleteRequest];
        }
    }
    else
    {
        [_loadingIndicator stopAnimating];
        [APPDELEGATE goToMain];
        return;
    }
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
}
-(void) wsGetApiKeys
{
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:@"NOT_REQUIRED" forKey:@"NOT_REQUIRED"];
        [afn getDataFromPath:WS_GET_APP_KEYS withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if ([[Parser sharedObject]parseApiKeys:response])
             {
                 HotlineConfig *config = [[HotlineConfig alloc]initWithAppID:HotlineAppId  andAppKey:HotlineAppKey];
                 [[Hotline sharedInstance] initWithConfig:config];
                 
                 [self wsAppSetting];
             }
         }];
        
    
}
-(void)wsCheckInCompleteRequest
{
    [[CurrentTrip sharedObject] resetObject];
          NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        
        [dictParam setObject:strUserId forKey:PARAM_USER_ID];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@%@/%@",USER_GET_TRIP_STATUS,strUserId,strUserToken];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
        [afn getDataFromPath:strForUrl withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if ([[Parser sharedObject] parseTripStatus:response])
             {
                 CurrentTrip *currentTrip=[CurrentTrip sharedObject];
                 NSInteger isProviderAccepted=0;
                 isProviderAccepted=[currentTrip.isProviderAccepted  integerValue] ;
                 if (isProviderAccepted==1)
                 {
                     IS_TRIP_EXSIST=YES;
                     IS_TRIP_ACCEPTED=YES;
                 }
                 else
                 {
                     IS_TRIP_EXSIST=YES;
                     IS_TRIP_ACCEPTED=NO;
                 }
                 
                 if (![UtilityClass isEmpty:currentTrip.providerId])
                 {
                     [self getProviderDetail:currentTrip.providerId];
                 }
                 else
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [[AppDelegate sharedAppDelegate] hideLoadingView];
                         [APPDELEGATE goToMap];
                         return;
                         
                     });
                 }
             }
             else
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[AppDelegate sharedAppDelegate] hideLoadingView];
                    [APPDELEGATE goToMap];
                     self.navigationController.navigationBarHidden=NO;
                     return;
                     
                 });
             }
         }];
}
-(void) getProviderDetail:(NSString *) strProviderId
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:strProviderId forKey:PARAM_PROVIDER_ID];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
        [afn getDataFromPath:WS_GET_PROVIDER_DETAIL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if ([[Parser sharedObject] parseProvider:response])
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[AppDelegate sharedAppDelegate] hideLoadingView];
                     self.navigationController.navigationBarHidden=NO;
                     [APPDELEGATE goToMap];
                     return;
                 });
             }
         }];
}
@end
