//
//  SettingsVC.m
//    Eber ClientProvider
//
//  Created by Elluminati Mini Mac 5 on 08/11/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "SettingsVC.h"
#import "SWRevealViewController.h"
#import "UIColor+Colors.h"
#import "PreferenceHelper.h"
#import "NSObject+Constants.h"

@interface SettingsVC ()

@end

@implementation SettingsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setLocalization];
    [_swtchTripStatusSound setOn:!PREF.isSoundOff];
    [_swtchDriverArrivedSound setOn:!PREF.isDriverArrivedSoundOff];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onClickBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onClickBtnSwitchTripStatusSound:(id)sender
{
    UISwitch *switchObject = (UISwitch *)sender;
    if(switchObject.isOn)
    {
        PREF.SoundOff=NO;
        [_imgTripStatusSound setImage:[UIImage imageNamed:@"menuSoundOn"]];
    }
    else
    {
        PREF.SoundOff=YES;
        [_imgTripStatusSound setImage:[UIImage imageNamed:@"menuSoundOff"]];
        
    }
}

- (IBAction)onClickBtnSwitchDriverArrivedSound:(id)sender
{
    UISwitch *switchObject = (UISwitch *)sender;
    if(switchObject.isOn)
    {
        PREF.DriverArrivedSoundOff=NO;
        [_imgDriverArrivedSound setImage:[UIImage imageNamed:@"menuSoundOn"]];
    }
    else
    {
        PREF.DriverArrivedSoundOff=YES;
        [_imgDriverArrivedSound setImage:[UIImage imageNamed:@"menuSoundOff"]];
        
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)setLocalization
{
    [self.view setBackgroundColor:[UIColor backGroundColor]];
    [_btnNavigation setTitle:NSLocalizedString(@"SETTINGS", nil) forState:UIControlStateNormal];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [_btnNavigation setTitleColor:[UIColor labelTitleColor] forState:UIControlStateNormal];
    
    [_viewForTripStatusSound setBackgroundColor:[UIColor backGroundColor]];
    [_viewForDriverArrivedSound setBackgroundColor:[UIColor backGroundColor]];
    [_lblTripStatusTitle setText:NSLocalizedString(@"SETTING_ITEM_1_TITLE", nil)];
    [_lblTripStatusDescription setText:NSLocalizedString(@"SETTING_ITEM_1_DESCRIPTION", nil)];
    [_lblTripStatusTitle setTextColor:[UIColor textColor]];
    [_lblTripStatusDescription setTextColor:[UIColor labelTextColor]];
    [_lblAppVersion setText:[NSLocalizedString(@"APP_VERSION", nil) capitalizedString]];
    [_lblAppVersion setTextColor:[UIColor textColor]];
    [_lblAppVesionValue setTextColor:[UIColor textColor]];
    [_lblAppVesionValue setText: [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]]];
    
    [_lblDriverArrivedTitle setText:NSLocalizedString(@"SETTING_ITEM_2_TITLE", nil)];
    [_lblDriverArrivedDescription setText:NSLocalizedString(@"SETTING_ITEM_2_DESCRIPTION", nil)];
    [_lblDriverArrivedTitle setTextColor:[UIColor textColor]];
    [_lblDriverArrivedDescription setTextColor:[UIColor labelTextColor]];
}

@end
