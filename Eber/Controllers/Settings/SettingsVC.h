//
//  SettingsVC.h
//    Eber ClientProvider
//
//  Created by Elluminati Mini Mac 5 on 08/11/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnNavigation;
/*viewForTripStatusSound*/
@property (weak, nonatomic) IBOutlet UIView *viewForTripStatusSound;
@property (weak, nonatomic) IBOutlet UISwitch *swtchTripStatusSound;
@property (weak, nonatomic) IBOutlet UIImageView *imgTripStatusSound;
@property (weak, nonatomic) IBOutlet UILabel *lblTripStatusTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTripStatusDescription;

/*viewForDriverArrivedSound*/
@property (weak, nonatomic) IBOutlet UIView *viewForDriverArrivedSound;
@property (weak, nonatomic) IBOutlet UISwitch *swtchDriverArrivedSound;
@property (weak, nonatomic) IBOutlet UIImageView *imgDriverArrivedSound;
@property (weak, nonatomic) IBOutlet UILabel *lblDriverArrivedTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDriverArrivedDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblAppVersion;
@property (weak, nonatomic) IBOutlet UILabel *lblAppVesionValue;

@end
