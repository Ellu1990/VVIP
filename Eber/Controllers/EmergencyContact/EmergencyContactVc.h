//
//  HistoryVc.h
//  Eber Client
//  Created by My Mac on 7/2/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
@import Contacts;
@import ContactsUI;

@interface EmergenctContactVc : UIViewController<ABPeoplePickerNavigationControllerDelegate,UINavigationControllerDelegate,CNContactPickerDelegate>


@property (strong, nonatomic) IBOutlet UITableView *tblContacts;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lblAdd5Contact;
- (IBAction)onClickBtnAddContact:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnAddContact;
@end
