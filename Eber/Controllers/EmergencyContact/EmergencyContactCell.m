//
//  HistoryCell.m
//  UberforX Provider
//
//  Created by My Mac on 11/15/14.
//  Copyright (c) 2014 Deep Gami. All rights reserved.
//

#import "EmergencyContactCell.h"
#import "Contact.h"
#import "UIColor+Colors.h"
#import "NSObject+Constants.h"
#import "UtilityClass.h"
@implementation EmergencyContactCell
@synthesize imageView;

- (void)awakeFromNib
{
    [super awakeFromNib];
    _viewForCell=[self addShadow:_viewForCell];
    [_viewForCell setBackgroundColor:[UIColor whiteColor]];
    [_lblName setTextColor:[UIColor textColor]];
    [_lblPhoneNumber setTextColor:[UIColor textColor]];
    [_lblShareDetail setTextColor:[UIColor textColor]];
    
}
-(void)setDataForCell:(Contact *)contact
{
    _lblName.text= [NSString stringWithFormat:@"%@",contact.contactName ];
    _lblPhoneNumber.text=[NSString stringWithFormat:@"%@",contact.contactNumber ];
    _lblShareDetail.text=NSLocalizedString(@"ALWAYS_SHARE_RIDE_DETAIL", nil);
    [_isSwitchOn setOn:contact.isAlwaysShareRideDetail];
}
-(UIView*)addShadow:(UIView*)view
{
    view.layer.shadowOpacity=0.4;
    view.layer.shadowOffset= CGSizeMake(0, 2.0f);
    view.layer.shadowColor = [UIColor textColor].CGColor;
    view.layer.masksToBounds=NO;
    view.clipsToBounds = NO;
    return view;
    
}
@end

