//
//  HistoryCell.h
//  UberforX Provider
//
//  Created by My Mac on 11/15/14.
//  Copyright (c) 2014 Deep Gami. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact.h"

@interface EmergencyContactCell : UITableViewCell
-(void)setDataForCell:(Contact*)contact;
@property (weak, nonatomic) IBOutlet UIView *viewForCell;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblShareDetail;
@property (weak, nonatomic) IBOutlet UISwitch *isSwitchOn;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;

@end
