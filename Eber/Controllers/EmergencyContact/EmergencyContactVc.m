//
//  HistoryVc.m
//  Eber Client
//  Created by My Mac on 7/2/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "EmergencyContactVc.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "UtilityClass.h"
#import "NSObject+Constants.h"
#import "Contact.h"
#import "EmergencyContactCell.h"
#import "UIColor+Colors.h"
#import "Parser.h"
#import "PreferenceHelper.h"

@interface EmergenctContactVc ()
{
    NSMutableArray *arrForContacts;
    NSString *strForUserId,*contactId;
    
}
@end
@implementation EmergenctContactVc
#pragma mark-View LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    arrForContacts=[[NSMutableArray alloc]init];
    strForUserId=PREF.userId;
    [self setLocalization];
    [_btnAddContact setBackgroundColor:[UIColor buttonColor]];
    [self getAllContacts];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)setLocalization
{
    [self.view setBackgroundColor:[UIColor backGroundColor]];
    
    [_lblAdd5Contact setText:NSLocalizedString(@"ADD_UP_TO_FIVE_CONTACTS", nil) ];
    [_lblAdd5Contact setTextColor:[UIColor textColor]];
    
    [_btnAddContact setTitle:[NSLocalizedString(@"ADD_CONTACT",nil) uppercaseString] forState:UIControlStateNormal];
    [_btnAddContact setBackgroundColor:[UIColor buttonColor]];
    [_btnAddContact setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [_btnBack setTitle:[NSLocalizedString(@"TITLE_EMERGENCY_CONTACTS", nil) capitalizedString] forState:UIControlStateNormal];
    [_btnBack setTitleColor:[UIColor labelTitleColor] forState:UIControlStateNormal];
    [self.navigationController setNavigationBarHidden:NO];
}
#pragma mark-WebServiceCalls
#pragma mark-Get Contacts
-(void)getAllContacts
{
		NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
		[dictParam setObject:strForUserId forKey:PARAM_USER_ID];
		[[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING_GETTING_CONTACT",nil)];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
		[afn getDataFromPath:WS_GET_EMERGENCY_CONTACT_LIST withParamData:dictParam withBlock:^(id response, NSError *error)
		 {
             dispatch_async(dispatch_get_main_queue(), ^
			 {
                 [arrForContacts removeAllObjects];
                 if ([[Parser sharedObject]parseEmergencyContactList:response toArray:&arrForContacts])
                 {
                     NSInteger contactCount = arrForContacts.count;
                     if (contactCount==5)
                     {
                         [_btnAddContact setEnabled:YES];
                     }
                     [self.tblContacts reloadData];
                     [self adjustHeightOfTableview];
                     [_tblContacts setHidden:NO];
                     [self.view bringSubviewToFront:_tblContacts];
                 }
                 else
                 {
                     _tblContacts.hidden=YES;
                 }
                 [APPDELEGATE hideLoadingView];
            });
		}];
}
-(IBAction)deleteContact:(UIButton*)sender
{
    [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING_DELETING_CONTACT", nil)];
    
    Contact *contact=[arrForContacts objectAtIndex:sender.tag];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:DELETE];
    NSString *url=[NSString stringWithFormat:@"%@%@",WS_DELETE_EMERGENCY_CONTACT,contact.contactId];
    NSMutableDictionary *contactDetail=[[NSMutableDictionary alloc]init];
    [contactDetail setObject:strForUserId forKey:PARAM_USER_ID];
    [afn getDataFromPath:url withParamData:contactDetail withBlock:^(id response, NSError *error)
     {
         if ([[Parser sharedObject]parseSuccess:response])
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [arrForContacts removeObjectAtIndex:sender.tag];
                                          [_tblContacts reloadData];
                     [self adjustHeightOfTableview];
          
                 });
         }
         dispatch_async(dispatch_get_main_queue(), ^{
         
                    [[AppDelegate sharedAppDelegate] hideLoadingView];
         });
         
     }];
    
    
}
-(void)updateContact:(UISwitch*)sender
{
    [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING_UPDATING_CONTACT", nil)];
    UISwitch *switchObject = (UISwitch *)sender;
    Contact *contact=[arrForContacts objectAtIndex:sender.tag];
    NSString *url=[NSString stringWithFormat:@"%@%@",WS_UPDATE_EMERGENCY_CONTACT,contact.contactId];
    NSMutableDictionary *contactDetail=[[NSMutableDictionary alloc]init];
    [contactDetail setObject:contact.contactId forKey:PARAM_EMERGENCY_CONTACT_ID];
    [contactDetail setObject:contact.contactName forKey:PARAM_EMERGENCY_CONTACT_NAME];
    [contactDetail setObject:contact.contactNumber forKey:PARAM_PHONE];
    if(switchObject.isOn)
    {
        contact.isAccessibilityElement=YES;
        [contactDetail setObject:[NSNumber numberWithBool:YES] forKey:PARAM_IS_ALWAYS_SHARE_DETAIL];
    }
    else
    {
        contact.isAccessibilityElement=NO;
        [contactDetail setObject:[NSNumber numberWithBool:NO] forKey:PARAM_IS_ALWAYS_SHARE_DETAIL];
    }
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:PUT];
    [afn getDataFromPath:url withParamData:contactDetail withBlock:^(id response, NSError *error)
     {
         if ([[Parser sharedObject]parseSuccess:response])
         {
             [arrForContacts replaceObjectAtIndex:sender.tag withObject:contact];
             dispatch_async(dispatch_get_main_queue(), ^{
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
             });
         }
         dispatch_async(dispatch_get_main_queue(), ^{
             [[AppDelegate sharedAppDelegate] hideLoadingView];
         });
    }];
    
    
    
}
-(void)addContact:(NSMutableDictionary*)contactDetail
{
    [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING_ADDING_CONTACT", nil)];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
    [afn getDataFromPath:WS_ADD_EMERGENCY_CONTACT withParamData:contactDetail withBlock:^(id response, NSError *error)
     {
        dispatch_async(dispatch_get_main_queue(), ^{
             if ([[Parser sharedObject]parseAddContact:response toArray:&arrForContacts])
             {       [_tblContacts reloadData];
                     [self adjustHeightOfTableview];
                        [[AppDelegate sharedAppDelegate] hideLoadingView];
             }
             else
             {
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
             }
                 
              });
         
         
     }];
    
    
    
}
#pragma mark-TABLE VIEW METHODS
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return   [arrForContacts count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"contactcell";
    EmergencyContactCell *cell = [self.tblContacts dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if (cell==nil)
    {
        cell=[[EmergencyContactCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    Contact  *contact=[arrForContacts objectAtIndex:indexPath.row];
    [cell.btnDelete setTag:indexPath.row];
    [cell.isSwitchOn setTag:indexPath.row];
    [cell setDataForCell:contact];
    [cell.btnDelete addTarget:self action:@selector(deleteContact:) forControlEvents:UIControlEventTouchUpInside];
    [cell.isSwitchOn addTarget:self action:@selector(updateContact:) forControlEvents:UIControlEventValueChanged];
    cell.contentView.backgroundColor=[UIColor backGroundColor];
    CGRect frame=cell.contentView.frame;
    frame.origin.x+=10;
    frame.origin.y+=2;
    frame.size.height-=4;
    frame.size.width-=20;
    cell.viewForCell.frame=frame;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100.0f;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}
#pragma  mark-Actions
#pragma mark-User Define Method
- (void)adjustHeightOfTableview
{
    CGFloat height = self.tblContacts.contentSize.height;
    CGFloat maxHeight = (self.tblContacts.superview.frame.size.height-(_btnAddContact.frame.size.height+_lblAdd5Contact.frame.size.height+20)) - self.tblContacts.frame.origin.y;
    
   
    if (height > maxHeight)
        height = maxHeight;
    
    
    [UIView animateWithDuration:0.25 animations:^{
        CGRect frame = self.tblContacts.frame;
        frame.size.height = height;
        self.tblContacts.frame = frame;
        
        // if you have other controls that should be resized/moved to accommodate
        // the resized tableview, do that here, too
    }];
    [_tblContacts setHidden:NO];
}
- (IBAction)onClickBtnAddContact:(id)sender
{
    
    if (NSClassFromString(@"CNContactPickerViewController"))
    {
        // iOS 9, 10, use CNContactPickerViewController
        CNContactPickerViewController *contactPicker = [CNContactPickerViewController new];
        contactPicker.delegate = self;
        [self presentViewController:contactPicker animated:YES completion:nil];
    }
    else
    {
        // iOS 8 Below, use ABPeoplePickerNavigationController
        ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
        picker.peoplePickerDelegate = self;
        picker.predicateForEnablingPerson = [NSPredicate predicateWithFormat:@"%K.@count > 0", ABPersonPhoneNumbersProperty];
        [self.navigationController presentViewController:picker animated:YES completion:nil];
  }
    
}
-(void) peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier{
    @try
    {
        ABMutableMultiValueRef phoneno  = ABRecordCopyValue(person, kABPersonPhoneProperty);
        NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
        NSString* phone = (__bridge NSString *)(ABMultiValueCopyValueAtIndex(phoneno, identifier));
        NSMutableDictionary *contactDetail;
        NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"-() "];
        phone = [[phone componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
        contactDetail=[[NSMutableDictionary alloc]init];
        [contactDetail setObject:strForUserId forKey:PARAM_USER_ID];
        [contactDetail setObject:firstName  forKey:PARAM_EMERGENCY_CONTACT_NAME];
        [contactDetail setObject:phone  forKey:PARAM_PHONE];
        [contactDetail setObject:@"1"  forKey:PARAM_IS_ALWAYS_SHARE_DETAIL];
        [self addContact:contactDetail];
        

    } @catch (NSException *exception)
    {
        [self dismissViewControllerAnimated:NO completion:^(){}];
        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_INVALID_CONTACT_DETAIL", nil)];
    }
}
- (void) contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact
{
    
    @try
    {
        NSString * phone = @"";
        NSString * userPHONE_NO = @"";
        
        for(CNLabeledValue * phonelabel in contact.phoneNumbers) {
            
            CNPhoneNumber * phoneNo = phonelabel.value;
            
            phone = [phoneNo stringValue];
            
            if (phone) {
                
                userPHONE_NO = phone;
                
            }}
        

        
        NSString *firstName = contact.givenName;
        NSMutableDictionary *contactDetail;
        NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"-() "];
        userPHONE_NO = [[userPHONE_NO componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
        if([UtilityClass isEmpty:userPHONE_NO] || [UtilityClass isEmpty:firstName])
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_INVALID_CONTACT_DETAIL", nil)];
        }
        else
        {
        contactDetail=[[NSMutableDictionary alloc]init];
        [contactDetail setObject:strForUserId forKey:PARAM_USER_ID];
        [contactDetail setObject:firstName  forKey:PARAM_EMERGENCY_CONTACT_NAME];
        [contactDetail setObject:userPHONE_NO  forKey:PARAM_PHONE];
        [contactDetail setObject:@"1"  forKey:PARAM_IS_ALWAYS_SHARE_DETAIL];
        [self addContact:contactDetail];
        }
        
    }
    @catch (NSException *exception)
    {
        [self dismissViewControllerAnimated:NO completion:^(){}];
        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_INVALID_CONTACT_DETAIL", nil)];
    }

    
    
}
- (IBAction)onClickBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
