//
//  FavouriteDriverCell.h
//  Eber
//
//  Created by Elluminati on 25/02/17.
//  Copyright © 2017 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavouriteDriverCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgDriver;
@property (weak, nonatomic) IBOutlet UILabel *lblDriverName;
@property (weak, nonatomic) IBOutlet UIButton *btnDeleteDriver;

@end
