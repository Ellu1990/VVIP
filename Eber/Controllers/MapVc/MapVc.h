//
//  MapVc.h
//  Eber Client
//  Created by My Mac on 6/27/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "MyCarouselView.h"
#import "CarouselView.h"
#import "CurrentTrip.h"
#import "CustomDatePicker.h"
#import "CustomAlertWithTitle.h"

@import CoreLocation;
typedef enum
{
    Payment_cash = 1,
    Payment_card = 0,
} PAYMENT_TYPE;

@interface MapVc : UIViewController
<CLLocationManagerDelegate,
GMSMapViewDelegate,
MyCarouselDataSource,
MyCarouselDelegate,
CustomDatePickerDelegate,
CustomAlertDelegate,
UITableViewDelegate,
UITableViewDataSource,
UIGestureRecognizerDelegate>

{
    UIButton *btnBack,*btnGetFareEstimate;
    	CLLocationManager *locationManager;
	/// This FLAG is used to knows which text field is currently in working.
	int flag;
    BOOL strPaymentMode;
	/// This String is used to knows weather Payment is done by CASH=1 or CARD=0.
	@private NSString
	///This String is Used to know Current City of Client.
	*strCity,
	///This String is Used to know Current Country of Client.
	*strCountry,
    //This String is Used to know Which Type Of Car is Selected By Client.
	*strCarTypeId,
    *strSubAdminArea,
	///This String is Used to know Current Trip Id .
	*strTripId,
	///This String is Used to know User Id .
	*strUserId,
	///This String is Used to know Token Id .
	*strUserToken,
	///This String is Used to Currrent Latlong on Map .
	*strPickUpLat,
	*strPickUpLong,
    ///This String is Used to Destination Latlong on Map .
    *strForDestLongitude,
    *strForDestLatitude,
    ///This String is Used to Reduce GetAll Application Type Web Service.
    *strTempCity,
    ///This String is Used to Indicate Complete PickUp Address.
    *source_address,
    *final_dest_address,
	*strServiceTypeID;


    
    ///This Integer is Used to Indicate index of Car-type Selected.
    NSUInteger strSelectedCarIndex;
    ///This Array is Used to Hold All Car type come from web.
    NSMutableArray *arrForCarType;
	///This Dictionary is Used to Hold Address from AutoComplete.
    NSDictionary *dictPlace;
    NSInteger numberOfProvider;
    
	GMSMarker *clientmarker,*destMarker;
	CLLocationCoordinate2D
    current_coordinate,finalDestCoordinate;
}
/*!Used To Get Get Predicted Autocomplete Suggestion for Addresses*/
@property(copy,nonatomic) 	NSMutableArray *arrForPredictedAddresses;
@property(copy,nonatomic) NSString *dest_address;
@property (weak, nonatomic) IBOutlet UISwitch *switchCompanyTrip;


/*General Views*/
@property (strong, nonatomic) IBOutlet UIView *viewForFavDriver;
@property (weak, nonatomic) IBOutlet UILabel *lblHavntFavDriver;
@property (weak, nonatomic) IBOutlet UITableView *tableFavDriver;
@property (strong,nonatomic) NSTimer *timerForCheckReqStatus;
@property (assign,nonatomic) CLLocationCoordinate2D source_coordinate;
@property (assign,nonatomic) CLLocationCoordinate2D dest_coordinate;
@property (strong, nonatomic) IBOutlet UITableView *tableForCity;
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
@property (strong, nonatomic) IBOutlet MyCarouselView *carousel;
@property (strong, nonatomic) IBOutlet UIView *viewForiCarousel;
@property (strong, nonatomic) IBOutlet UIButton *btnNavigation;
@property (weak, nonatomic) IBOutlet UILabel *lblmin;
#pragma mark-TextField
@property (strong, nonatomic) IBOutlet UITextField *txtDestinationAddress;
@property (strong, nonatomic) IBOutlet UITextField *txtPickupAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtTypeNote;
@property (weak, nonatomic) IBOutlet UILabel *lblEstTime;
#pragma mark-Buttons
@property (strong, nonatomic) IBOutlet UIButton *btnMylocation;
@property (strong, nonatomic) IBOutlet UIButton *btnPickUpMe;
@property (strong, nonatomic) IBOutlet UIButton *btnRideNow;
@property (strong, nonatomic) IBOutlet UIButton *btnRideLater;

- (void)focusMapToShowAllMarkers:(CLLocationCoordinate2D)sourceLatLong andDestinationLatLong:(CLLocationCoordinate2D)destLatlong;


#pragma mark-Actions

- (IBAction)onClickBtnPickUpMe:(id)sender;
- (IBAction)onClickBtnMyLocation:(id)sender;
- (IBAction)onClickBtnRideLater:(id)sender;
- (IBAction)onClickBtnRideNow:(id)sender;
- (IBAction)onClickBtnCancel:(id)sender;
- (IBAction)onClickFavouriteBack:(id)sender;

/**ALERT VIEW FOR DRIVER INFO*/
@property (weak, nonatomic) IBOutlet UIView *viewForDriver;
@property (weak, nonatomic) IBOutlet UILabel *lblProviderName;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelRequest;
@property (weak, nonatomic) IBOutlet UILabel *lblConnectingDriver;
@property (weak, nonatomic) IBOutlet UIImageView *imgDriverPro;
@property (weak, nonatomic) IBOutlet UIButton *btnContinue;
@property (weak, nonatomic) IBOutlet UIView *viewForDestinationAddress;
@property (weak, nonatomic) IBOutlet UIView *viewForpickupbg;
@property (weak, nonatomic) IBOutlet UIView *viewForPayment;
@property (weak, nonatomic) IBOutlet UIView *viewForTypeNote;
@property (weak, nonatomic) IBOutlet UIButton *imgPayment;
@property (weak, nonatomic) IBOutlet UILabel *lblPayment;
@property (weak, nonatomic) IBOutlet UIView *viewForPickAddress;
@property (weak, nonatomic) IBOutlet UIView *viewForDestinationbg;
@property (weak, nonatomic) IBOutlet UIButton *btnDestinationDrag;
- (IBAction)onClickBtnDestinationDrag:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *driverView;

/*iCarousel*/
-(NSDictionary*)getData;
@property (weak, nonatomic) IBOutlet UIView *viewForPickUp;
-(void)checkForRequestStatus;
/*pickUpView*/
@property (weak, nonatomic) IBOutlet UIImageView *imgPopUpForPickUp;
@property (weak, nonatomic) IBOutlet UIImageView *imgDestinationPin;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirmDestination;
- (IBAction)onClickBtnConfirmDestination:(id)sender;
-(void)setNewPlaceData:(NSString*)address;
/*View for Descrtion */
@property (weak, nonatomic) IBOutlet UIView *dialogForDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblTypeNameDialog;
@property (weak, nonatomic) IBOutlet UIImageView *imgTypeImageDialog;
@property (weak, nonatomic) IBOutlet UILabel *lblTypeDescriptionDialog;
@property (weak, nonatomic) IBOutlet UILabel *lblTypeDescriptionValueDialog;
@property (weak, nonatomic) IBOutlet UIView *viewForTypeDialog;
-(void)checkPush;
- (IBAction)onClickContinueRequest:(id)sender;

@end
