//
//  MapVc.m
//  Eber Client  Created by My Mac on 6/27/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//
#import "MapVc.h"

#import "SWRevealViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "AFNHelper.h"
#import "NSObject+Constants.h"
#import "UIColor+Colors.h"
#import "UtilityClass.h"
#import "AppDelegate.h"
#import "CarTypeDataModal.h"
#import "CurrentTrip.h"
#import "TripVc.h"
#import "FareEstimate.h"
#import "UIButton+CustomButtonProperty.h"
#import "Parser.h"
#import "CustomDatePicker.h"
#import "UIImageView+image.h"
#import "PreferenceHelper.h"
#import "AppDelegate.h"
#import "UILabel+overrideLabel.h"
#import "PaymentGateway.h"
#import "FavouriteDriverCell.h"
#import "FavouriteDriver.h"

@interface MapVc ()
{
CustomAlertWithTitle *dialogForRideLater,
    *gpsEnableDialog,
    *dialogForRequestAgain,
    *viewForAdminAlert,
    *dialogForSurgePrice,
    *dialogForSurgePriceFutureTrip;
    BOOL isDestinationDragFlagOn,isSurgeHour,isPickupClicked;
UIView *viewForCard;
NSMutableArray *arrForDriverMarkers;
    NSMutableArray *userDefaultLanguages;
FareEstimate *viewEstimate;
UITapGestureRecognizer *paymentGesture,
                       *paymentGestureCard,
                       *paymentGestureCash;
    NSString *surgeMultiplier,*surgeStartTime,*surgeEndTime,*previousProviderId,*startDateAndTimeForFutureTrip;
	NSArray *arrFavProvider;
	NSMutableArray *arrForAllFavProvider;
	int isRideNow;
   
}
@end
@implementation MapVc
{
    CLLocation *pickUp_location;
  
}
@synthesize arrForPredictedAddresses,btnMylocation,source_coordinate,dest_coordinate,dest_address,timerForCheckReqStatus;
#pragma mark-Life Cycle
- (void)viewDidLoad
{
	[super viewDidLoad];
	[self resetVariable];

	arrForAllFavProvider = [[NSMutableArray alloc]init];
    [self customSetup];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appplicationIsActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    previousProviderId=@"abc";
    isPickupClicked = NO;
    IS_PARENT_IS_REGISTER=FALSE;
    if (IS_TRIP_EXSIST && IS_TRIP_ACCEPTED)
    {
        if (self.navigationController.visibleViewController == self)
        {
            [self performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:self];
            return;
        }
    }
    else if(!IS_TRIP_ACCEPTED && IS_TRIP_EXSIST)
    {
        _txtPickupAddress.enabled=NO;
        [self onCreateNewTrip];
    }
    else
    {
        pickUp_location=[[CLLocation alloc]init];
        clientmarker = [[GMSMarker alloc] init];
        clientmarker.icon=[UIImage imageNamed:@"pin_pickup"];
        [self setLocalization];
        _mapView.delegate=self;
        _mapView.settings.allowScrollGesturesDuringRotateOrZoom = NO;
        _mapView.settings.rotateGestures = NO;
        self.mapView.settings.myLocationButton=NO;
        self.mapView.myLocationEnabled=NO;
        btnMylocation.hidden=NO;
        locationManager = [[CLLocationManager alloc] init];
        [self updateLocationManager];
    }
	
	UITapGestureRecognizer *singleFingerTap =
	[[UITapGestureRecognizer alloc] initWithTarget:self
											action:@selector(handleSingleTap:)];
	//[self.viewForFavDriver addGestureRecognizer:singleFingerTap];
	
	self.tableFavDriver.tableFooterView = [UIView new];
	
	//_switchCompanyTrip.tintColor = [UIColor grayColor];
	_switchCompanyTrip.layer.cornerRadius = 16;
	_switchCompanyTrip.backgroundColor =  [UIColor grayColor];

}
-(void)appplicationIsActive:(NSNotification *)notification
{
    if (PREF.UserApproved)
    {
        
    }
    else
    {
        if (!viewForAdminAlert)
        {
            viewForAdminAlert=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"ALERET_ADMIN_REVIEW", nil) message:NSLocalizedString(@"ALERET_ADMIN_REVIEW_MSG", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"LOGOUT", nil) otherButtonTitles:NSLocalizedString(@"ADMIN_EMAIL",nil)];
        }
    }
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [_imgDriverPro setRoundImageWithColor:[UIColor borderColor]];
    [_imgPayment setFrame:CGRectMake(_imgPayment.frame.origin.x, _imgPayment.frame.origin.y,MIN(_imgPayment.frame.size.height, _imgPayment.frame.size.width), MIN(_imgPayment.frame.size.height, _imgPayment.frame.size.width))];
    [_lblPayment setFrame:CGRectMake(_imgPayment.frame.origin.x+_imgPayment.frame.size.width+2, _lblPayment.frame.origin.y,_lblPayment.frame.size.width,_lblPayment.frame.size.height)];
    
    [self adjustFrameProperty:_imgPayment.imageView andLabel:_lblPayment inView:_viewForPayment];;
    
    /*view setUp*/
    _driverView=[[UtilityClass sharedObject]addShadow:_driverView];
    [_viewForTypeDialog setCenter:self.view.center];
    [_dialogForDescription setBackgroundColor:[UIColor clearColor]];
    [_viewForTypeDialog setBackgroundColor:[UIColor whiteColor]];
    
    //_driverView.frame = CGRectMake(_driverView.frame.origin.x, [UIScreen mainScreen].bounds.size.height/2 - _driverView.frame.size.height/2, _driverView.frame.size.width, _driverView.frame.size.height);
}
-(void)viewWillAppear:(BOOL)animated
{
   	[super viewWillAppear:animated];
    CGFloat x,y,height,widht;
    x=self.mapView.frame.size.width/2-self.viewForPickUp.frame.size.width/2;
    y=self.mapView.frame.size.height/2-self.viewForPickUp.frame.size.height/2;
    widht=self.viewForPickUp.frame.size.width;
    height=self.viewForPickUp.frame.size.height;
  	self.viewForPickUp.frame=CGRectMake(x, y, widht, height);
    [_btnNavigation setTitle:[NSLocalizedString(@"TITLE_CREATE_TRIP", nil) capitalizedString] forState:UIControlStateNormal];
    [_btnNavigation setTitleColor:[UIColor labelTitleColor] forState:UIControlStateNormal];
    [_btnNavigation.titleLabel sizeToFit];
    _imgDestinationPin.center=_mapView.center;
	
	if(![_btnRideNow isHidden])
        [self setTwoRightBackBarItem:[UIImage imageNamed:@"back"]];
    else
        [self setFareEstimateButton];
    [_carousel reloadData];
    
    if (PREF.UserApproved)
    {
       
    }
    else
    {
        if (!viewForAdminAlert)
        {
                viewForAdminAlert=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"ALERET_ADMIN_REVIEW", nil) message:NSLocalizedString(@"ALERET_ADMIN_REVIEW_MSG", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"LOGOUT", nil) otherButtonTitles:NSLocalizedString(@"ADMIN_EMAIL",nil)];
        }
     }
	self.navigationController.navigationBarHidden = false;
}
-(void)viewWillDisappear:(BOOL)animated
{
   
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
#pragma mark-Actions
-(void)didSelectMenuOptionAtIndex:(NSInteger)row
{
}
//Need To Optimize
-(IBAction)onClickBtnGetEstimate:(id)sender
{
    if (isDestinationDragFlagOn)
    {
		[self onClickBtnDestinationDrag:nil];
    }
    
	if(strCarTypeId)
    {
        CarTypeDataModal  *car;
        for (car in arrForCarType)
        {
            if ([strCarTypeId isEqualToString:car.id_])
            {
                
                if (![UtilityClass isEmpty:_txtDestinationAddress.text])
                {
                    NSArray *timeAndDistance=[self getTimeAndDistance:source_coordinate destLocation:dest_coordinate];
                    NSString *strTime=[NSString stringWithFormat:@"%@",[timeAndDistance objectAtIndex:2] ];
                    NSString *strDistance=[NSString stringWithFormat:@"%@",[timeAndDistance objectAtIndex:0]];
                    [self getFareEstimate:strTime andDistance:strDistance];
                }
                else
                {
                    if (viewEstimate)
                    {
                        viewEstimate=nil;
                        [viewEstimate removeFromSuperview];
                    }
                    viewEstimate=[FareEstimate getViewFareEstimatewithParent:self];
                    [viewEstimate setDataForEstimateView:car isSurge:[self checkSurgeStartTime:surgeStartTime surgeEndTime:surgeEndTime withServerTime:[CurrentTrip sharedObject].serverTime] ];
                    [APPDELEGATE.window addSubview:viewEstimate];
                    
                }
            }
        }
    }
    else
    {
        [[UtilityClass sharedObject] displayAlertWithTitle:NSLocalizedString(@"ALERT_TITLE_SERVICE_TYPE_NOT_SELECTED", nil)  andMessage:NSLocalizedString(@"ALERT_MSG_SERVICE_TYPE_NOT_SELECTED", nil)];
    }
 }
-(IBAction)onClickBtnMyLocation:(id)sender
{
    [self cameraFocus:current_coordinate ];
}
-(void)mapView:(GMSMapView *)mapView didTapOverlay:(GMSOverlay *)overlay
{
    
}
- (IBAction)onClickBtnRideLater:(id)sender
{
	isRideNow = 1;
    CustomDatePicker *viewDate=[CustomDatePicker getCustomDatePickerwithParent:self];
    [self.view addSubview:viewDate];
    [viewDate bringSubviewToFront:self.view];
}
- (IBAction)onClickBtnRideNow:(id)sender
{
    int isSurge=[self checkSurgeStartTime:surgeStartTime surgeEndTime:surgeEndTime withServerTime:[CurrentTrip sharedObject].serverTime];
    if (isSurge)
    {
        dialogForSurgePrice=[[CustomAlertWithTitle alloc]initWithTitle:[NSString stringWithFormat:@"%@ %@x ",NSLocalizedString(@"SURGE_COST", nil),surgeMultiplier]  message:NSLocalizedString(@"SURGE_MESSAGE", nil)   delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_CANCEL",nil) otherButtonTitles:NSLocalizedString(@"ALERT_BTN_OK",nil)];
    }
    else
    {
		isRideNow = 0;
		[self getFavDriver];
        //[self wsCreateTripRequest];
    }
}
-(void)onClickPaymentOption:(UITapGestureRecognizer *)recognizer
{
    if (recognizer.view.tag==0)
    {
        strPaymentMode=NO;
        [_lblPayment setText:NSLocalizedString(@"CARD", nil)];
        [_imgPayment setImage:[UIImage imageNamed:@"icon_card"]forState:UIControlStateNormal];
    }
    else
    {
	strPaymentMode=YES;
    [_lblPayment setText:NSLocalizedString(@"CASH", nil)];
        [_imgPayment setImage:[UIImage imageNamed:@"icon_cash"] forState:UIControlStateNormal];
    }
    [self hidePaymentOptions];
}
/**Cancel Request*/
- (IBAction)onClickBtnCancel:(id)sender
{
    NSString *strTempTripId=PREF.tripId;
	 NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:strUserId forKey:PARAM_USER_ID];
	[dictParam setValue:strUserToken forKey:PARAM_TOKEN];
	[dictParam setValue:strTempTripId forKey:PARAM_TRIP_ID];
    [dictParam setValue:@"" forKey:PARAM_CANCEL_TRIP_REASON];
	NSString *strUrl=[NSString stringWithFormat:@"%@%@",WS_CANCEL_TRIP,strTempTripId];
	[self wsCancelRequest:dictParam andUrl:strUrl];
}

- (IBAction)onClickFavouriteBack:(id)sender
{
	self.navigationController.navigationBarHidden = false;
	self.viewForFavDriver.hidden = true;
}
-(IBAction)onClickBtnRight:(id)sender
{
    dest_coordinate.longitude=0;
    dest_coordinate.latitude=0;
    dest_address=@"";
    isDestinationDragFlagOn=NO;

    [_mapView clear];
    if(viewForCard)
    {
        [self hidePaymentOptions];
    }
    [self updateUIIdealState];
}
- (IBAction)onClickBtnPayment:(id)sender
{
    if (PREF.isCardAvailable && PREF.isCashAvailable)
    {
        if(viewForCard)
        {
            [self hidePaymentOptions];
        }
        else
        {
            [self showPaymentOptions];
        }
    }
    else
    {
        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_PAYMENT_OPTION_IS_NOT_AVAILABLE", nil)];
    }
}
- (IBAction)onClickBtnPickUpMe:(id)sender
{
	if (strCarTypeId != nil) {
		///[self getFavDriver];
		isRideNow = 2;
		[self onClickContinueRequest:nil];
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Please wait" message:@"We are getting all services and then try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
		alert.tag = 23232;
		[alert show];
	}
}

-(void)getFavDriver
{
	[APPDELEGATE showLoadingWithTitle:nil];
	
	NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
	[dictParam setObject:strUserId forKey:PARAM_USER_ID];
	[dictParam setObject:strCarTypeId forKey:PARAM_SERVICE_TYPE_ID];
 // AUTOCOMPLETE API
	
	AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
	[afn getDataFromPath:WS_GET_FAV_DRIVER withParamData:dictParam withBlock:^(id response, NSError *error)
	 {
		if ([[Parser sharedObject]parseGetFavouriteDriver:response])
		 {
			dispatch_async(dispatch_get_main_queue(), ^
						{
							FavouriteDriver *obj = [FavouriteDriver sharedObject];
							arrFavProvider = [NSArray arrayWithArray:obj.arrFavDriver];
						
							[arrForAllFavProvider removeAllObjects];
							for (int i=0; i<arrFavProvider.count; i++)
							{
								NSDictionary *dictData = [arrFavProvider objectAtIndex:i];
								NSArray *arrProviderLocation = [[dictData valueForKey:@"provider_detail"] valueForKey:@"providerLocation"];
								NSString *strProviderLat = [arrProviderLocation objectAtIndex:0];
								NSString *strProviderLng = [arrProviderLocation objectAtIndex:1];
						
								CLLocation *locA = [[CLLocation alloc] initWithLatitude:[strForCurLatitude doubleValue] longitude:[strForCurLongitude doubleValue]];
						
								CLLocation *locB = [[CLLocation alloc] initWithLatitude:[strProviderLat doubleValue] longitude:[strProviderLng doubleValue]];
						
								CLLocationDistance distance = [locA distanceFromLocation:locB];
								NSLog(@"Distance =%f",distance);
						
							if ([[NSString stringWithFormat:@".%f",distance] integerValue] <= [obj.defaultRadious integerValue])
							{
								[arrForAllFavProvider addObjectsFromArray:obj.arrFavDriver];
							}
					}
						self.navigationController.navigationBarHidden = true;

							[self.tableFavDriver reloadData];
							self.tableFavDriver.hidden = false;
							self.lblHavntFavDriver.hidden = true;
							self.viewForFavDriver.hidden = false;
							[APPDELEGATE hideLoadingView];
						});
		 }
	 else
		 {
			dispatch_async(dispatch_get_main_queue(), ^{
							self.viewForFavDriver.hidden = true;
							isRideNow = 2;
							[APPDELEGATE hideLoadingView];
							[self wsCreateTripRequest];
						});
		 }
	 }];
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
	
	self.viewForFavDriver.hidden = true;
}

#pragma mark- LOCATION RELATED METHODS
-(void)updateLocationManager
{
    [locationManager startUpdatingLocation];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
	
    #ifdef __IPHONE_8_0
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8"))
    {
        // Use one or the other, not both. Depending on what you put in info.plist
        //[self.locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
    #endif
	
    [locationManager startUpdatingLocation];
    current_coordinate=[self getLocation];
	
}

-(CLLocationCoordinate2D) getLocation
{
    CLLocationCoordinate2D coordinate;
        if ([CLLocationManager locationServicesEnabled])
        {
             CLLocation *location = [locationManager location];
                if (location)
                {
                    pickUp_location=location;
                    coordinate = [location coordinate];
                    [self getCountryAndCity:location];
                    strForCurLatitude=[NSString stringWithFormat:@"%f",coordinate.latitude];
                    strForCurLongitude=[NSString stringWithFormat:@"%f",coordinate.longitude];
                    [_mapView animateToLocation:coordinate];
                    [_mapView animateToZoom:17.0f];
                }
        }
        else
        {
            gpsEnableDialog=nil;
            gpsEnableDialog=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"ALERT_TITLE_GPS_NOT_AVAILABEL", nil) message:NSLocalizedString(@"ALERT_MSG_LOCATION_SERVICE_NOT_AVAILABLE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_NO", nil) otherButtonTitles:NSLocalizedString(@"ALERT_BTN_YES", nil)];
        }
       return coordinate;
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    CLLocation *newLocation = locations.lastObject;
    CLLocation *oldLocation;
    if (newLocation)
    {
        strForCurLatitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
        strForCurLongitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
        current_coordinate=[newLocation coordinate];
        pickUp_location=newLocation;
    }
    if (locations.count > 1)
    {
        oldLocation = locations[locations.count - 2];
    }
}
- (void)locationManager:(CLLocationManager *)manager
	  didFailWithError:(NSError *)error
{
	NSLog(@"didFailWithError: %@", error);
}
/**Retuen Address of Given LatLong*/
-(NSString  *)getAddressForLocation:(CLLocationCoordinate2D)location
{   NSString *strAddress=nil;
    if ([APPDELEGATE connected])
    {
        NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f", location.latitude, location.longitude];
        
        NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:NULL];
        if (result)
        {
            NSData *data = [result dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            if([[jsonResponse valueForKey:GOOGLE_PARAM_STATUS]isEqualToString:GOOGLE_PARAM_STATUS_OK])
            {
                NSArray *arr=[jsonResponse valueForKey:GOOGLE_PARAM_RESULTS];
                NSDictionary *dict=[arr objectAtIndex:0];
                strAddress=[dict valueForKey:GOOGLE_PARAM_AUTOCOMLETE_FORMATTED_ADDRESS];
            }
            else
            {
                [[UtilityClass sharedObject]displayAlertWithTitle:NSLocalizedString(@"ALERT_LOCATION_NOT_FOUND", nil) andMessage:NSLocalizedString(@"ALERT_MSG_ENTER_VALID_LOCATION",nil )];
            }
        }
        else
        {
            [[UtilityClass sharedObject]displayAlertWithTitle:NSLocalizedString(@"ALERT_LOCATION_NOT_FOUND", nil) andMessage:NSLocalizedString(@"ALERT_MSG_ENTER_VALID_LOCATION",nil )];
            
        }
    }
    
    return strAddress;
}
/*Get LatLong From Address*/
-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr {
	double latitude = 0, longitude = 0;
	NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?address=%@", esc_addr];
	NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
	if (result)
    {
       
        NSData *data = [result dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        NSDictionary *dict=[[NSDictionary alloc]init];
        if([[jsonResponse valueForKey:GOOGLE_PARAM_STATUS]isEqualToString:GOOGLE_PARAM_STATUS_OK])
        {
            dict=[[[[jsonResponse valueForKey:GOOGLE_PARAM_RESULTS]objectAtIndex:0] objectForKey:GOOGLE_PARAM_GEOMETRY]objectForKey:GOOGLE_PARAM_LOCATION];
            latitude=[[dict valueForKey:GOOGLE_PARAM_AUTOCOMLETE_PLACE_LATITUDE]doubleValue ];
            longitude=[[dict valueForKey:GOOGLE_PARAM_AUTOCOMLETE_PLACE_LONGITUDE]doubleValue ];
        }
       
    }
	CLLocationCoordinate2D center;
	center.latitude=latitude;
	center.longitude = longitude;
   return center;
  
}
#pragma mark-MAP AND CAMERA METHOD

///Focus Camera On Map At Given Location-Coordinate
-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    [self.view endEditing:YES];

}

-(void)cameraFocus:(CLLocationCoordinate2D)coordinate
{
    [CATransaction begin];
    [CATransaction setValue:[NSNumber numberWithFloat:animateCameraDelay] forKey:kCATransactionAnimationDuration];
    [CATransaction setCompletionBlock:^{
    }];
    [_mapView animateToLocation:coordinate];
    [_mapView animateToZoom:17];
    [CATransaction commit];
}
-(void)showRouteFrom:(CLLocationCoordinate2D)f to:(CLLocationCoordinate2D)t{
	NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
	NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
	NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,GoogleServerKey];
   // NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&sensor=false&waypoints=21.5222,70.4579",saddr,daddr];
   // NSString *escapedUrlString = [apiUrlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //NSURL *apiUrl = [NSURL URLWithString:escapedUrlString];
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
	NSError* error = nil;
    NSData *data = [[NSData alloc]initWithContentsOfURL:apiUrl];
	NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
	GMSPath *path =[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
	GMSPolyline *polyLinePath = [GMSPolyline polylineWithPath:path];
	polyLinePath.strokeColor = [UIColor colorWithRed:(0.0f/255.0f) green:(255.0f/255.0f) blue:(000.0f/255.0f) alpha:1.0];
	polyLinePath.strokeWidth = 5.f;
    polyLinePath.geodesic = YES;
	polyLinePath.map = self.mapView;
    [_tableForCity setHidden:YES];
}
- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position{}
- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    
    NSLog(@"My Current Latitude %f and Current Longintude %f",position.target.latitude,position.target.longitude);
    if(self.txtPickupAddress.isEnabled)
        {
            
            if (!_txtPickupAddress.focused && [_tableForCity isHidden])
            {
                source_coordinate=position.target;
                pickUp_location=[pickUp_location  initWithLatitude:source_coordinate.latitude longitude:source_coordinate.longitude];
                source_address=[self getAddressForLocation:source_coordinate];
                strPickUpLat= [NSString stringWithFormat:@"%f",source_coordinate.latitude];
                strPickUpLong= [NSString stringWithFormat:@"%f",source_coordinate.longitude];
                if (![UtilityClass isEmpty:source_address])
                {
                    [self setNewPlaceData:source_address];
                    [self getAllApplicationType];
                }
             }
            
        }
        if(isDestinationDragFlagOn)
        {
            dest_coordinate=position.target;
            dest_address=[self getAddressForLocation:dest_coordinate];
            strForDestLatitude= [NSString stringWithFormat:@"%f",dest_coordinate.latitude];
            strForDestLongitude= [NSString stringWithFormat:@"%f",dest_coordinate.longitude];
            if (![UtilityClass isEmpty:dest_address])
            {
                [self setNewPlaceData:dest_address];
            }
        }
}
- (void)focusMapToShowAllMarkers:(CLLocationCoordinate2D)sourceLatLong andDestinationLatLong:(CLLocationCoordinate2D)destLatlong
{
    destMarker.map=nil;
    destMarker = [[GMSMarker alloc] init];
    destMarker.position = destLatlong;
    destMarker.icon = [UIImage imageNamed:@"pin_destination"];
    destMarker.map = self.mapView;
    if (clientmarker.map) {
        clientmarker.position = sourceLatLong;
    }
    else
    {
        clientmarker.position = sourceLatLong ;
        clientmarker.map=_mapView;
    }

    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    bounds = [bounds includingCoordinate:sourceLatLong];
    bounds = [bounds includingCoordinate:destLatlong];
    
    [CATransaction begin];
    [CATransaction setValue:[NSNumber numberWithFloat:animateCameraDelay] forKey:kCATransactionAnimationDuration];
    [CATransaction setCompletionBlock:^{
    }];
    [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:80.0f]];
    [CATransaction commit];
   
}
#pragma mark- NAVIGATION METHODS
/// This Method is Used To Custom Setup For Navigation Bar & RevealViewController Sidebar
- (void)customSetup
{
    SWRevealViewController *revealController = [self revealViewController];
    [revealController tapGestureRecognizer];
    [self.btnNavigation addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
}
#pragma mark
#pragma mark - UITextfield Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
	if(textField==self.txtPickupAddress)
	{
		flag=0;
        _tableForCity.frame=CGRectMake(_viewForPickAddress.frame.origin.x
                                       ,_viewForpickupbg.frame.origin.y+_viewForpickupbg.frame.size.height,
                                       _viewForPickAddress.frame.size.width, _tableForCity.frame.size.height);
		
	}
	else if (textField==self.txtDestinationAddress)
	{
		flag=1;
         _tableForCity.frame=CGRectMake(_viewForDestinationAddress.frame.origin.x
                                       ,_viewForDestinationbg.frame.origin.y+_viewForDestinationbg.frame.size.height,
                                       _viewForDestinationAddress.frame.size.width, _tableForCity.frame.size.height);
	}
	else
	{
	
	}
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{return YES;}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
	if(self.txtDestinationAddress==textField)
	{
		
	}
	else if(self.txtPickupAddress == textField)
	{
       
  	}
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return YES;
}
#pragma mark - AutoComplete Tableview Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView == _tableFavDriver) {
		return arrForAllFavProvider.count;
	}
	else
	{
		return arrForPredictedAddresses.count;
	}
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (tableView == _tableFavDriver)
	{
		FavouriteDriverCell *cell=[tableView dequeueReusableCellWithIdentifier:@"FavouriteDriverCell"];
		if(cell == nil)
		{
			cell=[[FavouriteDriverCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FavouriteDriverCell"];
		}
	
		if(arrForAllFavProvider.count > 0)
		{
			NSDictionary *dictData = [arrForAllFavProvider objectAtIndex:indexPath.row];
			cell.lblDriverName.text =[NSString stringWithFormat:@"%@ %@",[[dictData valueForKey:@"provider_detail"] valueForKey:@"first_name"],[[dictData valueForKey:@"provider_detail"] valueForKey:@"last_name"]];
			[cell.imgDriver downloadFromURL:[NSString stringWithFormat:@"%@%@",BASE_URL,[[dictData valueForKey:@"provider_detail"] valueForKey:@"picture"]] withPlaceholder:[UIImage imageNamed:@"user"]];
			[cell.btnDeleteDriver addTarget:self action:@selector(onClickDeleteDriver:) forControlEvents:UIControlEventTouchUpInside];
		cell.btnDeleteDriver.tag = indexPath.row;
		}
		return cell;
	}
	else
	{
		UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
		if(cell == nil)
		{
			cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];}
			if(arrForPredictedAddresses.count > 0)
				{
				NSString *formatedAddress=[[arrForPredictedAddresses objectAtIndex:indexPath.row] valueForKey:GOOGLE_PARAM_AUTOCOMLETE_DESCRIPTION];
				NSArray* myArray = [formatedAddress  componentsSeparatedByString:@","];
				NSString *strTitle=[[NSString alloc] initWithString:[myArray objectAtIndex:0]];
				cell.textLabel.text=strTitle;
				cell.textLabel.textColor=[UIColor blackColor];
				cell.detailTextLabel.text=formatedAddress;
		}
	return cell;
	}
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (tableView == _tableFavDriver)
	{
		NSDictionary *dictData = [arrForAllFavProvider objectAtIndex:indexPath.row];
		strCarTypeId = [[dictData valueForKey:@"provider_detail"] valueForKey:@"service_type"];
	
	if (isRideNow == 0) {
		[self wsCreateTripRequest];
	}
	else if(isRideNow == 1)
	{
		[self wsFutureRequest:NO];
	}
	else
	{
		
	}
	}
	else
	{
	dictPlace=[arrForPredictedAddresses objectAtIndex:indexPath.row];
	if(self.txtPickupAddress.isEnabled)
		{
		source_address=[NSString stringWithFormat:@"%@",[dictPlace objectForKey:GOOGLE_PARAM_AUTOCOMLETE_DESCRIPTION]];
		if (![UtilityClass isEmpty:source_address])
			{
			source_coordinate=[self getLocationFromAddressString:source_address];
			[self setNewPlaceData:source_address];
			[self cameraFocus:source_coordinate];
			}
		
		pickUp_location=[pickUp_location  initWithLatitude:source_coordinate.latitude longitude:source_coordinate.longitude];
		strPickUpLat= [NSString stringWithFormat:@"%f",source_coordinate.latitude];
		strPickUpLong= [NSString stringWithFormat:@"%f",source_coordinate.longitude];
		if (![UtilityClass isEmpty:source_address])
			{
			
			[self getAllApplicationType];
			
			}
		}
	
	if (flag==1)
		{
		if (isDestinationDragFlagOn)
			{
			[self onClickBtnDestinationDrag:nil];
			}
		dest_address=[NSString stringWithFormat:@"%@",[dictPlace objectForKey:GOOGLE_PARAM_AUTOCOMLETE_DESCRIPTION]];
		if (![UtilityClass isEmpty:dest_address])
			{
			dest_coordinate=[self getLocationFromAddressString:dest_address];
			[self setNewPlaceData:dest_address];
			strForDestLatitude=[NSString stringWithFormat:@"%f",dest_coordinate.latitude ];
			strForDestLongitude=[NSString stringWithFormat:@"%f",dest_coordinate.longitude ];
			destMarker.map=nil;
			destMarker = [[GMSMarker alloc] init];
			destMarker.position = dest_coordinate ;
			destMarker.icon = [UIImage imageNamed:@"pin_destination"];
			destMarker.map = self.mapView;
			final_dest_address=dest_address;
			finalDestCoordinate=dest_coordinate;
			[self focusMapToShowAllMarkers:source_coordinate andDestinationLatLong:dest_coordinate];
			}
		}
	self.tableForCity.hidden=YES;
	}
}

-(void)onClickDeleteDriver:(id)sender
{
	UIButton *btn = (UIButton*)sender;
	
	NSDictionary *dictData = [arrFavProvider objectAtIndex:btn.tag];
	
	[APPDELEGATE showLoadingWithTitle:nil];
	
	NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
	[dictParam setObject:strUserId forKey:PARAM_USER_ID];
	[dictParam setObject:[dictData valueForKey:@"provider_id"] forKey:PARAM_SERVICE_TYPE_ID];
 // AUTOCOMPLETE API
	
	AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
	[afn getDataFromPath:WS_DELETE_FAV_DRIVER withParamData:dictParam withBlock:^(id response, NSError *error)
	 {
		if ([[Parser sharedObject]parseGetFavouriteDriver:response])
		 {
		 
			dispatch_async(dispatch_get_main_queue(), ^
						   {
						   [APPDELEGATE hideLoadingView];
						   [self getFavDriver];
						   });
		 }
	 else
		 {
			dispatch_async(dispatch_get_main_queue(), ^{
				[APPDELEGATE hideLoadingView];

							});
		 }
	 }];
}

/*! USed To Search The Predicted Array Of Addresses for Autocomplete*/
- (IBAction)Searching:(id)sender
{
    [arrForPredictedAddresses removeAllObjects];
    self.tableForCity.hidden=YES;
    NSString *str;
    if(flag==0)
    {
        str=self.txtPickupAddress.text;
    }
    else if (flag==1)
    {
        str=self.txtDestinationAddress.text;
    }
    //NSlog(@"%@",str);
    if(str.length > 2)
    {
        self.tableForCity.hidden=YES;
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:str forKey:GOOGLE_PARAM_AUTOCOMLETE_INPUT]; // AUTOCOMPLETE API
        [dictParam setObject:GOOGLE_PARAM_AUTOCOMLETE_SENSOR forKey:@"false"]; // AUTOCOMPLETE API
        [dictParam setObject:GoogleServerKey forKey:GOOGLE_PARAM_AUTOCOMLETE_KEY];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:@"GET"];
        [afn getAddressFromGooglewAutoCompletewithParamData:dictParam withBlock:^(id response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                NSArray *arrAddress=[response valueForKey:GOOGLE_PARAM_AUTOCOMLETE_PREDICTION]; //AUTOCOMPLTE API
                                
                                if ([arrAddress count] > 1)
                                {
                                    self.tableForCity.hidden=NO;
                                    arrForPredictedAddresses=[[NSMutableArray alloc] initWithArray:arrAddress copyItems:YES];
                                    [UIView performWithoutAnimation:^{
                                        [self.tableForCity reloadData];
                                        [self.tableForCity beginUpdates];
                                        [self.tableForCity endUpdates];
                                    }];
                                    if(self.tableForCity.hidden)
                                    {
                                        self.tableForCity.hidden=NO;
                                    [self.view bringSubviewToFront:_tableForCity];
                                    }
                                }
                                else
                                {
                                    self.tableForCity.hidden=YES;
                                }
                            });
         }];
    }
}
-(void)setNewPlaceData:(NSString*)address
{
    NSString *myAddress=[UtilityClass formatAddress:address];
    if(flag==0)
    {
            self.txtPickupAddress.text = [NSString stringWithFormat:@"%@",myAddress];
            [self textFieldShouldReturn:self.txtPickupAddress];
    }
    else if(flag==1)
    {
            self.txtDestinationAddress.text = [NSString stringWithFormat:@"%@",myAddress];
            CGFloat pointSize=MIN(_txtDestinationAddress.font.pointSize, _txtPickupAddress.font.pointSize);
            UIFont *font=[UIFont fontWithName:_txtPickupAddress.font.fontName size:pointSize];
            _txtPickupAddress.font=_txtDestinationAddress.font=font;
            [self textFieldShouldReturn:self.txtDestinationAddress];
    }
    else
    {
    }
}
#pragma mark-SET LOCALIZED TEXT
-(void)setLocalization
{
    
    /*button setUp*/
    [_btnDestinationDrag setContentMode:UIViewContentModeCenter];
    [_btnConfirmDestination setBackgroundColor:[UIColor buttonColor]];
    [_btnConfirmDestination setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [_btnConfirmDestination setHidden:YES];
    [_btnConfirmDestination setTitle:[NSLocalizedString(@"CONFIRM", nil) uppercaseString] forState:UIControlStateNormal];
    
    [_btnCancelRequest setTitle:NSLocalizedString(@"CANCEL_TRIP", nil) forState:UIControlStateNormal];
    [_btnCancelRequest setBackgroundColor:[UIColor buttonColor]];
    [_btnCancelRequest setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
  
    [_btnNavigation setTitleColor:[UIColor labelTitleColor] forState:UIControlStateNormal];
    [_btnNavigation setTitle:NSLocalizedString(@"CREATE TRIP", nil) forState:UIControlStateNormal];
    [_btnRideNow setHidden:YES];
    [_btnRideLater setHidden:YES];
    [_btnRideNow setBackgroundColor:[UIColor buttonColor]];
    [_btnRideNow setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [_btnRideNow   setTitle:[NSLocalizedString(@"RIDE_NOW", nil) uppercaseString] forState:UIControlStateNormal];

    [_btnRideLater setTitle:[NSLocalizedString(@"RIDE_LATER", nil) uppercaseString] forState:UIControlStateNormal];
    [_btnRideLater setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    _btnContinue.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnContinue.titleLabel.lineBreakMode = NSLineBreakByClipping;
    [_btnDestinationDrag setBackgroundColor:[UIColor GreenColor]];
    
    /*label setUp*/
    [_lblTypeDescriptionValueDialog setTextColor: [UIColor textColor]];
    [_lblTypeDescriptionDialog setTextColor: [UIColor buttonColor]];
    [_lblTypeNameDialog setBackgroundColor:[UIColor GreenColor]];
    [_lblTypeNameDialog setTextColor:[UIColor labelTitleColor]];
    [_lblEstTime setTextColor:[UIColor labelTitleColor]];
    [_lblmin setText:TIME_SUFFIX];
    [_lblmin setTextColor:[UIColor labelTitleColor]];
    [_lblmin setFont:[UIFont systemFontOfSize:8]];
    [_lblEstTime setAdjustsFontSizeToFitWidth:YES];
    [_lblTypeDescriptionValueDialog setContentMode:UIViewContentModeTop];
    [_lblPayment setText:NSLocalizedString(@"CASH", nil)];
    [_lblConnectingDriver setText:NSLocalizedString(@"CONECTING_NEAR_BY_DRIVER", nil)];
    [_lblPayment setBackgroundColor:[UIColor GreenColor]];
    
     [_imgDestinationPin setHidden:YES];
    [self.view addSubview:self.tableForCity];
    [_tableForCity setHidden:YES];
    [_viewForDestinationbg setHidden:YES];
	[_viewForTypeNote setHidden:YES];
     _viewForPickAddress.layer.cornerRadius = 2;
    _viewForPickAddress.layer.masksToBounds = YES;
    _viewForDestinationAddress.layer.cornerRadius = 2;
    _viewForDestinationAddress.layer.masksToBounds = YES;
    _viewForpickupbg.backgroundColor=[UIColor GreenColor];
    [self.view setBackgroundColor:[UIColor backGroundColor]];
    [self.txtPickupAddress setBackgroundColor:[UIColor clearColor]];
    [self.viewForPickAddress setBackgroundColor:[UIColor lightGreenColor]];
    [self.viewForDestinationAddress setBackgroundColor:[UIColor lightGreenColor]];
    [_viewForDestinationbg setBackgroundColor:[UIColor GreenColor]];
    
    /*set Clear button*/
    
    UIButton *btnClear = [self.txtPickupAddress valueForKey:@"_clearButton"];
    UIImage *imageNormal = [btnClear imageForState:UIControlStateHighlighted];
    UIGraphicsBeginImageContextWithOptions(imageNormal.size, NO, 0.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGRect rect = (CGRect){ CGPointZero, imageNormal.size };
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    [imageNormal drawInRect:rect];
    
    CGContextSetBlendMode(context, kCGBlendModeSourceIn);
    [[UIColor whiteColor] setFill];
    CGContextFillRect(context, rect);
    
    UIImage *imageTinted  = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [btnClear setImage:imageTinted forState:UIControlStateNormal];
    UIButton *btnClearDestination = [self.txtDestinationAddress valueForKey:@"_clearButton"];
    [btnClearDestination setImage:imageTinted forState:UIControlStateNormal];
    
    /**/
    [_mapView clear];
    
    [_txtPickupAddress setValue:[UIColor textPlaceHolderColor] forKeyPath:@"_placeholderLabel.textColor"];
    [_txtDestinationAddress setValue:[UIColor textPlaceHolderColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    [_txtPickupAddress setTextColor:[UIColor labelTitleColor]];
    [_txtDestinationAddress setTextColor:[UIColor labelTitleColor]];
	self.txtPickupAddress.placeholder=NSLocalizedString(@"TYPE_PICKUP_ADDRESS", nil);
	self.txtDestinationAddress.placeholder=NSLocalizedString(@"TYPE_DESTINATION_ADDRESS", nil);
    self.viewForDriver.hidden=YES;
    self.viewForPickUp.hidden=NO;
    [_btnContinue setTitle:NSLocalizedString(@"CONTINUE", nil) forState:UIControlStateNormal];
    self.viewForDriver.backgroundColor=[UIColor clearColor];
    self.driverView.backgroundColor=[UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor GreenColor];
    self.navigationController.navigationBar.translucent = NO;
    
    /*Payment Button*/
    [_viewForPayment setHidden:YES];
    [self adjustFrameProperty:_imgPayment.imageView andLabel:_lblPayment inView:_viewForPayment];;
    paymentGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickBtnPayment:)]; // Declare the Gesture.
    paymentGesture.delegate = self;
    [_viewForPayment addGestureRecognizer:paymentGesture]; // Add Gesture to your view.
    
    [viewForCard setHidden:YES];
    [_imgPayment.imageView setContentMode:UIViewContentModeCenter];
    [_imgPayment setBackgroundImage:[UIImage imageNamed:@"circle_black"] forState:UIControlStateNormal];
	
	_viewForFavDriver.frame = self.view.frame;
	[self.view addSubview:_viewForFavDriver];
	_viewForFavDriver.hidden = true;
}

#pragma mark-WS METHODS
/** Get All Car Type Services Available in That Area*/
-(void)getAllApplicationType
{
    if (pickUp_location)
    {[self getCountryAndCity:pickUp_location];}
}

-(void)getServiceTypes
{
  
    strTempCity=nil;
    if (!strCountry)
    {
        strCountry=@"";
    }
    if (!strCity)
    {
        strCity=@"";
    }
    if (!strSubAdminArea)
    {
        strSubAdminArea=@"";
    }
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:strCountry forKey:PARAM_COUNTRY];
    [dictParam setObject:strUserId forKey:PARAM_USER_ID];
    [dictParam setObject:strUserToken   forKey:PARAM_TOKEN];
    
    [dictParam setObject:strCity forKey:PARAM_CITY];
    [dictParam setObject:strPickUpLat forKey:PARAM_LATITUDE];
    [dictParam setObject:strPickUpLong forKey:PARAM_LONGITUDE];
    [dictParam setObject:strSubAdminArea forKey:PARM_SUB_ADMIN_CITY];
    NSLog(@"Dictionary %@",dictParam);
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
    [afn getDataFromPath:USER_GET_CAR_TYPE withParamData:dictParam withBlock:^(id response, NSError *error)
    {
        strTempCity=strCity;
        if ([[Parser sharedObject]parseVehicalTypeList:response toArray:&arrForCarType])
         {
             
             strCarTypeId=  [[arrForCarType objectAtIndex:[arrForCarType count]/2] id_];
             isSurgeHour=  [[arrForCarType objectAtIndex:[arrForCarType count]/2] isSurgeHour];
             surgeEndTime= [[arrForCarType objectAtIndex:[arrForCarType count]/2] surgeEndHour];
             surgeStartTime= [[arrForCarType objectAtIndex:[arrForCarType count]/2] surgeStartHour];
             strSelectedCarIndex=[arrForCarType count]/2;
             surgeMultiplier=[[arrForCarType objectAtIndex:[arrForCarType count]/2] sugrgeMultiplier];
             _carousel.currentIndex=strSelectedCarIndex;
             
             [self getNearByProvider];
             
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                               [self.carousel reloadData];
                                if (!PREF.isCashAvailable && PREF.isCardAvailable)
                                {
                                    strPaymentMode=NO;
                                    [_viewForPayment setUserInteractionEnabled:NO];
                                    [_lblPayment setText:NSLocalizedString(@"CARD",nil)];
                                }
                                else if(!PREF.isCardAvailable && PREF.isCashAvailable)
                                {
                                    strPaymentMode=YES;
                                    [_viewForPayment setUserInteractionEnabled:NO];
                                    [_lblPayment setText:NSLocalizedString(@"CASH",nil)];
                                }
                                else
                                {
                                    [_lblPayment setText:NSLocalizedString(@"CASH",nil)];
                                    [_imgPayment setImage:[UIImage imageNamed:@"icon_cash"]forState:UIControlStateNormal];
                                    
                                }
                            });
             
         }
         else
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                _lblEstTime.text=@"0";
                                [_mapView clear];
                                strCarTypeId=nil;
                                isSurgeHour=FALSE;
                                [self.carousel reloadData];
                            });
             
         }
     }];

}
/*Request For Creating New Trip*/
-(void)wsCreateTripRequest
{
    [APPDELEGATE showLoadingWithTitle:@""];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
  	if (source_coordinate.latitude && source_coordinate.longitude)
    {
    
    if (dest_coordinate.latitude && dest_coordinate.longitude)
    {
        strForDestLatitude=[NSString stringWithFormat:@"%f",dest_coordinate.latitude ];
        strForDestLongitude=[NSString stringWithFormat:@"%f",dest_coordinate.longitude ];
        [dictParam setValue:strForDestLatitude forKey:PARAM_DEST_LATITUDE];
        [dictParam setValue:strForDestLongitude  forKey:PARAM_DEST_LONGITUDE];
        [dictParam setValue:dest_address forKey:PARAM_TRIP_DESTINATION_ADDRESS];
    }
    else
    {
        [dictParam setValue:@"" forKey:PARAM_DEST_LATITUDE];
        [dictParam setValue:@"" forKey:PARAM_DEST_LONGITUDE];
        [dictParam setValue:@"" forKey:PARAM_DESTINATION_LOCATION];
        [dictParam setValue:@"" forKey:PARAM_TRIP_DESTINATION_ADDRESS];
        
    }
    [dictParam setValue:[NSString stringWithFormat:@"%.8f", source_coordinate.latitude ] forKey:PARAM_LATITUDE];
	[dictParam setValue:[NSString stringWithFormat:@"%.8f", source_coordinate.longitude ] forKey:PARAM_LONGITUDE];
     NSTimeZone *timeZone = [NSTimeZone localTimeZone];
     NSString *tzName = [timeZone name];
    [dictParam setValue:strUserId forKey:PARAM_USER_ID];
    [dictParam setValue:strUserToken forKey:PARAM_TOKEN];
    [dictParam setValue:tzName forKey:PARAM_TIME_ZONE];
	[dictParam setValue:[NSNumber numberWithBool:strPaymentMode] forKey:PARAM_PAYMENT_MODE];
	[dictParam setValue:source_address forKey:PARAM_TRIP_SOURCE_ADDRESS];
	[dictParam setValue:strCarTypeId forKey:PARAM_SERVICE_TYPE];
    int isSurge=[self checkSurgeStartTime:surgeStartTime surgeEndTime:surgeEndTime withServerTime:[CurrentTrip sharedObject].serverTime];
    [dictParam setValue:[NSNumber numberWithInt:isSurge] forKey:PARAM_IS_SURGE_HOUR];
	[dictParam setValue:self.txtTypeNote.text	 forKey:PARAM_TYPE_NOTE];

		NSString *url=[NSString stringWithFormat:@"%@%@",USER_CREATE_REQUEST,PREF.userId];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
			[afn getDataFromPath:url withParamData:dictParam withBlock:^(id response, NSError *error)
             { if([[Parser sharedObject]parseTrip:response] )
                {
                        dispatch_async(dispatch_get_main_queue(), ^
							 {
                                    [self onCreateNewTrip];
							 });
                }
                 dispatch_async(dispatch_get_main_queue(), ^
                                {
                                    [[AppDelegate sharedAppDelegate]hideLoadingView];
                                });
			 }];
    
    }
    else
    {
        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_LOCATION", nil)];
    }
}
/**Called If Trip Is Successfully Created*/
-(void) onCreateNewTrip
{
    CurrentTrip *providerData= [CurrentTrip sharedObject];
    self.lblProviderName.text=[NSString stringWithFormat:@"%@ %@", providerData.providerFirstName,providerData.providerLastName];
    self.viewForDriver.frame=APPDELEGATE.window.frame;
    _driverView.center =APPDELEGATE.window.center;
    
    self.viewForDriver.hidden=NO;
    self.driverView.hidden=NO;
    [self.view bringSubviewToFront:_viewForDriver];
    [_imgDriverPro downloadFromURL:providerData.providerProfileImage withPlaceholder:[UIImage imageNamed:@"user"]];
    _viewForPickUp.hidden=YES;
    _txtPickupAddress.enabled=NO;
    [self checkForRequestStatus];
    [self setTimerToCheckDriverStatus];
    [[AppDelegate sharedAppDelegate]hideLoadingView];
}
/**Called Every Five Second To Check Weather Request Accepted Or Not*/
-(void)checkForRequestStatus
{
		NSString *strForUrl=[NSString stringWithFormat:@"%@%@/%@",USER_GET_TRIP_STATUS,strUserId,strUserToken];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
		[dictParam setObject:strUserId forKey:PARAM_USER_ID];
    	AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
		[afn getDataFromPath:strForUrl withParamData:dictParam withBlock:^(id response, NSError *error)
		 {
             if ([[Parser sharedObject] parseTripStatus:response])
             {
                 CurrentTrip *currentTrip=[CurrentTrip sharedObject];
                 NSInteger isProviderAccepted=0;
                 isProviderAccepted=[currentTrip.isProviderAccepted  integerValue] ;
                 if (isProviderAccepted==1)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [timerForCheckReqStatus invalidate];
                         timerForCheckReqStatus=nil;
                         self.viewForDriver.hidden=YES;
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         if (self.navigationController.visibleViewController == self)
                         {
                              [self performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:self];
                             return;
                         }
                        
                        
                     });
                 }
                 else if (isProviderAccepted==0)
                 {
                     [self getProviderDetail:currentTrip.providerId];
                 }
                 else
                 {
                     
                 }
                 
              }
             else
			 {
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [timerForCheckReqStatus invalidate];
                     timerForCheckReqStatus=nil;
                     [_viewForPickUp setHidden:YES];
                     [_viewForDriver setHidden:YES];
                     IS_TRIP_EXSIST=NO;
                     IS_TRIP_ACCEPTED=NO;
                     
                     if (dialogForRequestAgain)
                     {
                         [self.view bringSubviewToFront:dialogForRequestAgain];
                     }
                     else
                     {
                         dialogForRequestAgain=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"REQUEST_AGAIN", nil) message:NSLocalizedString(@"REQUEST_AGAIN_MESSAGE", nil) delegate:self cancelButtonTitle:@"NO" otherButtonTitles:NSLocalizedString(@"ALERT_BTN_YES",nil)];
                        [self.view bringSubviewToFront:dialogForRequestAgain];
                     }

                 });

             }
            
			 
		 }];
}
/*upDate Provider Data*/
-(void) getProviderDetail:(NSString *) strProviderId
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setObject:strProviderId forKey:PARAM_PROVIDER_ID];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
    [afn getDataFromPath:WS_GET_PROVIDER_DETAIL withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if ([[Parser sharedObject] parseProvider:response])
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 CurrentTrip *providerData= [CurrentTrip sharedObject];
                 self.lblProviderName.text=[NSString stringWithFormat:@"%@ %@", providerData.providerFirstName,providerData.providerLastName];
                 self.viewForDriver.hidden=NO;
                 self.driverView.hidden=NO;
                 [_imgDriverPro downloadFromURL:providerData.providerProfileImage withPlaceholder:[UIImage imageNamed:@"user"]];
             });
         }
     }];
}
/**Used To Cancel Created Request By User*/
-(void)wsCancelRequest:(NSMutableDictionary*)dictParam andUrl:(NSString *)strUrl
{
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOADING_CANCLE_REQUEST", nil)];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:PUT];
	[afn getDataFromPath:strUrl withParamData:dictParam withBlock:^(id response, NSError *error)
	 {
		 dispatch_async(dispatch_get_main_queue(), ^
					 {
						 NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
						 NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data							options:kNilOptions
																		    error:nil];
						 if (jsonResponse)
						 {
							 if([[jsonResponse valueForKey:SUCCESS]boolValue])
							 {
								 [timerForCheckReqStatus invalidate];
								 timerForCheckReqStatus=nil;
								 [[AppDelegate sharedAppDelegate]hideLoadingView];
								 self.viewForDriver.hidden=YES;
							 }
							 else
							 {
								 NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
								 [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
							 }
						 }
						 [[AppDelegate sharedAppDelegate]hideLoadingView];
						 
					 });
		 
	 }];
	
}
/**Get the NearBy Providers */
-(void)getNearByProvider
{
    
    if (strCarTypeId)
    {
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setObject:strUserId forKey:PARAM_USER_ID];
            [dictParam setObject:strUserToken forKey:PARAM_TOKEN];
            [dictParam setObject:strCarTypeId forKey:PARAM_SERVICE_TYPE];
            [dictParam setObject:strPickUpLong forKey:PARAM_LONGITUDE];
            [dictParam setObject:strPickUpLat forKey:PARAM_LATITUDE];
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
            [afn getDataFromPath:WS_GET_NEARBY_PROVIDER withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 dispatch_async(dispatch_get_main_queue(), ^
                                {
                                    if (_txtPickupAddress.enabled)
                                    {
                                    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                                    NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data							options:kNilOptions  error:nil];
                                        if([[jsonResponse valueForKey:SUCCESS] boolValue])
                                        {
                                            NSArray *arr=[jsonResponse valueForKey:PARAM_PROVIDERS];
                                            NSDictionary *dictTemp=[arr objectAtIndex:0];
                                            CLLocationCoordinate2D dest;
                                            dest.latitude = [[[dictTemp valueForKey:PARAM_PROVIDER_LOCATION] objectAtIndex:0]floatValue];
                                            dest.longitude= [[[dictTemp valueForKey:PARAM_PROVIDER_LOCATION] objectAtIndex:1]floatValue];
                                            _lblEstTime.text=[NSString stringWithFormat:@"%@", [[self getTimeAndDistance:source_coordinate	destLocation:dest]objectAtIndex:1] ];
                                            
                                            if (![previousProviderId isEqualToString:[NSString stringWithFormat:@"%@",[[arr objectAtIndex:0] valueForKey:PARAM_ID]]])
                                            {
                                                [_mapView clear];
                                                [arrForDriverMarkers removeAllObjects];
                                                 for (NSDictionary *dict in arr)
                                                {
                                                    previousProviderId=[dict valueForKey:PARAM_ID];
                                                    CLLocationCoordinate2D coor;
                                                    coor.latitude = [[[dict valueForKey:PARAM_PROVIDER_LOCATION] objectAtIndex:0]floatValue];
                                                    coor.longitude= [[[dict valueForKey:PARAM_PROVIDER_LOCATION] objectAtIndex:1]floatValue];
                                                    [arrForDriverMarkers addObject:[self setDriverMarker:coor withBearing:[[dict valueForKey:PARAM_BEARING] floatValue]]];
                                                    
                                                }
                                                numberOfProvider=[arr count];
                                                
                                            }
                                            
                                        }
                                        else
                                        {
                                            previousProviderId=@"abc";
                                            for (GMSMarker *marker in arrForDriverMarkers)
                                            {
                                                CABasicAnimation *theAnimation;
                                                theAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
                                                theAnimation.duration=0.5;
                                                theAnimation.repeatCount=0;
                                                theAnimation.autoreverses=NO;
                                                theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
                                                theAnimation.toValue=[NSNumber numberWithFloat:0.0];
                                                marker.opacity=[[theAnimation toValue] floatValue];
                                                [marker.layer addAnimation:theAnimation forKey:@"animateOpacity2"];
                                            }
                                            
                                        self.lblEstTime.text=[NSString stringWithFormat:@"0"];
                                        numberOfProvider=0;
                                     }
                                        
                                    }
                                });
                 
             }];
        
        }
}
-(GMSMarker*) setDriverMarker:(CLLocationCoordinate2D)driverLatlong withBearing:(float)bearing
{
    GMSMarker *marker=[[GMSMarker alloc]init];
    marker.icon= [UIImage imageNamed:@"pin_driver"];
    marker.position = driverLatlong;
    marker.rotation=bearing;
    marker.map = _mapView;
    CABasicAnimation *theAnimation;
    theAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration=0.5;
    theAnimation.repeatCount=0;
    theAnimation.autoreverses=NO;
    theAnimation.fromValue=[NSNumber numberWithFloat:0.0];
    theAnimation.toValue=[NSNumber numberWithFloat:1.0];
    [marker.layer addAnimation:theAnimation forKey:@"animateOpacity"];
    return marker;
}
#pragma mark -
#pragma mark iCarousel methods
- (NSInteger)numberOfItemsInMyView:(MyCarouselView *)myView
{
	return [arrForCarType count];
}
-(CarouselView *)myView:(MyCarouselView *)myView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    CarouselView *v= [[NSBundle mainBundle] loadNibNamed:@"Carousel" owner:self options:nil][0];
	if(arrForCarType.count>0)
	{
            CarTypeDataModal *car=[arrForCarType objectAtIndex:index];
            [v.img downloadFromURL:car.picture withPlaceholder:[UIImage imageNamed:@"car"]];
			v.label.text	=[[arrForCarType objectAtIndex:index] name];
            v.label.textColor=[UIColor labelTitleColor];
            if (index==_carousel.currentIndex)
			{
				v.label.font = [v.label.font fontWithSize:14.0f];
				v.selecter.hidden=NO;
            }
			else
			{
				v.label.font = [v.label.font fontWithSize:12.0f];
				v.selecter.hidden=YES;
			}
	}
	return v;
}
-(void) myView:(MyCarouselView *)myView didLongPressedItemAtIndex:(NSInteger)index
{
    CarTypeDataModal *car=[arrForCarType objectAtIndex:_carousel.currentIndex];
    [_lblTypeNameDialog setText:car.name];
    [_imgTypeImageDialog downloadFromURL:car.picture withPlaceholder:[UIImage imageNamed:@"car"]];
    [_lblTypeDescriptionValueDialog setText:car.description];
    [_lblTypeDescriptionValueDialog sizeToFit];
    _dialogForDescription=[[UtilityClass sharedObject]addShadow:_dialogForDescription];
    [_dialogForDescription setHidden:NO];
    
    [self.view bringSubviewToFront:_dialogForDescription];
}
-(void) myView:(MyCarouselView *)myView didSelectItemAtIndex:(NSInteger)index
{
  
    for (CarTypeDataModal *car in arrForCarType)
    { car.isSelected=NO;}
    CarTypeDataModal *car=[arrForCarType objectAtIndex:_carousel.currentIndex];
    car.isSelected=YES;
    [_carousel reloadItemAtIndexes:strSelectedCarIndex andCurrentSelectIndex:index];
    if (arrForCarType.count > 0 )
	{
        strCarTypeId= [NSString stringWithFormat:@"%@",[arrForCarType [_carousel.currentIndex] id_]];
        isSurgeHour=[arrForCarType [_carousel.currentIndex] isSurgeHour];
        surgeEndTime= [arrForCarType[_carousel.currentIndex] surgeEndHour];
        surgeStartTime=[arrForCarType[_carousel.currentIndex] surgeStartHour];
        strSelectedCarIndex=_carousel.currentIndex;
        surgeMultiplier=[arrForCarType[_carousel.currentIndex] sugrgeMultiplier];
        [self getNearByProvider];
    }
    
}
- (IBAction)onClickBtnCloseDialogForTypeDescription:(id)sender {
    [_dialogForDescription setHidden:YES];
 }

#pragma mark -Time And Date Related
-(void)setTimerToCheckDriverStatus
{
	timerForCheckReqStatus = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkForRequestStatus) userInfo:nil repeats:YES];
}
-(NSArray*) getTimeAndDistance:(CLLocationCoordinate2D) src_location destLocation:(CLLocationCoordinate2D)dest_location
{
	NSArray *arrayForTimeAndDistance;
	NSString *src_lat=[NSString stringWithFormat:@"%f",src_location.latitude];
	NSString *src_long=[NSString stringWithFormat:@"%f",src_location.longitude];
	NSString *dest_lat=[NSString stringWithFormat:@"%f",dest_location.latitude];
	NSString *dest_long=[NSString stringWithFormat:@"%f",dest_location.longitude];
	NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%@,%@&destinations=%@,%@&key=%@",src_lat,  src_long, dest_lat, dest_long, GoogleServerKey];
	NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
	NSData *jsonData = [NSData dataWithContentsOfURL:url];
	if(jsonData != nil)
	{
        NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
		NSString *status=[[[[[dict valueForKey:GOOGLE_PARAM_ROWS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_ELEMENTS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_STATUS];
		if ([status isEqualToString:GOOGLE_PARAM_STATUS_OK])
		{
			NSString *distance=[[[[[[dict valueForKey:GOOGLE_PARAM_ROWS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_ELEMENTS] objectAtIndex:0] objectForKey:GOOGLE_PARAM_DISTANCE] valueForKey:GOOGLE_PARAM_VALUES];
			NSString *second=[[[[[[dict valueForKey:GOOGLE_PARAM_ROWS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_ELEMENTS] objectAtIndex:0] objectForKey:GOOGLE_PARAM_DURATION] valueForKey:GOOGLE_PARAM_VALUES];
			NSString *time=[[UtilityClass sharedObject]secondToTime:[second intValue]];
			arrayForTimeAndDistance=[[NSArray alloc]initWithObjects:distance, time,second, nil];
		}
		else
		{
			
		}
    }
	else
	{
		//NSlog(@"API NOT CALLED");
	}
	return arrayForTimeAndDistance;
	
}
#pragma mark-ResetAllVariables
/**Used To intialize All Variables*/
-(void)resetVariable
{
    arrForPredictedAddresses=[[NSMutableArray alloc]init];
    arrForCarType=[[NSMutableArray alloc]init];
    arrForDriverMarkers=[[NSMutableArray alloc]init];
    dictPlace=[[NSDictionary alloc]init];
    numberOfProvider=0;
    strPaymentMode=YES;
    strForDestLatitude=@"";
    strForDestLongitude=@"";
    strUserId=PREF.userId;
	strUserToken=PREF.userToken;
    strSubAdminArea=@"";
	flag=0;strPickUpLat=@"";strPickUpLong=@"";strCity=@"";strCountry=@"";strCarTypeId=nil;strTripId=0;
}

/**Used To Pass User Token, Car Id To Get Fare Estimate File*/
-(NSDictionary*)getData
{
    NSDictionary *dict= [[NSDictionary alloc] initWithObjectsAndKeys:strUserId, @"strUserId", strUserToken, @"strUserToken",strCarTypeId, @"strCarTypeId", nil];
    return dict;
}
-(void)onDateSelected:(NSString *)SelectedDate andHour:(NSInteger)hours
{
    startDateAndTimeForFutureTrip=SelectedDate;
    if([self checkSurgeForFutureRequestStartTime:surgeStartTime surgeEndTime:surgeEndTime withServerTime:hours])
    {
        dialogForSurgePriceFutureTrip=[[CustomAlertWithTitle alloc]initWithTitle:[NSString stringWithFormat:@"%@ %@x ",NSLocalizedString(@"SURGE_COST", nil),surgeMultiplier]  message:NSLocalizedString(@"SURGE_MESSAGE", nil)   delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_CANCEL",nil) otherButtonTitles:NSLocalizedString(@"ALERT_BTN_OK",nil)];
    }
    else
    {
		isRideNow = 1;
		[self getFavDriver];
    }
}
-(void)wsFutureRequest:(BOOL)isSurge
{
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_BOOKING_RIDE", nil)];
    
    NSString *stringDestAddress=@"";
    if (_txtDestinationAddress.text)
    {
     stringDestAddress=_txtDestinationAddress.text;
    }
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:[NSNumber numberWithBool:isSurge] forKey:PARAM_IS_SURGE_HOUR];
    [dictParam setValue:strUserId forKey:PARAM_USER_ID];
    [dictParam setValue:strUserToken forKey:PARAM_TOKEN];
    [dictParam setValue:_txtPickupAddress.text forKey:PARAM_TRIP_SOURCE_ADDRESS];
    [dictParam setValue:dest_address forKey:PARAM_DESTINATION_LOCATION];
    [dictParam setValue:[NSNumber numberWithBool:strPaymentMode] forKey:PARAM_PAYMENT_MODE];
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    [dictParam setValue:tzName forKey:PARAM_TIME_ZONE];
    [dictParam setValue:startDateAndTimeForFutureTrip forKey:PARAM_START_TIME];
    [dictParam setValue:strPickUpLat forKey:PARAM_LATITUDE];
    [dictParam setValue:strPickUpLong forKey:PARAM_LONGITUDE];
    [dictParam setValue:strForDestLatitude forKey:PARAM_DEST_LATITUDE];
    [dictParam setValue:strForDestLongitude forKey:PARAM_DEST_LONGITUDE];
    [dictParam setValue:strCarTypeId forKey:PARAM_SERVICE_TYPE];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
    [afn getDataFromPath:USER_FUTURE_TRIP withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^
                        {
                            NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data							options:kNilOptions
                                                                                           error:nil];
                            if (jsonResponse)
                            {
                                if([[jsonResponse valueForKey:SUCCESS]boolValue])
                                {
                                    [APPDELEGATE hideLoadingView];
                                     NSString *messageCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:MESSAGE_CODE]];
                                    [[UtilityClass sharedObject] showToast:NSLocalizedString(messageCode, nil)];
                                    
                                }
                                else
                                {
                                    [APPDELEGATE hideLoadingView];
                                    NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                                    [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                                }
                            }
                            [APPDELEGATE hideLoadingView];
                        });
     }];
    
    
}
-(void)updateUIIdealState
{
    [self getLocation];
    [_txtPickupAddress setClearButtonMode:UITextFieldViewModeAlways];
    self.tableForCity.hidden=true;
    [_viewForPayment setHidden:YES];
    _viewForDestinationbg.hidden=true;
	_viewForTypeNote.hidden=true;
    self.btnRideLater.hidden=true;
    self.btnRideNow.hidden=true;
    [self setFareEstimateButton];
  /*  if (!_btnRightBar.isHidden)
    {
        self.btnRightBar.hidden=YES;
        _getEstimate.frame=_btnRightBar.frame;
    }*/
    self.txtPickupAddress.enabled=true;
    self.viewForPickUp.hidden=NO;
    self.viewForiCarousel.hidden=NO;
    self.txtDestinationAddress.text=@"";
    dest_coordinate.latitude=0;
    dest_coordinate.longitude=0;
    flag=0;
    isPickupClicked = NO;
    _btnConfirmDestination.hidden=true;
    _imgDestinationPin.hidden=true;
}
-(void)updateUIActiveState
{
    self.txtPickupAddress.enabled=NO;
    [_txtPickupAddress setClearButtonMode:UITextFieldViewModeNever];
    [_viewForPayment setHidden:NO];
    self.viewForDestinationbg.hidden=false;
	self.viewForTypeNote.hidden = false;
    self.btnRideLater.hidden=false;
    self.btnRideNow.hidden=false;
    [_btnRideNow setEnabled:YES];
    [_btnRideNow setAlpha:1];
    [self setTwoRightBackBarItem:[UIImage imageNamed:@"back"]];
    self.viewForPickUp.hidden=YES;
    self.viewForiCarousel.hidden=YES;
    //self.txtDestinationAddress.text=@"";
}
-(void) getCountryAndCity:(CLLocation *)bestLocation
{
    NSLog(@"%f %f", bestLocation.coordinate.latitude, bestLocation.coordinate.longitude);
    userDefaultLanguages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en_US", nil] forKey:@"AppleLanguages"];

    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;

    [geocoder reverseGeocodeLocation:bestLocation
                   completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error)
         {
             NSLog(@"Geocode failed with error: %@", error);
             return;
         }
         
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         if (placemark.locality)
         {
             
             strCity=placemark.locality;
         }
         else
         {
             if(placemark.subAdministrativeArea)
                 strCity=placemark.subAdministrativeArea;
             else
                 strCity=placemark.administrativeArea;
             
         }
         strCountry=placemark.country;
         if (placemark.subAdministrativeArea)
         {
             strSubAdminArea=placemark.subAdministrativeArea;
         }
         else
         {
             strSubAdminArea=placemark.administrativeArea;
         }
         if ([UtilityClass isEmpty:strCity] || strCity==nil)
         {
             [[UtilityClass sharedObject]displayAlertWithTitle:NSLocalizedString(@"ALERT_LOCATION_NOT_FOUND", nil)  andMessage:NSLocalizedString(@"ALERT_MSG_ENTER_VALID_LOCATION",nil )];
             strCity=@"";
         }
         [[NSUserDefaults standardUserDefaults] setObject:userDefaultLanguages forKey:@"AppleLanguages"];
         if ([strTempCity isEqualToString:strCity])
         {
             [self getNearByProvider];
         }
         else
         {
             [self getServiceTypes];
         }
     }];
}

/**Get Fare Estimate*/
-(void)getFareEstimate:(NSString *)strTime andDistance:(NSString*)strDistance
{
        NSDictionary *dict=[self getData];
        strUserToken=[dict valueForKey:@"strUserToken"];
        strUserId=[dict valueForKey:@"strUserId"];
        strCarTypeId=[dict valueForKey:@"strCarTypeId"];
        if (![UtilityClass isEmpty:strTime] && ![UtilityClass isEmpty:strDistance] )
            {
                NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                [dictParam setObject:strDistance forKey:PARAM_DISTANCE];
                [dictParam setObject:strTime forKey:PARAM_TIME];
                [dictParam setObject:strUserId forKey:PARAM_USER_ID];
                [dictParam setObject:strUserToken forKey:PARAM_TOKEN];
                [dictParam setObject:strCarTypeId forKey:PARAM_SERVICE_TYPE];
                [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOADING_FAREESTIMATE", nil)];
                AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
                [afn getDataFromPath:USER_GET_FARE_ESTIMATE withParamData:dictParam withBlock:^(id response, NSError *error)
                 {
                     dispatch_async(dispatch_get_main_queue(), ^
                            {

                                [APPDELEGATE hideLoadingView];
                                if ([[Parser sharedObject]parseFareEstimate:response])
                                {
                                    if (viewEstimate) {
                                        [viewEstimate removeFromSuperview];
                                        viewEstimate=nil;
                                    }
                                    else
                                    {
                                     viewEstimate=[FareEstimate getViewFareEstimatewithParent:self];
                                        [viewEstimate setDataForEstimateView:[arrForCarType objectAtIndex:strSelectedCarIndex] isSurge:[self checkSurgeStartTime:surgeStartTime surgeEndTime:surgeEndTime withServerTime:[CurrentTrip sharedObject].serverTime]];
                                    
                                        //[self.view addSubview:viewEstimate];
                                        [APPDELEGATE.window addSubview:viewEstimate];
                                    }
                                }
                            });
				 }];
        }
}
- (IBAction)onClickBtnDestinationDrag:(id)sender
{
    if(isDestinationDragFlagOn)
    {
        [_imgDestinationPin setHidden:YES];
        [_btnConfirmDestination setHidden:YES];
        [_btnRideNow setHidden:NO];
        [_btnRideLater setHidden:NO];
        destMarker.map=nil;
        destMarker = [[GMSMarker alloc] init];
        destMarker.position = finalDestCoordinate ;
        destMarker.icon = [UIImage imageNamed:@"pin_destination"];
        destMarker.map = self.mapView;
        [self setNewPlaceData:final_dest_address];
        flag=0;
        isDestinationDragFlagOn=NO;
    }
    else
    {
        flag=1;
        destMarker.map=nil;
        if (finalDestCoordinate.latitude && finalDestCoordinate.longitude) {
           
            [_mapView animateToLocation:finalDestCoordinate];
        }
        [_btnRideNow setHidden:YES];
        [_btnRideLater setHidden:YES];
        [_imgDestinationPin setHidden:NO];
        [self.view bringSubviewToFront:_imgDestinationPin];
        [_btnConfirmDestination setHidden:NO];
        isDestinationDragFlagOn=YES;
    }
}
- (IBAction)onClickBtnConfirmDestination:(id)sender
{
    if (![UtilityClass isEmpty:dest_address])
    {
        destMarker.map=nil;
        destMarker = [[GMSMarker alloc] init];
        destMarker.position = dest_coordinate ;
        destMarker.icon = [UIImage imageNamed:@"pin_destination"];
        destMarker.map = self.mapView;
        final_dest_address=dest_address;
        finalDestCoordinate=dest_coordinate;
        [self focusMapToShowAllMarkers:source_coordinate andDestinationLatLong:dest_coordinate];
        [_imgDestinationPin setHidden:YES];
        [_btnConfirmDestination setHidden:YES];
        [_btnRideNow setHidden:NO];
        [_btnRideLater setHidden:NO];
        flag=0;
        isDestinationDragFlagOn=NO;
    }
}
#pragma mark-CUSTOM DIALOG DELEGATE METHODS
-(void)onClickCustomDialogOk:(CustomAlertWithTitle *)view
{
    if (view==dialogForRideLater)
    {
        [UIView animateWithDuration:0.5 animations:^{
            [self updateUIActiveState];
        }
                         completion:^(BOOL finished)
         {
             if (clientmarker.map)
             {
                 clientmarker.position = source_coordinate;
             }
             else
             {
                 clientmarker.map=_mapView;
                 clientmarker.position = source_coordinate;
             }
             [self cameraFocus:source_coordinate];
         }
		 ];

        [_btnRideNow setEnabled:NO];
        [_btnRideNow setAlpha:0.5];
    
    }
    if (view==gpsEnableDialog)
    {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
    if (view==dialogForRequestAgain)
    {
        dialogForRequestAgain=nil;
        [self wsCreateTripRequest];
    }
    if(view==viewForAdminAlert)
    {
        NSString *goToEmailApp=[NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@",PREF.userEmail,@"title",@"Message"];
        NSString *url = [goToEmailApp stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
        [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url]];
        viewForAdminAlert=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"ALERET_ADMIN_REVIEW", nil) message:NSLocalizedString(@"ALERET_ADMIN_REVIEW_MSG", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"LOGOUT", nil) otherButtonTitles:NSLocalizedString(@"ADMIN_EMAIL",nil)];
    }
    if(view==dialogForSurgePrice)
    {
        [self wsCreateTripRequest];
    }
    if (view==dialogForSurgePriceFutureTrip)
    {
        //[self wsFutureRequest:YES];
			isRideNow = 1;
			[self getFavDriver];
    }
}
-(void)showPaymentOptions
{
 /*ViewForCard*/
    viewForCard=[[UIView alloc]initWithFrame:_viewForPayment.frame];
    UILabel *lblCard=[[UILabel alloc]initWithFrame:_lblPayment.frame];
   
    UIButton *imgCard=[[UIButton alloc]initWithFrame:_imgPayment.frame];
   [imgCard setBackgroundImage:[UIImage imageNamed:@"circle_gray"] forState:UIControlStateNormal];
    [imgCard setContentMode:UIViewContentModeCenter];
    [imgCard setUserInteractionEnabled:NO];
    if ([_lblPayment.text isEqualToString:NSLocalizedString(@"CASH", nil)])
    {
        [viewForCard setTag:0];
        [lblCard setText:NSLocalizedString(@"CARD",nil)];
        [imgCard setImage:[UIImage imageNamed:@"icon_card" ] forState:UIControlStateNormal];
        
    }
    else
    {
        [lblCard setText:NSLocalizedString(@"CASH",nil)];
        [imgCard setImage:[UIImage imageNamed:@"icon_cash" ] forState:UIControlStateNormal];
        [viewForCard setTag:1];
    }
    
    [imgCard addTarget:self action:@selector(onClickPaymentOption:) forControlEvents:UIControlEventTouchUpInside];
    [viewForCard addSubview:imgCard];
    [viewForCard addSubview:lblCard];
    [self adjustFrameProperty:imgCard.imageView andLabel:lblCard inView:viewForCard];
    paymentGestureCard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickPaymentOption:)]; // Declare the Gesture.
    paymentGestureCard.delegate = self;
    [self.view addSubview:viewForCard];
    [viewForCard addGestureRecognizer:paymentGestureCard];
    [UIView animateWithDuration:0.4f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGAffineTransform scaleTrans  = CGAffineTransformMakeScale(1.0f, 1.0f);
                         CGAffineTransform lefttorightTrans  = CGAffineTransformMakeTranslation(0.0f,_viewForPayment.frame.size.height);
                         viewForCard.transform = CGAffineTransformConcat(scaleTrans, lefttorightTrans);
                         [imgCard setAlpha:1];
                         [lblCard setAlpha:0.7];
                     }
                     completion:^(BOOL finished){
                         if (finished)
                         {
                            
                         }
                     }];
}
-(void)hidePaymentOptions
{
                         [UIView animateWithDuration:0.3f
                                               delay:0.0f
                                             options:UIViewAnimationOptionCurveEaseIn
                                          animations:^{
                                              [viewForCard setAlpha:0];
                                              CGAffineTransform scaleTransB  = CGAffineTransformMakeScale(1.0f, 1.0f);
                                              CGAffineTransform lefttorightTransB  = CGAffineTransformMakeTranslation(0.0f,0.0f);
                                              viewForCard.transform = CGAffineTransformConcat(scaleTransB, lefttorightTransB);
                                          }
                                          completion:^(BOOL finished){
                                              if (finished)
                                              {
                                                  [viewForCard setHidden:YES];
                                                  viewForCard=nil;
                                                  [_viewForPayment setUserInteractionEnabled:YES];
                                              }
                                          }];
}
-(void)adjustFrameProperty:(UIImageView*)image andLabel:(UILabel*)label inView:(UIView*)view
{
    CGRect frame=image.frame;
    frame.size.height=MIN(image.frame.size.width, image.frame.size.height) ;
    frame.size.width=frame.size.height;
    image.frame=frame;
    
    [view setBackgroundColor:[UIColor clearColor]];
    [image setBackgroundColor:[UIColor clearColor]];
    [label setBackgroundColor:[UIColor lightGreenColor]];
    [image setContentMode:UIViewContentModeCenter];
    [label setTextColor:[UIColor labelTitleColor]];
    [label setFont:_lblPayment.font];
    [label setTextAlignment:NSTextAlignmentCenter];
    view.frame = _viewForPayment.frame;
    label.layer.cornerRadius = 5.0f;
    label.layer.masksToBounds=YES;
}
-(void)onClickClose:(CustomAlertWithTitle *)view
{
    if (view==dialogForRequestAgain)
    {
		self.navigationController.navigationBarHidden = NO;
        dialogForRequestAgain=nil;
        [self updateUIIdealState];
        [self setLocalization];
    }
    else if(view==dialogForRideLater)
    {
        clientmarker.map=nil;
        destMarker.map=nil;
        [self updateUIIdealState];
    }
    if (view==viewForAdminAlert)
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOADING_LOGOUT", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:PREF.userId forKey:PARAM_USER_ID];
        [dictParam setObject:PREF.userToken forKey:PARAM_TOKEN];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
        [afn getDataFromPath:USER_LOGOUT withParamData:dictParam withBlock:^(id response, NSError *error)
        {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[AppDelegate sharedAppDelegate]hideLoadingView];
                    if([[Parser sharedObject] parseSuccess:response] )
                    {
                        PREF.UserLogin=NO;
                        [[UtilityClass sharedObject] animateHide:viewForAdminAlert];
                        [APPDELEGATE goToMain];
                    }
                });
        }];
	}
}
-(int)checkSurgeStartTime:(NSString*)surgeStartHour surgeEndTime:(NSString*)surgeEndHour withServerTime:(NSString*)serverTime
{
    @try
    {
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:DATE_TIME_FORMAT_WEB];
        NSDateFormatter *tempdateFormatter=[[NSDateFormatter alloc]init];
        [tempdateFormatter setDateFormat:DATE_TIME_FORMAT_WEB];
        [tempdateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        NSDate *serverDateTime=[tempdateFormatter dateFromString:serverTime];
        [tempdateFormatter setTimeZone: [NSTimeZone timeZoneWithName:[CurrentTrip sharedObject].timeZone]];
        NSString *tempDate=[tempdateFormatter stringFromDate:serverDateTime];
        NSDate *tempDate1=[dateFormatter dateFromString:tempDate];
        NSCalendar *cal=[NSCalendar currentCalendar];
        NSInteger serverHour=[cal component:NSCalendarUnitHour fromDate:tempDate1] ;
        if ([surgeStartHour integerValue]  < serverHour &&
            [surgeEndHour integerValue] > serverHour && isSurgeHour)
        {
            return 1;
        }
    }
    @catch (NSException *e)
    {
    }
    return 0;
}
-(int)checkSurgeForFutureRequestStartTime:(NSString*)surgeStartHour surgeEndTime:(NSString*)surgeEndHour withServerTime:(NSInteger)bookingTime
{
    @try
    {
        if ([surgeStartHour integerValue]  < bookingTime &&
            [surgeEndHour integerValue] > bookingTime && isSurgeHour)
        {
            return 1;
        }
    }
    @catch (NSException *e)
    {
    }
    return 0;
}
-(void)setTwoRightBackBarItem:(UIImage *)image
{
    self.navigationItem.rightBarButtonItems=nil;
    self.navigationItem.hidesBackButton = YES;
    btnBack=[UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame=CGRectMake(0, 0, 25, 25);
    [btnBack setImage:image forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(onClickBtnRight:) forControlEvents: UIControlEventTouchUpInside];
    UIBarButtonItem *menu=[[UIBarButtonItem alloc]initWithCustomView:btnBack];
    self.navigationItem.hidesBackButton = YES;
    btnGetFareEstimate=[UIButton buttonWithType:UIButtonTypeCustom];
    btnGetFareEstimate.frame=CGRectMake(0, 0, 25, 25);
    [btnGetFareEstimate setImage:[UIImage imageNamed:@"pin_fareestimate"] forState:UIControlStateNormal];
    [btnGetFareEstimate addTarget:self action:@selector(onClickBtnGetEstimate:) forControlEvents: UIControlEventTouchUpInside];
    UIBarButtonItem *bar=[[UIBarButtonItem alloc]initWithCustomView:btnGetFareEstimate];
    [UIView setAnimationsEnabled:NO];
    NSArray *arrBtns = [[NSArray alloc]initWithObjects:menu,bar,nil];
    self.navigationItem.rightBarButtonItems = arrBtns;
    [UIView setAnimationsEnabled:YES];
}
-(void)checkPush
{
    if (checkApprove)
    {
     
        [APPDELEGATE goToMap];
    }
    else if(checkDecline)
    {
     
        if (viewForAdminAlert==nil)
        {
            viewForAdminAlert=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"ALERET_ADMIN_REVIEW", nil) message:NSLocalizedString(@"ALERET_ADMIN_REVIEW_MSG", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"LOGOUT", nil) otherButtonTitles:NSLocalizedString(@"ADMIN_EMAIL",nil)];
        }
    }
    else
    {
        
    }
}

- (IBAction)onClickContinueRequest:(id)sender
{
	if (isRideNow == 0) {
		[self wsCreateTripRequest];
		return;
	}
	else if(isRideNow == 1)
	{
		[self wsFutureRequest:NO];
		return;
	}
	else
		{
	
	self.navigationController.navigationBarHidden = false;
	self.viewForFavDriver.hidden = true;
	
	if(source_coordinate.latitude && source_coordinate.longitude)
		{
		if (self.txtPickupAddress.text.length >1  && numberOfProvider!=0)
			{
			[UIView animateWithDuration:0.5 animations:^{
				isPickupClicked = YES;
				[self updateUIActiveState];
			}
							 completion:^(BOOL finished)
			 {
			 if (clientmarker.map)
				 {
				 clientmarker.position = source_coordinate;
				 }
			 else
				 {
				 clientmarker.map=_mapView;
				 clientmarker.position = source_coordinate;
				 }
			 [self cameraFocus:source_coordinate];
			 }
			 ];
			}
		else
			{
			if(_txtPickupAddress.text.length<1)
				
				[[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_PLEASE_ENTER_PICK_UP_ADDRESS", nil)];
			else
				{
				dialogForRideLater=[[CustomAlertWithTitle alloc] initWithTitle:NSLocalizedString(@"ALERT_TITLE_RIDE_LATER",nil) message:NSLocalizedString(@"ALERT_MSG_RIDE_LATER",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_CANCEL",nil) otherButtonTitles:[NSLocalizedString(@"RIDE_LATER",nil) uppercaseString]];
				[self.view bringSubviewToFront:dialogForRideLater];
				}
			}
		}
	else
		{
		[[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_LOCATION", nil)];
		}
		}
}
-(void)setFareEstimateButton
{
    self.navigationItem.rightBarButtonItems=nil;
    self.navigationItem.hidesBackButton = YES;
    btnGetFareEstimate=[UIButton buttonWithType:UIButtonTypeCustom];
    [btnGetFareEstimate addTarget:self action:@selector(onClickBtnGetEstimate:) forControlEvents: UIControlEventTouchUpInside];
    btnGetFareEstimate.frame=CGRectMake(0, 0, 25, 25);
    [btnGetFareEstimate setImage:[UIImage imageNamed:@"pin_fareestimate"] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnGetFareEstimate];}

@end
