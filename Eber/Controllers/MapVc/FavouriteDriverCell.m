//
//  FavouriteDriverCell.m
//  Eber
//
//  Created by Elluminati on 25/02/17.
//  Copyright © 2017 Jaydeep. All rights reserved.
//

#import "FavouriteDriverCell.h"

@implementation FavouriteDriverCell

- (void)awakeFromNib {
    [super awakeFromNib];
	
	self.imgDriver.layer.cornerRadius = self.imgDriver.frame.size.height / 2;
	self.imgDriver.clipsToBounds = YES;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
