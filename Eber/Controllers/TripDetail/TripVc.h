//
//  TripVc.h
//  
//
//  Created by My Mac on 7/8/16.
//
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "CurrentTrip.h"
#import "NSObject+Constants.h"
#import "CustomCancelTripDialog.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "CustomAlertWithTextInput.h"
#import "CustomAlertWithTitle.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>

@interface TripVc : UIViewController
<GMSMapViewDelegate,
CLLocationManagerDelegate,
UIScrollViewDelegate,
CustomCancelTripDialogDelegate,
CustomAlertWithTextInput,
CustomAlertDelegate,
UIGestureRecognizerDelegate,
AVAudioPlayerDelegate,
MFMailComposeViewControllerDelegate>
{
    BOOL strPaymentMode;
	NSString
    *strUserId,
    *strTripId,
    *strUserToken,
    *strProviderPhone,
	*strTime,
	*strDistance,
	*strForDestLat,
	*strForDestLong,
	*strForProviderLat,
	*strForProviderLong,
    *strForLatitude,
    *strForLongitude,
    *strDestAddress,
    *strFinalDestinationAddress,
    *strForClientLat,
    *strForClientLong,
    *strForCancelRequest,
	
    *strForCity,
    *strForCountry;
	
    BOOL isProviderDetail,
    isDriverStarted,
    isTripStarted,
    isTripEnd,
    isDriverArived,
    isDestinationSet;
    double providerBearing;
    
    CurrentTrip *currentTrip;

    CLLocationManager *locationManager;
    GMSMutablePath *pathUpdates;
    GMSMarker *clientMarker,*driverMarker,*destinationMarker;
    NSMutableArray *arrPath;
	NSMutableArray *arrForPredictedAddresses;
    CLLocationCoordinate2D
    dest_coordinate,
    driver_coordinate,
    pickup_coordinate,
    current_coordinate;
    NSMutableDictionary *dictPlace;
    
   
}
/*!Used To Play Sound When Receive Notification*/
@property (strong, nonatomic) AVAudioPlayer *soundPlayer;
@property (weak, nonatomic) IBOutlet UIButton *btnTransparent;

- (IBAction)onClickBtnCall:(id)sender;
@property (strong , nonatomic) NSTimer *timerForCheckReqStatus;
@property (strong , nonatomic) NSTimer *timerForSOS,*timerForWaitingTime;
@property (weak, nonatomic) IBOutlet UITableView *tableForCity;
@property (nonatomic, assign) ProviderStatus providerStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnDestinatioDrag;
- (IBAction)onClickBtnDestinationDrag:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imgDestinationPin;
@property (weak, nonatomic) IBOutlet UIButton *btnApplyPromo;

/*Views*/
@property (weak, nonatomic) IBOutlet UITextField *txtPickUpAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;

@property (strong, nonatomic) IBOutlet GMSMapView *mapview_;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirmDestination;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;
@property (strong, nonatomic) IBOutlet UITextField *txtDestLocation;
/*View For Status*/
@property (strong, nonatomic) IBOutlet UIView *viewForStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblForStatus;
@property (strong, nonatomic) IBOutlet UIButton *btnCloseView;

/*Payment View*/
@property (weak, nonatomic) IBOutlet UILabel *lblPayment;
@property (weak, nonatomic) IBOutlet UIButton *imgPayment;
/*Driver Info View*/
@property (strong, nonatomic) IBOutlet UIView *viewForDriver;
@property (strong, nonatomic) IBOutlet UIImageView *imgProPic;
@property (strong, nonatomic) IBOutlet UILabel *lblProviderName;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelRequest;
@property (weak, nonatomic) IBOutlet UIView *viewForOngoingTrip;
@property (weak, nonatomic) IBOutlet UILabel *lblTripTimeAndCarId;
@property (weak, nonatomic) IBOutlet UILabel *lblTripDistanceAndCarNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblTripDistanceAndCarNumberValue;
@property (weak, nonatomic) IBOutlet UILabel *lblTripTimeAndCarModel;

/*View for PickUp Address*/
@property (weak, nonatomic) IBOutlet UIView *viewForPickUpAddress;

/*View for Destination Address*/
@property (weak, nonatomic) IBOutlet UIView *viewForDestinationAddress;
@property (weak, nonatomic) IBOutlet UIView *viewForPayment;
-(void)checkForTripStatus;
/*Actions*/
- (IBAction)onClickBtnClose:(id)sender;
- (IBAction)onClickBtnRequestCancel:(id)sender;
- (IBAction)onClickBtnApplyPromo:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTripId;
@property (weak, nonatomic) IBOutlet UILabel *lblTripIdValue;
/*View For Waiting Time*/
@property (weak, nonatomic) IBOutlet UIView *viewForWaitingTime;
@property (weak, nonatomic) IBOutlet UILabel *lblWaitingTimeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblWaitingTimeValue;
@property (weak, nonatomic) IBOutlet UIButton *btnshare;
- (IBAction)onClickBtnShareEta:(id)sender;


@end
