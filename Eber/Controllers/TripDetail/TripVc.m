//
//  TripVc.m
//  
//
//  Created by My Mac on 7/8/16.
//
//

#import "TripVc.h"
#import "AppDelegate.h"
#import "NSObject+Constants.h"
#import "AFNHelper.h"
#import "UIColor+Colors.h"
#import "SWRevealViewController.h"
#import "UtilityClass.h"
#import "CurrentTrip.h"
#import <CoreLocation/CoreLocation.h>
#import "Parser.h"
#import "PreferenceHelper.h"
#import "UIImageView+image.h"
#define degreesToRadians(x) (M_PI * x / 180.0)
#define radiansToDegrees(x) (x * 180.0 / M_PI)
@interface TripVc ()
{
    CustomAlertWithTextInput *dialogForPromoCode;
    CustomAlertWithTitle *dialogForSos;
    NSInteger timeForSos,statusProvider;
    NSMutableArray *userDefaultLanguages;
    GMSPolyline *polyLinePath,*polyLineCurrentProviderPath;
    GMSMutablePath *currentProviderPath;
    CLLocationCoordinate2D previousDriverLatLong;
    BOOL isCameraBearingChange,isPathCurrentPathDraw;
    double mapBearing;
    NSInteger currentWaitingTime;
    
}
@end
@implementation TripVc
{
    BOOL isDestinationDragFlagOn,isStatusSaved,isMapBounds,isPathDrawn;
    UIView *viewForCard;
    UITapGestureRecognizer *paymentGesture,*paymentGestureCard,*paymentGestureCash;
}
@synthesize mapview_,providerStatus,soundPlayer;
#pragma mark-View Life-Cycle

- (void)viewDidLoad
{
	[super viewDidLoad];
    isPathCurrentPathDraw=FALSE;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appplicationIsActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didEnterInBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    isPathDrawn=true;
    statusProvider=ProviderStatusComing;
    isStatusSaved=NO;
    isCameraBearingChange=FALSE;
    providerBearing=0;
    dictPlace=[[NSMutableDictionary alloc]init];
    arrForPredictedAddresses=[[NSMutableArray alloc]init];
    currentTrip=[CurrentTrip sharedObject];
    clientMarker=[[GMSMarker alloc]init];
    driverMarker=[[GMSMarker alloc]init];
    driverMarker.icon= [UIImage imageNamed:@"pin_driver"];
    clientMarker.icon= [UIImage imageNamed:@"pin_pickup"];
    [driverMarker setGroundAnchor:CGPointMake(0.5, 0.5)];
    strUserId=PREF.userId;
	strUserToken=PREF.userToken;
    strTripId=PREF.tripId;
    isDriverStarted=NO;
    mapview_.myLocationEnabled = NO;
    mapview_.delegate=self;
    [self setLocalization];
    [self customSetup];
	[self updateLocationManager];
	[self getLocation];
    [self checkForTripStatus];
    [self setProviderInfo];
    [self performSelectorOnMainThread:@selector(startCheckRequestTimer) withObject:nil waitUntilDone:YES];
  
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[AppDelegate sharedAppDelegate] hideLoadingView];
    [_btnMenu setTitle:NSLocalizedString(@"TITLE_ON_GOING_TRIP", nil) forState:UIControlStateNormal];
    [_btnMenu setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
	self.navigationController.navigationBarHidden = NO;
}
-(void)appplicationIsActive:(NSNotification *)notification
{
    self.imgDestinationPin.center=mapview_.center;
    [self getGooglePathFromServer];
    [self checkForTripStatus];
    [self performSelectorOnMainThread:@selector(startCheckRequestTimer) withObject:nil waitUntilDone:YES];
}
-(void)didEnterInBackground:(NSNotification *)notification
{
    [currentProviderPath removeAllCoordinates];
    //[polyLineCurrentProviderPath setMap:nil];
    isPathCurrentPathDraw=FALSE;
   [self performSelectorOnMainThread:@selector(stopTimer) withObject:nil waitUntilDone:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [_imgProPic setRoundImageWithColor:[UIColor borderColor]];
    [_imgPayment setFrame:CGRectMake(_imgPayment.frame.origin.x, _imgPayment.frame.origin.y,MIN(_imgPayment.frame.size.height, _imgPayment.frame.size.width), MIN(_imgPayment.frame.size.height, _imgPayment.frame.size.width))];
    [_lblPayment setFrame:CGRectMake(_imgPayment.frame.origin.x+_imgPayment.frame.size.width+2, _lblPayment.frame.origin.y,_lblPayment.frame.size.width,_lblPayment.frame.size.height)];
    [_btnApplyPromo setFrame:CGRectMake(_btnApplyPromo.frame.origin.x, _btnApplyPromo.frame.origin.y,MIN(_btnApplyPromo.frame.size.height, _btnApplyPromo.frame.size.width), MIN(_btnApplyPromo.frame.size.height, _btnApplyPromo.frame.size.width))];
    [self adjustFrameProperty:_imgPayment.imageView andLabel:_lblPayment inView:_viewForPayment];;
    _viewForPickUpAddress.layer.cornerRadius = 2;
    _viewForPickUpAddress.layer.masksToBounds = YES;
    _viewForDestinationAddress.layer.cornerRadius = 2;
    _viewForDestinationAddress.layer.masksToBounds = YES;
}
#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}
-(void)setProviderInfo
{
    
    self.lblProviderName.text=[NSString stringWithFormat:@"%@ %@",currentTrip.providerFirstName,currentTrip.providerLastName];
    self.lblTripTimeAndCarModel.text=currentTrip.providerCarModal;
    self.lblTripDistanceAndCarNumberValue.text=currentTrip.providerCarNumber;
    [_imgProPic downloadFromURL:currentTrip.providerProfileImage withPlaceholder:[UIImage imageNamed:@"user"]];
}
#pragma mark - Localization
-(void)setLocalization
{
    /*label setUp*/
    [_lblTripId setTextColor:[UIColor labelTextColor]];
    [_lblTripId setText:NSLocalizedString(@"TRIP_ID", nil)];
    
    [_lblTripIdValue setTextColor:[UIColor textColor]];
    [_lblTripIdValue setText:NSLocalizedString(@"00", nil)];
    
    [_lblTripTimeAndCarId setTextColor:[UIColor labelTextColor]];
    [_lblTripTimeAndCarId setText:NSLocalizedString(@"CAR_ID", nil)];
   
    [_lblTripDistanceAndCarNumber setTextColor:[UIColor labelTextColor]];
    [_lblTripDistanceAndCarNumber setText:NSLocalizedString(@"CAR_NUMBER", nil)];
    
    [_lblProviderName setTextColor:[UIColor textColor]];
    [_lblTripTimeAndCarModel setTextColor:[UIColor textColor]];
    [_lblTripDistanceAndCarNumberValue setTextColor:[UIColor textColor]];
    [_lblForStatus setTextColor:[UIColor labelTextColor]];
    /*button setup*/
    [_btnApplyPromo setImage:[UIImage imageNamed:@"promo"] forState:UIControlStateNormal];
    [_btnApplyPromo setBackgroundColor:[UIColor clearColor]];
    [_btnApplyPromo setBackgroundImage:[UIImage imageNamed:@"circle_blue"] forState:UIControlStateNormal];
    
    [_btnCloseView setBackgroundColor:[UIColor buttonColor]];
    [_btnCloseView setTitle:[NSLocalizedString(@"BTN_CLOSE",nil) uppercaseString] forState:UIControlStateNormal];
    [_btnCloseView setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
   
    [_btnConfirmDestination setBackgroundColor:[UIColor buttonColor]];
    [_btnConfirmDestination setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [_btnConfirmDestination setTitle:[NSLocalizedString(@"CONFIRM", nil) uppercaseString] forState:UIControlStateNormal];
    [_btnConfirmDestination setHidden:YES];
    [_btnDestinatioDrag setBackgroundColor:[UIColor GreenColor]];
    
    _btnCancelRequest.titleLabel.numberOfLines = 1;
    _btnCancelRequest.titleLabel.adjustsFontSizeToFitWidth = YES;
    _btnCancelRequest.titleLabel.lineBreakMode = NSLineBreakByClipping;
    _btnCancelRequest.layer.cornerRadius=10;
    _btnCancelRequest.layer.masksToBounds=NO;
    
    [_btnCancelRequest setTitleColor:[UIColor buttonTextColor ] forState:UIControlStateNormal];
    [_btnCancelRequest setTitle:[NSLocalizedString(@"CANCEL_TRIP", nil) uppercaseString] forState:UIControlStateNormal];
    [_btnCancelRequest setBackgroundColor:[UIColor buttonColor]];

    /*view setUp*/
    [_imgDestinationPin setHidden:YES];
    self.navigationController.navigationBar.barTintColor = [UIColor GreenColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = [UIColor GreenColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.title=[NSLocalizedString(@"TITLE_ON_GOING_TRIP", nil) capitalizedString];
    
    
    [_btnTransparent setHidden:YES];
    _viewForStatus=[[UtilityClass sharedObject]addShadow:_viewForStatus];
    
    [_viewForDestinationAddress setBackgroundColor:[UIColor lightGreenColor]];
    [_viewForOngoingTrip setBackgroundColor:[UIColor GreenColor]];
   [_viewForPickUpAddress setBackgroundColor:[UIColor lightGreenColor]];
    [_tableForCity setHidden:YES];
    [_lblPayment setBackgroundColor:[UIColor GreenColor]];
    [_imgPayment.imageView setContentMode:UIViewContentModeCenter];
    [_imgPayment setBackgroundImage:[UIImage imageNamed:@"circle_black"] forState:UIControlStateNormal];

    
    
    paymentGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickBtnPayment:)]; // Declare the Gesture.
    paymentGesture.delegate = self;
    
    [_viewForPayment addGestureRecognizer:paymentGesture]; // Add Gesture to your view.
    [_viewForPayment setBackgroundColor:[UIColor clearColor]];
    
    /*textView SetUp*/
    [_txtPickUpAddress setTextColor:[UIColor labelTitleColor]];
    [_txtPickUpAddress setUserInteractionEnabled:NO];
    [_txtPickUpAddress setValue:[UIColor textPlaceHolderColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    [_txtDestLocation setTextColor:[UIColor labelTitleColor]];
    [_txtDestLocation setValue:[UIColor textPlaceHolderColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    [self.view bringSubviewToFront:_viewForDriver];
    [self.view bringSubviewToFront:_viewForPayment];
    [self.view bringSubviewToFront:_viewForOngoingTrip];
    [self.view bringSubviewToFront:_btnApplyPromo];
    
}
-(void)loadInvoice
{
    if (self.navigationController.visibleViewController == self)
    {
        [self performSegueWithIdentifier:SEGUE_TO_INVOICE sender:self];
        return;
    }
}
-(void) getProviderLatLong:(NSString *) strProviderId
{
	NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
	[dictParam setObject:strProviderId forKey:PARAM_PROVIDER_ID];
    [dictParam setObject:strTripId  forKey:PARAM_TRIP_ID];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
	[afn getDataFromPath:WS_GET_PROVIDER_LATLONG withParamData:dictParam withBlock:^(id response, NSError *error)
	 {
		 dispatch_async(dispatch_get_main_queue(), ^
					 {
						 NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                         NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data							options:kNilOptions
                                                                                        error:nil];
                         if([[jsonResponse valueForKey:SUCCESS]boolValue])
						 {
							 NSArray *dictTemp=[jsonResponse valueForKey:PARAM_PROVIDER_LOCATION];
                             providerBearing=[[jsonResponse valueForKey:PARAM_BEARING] doubleValue];
							 
                             currentTrip.providerLatitude=[NSString stringWithFormat:@"%@",[dictTemp objectAtIndex:0]];
                             currentTrip.providerLongitude=[NSString stringWithFormat:@"%@",[dictTemp objectAtIndex:1]];
                             driver_coordinate.latitude=[currentTrip.providerLatitude doubleValue];
                             driver_coordinate.longitude=[currentTrip.providerLongitude doubleValue];
                             pickup_coordinate.latitude=[currentTrip.srcLatitude doubleValue];
                             pickup_coordinate.longitude=[currentTrip.srcLongitude doubleValue];
                             if (driver_coordinate.latitude && driver_coordinate.longitude)
                             {
                                 if (providerStatus ==ProviderStatusTripStarted)
                                 {
                                             [_lblTripTimeAndCarId setText:NSLocalizedString(@"TOTAL_TIME", nil)];
                                             [_lblTripDistanceAndCarNumber setText:NSLocalizedString(@"TOTAL_DISTANCE", nil)];
                                     
                                             double tempTime=[[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_TOTAL_TIME]] doubleValue];
                                             double tempDistance=[[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_TOTAL_DISTANCE]] doubleValue];
                                     
                                             [_lblTripTimeAndCarModel setText:[NSString stringWithFormat:@"%.2f %@",tempTime,TIME_SUFFIX]];
                                             [_lblTripDistanceAndCarNumberValue setText:[NSString stringWithFormat:@"%.2f %@",tempDistance,DISTANCE_SUFFIX]];
                                             [CATransaction begin];
                                             [CATransaction setValue:[NSNumber numberWithFloat: 0.60f] forKey:kCATransactionAnimationDuration];
                                             [CATransaction setCompletionBlock:^{
                                                     [self drawCurrentPath];
                                                     [currentProviderPath addCoordinate:driver_coordinate];
                                             }];
                                             [self setDriverMarker:driver_coordinate];
                                            [CATransaction commit];
                                 }
                                 else
                                 {
                                      [self setDriverMarker:driver_coordinate];
                                 }
                             }
                         }
					 });
			}];
}
#pragma mark- NAVIGATION METHODS
/// This Method is Used To Custom Setup For Navigation Bar & RevealViewController Sidebar
- (void)customSetup{
	[self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark- ACTION METHODS
-(void)     onClickMenu{
	[self.view endEditing:YES];
}
-(IBAction) onClickBtnClose:(id)sender{
	self.viewForStatus.hidden=YES;
    [_btnTransparent setHidden:YES];
}
-(void)     onClickCancelTripDialogWithReason:(NSString *)strCancelTripReason{
        [self wsCancelTrip:strCancelTripReason];
}
-(IBAction) onClickBtnTargetLocation:(id)sender{
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
        if (providerStatus>=ProviderStatusArrived)
        {
            if(destinationMarker.position.latitude && destinationMarker.position.longitude)
            {bounds = [bounds includingCoordinate:destinationMarker.position];}
            else
            {bounds = [bounds includingCoordinate:clientMarker.position];}
        }
        else
        {
            bounds = [bounds includingCoordinate:clientMarker.position];
        }
        bounds = [bounds includingCoordinate:driverMarker.position];
    
    [CATransaction begin];
    [CATransaction setValue:[NSNumber numberWithFloat:animateCameraDelay] forKey:kCATransactionAnimationDuration];
    [CATransaction setCompletionBlock:^{
    }];
    [mapview_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:60.0f]];
    isCameraBearingChange=FALSE;
    [CATransaction commit];
    
    
    
}
-(IBAction) onClickBtnPayment:(id)sender
{
    if (PREF.isCardAvailable && PREF.isCashAvailable)
    {
            if (viewForCard)
            {
                [self hidePaymentOptions];
            }
            else
            {
                [self showPaymentOptions];
			}
	}
    else
    {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_PAYMENT_OPTION_IS_NOT_AVAILABLE", nil)];
	}
}
-(void)onClickPaymentOption:(UITapGestureRecognizer *)recognizer
{
        if (recognizer.view.tag==0)
        {
            strPaymentMode=NO;
            [_lblPayment setText:NSLocalizedString(@"CARD", nil)];
            [_imgPayment setImage:[UIImage imageNamed:@"icon_card"] forState:UIControlStateNormal];
        }
        else
        {
            strPaymentMode=YES;
            [_lblPayment setText:NSLocalizedString(@"CASH", nil)];
            [_imgPayment setImage:[UIImage imageNamed:@"icon_cash"] forState:UIControlStateNormal];
        }
        [self hidePaymentOptions];
        [self wsUpdatePaymentMode:strPaymentMode];
}
-(IBAction) onClickBtnRequestCancel:(id)sender{
    UIButton *button=(UIButton*)sender;
    if (button.tag==1)
    {
        dialogForSos=nil;
        dialogForSos=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"BTN_SOS", nil) message:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_CANCEL", nil) otherButtonTitles:NSLocalizedString(@"SEND", nil)];
        timeForSos=10;
        _timerForSOS=  [NSTimer scheduledTimerWithTimeInterval:1.0f
                                         target:self selector:@selector(setTimerForSos) userInfo:nil repeats:YES];
    }
    else
    {
        CustomCancelTripDialog *viewForAlert=[[CustomCancelTripDialog alloc]initInView:self];
        if (providerStatus == ProviderStatusArrived)
        {
            NSString *strCancelCharge=[NSString stringWithFormat:@"%@ %@",CurrencySign,currentTrip.cancelTripCharge];
            [viewForAlert setCancelationCharge:strCancelCharge];
        }
        [self.view bringSubviewToFront:viewForAlert];
    }
}
-(void)     onClickClose:(CustomAlertWithTitle *)view{
    if (view==dialogForSos)
    {
        dialogForSos=nil;
        [_timerForSOS invalidate];
    }
    [_timerForSOS invalidate];
}
-(void)     setTimerForSos{
    
    [dialogForSos setTitle:NSLocalizedString(@"BTN_SOS", nil) andMessage:[NSString stringWithFormat:@"%li",
                                                                      (long)timeForSos]];
    
    timeForSos-=1;
    if (timeForSos==0)
    {
        [self wsSos];
        [dialogForSos removeFromSuperview];
        [_timerForSOS invalidate];
    }
}
-(void)onClickCustomDialogOk:(CustomAlertWithTitle *)view{
    if (view==dialogForSos)
    {
        [self wsSos];
    }
}
-(IBAction) onClickBtnApplyPromo:(id)sender{
    dialogForPromoCode=[[CustomAlertWithTextInput alloc]initWithTitle:NSLocalizedString(@"ENTER_PROMO_CODE", nil) placeHolder:NSLocalizedString(@"ENTER_PROMO_CODE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_CANCEL", nil) okButtonTitle:NSLocalizedString(@"APPLY", nil)];
}
-(void)     onClickOkButton:(NSString *)inputTextData view:(CustomAlertWithTextInput *)view
{
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_APPLY_PROMO_CODE", nil)];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:PUT];
    NSString *strUrl = [NSString stringWithFormat:@"%@%@",USER_APPLY_PROMO,strUserId];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:inputTextData forKey:PARAM_PROMO_CODE];
    [dictParam setValue:strUserId forKey:PARAM_USER_ID];
    [dictParam setValue:strForCountry forKey:PARAM_COUNTRY];
    [dictParam setValue:strForCity forKey:PARAM_CITY];
    [dictParam setValue:strTripId forKey:PARAM_TRIP_ID];
    [afn getDataFromPath:strUrl withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^
                        {
                            NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data							options:kNilOptions error:nil];
                            if (jsonResponse)
                            {
                                if([[jsonResponse valueForKey:SUCCESS]boolValue])
                                {
                                    NSString *messageCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:MESSAGE_CODE]];
                                    [view removeFromSuperview];
                                    [[UtilityClass sharedObject] showToast:NSLocalizedString(messageCode, nil)];
                                    [_btnApplyPromo setImage:[UIImage imageNamed:@"promoApplied"] forState:UIControlStateNormal];
                                    [_btnApplyPromo setUserInteractionEnabled:NO];
                                    
                                }
                                else
                                {
                                    NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                                    [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                                    
                                }
                            }
                            [[AppDelegate sharedAppDelegate]hideLoadingView];
                            
                        });
     }];
}
-(IBAction) onClickBtnCall:(id)sender{
        NSString *mobileNo=[NSString stringWithFormat:@"tel:%@%@",currentTrip.phoneCountryCode,currentTrip.providerPhone] ;
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mobileNo]];}
-(IBAction) onClickBtnDestinationDrag:(id)sender{
        if(isDestinationDragFlagOn)
        {
            [_txtDestLocation setText:[UtilityClass formatAddress:strFinalDestinationAddress]];
            [_imgDestinationPin setHidden:YES];
            [_btnConfirmDestination setHidden:YES];
            isDestinationDragFlagOn=NO;
            [_viewForDriver setHidden:NO];
            CGRect frame=mapview_.frame;
            if (destinationMarker.position.latitude && destinationMarker.position.longitude)
            {
                destinationMarker.map=mapview_;
            }
            frame.size.height-=_viewForDriver.frame.size.height;
            [mapview_ setFrame:frame];
            [self.view bringSubviewToFront:_viewForDriver];
            [self.view bringSubviewToFront:_viewForPayment];
            [self.view bringSubviewToFront:_viewForOngoingTrip];
            [self.view bringSubviewToFront:_btnApplyPromo];
        }
        else
        {
            destinationMarker.map=nil;
            [_imgDestinationPin setHidden:NO];
            [self.view bringSubviewToFront:_imgDestinationPin];
            [_btnConfirmDestination setHidden:NO];
            [_viewForDriver setHidden:YES];
            CGRect frame=mapview_.frame;
            frame.size.height+=_viewForDriver.frame.size.height;
            [mapview_ setFrame:frame];
            [self.view bringSubviewToFront:_btnConfirmDestination];
            isDestinationDragFlagOn=YES;
        }
}
-(IBAction) onClickBtnConfirmDestination:(id)sender{
        if (![UtilityClass isEmpty:strDestAddress])
        {
            [_imgDestinationPin setHidden:YES];
            [_btnConfirmDestination setHidden:YES];
            isDestinationDragFlagOn=NO;
            strForDestLat=[NSString stringWithFormat:@"%f",dest_coordinate.latitude ];
            strForDestLong=[NSString stringWithFormat:@"%f",dest_coordinate.longitude ];
            
            [self wsUpdateDestination:strDestAddress];
            [_viewForDriver setHidden:NO];
            CGRect frame=mapview_.frame;
            destinationMarker.map=mapview_;
            destinationMarker.position=dest_coordinate;
            frame.size.height-=_viewForDriver.frame.size.height;
            [mapview_ setFrame:frame];
        }
        
}

#pragma mark -
#pragma mark - UITextfield Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField==_txtDestLocation)
    {
     _tableForCity.frame=CGRectMake(_viewForDestinationAddress.frame.origin.x
                                       ,_viewForDestinationAddress.frame.origin.y+_txtDestLocation.frame.size.height+10,
                                       _viewForDestinationAddress.frame.size.width,
                                       _tableForCity.frame.size.height);
        
    }
    else
    {
        
    }
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{return YES;}
-(void)textFieldDidEndEditing:(UITextField *)textField{
     
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
-(void)setNewPlaceData{
        NSString *myAddress=[UtilityClass formatAddress:[dictPlace objectForKey:GOOGLE_PARAM_AUTOCOMLETE_DESCRIPTION]];
        strDestAddress=[NSString stringWithFormat:@"%@",[dictPlace objectForKey:GOOGLE_PARAM_AUTOCOMLETE_DESCRIPTION]];;
        self.txtDestLocation.text = [NSString stringWithFormat:@"%@",myAddress];
        dest_coordinate= [self getLocationFromAddressString:strDestAddress];
        if (dest_coordinate.latitude && dest_coordinate.longitude)
        {
            [self setDestinationMarker:dest_coordinate];
            destinationMarker.position = dest_coordinate;
            [self wsUpdateDestination:strDestAddress];
        }
        [_txtDestLocation resignFirstResponder];

}
/*! USed To Search The Predicted Array Of Addresses for Autocomplete*/
- (IBAction)Searching:(id)sender{
        [arrForPredictedAddresses removeAllObjects];
        self.tableForCity.hidden=YES;
        
        NSString *str;
        str=self.txtDestLocation.text;
        
        //NSlog(@"%@",str);
        if(str.length > 2)
        {
            self.tableForCity.hidden=YES;
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
            [dictParam setObject:str forKey:GOOGLE_PARAM_AUTOCOMLETE_INPUT]; // AUTOCOMPLETE API
            [dictParam setObject:GOOGLE_PARAM_AUTOCOMLETE_SENSOR forKey:@"false"]; // AUTOCOMPLETE API
            [dictParam setObject:GoogleServerKey forKey:GOOGLE_PARAM_AUTOCOMLETE_KEY];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:@"GET"];
            [afn getAddressFromGooglewAutoCompletewithParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 
                 dispatch_async(dispatch_get_main_queue(), ^
                                {
                                    NSArray *arrAddress=[response valueForKey:GOOGLE_PARAM_AUTOCOMLETE_PREDICTION]; //AUTOCOMPLTE API
                                    if ([arrAddress count] > 1)
                                    {
                                        self.tableForCity.hidden=NO;
                                        [self.view bringSubviewToFront:_tableForCity];
                                        arrForPredictedAddresses=[[NSMutableArray alloc] initWithArray:arrAddress copyItems:YES];
                                        
                                        [UIView performWithoutAnimation:^{
                                            [self.tableForCity reloadData];
                                            [self.tableForCity beginUpdates];
                                            [self.tableForCity endUpdates];
                                        }];
                                        if(self.tableForCity.hidden)
                                        self.tableForCity.hidden=NO;
                                        [self.view bringSubviewToFront:_tableForCity];
                                        
                                    }
                                    else
                                    {
                                        self.tableForCity.hidden=YES;
                                    }
                                 });
             }];
        }
}
#pragma mark -
#pragma mark - Location Delegate
-(void)updateLocationManager{
	[locationManager startUpdatingLocation];
	locationManager = [[CLLocationManager alloc] init];
	locationManager.delegate=self;
	locationManager.desiredAccuracy=kCLLocationAccuracyBest;
	locationManager.distanceFilter=kCLDistanceFilterNone;
	
#ifdef __IPHONE_8_0
	if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
		// Use one or the other, not both. Depending on what you put in info.plist
		//[self.locationManager requestWhenInUseAuthorization];
		[locationManager requestAlwaysAuthorization];
	}
#endif
	
	[locationManager startUpdatingLocation];
	
}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
	NSLog(@"didFailWithError: %@", error);
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    CLLocation *newLocation = locations.lastObject;
    CLLocation *oldLocation;
    if (newLocation)
    {
        current_coordinate=[newLocation coordinate];
    }
    if (locations.count > 1)
    {
        oldLocation = locations[locations.count - 2];
    }
    
}
/** Return LatLong Of Given String */
-(CLLocationCoordinate2D) getLocationFromAddressString: (NSString*) addressStr{
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [addressStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result)
    {
        
        NSData *data = [result dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        NSDictionary *dict=[[NSDictionary alloc]init];
        if([[jsonResponse valueForKey:GOOGLE_PARAM_STATUS]isEqualToString:GOOGLE_PARAM_STATUS_OK])
        {
            dict=[[[[jsonResponse valueForKey:GOOGLE_PARAM_RESULTS]objectAtIndex:0] objectForKey:GOOGLE_PARAM_GEOMETRY]objectForKey:GOOGLE_PARAM_LOCATION];
            latitude=[[dict valueForKey:GOOGLE_PARAM_AUTOCOMLETE_PLACE_LATITUDE]doubleValue ];
            longitude=[[dict valueForKey:GOOGLE_PARAM_AUTOCOMLETE_PLACE_LONGITUDE]doubleValue ];
        }
        
    }
    CLLocationCoordinate2D center;
    center.latitude=latitude;
    center.longitude = longitude;
    
    return center;
}
/** Return Address Of Given Latlong */
-(NSString  *)getAddressForLocation:(CLLocationCoordinate2D)location{
        NSString *strAddress=nil;
        NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f", location.latitude, location.longitude];
        NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:NULL];
        
        if (result)
        {
            NSData *data = [result dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            if([[jsonResponse valueForKey:GOOGLE_PARAM_STATUS]isEqualToString:GOOGLE_PARAM_STATUS_OK])
            {
                NSArray *arr=[jsonResponse valueForKey:GOOGLE_PARAM_RESULTS];
                NSDictionary *dict=[arr objectAtIndex:0];
                strAddress=[dict valueForKey:GOOGLE_PARAM_AUTOCOMLETE_FORMATTED_ADDRESS];
            }
            else
            {
                [[UtilityClass sharedObject]displayAlertWithTitle:NSLocalizedString(@"ALERT_LOCATION_NOT_FOUND", nil) andMessage:NSLocalizedString(@"ALERT_MSG_ENTER_VALID_LOCATION",nil )];
            }
        }
        else
        {
            [[UtilityClass sharedObject]displayAlertWithTitle:NSLocalizedString(@"ALERT_LOCATION_NOT_FOUND", nil) andMessage:NSLocalizedString(@"ALERT_MSG_ENTER_VALID_LOCATION",nil )];
            
        }
        return strAddress;
    }
/**Return Current location*/
-(CLLocationCoordinate2D) getLocation
{
    CLLocationCoordinate2D coordinate;
	CLLocation *location = [locationManager location];
    if (location)
    {
        coordinate = [location coordinate];
        [self getCountryAndCity:coordinate];
        GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:location.coordinate zoom:17];
        [self.mapview_ animateWithCameraUpdate:updatedCamera];
        
    }
   return coordinate;
}
#pragma mark-WebService Methods
-(void)checkForTripStatus{
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:strUserId forKey:PARAM_USER_ID];
        NSString *strUrl=[NSString stringWithFormat:@"%@%@/%@",USER_GET_TRIP_STATUS,strUserId,strUserToken];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
        [afn getDataFromPath:strUrl withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if ([[Parser sharedObject] parseTripStatus:response])
             {
                 
                 [self checkPaymentModeAndPromoAvailable];
                 strTripId=PREF.tripId;
                 NSString *strProviderId= currentTrip.providerId;
                 providerStatus=[currentTrip.providerStatus integerValue];
                 [self getProviderLatLong:strProviderId];
                 dispatch_async(dispatch_get_main_queue(), ^
                                {
                                    if ([CurrentTrip sharedObject].paymentMode )
                                    {
                                        [_lblPayment setText:NSLocalizedString(@"CASH", nil)];
                                        [_imgPayment setImage:[UIImage imageNamed:@"icon_cash"] forState:UIControlStateNormal];
                                    }
                                    else
                                    {
                                        [_lblPayment setText:NSLocalizedString(@"CARD", nil)];
                                        [_imgPayment setImage:[UIImage imageNamed:@"icon_card"] forState:UIControlStateNormal];
                                        
                                    }
                                    [_lblTripIdValue setText:currentTrip.tripNumber];
                                    _txtPickUpAddress.text=[UtilityClass formatAddress:currentTrip.srcAddress ];
                                    if (!isDestinationSet)
                                    {
                                        strFinalDestinationAddress=currentTrip.destAddress;
                                        _txtDestLocation.text=[UtilityClass formatAddress:currentTrip.destAddress ];
                                        CGFloat pointSize=MIN(_txtDestLocation.font.pointSize, _txtPickUpAddress.font.pointSize);
                                        UIFont *font=[UIFont fontWithName:_txtPickUpAddress.font.fontName size:pointSize];
                                        _txtPickUpAddress.font=_txtDestLocation.font=font;
                                        
                                        isDestinationSet=YES;
                                        if(_txtDestLocation.text==NULL)
                                        {
                                            [_txtDestLocation setText:@""];
                                            strFinalDestinationAddress=@"";
                                        }
                                    }
                                    if (!isStatusSaved && providerStatus>=ProviderStatusComing)
                                    {
                                        statusProvider=providerStatus;
                                        isStatusSaved=YES;
                                        if ([CurrentTrip sharedObject].isPromoUsed )
                                        {
                                            [_btnApplyPromo setImage:[UIImage imageNamed:@"promoApplied"] forState:UIControlStateNormal];
                                            [_btnApplyPromo setUserInteractionEnabled:NO];
                                            
                                        }
                                        else
                                        {
                                            [_btnApplyPromo setImage:[UIImage imageNamed:@"promo"] forState:UIControlStateNormal];
                                            [_btnApplyPromo setBackgroundColor:[UIColor clearColor]];
                                            [_btnApplyPromo setBackgroundImage:[UIImage imageNamed:@"circle_blue"] forState:UIControlStateNormal];
                                            
                                        }
                                        
                                    }
                                    switch (providerStatus)
                                    {
                                        
                                        case ProviderStatusComing:
                                        
                                        if(statusProvider==providerStatus)
                                        {
                                            self.lblForStatus.text=NSLocalizedString(@"TEXT_DRIVER_COMING", nil);
                                            self.viewForStatus.hidden=NO;
                                            statusProvider=ProviderStatusArrived;
                                            [self.view bringSubviewToFront:_viewForStatus];
                                            isDriverStarted=YES;
                                            [_btnTransparent setHidden:NO];
                                            [self playSound];
                                        }
                                        break;
                                        case ProviderStatusArrived:
                                        
                                        if(statusProvider==providerStatus)
                                        {
                                            currentWaitingTime=WAITING_SECONDS_REMAIN;
                                            if(![_timerForWaitingTime isValid]&&currentTrip.waitingPrice>0.0)
                                            {
                                            _timerForWaitingTime=[NSTimer scheduledTimerWithTimeInterval:1.0f target:self
                                                                           selector:@selector(waitingCounter) userInfo:nil repeats:YES];
                                            }
                                            
                                            [self getGooglePathFromServer];
                                            statusProvider=ProviderStatusTripStarted;
                                            self.lblForStatus.text=NSLocalizedString(@"TEXT_DRIVER_ARRIVED", nil);
                                            self.viewForStatus.hidden=NO;
                                            [self.view bringSubviewToFront:_viewForStatus];
                                            isDriverArived=YES;
                                            [_btnTransparent setHidden:NO];
                                            [self playSound];
                                        }
                                        break;
                                        case ProviderStatusTripStarted:
                                        [_btnCancelRequest setTag:1];
                                        [_btnCancelRequest setTitle:NSLocalizedString(@"BTN_SOS", nil) forState:UIControlStateNormal];
                                        if(statusProvider==providerStatus)
                                        {
                                            
                                            
                                            [self getGooglePathFromServer];
                                            statusProvider=ProviderStatusTripEnd;
                                            self.lblForStatus.text=NSLocalizedString(@"TEXT_DRIVER_TRIP_STARTED", nil);
                                            self.viewForStatus.hidden=NO;
                                            [self.view bringSubviewToFront:_viewForStatus];
                                            isTripStarted=YES;
                                            [_btnTransparent setHidden:NO];
                                            [self playSound];
                                        }
                                        break;
                                        
                                        case ProviderStatusTripEnd:
                                        
                                        
                                        
                                        case ProviderStatusTripCompleted:
                                        
                                        [self performSelectorOnMainThread:@selector(stopTimer) withObject:nil waitUntilDone:YES];
                                        [NSTimer scheduledTimerWithTimeInterval:1.0f target:self
                                                                       selector:@selector(loadInvoice) userInfo:nil repeats:NO];
                                        default:
                                        NSLog(@"Nothing");
                                        break;
                                    }
                                    dest_coordinate.latitude=[currentTrip.destLatitude doubleValue];
                                    dest_coordinate.longitude=[currentTrip.destLongitude doubleValue];
                                    [self setMarkerAndCameraWithProviderLatLong:driver_coordinate pickUpLatitude:pickup_coordinate andDestinationLatLong:dest_coordinate];
                                });
                 
             }
             else
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     IS_TRIP_ACCEPTED=NO;
                     IS_TRIP_EXSIST=NO;
                     [[CurrentTrip sharedObject]resetObject];
                     [self performSelectorOnMainThread:@selector(stopTimer) withObject:nil waitUntilDone:YES];
                     [APPDELEGATE goToMap];
                 });
             }
         }];
}
-(void)wsSos{
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_SENDING_SMS", nil)];
    CLLocation *location = [locationManager location];
    NSString *sosLatitude=[NSString stringWithFormat:@"%.8f",location.coordinate.latitude ];
    NSString *sosLongitude=[NSString stringWithFormat:@"%.8f",location.coordinate.longitude ];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
    NSString *strUrl=[NSString stringWithFormat:@"https://www.google.co.in/maps/place/%@,%@",sosLatitude,sosLongitude];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:strUserId forKey:PARAM_USER_ID];
    [dictParam setValue:strUrl forKey:PARAM_MAP_IMAGE];
    [afn getDataFromPath:WS_SEND_SMS_TO_EMERGENCY_CONTACT withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^
                        {
                            NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data							options:kNilOptions
                                                                                           error:nil];
                            if (jsonResponse)
                            {
                                if([[jsonResponse valueForKey:SUCCESS]boolValue])
                                {
                                    NSString *messageCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:MESSAGE_CODE]];
                                    [[UtilityClass sharedObject] showToast:NSLocalizedString(messageCode, nil)];
                                }
                                else
                                {
                                    NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                                    [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                                }
                            }
                            [[AppDelegate sharedAppDelegate]hideLoadingView];
                            
                        });
			}];
}

-(void)getCountryAndCity:(CLLocationCoordinate2D )location2d{
    CLLocation *location = [[CLLocation alloc] initWithLatitude:location2d.latitude longitude:location2d.longitude];
    userDefaultLanguages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en_US", nil] forKey:@"AppleLanguages"];

    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *array, NSError *error){
                       if (error)
                       {
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                       CLPlacemark *placemark = [array objectAtIndex:0];
                       strForCity=placemark.subAdministrativeArea;
                       strForCountry=placemark.country;
                       [[NSUserDefaults standardUserDefaults] setObject:userDefaultLanguages forKey:@"AppleLanguages"];
                       
                       if (strForCountry==nil || strForCity == nil) {
                           strForCity=@"";
                           strForCountry=@"";
                       }
                   }];
    
}
-(void)wsUpdatePaymentMode:(BOOL)paymentType{
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_UPDATE_PAYMENT_MODE", nil)];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:strUserId forKey:PARAM_USER_ID];
    [dictParam setValue:strTripId forKey:PARAM_TRIP_ID];
    [dictParam setValue:[NSNumber numberWithBool:paymentType] forKey:PARAM_PAYMENT_TYPE];
    [afn getDataFromPath:USER_CHANGE_PAYMENT_MODE withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^
                        {
                            NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data							options:kNilOptions
                                                                                           error:nil];
                            if (jsonResponse)
                            {
                                if([[jsonResponse valueForKey:SUCCESS]boolValue])
                                {
                                    [APPDELEGATE hideLoadingView];
                                }
                                else
                                {
                                    [APPDELEGATE hideLoadingView];
                                    NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                                    [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                                }
                            }
                            [APPDELEGATE hideLoadingView];
                            
                        });
         
     }];
    
    
}
-(void)wsUpdateDestination:(NSString*)destination_address{
    if (dest_coordinate.latitude && dest_coordinate.longitude)
    {
    
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_UPDATING_DESTINATION_ADDRESS", nil)];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:strUserId forKey:PARAM_USER_ID];
    [dictParam setValue:strTripId forKey:PARAM_TRIP_ID];
    [dictParam setValue:destination_address forKey:PARAM_TRIP_DESTINATION_ADDRESS];
    [dictParam setValue:[NSString stringWithFormat:@"%f",dest_coordinate.latitude] forKey:PARAM_DEST_LATITUDE];
    [dictParam setValue:[NSString stringWithFormat:@"%f",dest_coordinate.longitude] forKey:PARAM_DEST_LONGITUDE];
    [afn getDataFromPath:USER_UPDATE_DESTINATION_ADDRESS withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^
                        {
                            NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data							options:kNilOptions
                                                                                           error:nil];
                            if (jsonResponse)
                            {
                                if([[jsonResponse valueForKey:SUCCESS]boolValue])
                                {
                                    [_txtDestLocation setText:[UtilityClass formatAddress:destination_address]];
                                    strFinalDestinationAddress=destination_address;
                                    isDestinationSet=YES;
                                    CGFloat pointSize=MIN(_txtDestLocation.font.pointSize, _txtPickUpAddress.font.pointSize);
                                    UIFont *font=[UIFont fontWithName:_txtPickUpAddress.font.fontName size:pointSize];
                                    _txtPickUpAddress.font=_txtDestLocation.font=font;
                                    [APPDELEGATE hideLoadingView];
                                     NSString *messageCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:MESSAGE_CODE]];
                                    [[UtilityClass sharedObject] showToast:NSLocalizedString(messageCode, nil)];
                                    isPathDrawn=true;
                                    NSArray *arrDestLocation=[jsonResponse valueForKey:PARAM_DESTINATION_LOCATION];
                                    dest_coordinate.latitude=[[arrDestLocation objectAtIndex:0] doubleValue];
                                    dest_coordinate.longitude=[[arrDestLocation objectAtIndex:1] doubleValue];
                                    
                                    if (providerStatus==ProviderStatusArrived ||providerStatus==ProviderStatusTripStarted)
                                    {
                                        [self showRouteFromSourceToDestination:pickup_coordinate to:CLLocationCoordinate2DMake([[arrDestLocation objectAtIndex:0] doubleValue], [[arrDestLocation objectAtIndex:1] doubleValue])];
                                        isCameraBearingChange=FALSE;
                                        
                                    }
                                 }
                                else
                                {
                                    isDestinationSet=NO;
                                    [APPDELEGATE hideLoadingView];
                                    NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                                    [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                                }
                            }
                            [APPDELEGATE hideLoadingView];
                            
                        });
			}];
    }
    else
    {
        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_LOCATION", nil)];
    }
    
}
-(void)wsCancelTrip:(NSString*)cancelReason{
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:strUserId forKey:PARAM_USER_ID];
        [dictParam setObject:strTripId forKey:PARAM_TRIP_ID];
        [dictParam setObject:cancelReason forKey:PARAM_CANCEL_TRIP_REASON];
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING_CANCLE_REQUEST", nil) ];
        NSString *strUrl=[NSString stringWithFormat:@"%@%@",WS_CANCEL_TRIP,strUserId];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:PUT];
        [afn getDataFromPath:strUrl withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                                NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data							options:kNilOptions error:nil];
                                if (jsonResponse)
                                {
                                    if([[jsonResponse valueForKey:SUCCESS]boolValue])
                                    {
                                       [self performSelectorOnMainThread:@selector(stopTimer) withObject:nil waitUntilDone:YES];                                        [[AppDelegate sharedAppDelegate]hideLoadingView];
                                        self.viewForDriver.hidden=YES;
                                        [APPDELEGATE goToMap];
                                        IS_TRIP_EXSIST=NO;
                                    }
                                    else
                                    {
                                        NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                                        [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                                    }
                                }
                                [[AppDelegate sharedAppDelegate]hideLoadingView];
                                
                            });
             
         }];
}
#pragma mark - AutoComplete Tableview Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView==self.tableForCity)
    {
        return arrForPredictedAddresses.count;
    }
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(cell == nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    if(arrForPredictedAddresses.count >0)
    {
        NSString *formatedAddress=[[arrForPredictedAddresses objectAtIndex:indexPath.row] valueForKey:GOOGLE_PARAM_AUTOCOMLETE_DESCRIPTION]; // AUTOCOMPLETE API
        NSArray* myArray = [formatedAddress  componentsSeparatedByString:@","];
        NSString *strTitle=[[NSString alloc] initWithString:[myArray objectAtIndex:0]];
        /*  NSRange selectedRange = NSMakeRange(0, _txtPickupAddress.text.length); // 4 characters, starting at index 22
         [strTitle beginEditing];
         [strTitle addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:25] range:selectedRange];
         [strTitle endEditing];
         */
        cell.textLabel.text=strTitle;
        cell.textLabel.textColor=[UIColor blackColor];
        cell.textLabel.alpha=0.7;
        cell.detailTextLabel.text=formatedAddress;
        cell.detailTextLabel.textColor=[UIColor blackColor];
        cell.detailTextLabel.font=[UIFont fontWithName:@"Roboto_Regular_0" size:15];
    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    dictPlace=[arrForPredictedAddresses objectAtIndex:indexPath.row];
    self.tableForCity.hidden=YES;
    if (isDestinationDragFlagOn)
    {
        [self onClickBtnDestinationDrag:nil];
    }
    [self setNewPlaceData];
}
#pragma mark-SetMarkers
-(void)setMarkerAndCameraWithProviderLatLong:(CLLocationCoordinate2D)providerLatlong pickUpLatitude:(CLLocationCoordinate2D)pickUpLatitude andDestinationLatLong:(CLLocationCoordinate2D)destinationLatLong{
    isMapBounds=NO;
    [self setDestinationMarker:destinationLatLong];
    [self setPickUpMarker:pickUpLatitude];
    if (isMapBounds)
    {
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
        if (providerStatus>=ProviderStatusArrived)
        {
            bounds = [bounds includingCoordinate:destinationMarker.position];
        }
        else
        {
            bounds = [bounds includingCoordinate:clientMarker.position];
        }
        bounds = [bounds includingCoordinate:driverMarker.position];
       
        [CATransaction begin];
        [CATransaction setValue:[NSNumber numberWithFloat: animateCameraDelay] forKey:kCATransactionAnimationDuration];
        [CATransaction setCompletionBlock:^{
        }];
        [mapview_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:60.0f]];
        isCameraBearingChange=FALSE;
        [CATransaction commit];
    }
}
-(void)setDestinationMarker:(CLLocationCoordinate2D)destinationLatLong
{
    if (destinationLatLong.latitude!=0 && destinationLatLong.longitude!=0)
    {
        if (destinationMarker==NULL)
        {
            destinationMarker=[[GMSMarker alloc]init];
            destinationMarker.icon=[UIImage imageNamed:@"pin_destination"];
            destinationMarker.map=mapview_;
            destinationMarker.position = destinationLatLong;
        }
    }
}
-(void)setPickUpMarker:(CLLocationCoordinate2D)pickUpLatLong{
    if (pickUpLatLong.latitude!=0 && pickUpLatLong.longitude!=0)
    {
        clientMarker.position = pickUpLatLong;
        if (!clientMarker.map)
        {
            clientMarker.map = mapview_;
        }
    }
}
-(void)setDriverMarker:(CLLocationCoordinate2D)providerLatLong{
    if (providerLatLong.latitude!=0 && providerLatLong.longitude!=0)
    {
        if (driverMarker.map)
        {
            driverMarker.position = providerLatLong;
            if(_btnConfirmDestination.isHidden)
            {
                    [mapview_ animateToLocation:providerLatLong];
            }
        }
        else
        {
            isMapBounds=YES;
            
            driverMarker.position = providerLatLong;
            driverMarker.map = mapview_;
        }
        if (isCameraBearingChange)
        {
            driverMarker.rotation=providerBearing-(360.0+mapBearing);
        }
        else
        {
            if (providerStatus==ProviderStatusComing && pickup_coordinate.latitude && pickup_coordinate.longitude)
            {
                isCameraBearingChange=TRUE;
                mapBearing=[self getHeadingForDirectionFromCoordinate:driver_coordinate toCoordinate:pickup_coordinate];
            }
            else if((providerStatus==ProviderStatusArrived||providerStatus==ProviderStatusTripStarted) && (dest_coordinate.latitude && dest_coordinate.longitude))
            {
                mapBearing=[self getHeadingForDirectionFromCoordinate:driver_coordinate toCoordinate:dest_coordinate];
                isCameraBearingChange=TRUE;
            }
            else
            {
                isCameraBearingChange=FALSE;
            }
            if (isCameraBearingChange)
            {
                [CATransaction begin];
                [CATransaction setValue:[NSNumber numberWithFloat: 0.60f] forKey:kCATransactionAnimationDuration];
                [CATransaction setCompletionBlock:^{
                    
                }];
                [mapview_ animateToBearing:mapBearing];
                [CATransaction commit];
            }
        }
        
        NSLog(@"provider bearing= %.2f",providerBearing);
        NSLog(@"map bearing= %.2f",mapBearing);
    }
}
#pragma mark-Mapview Delegate
-(void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    if (isDestinationDragFlagOn)
    {
        dest_coordinate=position.target;
        strDestAddress=[self getAddressForLocation:dest_coordinate];
        strForDestLat= [NSString stringWithFormat:@"%f",dest_coordinate.latitude];
        strForDestLong= [NSString stringWithFormat:@"%f",dest_coordinate.longitude];
        self.txtDestLocation.text = [NSString stringWithFormat:@"%@",strDestAddress];
    }
    
}
- (double)getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc toCoordinate:(CLLocationCoordinate2D)toLoc{
    float fLat = degreesToRadians(fromLoc.latitude);
    float fLng = degreesToRadians(fromLoc.longitude);
    float tLat = degreesToRadians(toLoc.latitude);
    float tLng = degreesToRadians(toLoc.longitude);
    
    float degree = radiansToDegrees(atan2(sin(tLng-fLng)*cos(tLat), cos(fLat)*sin(tLat)-sin(fLat)*cos(tLat)*cos(tLng-fLng)));
    
    if (degree >= 0)
    {
        return degree;
    } else {
        return 360+degree;
    }
}
#pragma mark-Payment options
-(void)showPaymentOptions{
    /*ViewForCard*/
    viewForCard=[[UIView alloc]initWithFrame:_viewForPayment.frame];
    UILabel *lblCard=[[UILabel alloc]initWithFrame:_lblPayment.frame];
    
    UIButton *imgCard=[[UIButton alloc]initWithFrame:_imgPayment.frame];
    [imgCard setBackgroundImage:[UIImage imageNamed:@"circle_gray"] forState:UIControlStateNormal];
    [imgCard setContentMode:UIViewContentModeCenter];
    [imgCard setUserInteractionEnabled:NO];
    if ([_lblPayment.text isEqualToString:NSLocalizedString(@"CASH", nil)])
    {
        [viewForCard setTag:0];
        [lblCard setText:NSLocalizedString(@"CARD",nil)];
        [imgCard setImage:[UIImage imageNamed:@"icon_card" ] forState:UIControlStateNormal];
        
    }
    else
    {
        [lblCard setText:NSLocalizedString(@"CASH",nil)];
        [imgCard setImage:[UIImage imageNamed:@"icon_cash" ] forState:UIControlStateNormal];
        [viewForCard setTag:1];
    }
    
    [imgCard addTarget:self action:@selector(onClickPaymentOption:) forControlEvents:UIControlEventTouchUpInside];
    [viewForCard addSubview:imgCard];
    [viewForCard addSubview:lblCard];
    [self adjustFrameProperty:imgCard.imageView andLabel:lblCard inView:viewForCard];
    paymentGestureCard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickPaymentOption:)]; // Declare the Gesture.
    paymentGestureCard.delegate = self;
    [self.view addSubview:viewForCard];
    [viewForCard addGestureRecognizer:paymentGestureCard];
    [UIView animateWithDuration:0.4f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGAffineTransform scaleTrans  = CGAffineTransformMakeScale(1.0f, 1.0f);
                         CGAffineTransform lefttorightTrans  = CGAffineTransformMakeTranslation(0.0f,_viewForPayment.frame.size.height);
                         viewForCard.transform = CGAffineTransformConcat(scaleTrans, lefttorightTrans);
                         [imgCard setAlpha:1];
                         [lblCard setAlpha:0.7];
                     }
                     completion:^(BOOL finished){
                         if (finished)
                         {
                             
                         }
                     }];
    
    
}
-(void)hidePaymentOptions{
    [UIView animateWithDuration:0.3f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [viewForCard setAlpha:0];
                         CGAffineTransform scaleTransB  = CGAffineTransformMakeScale(1.0f, 1.0f);
                         CGAffineTransform lefttorightTransB  = CGAffineTransformMakeTranslation(0.0f,0.0f);
                         viewForCard.transform = CGAffineTransformConcat(scaleTransB, lefttorightTransB);
                     }
                     completion:^(BOOL finished){
                         if (finished)
                         {
                             [viewForCard setHidden:YES];
                             viewForCard=nil;
                             [_viewForPayment setUserInteractionEnabled:YES];
                         }
                     }];
}
-(void)adjustFrameProperty:(UIImageView*)image andLabel:(UILabel*)label inView:(UIView*)view{
    CGRect frame=image.frame;
    frame.size.height=MIN(image.frame.size.width, image.frame.size.height) ;
    frame.size.width=frame.size.height;
    image.frame=frame;
    
    [view setBackgroundColor:[UIColor clearColor]];
    [image setBackgroundColor:[UIColor clearColor]];
    [label setBackgroundColor:[UIColor lightGreenColor]];
    [image setContentMode:UIViewContentModeCenter];
    [label setTextColor:[UIColor labelTitleColor]];
    [label setFont:_lblPayment.font];
    [label setTextAlignment:NSTextAlignmentCenter];
    view.frame = _viewForPayment.frame;
    label.layer.cornerRadius = 5.0f;
    label.layer.masksToBounds=YES;
}
#pragma mark Play Sound
-(void) playSound{
    if (providerStatus==ProviderStatusArrived)
    {
        if (!PREF.isDriverArrivedSoundOff)
        {
            NSString *sound;
            sound=[NSString stringWithFormat:@"alertArriveNotification"];
            NSString *path = [[NSBundle mainBundle] pathForResource:sound ofType:@"mp3"];
            NSURL *url=[NSURL fileURLWithPath:path];
            soundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
            if(!soundPlayer)
                NSLog(@"error in play sound");
            soundPlayer.delegate=self;
            [soundPlayer setNumberOfLoops:0];
            [soundPlayer play];
        }
       
    }
    else
    {
        if (!PREF.isSoundOff)
        {
            NSString *sound;
            sound=[NSString stringWithFormat:@"alertNotification"];
            NSString *path = [[NSBundle mainBundle] pathForResource:sound ofType:@"mp3"];
            NSURL *url=[NSURL fileURLWithPath:path];
            soundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
            if(!soundPlayer)
                NSLog(@"error in play sound");
            soundPlayer.delegate=self;
            [soundPlayer setNumberOfLoops:0];
            [soundPlayer play];
    
        }
        
    }
}
#pragma mark -Draw Path
-(void)showRouteFromSourceToDestination:(CLLocationCoordinate2D)source to:(CLLocationCoordinate2D)destination{
    if (PREF.isPathDraw)
    {
        NSString* saddr = [NSString stringWithFormat:@"%f,%f", source.latitude, source.longitude];
        NSString* daddr = [NSString stringWithFormat:@"%f,%f", destination.latitude, destination.longitude];
        NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,GoogleServerKey];
        NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
        NSError* error = nil;
        NSData *data = [[NSData alloc]initWithContentsOfURL:apiUrl];
        NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        NSArray *routes = json[@"routes"];
        if(routes.count!=0)
        {
            GMSPath *googlePath =[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
            [self drawPath:googlePath withColor:[UIColor googlePathColor]];
            
        }
    }
}
-(void)getGooglePathFromServer{
    if(PREF.isPathDraw)
    {
        NSMutableDictionary *dictParam;
        dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:PREF.tripId forKey:PARAM_TRIP_ID];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
        [afn getDataFromPath:WS_GET_GOOGLE_MAP_PATH withParamData:dictParam withBlock:^(id response, NSError *error)
        {
             dispatch_async(dispatch_get_main_queue(), ^{
                 NSDictionary *dictResponse=[[NSDictionary alloc]init];
                 if ([[Parser sharedObject] stringToJson:response To:&dictResponse])
                 {
                     if ([[dictResponse valueForKey:SUCCESS]boolValue])
                     {   NSString *path;
                         NSDictionary *dict=[dictResponse valueForKey:PARAM_TRIP_LOCATION];
                         path=[dict objectForKey:PARAM_GET_GOOGLE_MAP_PATH_PICKUP_LOCATION_TO_DESTINATION_LOCATION];
                         NSError * err;
                         NSData *data =[path dataUsingEncoding:NSUTF8StringEncoding];
                         NSDictionary *json;
                         if ([[CurrentTrip sharedObject].providerStatus integerValue]==ProviderStatusTripStarted||[[CurrentTrip sharedObject].providerStatus integerValue]==ProviderStatusArrived)
                         {
                             if(data!=nil && ![UtilityClass isEmpty:path])
                             {
                                 json = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
                                 NSArray *routes = json[@"routes"];
                                 if(routes.count!=0)
                                 {
                                     GMSPath *path =[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
                                     [self drawPath:path withColor:[UIColor googlePathColor]];
                                 }
                             }
                             else
                             {
                                     if (dest_coordinate.latitude && dest_coordinate.longitude)
                                     {
                                         [self showRouteFromSourceToDestination:pickup_coordinate to:dest_coordinate];
                                     }
                             }
                             if ([[CurrentTrip sharedObject].providerStatus integerValue]==ProviderStatusTripStarted)
                             {
                                 NSArray *startToEndTripLocation=[dict valueForKey:PARAM_START_TRIP_TO_END_TRIP_LOCATION];
                                 if (isPathCurrentPathDraw)
                                 {
                                     
                                 }
                                 else
                                 {
                                 if (startToEndTripLocation.count)
                                 {
                                     currentProviderPath=[[GMSMutablePath alloc]init];
                                     for (NSArray *location in startToEndTripLocation)
                                     {
                                         CLLocationCoordinate2D currentCoordinate=CLLocationCoordinate2DMake([[location objectAtIndex:0] doubleValue], [[location objectAtIndex:1] doubleValue]);
                                         [currentProviderPath addCoordinate:currentCoordinate];
                                         previousDriverLatLong=currentCoordinate;
                                     }
                                     polyLineCurrentProviderPath= [GMSPolyline polylineWithPath:currentProviderPath];
                                     polyLineCurrentProviderPath.strokeWidth = 5.f;
                                     polyLineCurrentProviderPath.strokeColor=[UIColor currentPathColor];
                                     polyLineCurrentProviderPath.map = mapview_;
                                     isPathCurrentPathDraw=TRUE;
                                 }
                                 }
                                 
                             }
                         }
                      }
                     }
             });
             
         }];
    }
}
-(void)drawPath:(GMSPath*)path withColor:(UIColor*)color{
    if(isPathDrawn)
    {
        isPathDrawn=false;
        if (polyLinePath.map)
        {
            polyLinePath.map=nil;
        }
        polyLinePath = [GMSPolyline polylineWithPath:path];
        polyLinePath.strokeColor = color;
        polyLinePath.strokeWidth = 5.f;
        polyLinePath.geodesic = YES;
        polyLinePath.map = mapview_;
    }
}
-(void)drawCurrentPath{
    if (isPathCurrentPathDraw && PREF.isPathDraw && providerStatus==ProviderStatusTripStarted)
    {
        if (driver_coordinate.latitude && driver_coordinate.longitude)
        {
        GMSMutablePath *tempPath=[[GMSMutablePath alloc]init];
        
            
        if (previousDriverLatLong.latitude && previousDriverLatLong.longitude)
        {
            NSLog(@" PRe Latitude %f and Longitude %f",previousDriverLatLong.latitude,previousDriverLatLong.longitude);
            [tempPath addCoordinate:previousDriverLatLong];
        }
        [tempPath addCoordinate:driver_coordinate];
        NSLog(@"Latitude %f and Longitude %f",driver_coordinate.latitude,driver_coordinate.longitude);
        polyLineCurrentProviderPath= [GMSPolyline polylineWithPath:tempPath];
        polyLineCurrentProviderPath.strokeWidth = 5.f;
        polyLineCurrentProviderPath.strokeColor=[UIColor currentPathColor];
        polyLineCurrentProviderPath.map = mapview_;
        previousDriverLatLong=driver_coordinate;
            
       }
    }
}


-(void)checkPaymentModeAndPromoAvailable
{
    if ([CurrentTrip sharedObject].paymentMode )
    {
        [_btnApplyPromo setHidden:!PREF.isPromoForCash];
        
    }
    else
    {
        [_btnApplyPromo setHidden:!PREF.isPromoForCard];
    }
}
-(void)waitingCounter
{
    currentWaitingTime++;
    if (currentWaitingTime<0)
    {
        [_lblWaitingTimeTitle setText:NSLocalizedString(@"WAIT_TIME_START_AFTER", nil)];
    }
    else
    {
        [_lblWaitingTimeTitle setText:NSLocalizedString(@"WAIT_TIME_START", nil)];
    }
    [_lblWaitingTimeValue setText:[NSString stringWithFormat:@"%li s",(long)currentWaitingTime ]];
    if([[CurrentTrip sharedObject].providerStatus integerValue] ==ProviderStatusArrived)
    {
        [_viewForWaitingTime setHidden:NO];
        [self.view bringSubviewToFront:_viewForWaitingTime ];
    }
    else
    {
        [_timerForWaitingTime invalidate];
        _timerForWaitingTime=nil;
        [_viewForWaitingTime setHidden:YES];
    }
}

- (IBAction)onClickBtnShareEta:(id)sender
{
    if ([UtilityClass isEmpty:currentTrip.destAddress ])
    {
        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_PLEASE_ENTER_DESTINATION_FIRST", nil)];
    }
    else
    {
        NSString *eta=[self getTime:current_coordinate destLocation:dest_coordinate];
        [APPDELEGATE hideLoadingView];
        NSString *shareEta=[NSString stringWithFormat:NSLocalizedString(@"MSG_SHARE_ETA", nil), currentTrip.destAddress,[_lblProviderName.text capitalizedString],eta];
        NSArray *activityItems = @[shareEta];
        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        
        activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
        [self presentViewController:activityVC animated:TRUE completion:nil];
    }
}

-(NSString*) getTime:(CLLocationCoordinate2D) src_location destLocation:(CLLocationCoordinate2D)dest_location
{
    [APPDELEGATE showLoadingWithTitle:@""];
    
    NSString *src_lat=[NSString stringWithFormat:@"%f",src_location.latitude];
    NSString *src_long=[NSString stringWithFormat:@"%f",src_location.longitude];
    NSString *dest_lat=[NSString stringWithFormat:@"%f",dest_location.latitude];
    NSString *dest_long=[NSString stringWithFormat:@"%f",dest_location.longitude];
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%@,%@&destinations=%@,%@&key=%@",src_lat,  src_long, dest_lat, dest_long, GoogleServerKey];
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSData *jsonData = [NSData dataWithContentsOfURL:url];
    if(jsonData != nil)
    {
        NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
        NSString *status=[[[[[dict valueForKey:GOOGLE_PARAM_ROWS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_ELEMENTS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_STATUS];
        if ([status isEqualToString:GOOGLE_PARAM_STATUS_OK])
        {

            NSString *second=[[[[[[dict valueForKey:GOOGLE_PARAM_ROWS] objectAtIndex:0] valueForKey:GOOGLE_PARAM_ELEMENTS] objectAtIndex:0] objectForKey:GOOGLE_PARAM_DURATION] valueForKey:GOOGLE_PARAM_VALUES];
            NSString *time=[[UtilityClass sharedObject]secondToTime:[second intValue]];
            return time;
        }
        else
        {
            return @"";
        }
    }
    else
    {
        return @"";
    }
    
}
-(void)share:(NSString*)msgToShare
{
    
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailer=[[MFMailComposeViewController alloc]init ];
            mailer.mailComposeDelegate=self;
            NSArray *toRecipients=[[NSArray alloc]initWithObjects:@"",nil];
            NSString *msg=msgToShare;
            [mailer setSubject:@"SHARE ETA"];
            [mailer setMessageBody:msg isHTML:NO];
            [mailer setToRecipients:toRecipients];
            
            
            [mailer setDefinesPresentationContext:YES];
            [mailer setEditing:YES];
            [mailer setModalInPopover:YES];
            [mailer setNavigationBarHidden:NO animated:YES];
            
            [self presentViewController:mailer animated:YES completion:nil];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                            message:@"Your device doesn't support the composer sheet"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
            
        }
    

}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail send canceled");
            [self.tabBarController setSelectedIndex:0];
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSent:
            [[UtilityClass sharedObject]showToast:@"Mail sent successfully."];
            [self dismissViewControllerAnimated:YES completion:NULL];
            
            NSLog(@"Mail send successfully");
            break;
        case MFMailComposeResultSaved:
            [[UtilityClass sharedObject]showToast:@"Mail saved"];
            
            NSLog(@"Mail Saved");
            break;
        case MFMailComposeResultFailed:
            [[UtilityClass sharedObject]showToast:[NSString stringWithFormat:@"Error:%@.", [error localizedDescription]]];
            NSLog(@"Mail send error : %@",[error localizedDescription]);
            break;
        default:
            break;
    }
}
-(void)stopTimer
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_timerForCheckReqStatus invalidate];
        _timerForCheckReqStatus=nil;
    });

}
-(void)startCheckRequestTimer

{
    dispatch_async(dispatch_get_main_queue(), ^{
        
    if ([_timerForCheckReqStatus isValid])
    {
        
    }
    else
    {
        _timerForCheckReqStatus = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkForTripStatus) userInfo:nil repeats:YES];
    }
    });
}
@end
