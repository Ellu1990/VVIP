//
//  HomeVc.m
//    Eber Client
//
//  Created by My Mac on 6/14/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "HomeVc.h"
#import "NSObject+Constants.h"
#import "UIColor+Colors.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "Parser.h"
#import "CurrentTrip.h"
#import "UtilityClass.h"
#import "UILabel+overrideLabel.h"

@implementation HomeVc
{
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setLocalization];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:NO];
    [self.navigationController setNavigationBarHidden:YES];
}
/*Set Intial View Colors & Text  */
-(void) setLocalization
{
    [self.view setBackgroundColor:[UIColor backGroundColor]];
    [_btnSignIn setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [_btnRegister setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    
    [self.btnSignIn setBackgroundColor:[UIColor clearColor]];
	[self.btnRegister setBackgroundColor:[UIColor clearColor]];
    
	[self.btnRegister setTitle:[NSLocalizedString(@"REGISTER", nil) uppercaseString] forState:UIControlStateNormal];
	[self.btnSignIn setTitle:[NSLocalizedString(@"LOGIN", nil) uppercaseString] forState:UIControlStateNormal];
    [self.lblTerms setText:[NSLocalizedString(@"ALL_COPYRIGHTS_RESERVED", nil) capitalizedString] withColor:[UIColor labelTitleColor]];
}
@end
