//
//  SliderVC.m
//  Employee
//
//  Created by Elluminati - macbook on 19/05/14.
//  Copyright (c) 2014 Elluminati MacBook Pro 1. All rights reserved.
//

#import "SliderVC.h"
#import "NSObject+Constants.h"
#import "UIImageView+image.h"
#import "SWRevealViewController.h"
#import "MapVc.h"
#import "CellSlider.h"
#import "UIColor+Colors.h"
#import "UtilityClass.h"
#import "DisplayCardVC.h"
#import "ProfileVC.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "PreferenceHelper.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Hotline.h"

@interface SliderVC ()
{
    CustomAlertWithTitle *logoutView;
    UITapGestureRecognizer *singleTap;
}
@end

@implementation SliderVC


#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    UINavigationController *nav=(UINavigationController *)self.revealViewController.frontViewController;
    self.mapViewObject=(MapVc *)[nav.childViewControllers objectAtIndex:0];
    
    [_viewForProfile setBackgroundColor:[UIColor clearColor]];
   [self setLocalization];
	
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

}
-(void)setLocalization
{
    
    UINavigationController *nav=(UINavigationController *)self.revealViewController.frontViewController;
    frontVC=[nav.childViewControllers objectAtIndex:0];
    [_lblName setTextColor:[UIColor labelTitleColor]];
    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [_viewForProfile setUserInteractionEnabled:YES];
    [_viewForProfile addGestureRecognizer:singleTap];
    arrayMenuIcons=[[NSMutableArray alloc]initWithObjects:@"menuProfile",@"menuPayment",@"menuMyBooking",@"menuHistory",@"menuReferral",@"menuSettings",@"menuContact",@"menuHelp",@"menuLogout",nil ];
    arrayMenuItems=[[NSMutableArray alloc]initWithObjects:
                                    NSLocalizedString(@"Profile",nil),NSLocalizedString(@"Payment", nil),NSLocalizedString(@"MyBooking", nil),NSLocalizedString(@"History", nil),NSLocalizedString(@"Referral", nil),NSLocalizedString(@"SETTINGS", nil),NSLocalizedString(@"EMERGENCY_CONTACTS", nil),NSLocalizedString(@"HELP_LINE", nil),NSLocalizedString(@"Logout", nil),nil ];
    arraySegueIdentifiers=[[NSMutableArray alloc]initWithObjects:SEGUE_PROFILE,SEGUE_TO_PAYMENT, nil];
    
}
-(void)tapDetected
{
    [self.revealViewController rightRevealToggle:self];
    [self.mapViewObject performSegueWithIdentifier:SEGUE_PROFILE sender:self];
    return;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.revealViewController.frontViewController.view setUserInteractionEnabled:NO];
    [self.revealViewController.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [_imgProfilePic downloadFromURL:PREF.userPicture withPlaceholder:[UIImage imageNamed:@"user"]];
    self.lblName.text=[NSString stringWithFormat:@"%@ %@",PREF.userFirstName,PREF.userLastName];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.revealViewController.frontViewController.view setUserInteractionEnabled:YES];
}

#pragma mark -
#pragma mark - UITableView Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayMenuItems count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellSlider *cell=(CellSlider *)[tableView dequeueReusableCellWithIdentifier:@"CellSlider"];
    if (cell==nil)
    {
        cell=[[CellSlider alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellSlider"];

    }
    [cell setCellData:[arrayMenuItems objectAtIndex:indexPath.row] andImage:[NSString stringWithFormat:@"%@",[arrayMenuIcons objectAtIndex:indexPath.row]]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   [tableView deselectRowAtIndexPath:indexPath animated:YES];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.revealViewController rightRevealToggle:self];
    });
    
    
    if ([[arrayMenuItems objectAtIndex:indexPath.row]isEqualToString:@"Logout"])
    {
	    logoutView=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"LOGOUT", nil) message:NSLocalizedString(@"ALERT_MSG_ARE_YOU_SURE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_NO", nil) otherButtonTitles:NSLocalizedString(@"ALERT_BTN_YES", nil)];
        return;
    }
    else if ([[arrayMenuItems objectAtIndex:indexPath.row]isEqualToString:NSLocalizedString(@"MyBooking", nil)])
    {
        [self.mapViewObject performSegueWithIdentifier:SEGUE_TO_MY_BOOKING sender:self];
        return;
        
    }
	else if ([[arrayMenuItems objectAtIndex:indexPath.row]isEqualToString:@"Profile"])
	{
        [self.mapViewObject performSegueWithIdentifier:SEGUE_PROFILE sender:self];
		return;
    }
	else if ([[arrayMenuItems objectAtIndex:indexPath.row]isEqualToString:@"Payment"])
	{
		[self.mapViewObject performSegueWithIdentifier:SEGUE_TO_PAYMENT sender:self];
		return;
	}
	else if ([[arrayMenuItems objectAtIndex:indexPath.row]isEqualToString:@"Referral"])
	{
		[self.mapViewObject performSegueWithIdentifier:SEGUE_TO_REFERRAL_CODE sender:self];
		return;
	}
	else if ([[arrayMenuItems objectAtIndex:indexPath.row]isEqualToString:@"History"])
	{
		[self.mapViewObject performSegueWithIdentifier:SEGUE_TO_HISTORY sender:self];
		return;
	}
    else if ([[arrayMenuItems objectAtIndex:indexPath.row]isEqualToString:NSLocalizedString(@"SETTINGS", nil)])
    {
        [self.mapViewObject performSegueWithIdentifier:SEGUE_TO_SETTINGS sender:self];
        return;
    }
    else if ([[arrayMenuItems objectAtIndex:indexPath.row]isEqualToString:NSLocalizedString(@"EMERGENCY_CONTACTS", nil)])
    {
        [self.mapViewObject performSegueWithIdentifier:SEGUE_TO_CONTACT sender:self];
        return;
    }
    else if ([[arrayMenuItems objectAtIndex:indexPath.row]isEqualToString: NSLocalizedString(@"HELP_LINE", nil)])
    {
        [[Hotline sharedInstance] showConversations:self];
        return;
    }
   
    else
    {
    
    }
}
#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [_imgProfilePic setRoundImageWithColor:[UIColor borderColor]];
    _imgProfilePic.center= CGPointMake(([UIScreen mainScreen].bounds.size.width * 0.8/2), _viewForProfile.frame.size.height/2-_lblName.frame.size.height/2);
    [_lblName setCenter:CGPointMake(_imgProfilePic.center.x,_imgProfilePic.center.y+_imgProfilePic.frame.size.height/2+_lblName.frame.size.height/2+10)];
    
}

-(void)onClickCustomDialogOk:(CustomAlertWithTitle *)view
{
    if (view==logoutView)
    {
        [APPDELEGATE showLoadingWithTitle:@""];
        NSString *url=[[NSString alloc]initWithFormat:@"%@",USER_LOGOUT] ;
        //NSlog(@"%@",url);
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:PREF.userId forKey:PARAM_USER_ID];
        [dictParam setObject:PREF.userToken forKey:PARAM_TOKEN];
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
            [afn getDataFromPath:url withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 
                 if (response)
                 {
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         
                         NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                         NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                                      options:kNilOptions
                                                                                        error:nil];
                         
                         
                         if([[jsonResponse valueForKey:SUCCESS] boolValue])
                         {
                             [PREF clearPreference];
                             FBSDKLoginManager *logout = [[FBSDKLoginManager alloc] init];
                             [logout logOut];
                             [[GIDSignIn sharedInstance] signOut];
                             [APPDELEGATE goToMain];
                         }
                     });
                 }
                 
             }];
        }
    
}

@end
