//
//  CountryCode.m
//  Eber Client
//  Created by Elluminati Mini Mac 5 on 09/08/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "CountryCodeCell.h"
#import "UIColor+Colors.h"
@implementation CountryCodeCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [_lblCountryCode setTextColor:[UIColor labelTextColor]];
    [_lblCountryName setTextColor:[UIColor labelTextColor]];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setCellData:(NSString*)strCountryCode andCountry:(NSString *)strCountryName
{
    [_lblCountryCode setText:strCountryCode];
    [_lblCountryName  setText:strCountryName];
}

@end
