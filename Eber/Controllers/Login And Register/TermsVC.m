//
//  TermsVC.m
//    Eber Client
//
//  Created by My Mac on 6/16/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "TermsVC.h"
#import "AppDelegate.h"
#import "UIColor+Colors.h"
#import "NSObject+Constants.h"
@implementation TermsVC

- (void)viewDidLoad
{
	[super viewDidLoad];
    [_viewForNavigation setBackgroundColor:[UIColor GreenColor]];
    [_lblTerms setTextColor:[UIColor labelTitleColor]];
    [_lblTerms setText:[NSLocalizedString(@"TITLE_TERMS_AND_CONDITION", nil) capitalizedString]];
    [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
	NSURL *websiteUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_URL,@"terms"]];
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
	[self.webview loadRequest:urlRequest];
	
}
- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	[[AppDelegate sharedAppDelegate] hideLoadingView];
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{

}
- (IBAction)onClickBtnTerms:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

@end
