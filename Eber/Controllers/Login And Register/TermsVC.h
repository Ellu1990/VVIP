//
//  TermsVC.h
//    Eber Client
//
//  Created by My Mac on 6/16/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsVC : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *webview;
@property (strong, nonatomic) IBOutlet UIButton *btnTerms;
@property (weak, nonatomic) IBOutlet UILabel *lblTerms;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigation;

@end
