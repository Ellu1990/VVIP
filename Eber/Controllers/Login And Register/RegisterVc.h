//
//  RegisterVc.h
//    Eber Client
//
//  Created by My Mac on 6/14/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNHelper.h"
#import "SelectImage.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import <CoreLocation/CoreLocation.h>
#import "CustomOtpDialog.h"
#import "CustomAlertWithTitle.h"
@import Firebase;
@interface RegisterVc : UIViewController<
CLLocationManagerDelegate,
UITextFieldDelegate,
UINavigationControllerDelegate,
UIActionSheetDelegate,
UIGestureRecognizerDelegate,
CustomOtpDialogDelegate,
CustomAlertDelegate>
{
    CLLocationCoordinate2D current_location;
}
//Views
@property(nonatomic, strong) CLLocationManager *locationManager;

@property (weak, nonatomic) IBOutlet UIView *viewForRegister;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imgProPic;
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtBio;
@property (weak, nonatomic) IBOutlet UITextField *txtZipCode;
@property(nonatomic,weak)IBOutlet UITextField *activeTextField;
@property (weak, nonatomic) IBOutlet UITextField *txtCompanyCode;
//View For Country Code
@property (weak, nonatomic) IBOutlet UIView *viewForSelectCountryCode;
@property (weak, nonatomic) IBOutlet UISearchBar *searchbar;
@property (weak, nonatomic) IBOutlet UITableView *tblForCountryCode;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectCountry;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

//Buttons
@property (weak, nonatomic) IBOutlet UIButton *btnSelectCountry;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckBox;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnTerm;


////// Actions
- (IBAction)onClickBtnPickerCancel:(id)sender;
- (IBAction)onClickBtnFb:(id)sender;
- (IBAction)onClickBtnselectCountry:(id)sender;
- (IBAction)onClickBtnGoogle:(id)sender;
- (IBAction)onClickBtnRegister:(id)sender;
- (IBAction)checkBoxBtnPressed:(id)sender;
- (IBAction)termsBtnPressed:(id)sender;
- (IBAction)onClickBtnSignIn:(id)sender;

//Other Properties
@property(nonatomic,copy)NSMutableArray *arrForCountry;
@property(nonatomic,copy) NSMutableDictionary *dictParam;
@property(nonatomic,copy) NSString *strImageData,
*strForRegistrationType,
*strForSocialId,
*strForToken,
*strForID,
*strForCity,
*strForCountry,
*jsonParameter;
@property(nonatomic,assign) BOOL isPicAdded;
/*FooterView*/
@property (weak, nonatomic) IBOutlet UIView *viewForFooter;
@property (weak, nonatomic) IBOutlet UILabel *lblAllReadyHaveAccount;
@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
/*view For Navigation*/
@property (weak, nonatomic) IBOutlet UIView *viewForNavigation;
@property (weak, nonatomic) IBOutlet UIButton *btnFb;
@property (weak, nonatomic) IBOutlet UIButton *btnGoogl;
@end
