//
//  LoginVc.m
//    Eber Client
//
//  Created by My Mac on 6/14/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "LoginVc.h"
#import "UIColor+Colors.h"
#import "NSObject+Constants.h"
#import "UtilityClass.h"
#import "AFNHelper.h"
#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "UITextField+BaseText.h"
#import "Parser.h"
#import "UILabel+overrideLabel.h"
#import "PreferenceHelper.h"
#import "CurrentTrip.h"
@interface LoginVc ()
{
	CustomAlertWithTextInput *dialogForForgetPassword;
    CGFloat keyBoardSize;
    UITapGestureRecognizer     *tapGesture;
}
@end

@implementation LoginVc
@synthesize strEmail,strLoginBy,strSocialUniqueID,dictParam,txtEmail,txtPassword,btnFb,btnForgotPsw,btnSignIn,btnSignUp,btnGoogle,activeTextField;
#pragma mark-View Life-Cycle
- (void)viewDidLoad
{
	[super viewDidLoad];
	[self setLocalization];
	/*Google and Facebook SignIn Delegate*/
	[GIDSignIn sharedInstance].uiDelegate = self;
	[GIDSignIn sharedInstance].delegate = self;
    FBSDKLoginManager *logout = [[FBSDKLoginManager alloc] init];
    [logout logOut];
    /************************/
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard)];
    [_scrLogin addGestureRecognizer:tapGesture];;
    
	dictParam=[[NSMutableDictionary alloc]init];
	strLoginBy=MANUAL;
    keyBoardSize=216.0f;
    strSocialUniqueID=@"";
	[dictParam setValue:strLoginBy forKey:PARAM_LOGIN_BY];
	[dictParam setValue:PREF.deviceToken forKey:PARAM_DEVICE_TOKEN];
}
-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    [_viewForNavigation setHidden:NO];
    /*keyBoard Notification Methods*/
    [self.navigationController setNavigationBarHidden:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:self.view.window];
}
-(void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}
-(void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}
-(void)dealloc
{
    [GIDSignIn sharedInstance].uiDelegate = nil;
    [GIDSignIn sharedInstance].delegate = nil;
    btnGoogle=nil;
    btnFb=nil;
}
#pragma mark-Actions (Click Events)
-(IBAction)onClickMoveToRegister:(id)sender
{
 [self performSegueWithIdentifier:SEGUE_TO_DIRCET_REGI sender:self];
}
-(IBAction)onClickLogin:(id)sender
{
    [_scrLogin setContentOffset:CGPointMake(0, 0)];
   if (![[UtilityClass sharedObject]isValidEmailAddress:txtEmail.text])
	{
		[[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_EMAIL", nil)];
	}
	else if (txtPassword.text.length<6)
	{
		[[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_PASSWORD", nil)];
	}
	else
	{
		[dictParam setValue:txtEmail.text forKey:PARAM_EMAIL];
		[dictParam setValue:txtPassword.text forKey:PARAM_PASSWORD];
		[dictParam setValue:DEVICE_TYPE forKey:PARAM_DEVICE_TYPE];
		[dictParam setValue:@"" forKey:PARAM_SOCIAL_UNIQUE_ID];
		[dictParam setValue:PREF.deviceToken forKey:PARAM_DEVICE_TOKEN];
		[dictParam setValue:MANUAL forKey:PARAM_LOGIN_BY];
		[self callWebService];
	}
}
//FB Login
-(IBAction)onClickFacebook:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:GOOGLE];
	 strLoginBy=FACEBOOK;
	 FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
	[login prepareForInterfaceBuilder];
    [login logOut];
		//NSlog(@"logout %@",[FBSDKAccessToken currentAccessToken].tokenString);
		[login 	logInWithReadPermissions: @[PARAM_FB_PUBLIC_PROFILE, PARAM_FB_EMAIL,PARAM_FB_USER_FRIENDS]
							handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
		 {
				if (error)
				{
					
				}
				else if (result.isCancelled)
				{
					
				}
				else
				{
					
					[APPDELEGATE  showLoadingWithTitle:NSLocalizedString(@"LOADING_FACEBOOK_INFO", nil)];
					
                    if ([FBSDKAccessToken currentAccessToken])
					{
                        NSDictionary *faceBookParams=[[NSDictionary alloc] initWithObjectsAndKeys:
                                                      PARAM_FB_REQUIRED_FIELDS ?: [NSNull null], PARAM_FB_FIELDS,
                                                      
                                                      nil];
						FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
												initWithGraphPath:@"me"
												parameters:faceBookParams
												HTTPMethod:nil];
						
						[request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
													   id result,
													   NSError *error)
						 {
							 // Handle the result
							 [APPDELEGATE hideLoadingView];
							 strSocialUniqueID=[result valueForKey:PARAM_FB_ID];
							 strEmail=[result valueForKey:PARAM_FB_EMAIL];
							 strLoginBy=FACEBOOK;

							 [dictParam setValue:strEmail forKey:PARAM_EMAIL];
							 [dictParam setValue:@"" forKey:PARAM_PASSWORD];
							 [dictParam setValue:DEVICE_TYPE forKey:PARAM_DEVICE_TYPE];
							 [dictParam setValue:strSocialUniqueID forKey:PARAM_SOCIAL_UNIQUE_ID];
							 [dictParam setValue:PREF.deviceToken forKey:PARAM_DEVICE_TOKEN];
							 [dictParam setValue:strLoginBy forKey:PARAM_LOGIN_BY];
							 [self callWebService];
							 
							 }];
					}
				}
		 }];
	  	
}
//Google Login
-(IBAction)onClickGooglePlus:(id)sender
{
		[[GIDSignIn sharedInstance] signIn];
}
-(IBAction)onClickForgotPsw:(id)sender
{
    [self.view endEditing:YES];
    dialogForForgetPassword=nil;
    dialogForForgetPassword=[[CustomAlertWithTextInput alloc]initWithTitle:NSLocalizedString(@"FORGET_PASSWORD", nil) placeHolder:NSLocalizedString(@"EMAIL", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_CANCEL", nil) okButtonTitle:NSLocalizedString(@"SEND", nil)];
    
}
-(void)onClickOkButton:(NSString *)inputTextData view:(CustomAlertWithTextInput *)view
{
    if([[UtilityClass sharedObject]isValidEmailAddress:inputTextData])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_SENDING_PASSWORD", nil)];
        NSMutableDictionary *tempData=[[NSMutableDictionary alloc]init];
        [tempData setObject:inputTextData forKey:PARAM_EMAIL];
        [tempData setObject:[NSNumber numberWithInteger:TYPE_USER ] forKey:PARAM_TYPE];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
        [afn getDataFromPath:WS_USER_FORGET_PASSWORD withParamData:tempData withBlock:^(id response, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                               NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                               if([[jsonResponse valueForKey:SUCCESS] boolValue])
                               {
                                   [view removeFromSuperview];
                                   [APPDELEGATE hideLoadingView];
                               }
                               else
                               {
                                   [APPDELEGATE hideLoadingView];
                                   NSString *str=[jsonResponse valueForKey:ERROR_CODE];
                                   [[UtilityClass sharedObject]showToast:NSLocalizedString(str, nil)];
                               }
                           });
            
        }];
    }
    else
    {
        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_EMAIL", nil)];
    }
    dialogForForgetPassword=nil;
}

#pragma mark-GOOGLE DELEGATE
/// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController
{
	[self presentViewController:viewController animated:YES completion:nil];
}
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

/**CALLED AFTER  LOGIN WITH GOOGLE TO RETRIVE DATA */
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
	withError:(NSError *)error {
	if (error)
	{	//NSlog(@"%@", error.localizedDescription);
	}
	else
	{
        if ([UtilityClass isEmpty:DEVICE_TOKEN])
        {
            DEVICE_TOKEN=@"";
        }
		strSocialUniqueID= user.authentication.idToken;
		strLoginBy=GOOGLE;
		strEmail=user.profile.email;
		[dictParam setValue:strEmail forKey:PARAM_EMAIL];
		[dictParam setValue:@"" forKey:PARAM_PASSWORD];
		[dictParam setValue:DEVICE_TYPE forKey:PARAM_DEVICE_TYPE];
		[dictParam setValue:strSocialUniqueID forKey:PARAM_SOCIAL_UNIQUE_ID];
		[dictParam setValue:DEVICE_TOKEN forKey:PARAM_DEVICE_TOKEN];
		[dictParam setValue:strLoginBy forKey:PARAM_LOGIN_BY];
        [self callWebService];

	}
}
#pragma mark- Web Service Methods
-(void)callWebService
{
    [self.view endEditing:YES];
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_LOGGING_YOU_IN", nil)];

		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
	
		[afn getDataFromPath:USER_LOGIN withParamData:dictParam withBlock:^(id response, NSError *error)
		 {
			 if (response)
			 {
                 if ([[Parser sharedObject] parseLogin:response])
                 {
                     dispatch_async(dispatch_get_main_queue(), ^
                                    {
                                        
                                        [self wsCheckInCompleteRequest];
                                    });
                 }
                 else
                 {
                     dispatch_async(dispatch_get_main_queue(), ^
                                    {
                                        [APPDELEGATE hideLoadingView];
                                    });
                 }
		   }
		else
		 {
			 
		 }
	}];
}
-(void)wsForgetPassword:(NSString*)email
{
    
    
}
#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
}

#pragma mark - SET LOCALIZATION
-(void) setLocalization
{
    
    UIFont *font=[UIFont fontWithName:@"Roboto-Bold" size: btnSignUp.titleLabel.font.pointSize];
    [btnSignUp.titleLabel setFont:font];
    [btnForgotPsw.titleLabel setFont:font];
    /*label setUp*/
    [_lblTitle setText:[NSLocalizedString(@"TITLE_LOGIN", nil)capitalizedString] withColor:[UIColor labelTitleColor]];
    [_lblAlredyHaveAccount setText:NSLocalizedString(@"HAVE_NOT_ACCOUNT_YET", nil)];

    /*button setup*/
    [btnForgotPsw setTitle:NSLocalizedString(@"Forgot Password?", nil) forState:UIControlStateNormal];
	[btnSignIn setTitle:[NSLocalizedString(@"TITLE_LOGIN", nil) uppercaseString] forState:UIControlStateNormal];
    [btnSignUp setTitle:[NSLocalizedString(@"REGISTER", nil) capitalizedString] forState:UIControlStateNormal];
    [btnForgotPsw setTitleColor:[UIColor textColor] forState:UIControlStateNormal];
    [btnSignUp setTitleColor:[UIColor textColor] forState:UIControlStateNormal];
    [btnSignIn setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    /*textView setUp*/
    [txtEmail setPlaceholder:[NSLocalizedString(@"EMAIL", nil) capitalizedString]];
    [txtPassword setPlaceholder:[NSLocalizedString(@"PASSWORD", nil) capitalizedString]];
    [txtEmail setTextColor:[UIColor textColor]];
    [txtPassword setTextColor:[UIColor textColor]];
    [_viewForNavigation setBackgroundColor:[UIColor GreenColor]];
    [_viewForLogin setBackgroundColor:[UIColor backGroundColor]];
    [self.view setBackgroundColor:[UIColor backGroundColor]];
    btnSignIn.layer.shadowOpacity=0.8;
    btnSignIn.layer.shadowOffset= CGSizeMake(0, 3.0f);
    btnSignIn.layer.shadowColor = [UIColor blackColor].CGColor;
    [btnSignIn setBackgroundColor:[UIColor buttonColor]];
    
    [txtEmail setBorder];
    [txtPassword setBorder];
}
#pragma mark-TEXTVIEW DELEGATE
-(void)textFieldDidBeginEditing:(UITextField *)textField
{ activeTextField=textField;}
-(void)textFieldDidEndEditing:(UITextField*)textField
{
    activeTextField=nil;
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==txtEmail)
    {
        [txtPassword becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        [self onClickLogin:nil];
    }
    return YES;
}

// Called when UIKeyboardWillShowNotification is sent
- (void)keyboardWillShow:(NSNotification*)aNotification
{
    if (activeTextField)
    {
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        CGPoint pt;
        CGRect rc = [activeTextField bounds];
        rc = [activeTextField convertRect:rc toView:_scrLogin];
        pt = rc.origin;
        pt.x = 0;
        pt.y -= kbSize.height;
        CGRect mainRect=self.scrLogin.frame;
        mainRect.size.height-=kbSize.height;
        if (!CGRectContainsRect(mainRect, rc))
        {
            [_scrLogin setContentOffset:pt animated:YES];
        }
    }
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillHide:(NSNotification*)aNotification
{}
- (void)hideKeyBoard
{
    [self.view endEditing:YES];
    [_scrLogin setContentOffset:CGPointZero];
}


-(void)wsCheckInCompleteRequest
{
    [[CurrentTrip sharedObject] resetObject];
    NSMutableDictionary *dictTripParam=[[NSMutableDictionary alloc]init];
    
    [dictTripParam setObject:strUserId forKey:PARAM_USER_ID];
    
    NSString *strForUrl=[NSString stringWithFormat:@"%@%@/%@",USER_GET_TRIP_STATUS,strUserId,strUserToken];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
    [afn getDataFromPath:strForUrl withParamData:dictTripParam withBlock:^(id response, NSError *error)
     {
         if ([[Parser sharedObject] parseTripStatus:response])
         {
             CurrentTrip *currentTrip=[CurrentTrip sharedObject];
             NSInteger isProviderAccepted=0;
             isProviderAccepted=[currentTrip.isProviderAccepted  integerValue] ;
             if (isProviderAccepted==1)
             {
                 IS_TRIP_EXSIST=YES;
                 IS_TRIP_ACCEPTED=YES;
             }
             else
             {
                 IS_TRIP_EXSIST=YES;
                 IS_TRIP_ACCEPTED=NO;
             }
             
             if (![UtilityClass isEmpty:currentTrip.providerId])
             {
                 [self getProviderDetail:currentTrip.providerId];
             }
             else
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[AppDelegate sharedAppDelegate] hideLoadingView];
                     [APPDELEGATE goToMap];
                     return;
                     
                 });
                 
             }
             
         }
         else
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 [APPDELEGATE goToMap];
                 self.navigationController.navigationBarHidden=NO;
                 return;
                 
             });
         }
     }];
    
}
-(void) getProviderDetail:(NSString *) strProviderId
{
    NSMutableDictionary *dictProviderDetail=[[NSMutableDictionary alloc]init];
    [dictProviderDetail setObject:strProviderId forKey:PARAM_PROVIDER_ID];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
    [afn getDataFromPath:WS_GET_PROVIDER_DETAIL withParamData:dictProviderDetail withBlock:^(id response, NSError *error)
     {
         if ([[Parser sharedObject] parseProvider:response])
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [[AppDelegate sharedAppDelegate] hideLoadingView];
                 self.navigationController.navigationBarHidden=NO;
                 [APPDELEGATE goToMap];
                 return;
             });
         }
         
     }];
    
    
    
}

@end
