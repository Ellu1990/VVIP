//
//  RegisterVc.m
//    Eber Client
//
//  Created by My Mac on 6/14/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "RegisterVc.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "UtilityClass.h"
#import "AppDelegate.h"
#import "NSObject+Constants.h"
#import "UIColor+Colors.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "UITextField+BaseText.h"
#import "CountryCodeCell.h"
#import "GoogleUtility.h"
#import "Parser.h"
#import "UILabel+overrideLabel.h"
#import "PreferenceHelper.h"
#import "UIImageView+image.h"
#define MAX_NUMBER 10

@interface RegisterVc ()
{
    BOOL searching;
    NSString *strSelectedCountryCode;
    NSMutableArray *tempArrForCountry,*userDefaultLanguages;;
    UITapGestureRecognizer *singleTap,*tapGesture;
    CustomOtpDialog *optDialog;
    CustomAlertWithTitle *gpsEnableDialog,*networkDialog;
}
@end

@implementation RegisterVc
@synthesize activeTextField,strForID,strForRegistrationType,strForSocialId,strForToken,strImageData,arrForCountry,isPicAdded,dictParam,strForCity,strForCountry,locationManager;
#pragma mark
#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arrForCountry=[[NSMutableArray alloc]init];
    dictParam=[[NSMutableDictionary alloc]init];
    tempArrForCountry=[[NSMutableArray alloc]init];
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyBoard)];
    [_scrollView addGestureRecognizer:tapGesture];
    
    strForRegistrationType=MANUAL;
    strForSocialId=@"";
    strForCountry=@"United States";
    strForCity=@"";
    strSelectedCountryCode=@"+1";
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
   	[self getLocation];
    [self SetLocalization];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:self.view.window];
}
-(void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}
-(void)dealloc
{
}
#pragma mark
#pragma mark - Action Methods
-(IBAction)onClickBtnRegister:(id)sender
{
    if (current_location.latitude && current_location.longitude)
    {
        
        if([self checkValidation])
        {
            [self buildParameter];
            if (PREF.isEmailOtpOn || PREF.isSmsOtpOn)
            {
            [self wsGetOtp];
            }
            else
            {
				[self wsRegister];
            }
        }
    }
    else
    {
        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"ALERT_LOCATION_NOT_FOUND", nil)];
    }
}
-(IBAction)onClickBtnGoogle:(id)sender
{
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_GOOGLE_INFO", nil)];
        [[GoogleUtility sharedObject]signInGoogle:^(BOOL success, GIDGoogleUser *user, NSError *error)
    {
            if (success)
            {
                _txtPassword.userInteractionEnabled=NO;
                strForSocialId=user.userID;
                _txtEmail.text=user.profile.email;
                NSArray *strname = [user.profile.name componentsSeparatedByString: @" "];
                _txtFirstName.text=[strname objectAtIndex:0];
                _txtLastName.text=[strname objectAtIndex:1];
                strForRegistrationType=GOOGLE;
                if (user.profile.hasImage)
                {
                    _imgProPic.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[user.profile imageURLWithDimension:100]]]?:[UIImage imageNamed:@"user"];
                }
                [[NSUserDefaults  standardUserDefaults] setBool:YES forKey:GOOGLE];
            }
            [APPDELEGATE hideLoadingView];
        } withParent:self];
}
-(void)onClickBtnFb:(id)sender
{
    //Set Preference Login With Facebook
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:GOOGLE];
    strForRegistrationType=FACEBOOK;
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login prepareForInterfaceBuilder];
        [login 	logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
                                 handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
         {
             if (error)
             {
                 NSLog(@"Process error");
             }
             else if (result.isCancelled)
             {
                 NSLog(@"Cancelled");
             }
             else
             {
                 NSLog(@"Logged in");
                 [APPDELEGATE  showLoadingWithTitle:NSLocalizedString(@"LOADING_FACEBOOK_INFO", nil) ];
                 
                 if ([FBSDKAccessToken currentAccessToken])
                 {
                     FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                                   initWithGraphPath:@"me"
                                                   parameters:@{@"fields": @"first_name, last_name, picture.type(large), email, name, id, gender"}
                                                   HTTPMethod:@"GET"];
                     
                     [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                           id result,
                                                           NSError *error)
                      {
                          // Handle the result
                          [APPDELEGATE hideLoadingView];
                          _txtPassword.userInteractionEnabled=NO;
                          strForSocialId=[result valueForKey:PARAM_FB_ID];
                          _txtEmail.text=[result valueForKey:PARAM_FB_EMAIL];
                          NSArray *arr=[[result valueForKey:PARAM_FB_NAME] componentsSeparatedByString:@" "];
                          _txtFirstName.text=[arr objectAtIndex:0];
                          _txtLastName.text=[arr objectAtIndex:1];
                          NSURL *pictureURL = [NSURL URLWithString:[[[result objectForKey:PARAM_FB_PICTURE] objectForKey:PARAM_FB_DATA] objectForKey:PARAM_FB_URL]];
                          UIImage *img=[UIImage imageWithData:[NSData dataWithContentsOfURL:pictureURL]];
                          [_imgProPic setImage:img];
                          isPicAdded=YES;
                      }];
                 }
             }
         }];
        [login logOut];
    
}
- (IBAction)onClickBtnPickerCancel:(id)sender
{   _viewForSelectCountryCode.hidden=YES;}
- (IBAction)onClickBtnselectCountry:(id)sender
{
    [self.view endEditing:YES];
    [[Parser sharedObject]parseCountryListToArray:&arrForCountry];
    [_tblForCountryCode reloadData];
    _searchbar.text=@"";
    _viewForSelectCountryCode.hidden=NO;
    _viewForSelectCountryCode=[[UtilityClass sharedObject]addShadow:_viewForSelectCountryCode];
    
}
- (IBAction)checkBoxBtnPressed:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if(!btn.tag )
    {
        btn.tag=1;
        [btn setImage:[UIImage imageNamed:@"cb_glossy_on"] forState:UIControlStateNormal];
        [_btnRegister setBackgroundColor:[UIColor buttonColor]];
        _btnRegister.enabled=TRUE;
        _btnRegister.alpha = 1.0;
        
    }
    else
    {
        btn.tag=0;
        [btn setImage:[UIImage imageNamed:@"cb_glossy_off"] forState:UIControlStateNormal];
        [_btnRegister setBackgroundColor:[UIColor buttonColor]];
        _btnRegister.enabled=FALSE;
        _btnRegister.alpha = 0.5;
        
        
    }
    
}
- (IBAction)termsBtnPressed:(id)sender
{
    
    
}

- (IBAction)onClickBtnSignIn:(id)sender
{
    //[APPDELEGATE goToMain];
}


#pragma Mark-WEB_SERRVICES
-(void)wsGetOtp
{
    
    
    NSMutableDictionary *dictOtpParam=[[NSMutableDictionary alloc]init];
    [dictOtpParam setObject:_txtEmail.text forKey:PARAM_EMAIL];
    [dictOtpParam setObject:_txtNumber.text forKey:PARAM_PHONE];
    [dictOtpParam setObject:_btnSelectCountry.titleLabel.text forKey:PARAM_COUNTRY_CODE];
    [dictOtpParam setObject:[NSNumber numberWithInteger:USER_TYPE] forKey:PARAM_TYPE];
    
   
        [[AppDelegate sharedAppDelegate] showLoadingWithTitle:NSLocalizedString(@"LOADING_SENDING_OTP", nil)];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
        [afn getDataFromPath:WS_GET_VERIFICATION_OTP withParamData:dictOtpParam withBlock:^(id response, NSError *error)
         {
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                                NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                                             options:kNilOptions
                                                                                               error:nil];
                                
                                
                                if([[jsonResponse valueForKey:SUCCESS] boolValue])
                                {
                                    NSString *emailOtp=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_EMAIL_OTP] ];
                                    NSString *smsOtp=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_SMS_OTP]];
                                    optDialog=[[CustomOtpDialog alloc]initWithOtpEmail:emailOtp optSms:smsOtp emailOtpOn:PREF.isEmailOtpOn smsOtpOn:PREF.isSmsOtpOn delegate:self];
                                    [self.view bringSubviewToFront:optDialog];
                                }
                                else
                                {
                                    NSString *str=[jsonResponse valueForKey:ERROR_CODE];
                                    [[UtilityClass sharedObject]showToast:str];
                                }
                                [[AppDelegate sharedAppDelegate] hideLoadingView];
                            });
             
         }];
}
-(void) wsRegister
{
    //[PreferenceHelper setObject:_txtPassword.text forKey:PREF_PASSWORD];
    
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_REGISTER", nil) ];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
        
        [afn getDataFromPath:WS_USER_REGISTER withParamData:dictParam withBlock:^(id response, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               
                               if([[Parser sharedObject]parseLogin:response])
                               {
                                   [APPDELEGATE hideLoadingView];
                                   [self performSegueWithIdentifier:SEGUE_TO_REFERRAL_CODE sender:self];
                                   return ;
                               }
                               else
                               {
                                   [APPDELEGATE hideLoadingView];
                               }
                           });
        }];
    
}
#pragma mark-TableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (searching)
        return tempArrForCountry.count;
    else
        return arrForCountry.count;
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"cell";
    CountryCodeCell *cell = (CountryCodeCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
        cell=[[CountryCodeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SimpleTableCell"];
    if (searching)
        [cell setCellData: [[tempArrForCountry objectAtIndex:[indexPath row]] valueForKey:PHONE_CODE_JSON_PARAMETER] andCountry:[[tempArrForCountry objectAtIndex:[indexPath row]] valueForKey:COUNTRY_NAME_JSON_PARAMETER]];
    else
        [cell setCellData: [[arrForCountry objectAtIndex:[indexPath row]] valueForKey:PHONE_CODE_JSON_PARAMETER] andCountry:[[arrForCountry objectAtIndex:[indexPath row]] valueForKey:COUNTRY_NAME_JSON_PARAMETER]];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(searching)
    {
        [_btnSelectCountry setTitle:[[tempArrForCountry objectAtIndex:[indexPath row]] valueForKey:PHONE_CODE_JSON_PARAMETER] forState:UIControlStateNormal];
        
        strForCountry=[[tempArrForCountry objectAtIndex:[indexPath row]] valueForKey:COUNTRY_NAME_JSON_PARAMETER];
    }else
    {   [_btnSelectCountry setTitle:[[arrForCountry objectAtIndex:[indexPath row]] valueForKey:PHONE_CODE_JSON_PARAMETER] forState:UIControlStateNormal];
        
        strForCountry=[[arrForCountry objectAtIndex:[indexPath row]] valueForKey:COUNTRY_NAME_JSON_PARAMETER];
    }
    [_tblForCountryCode deselectRowAtIndexPath:indexPath animated:YES];
    [_viewForSelectCountryCode setHidden:YES];
}
#pragma  mark Search Country;
- (void)searchBar:(UISearchBar *)SearchBar textDidChange:(NSString *) searchText
{
    [tempArrForCountry removeAllObjects];
    if (searchText.length == 0)
    {   searching = NO; }
    else
    {
        searching = YES;
        for (NSDictionary *Country in arrForCountry)
        {
            NSString *countryName=[Country valueForKey:COUNTRY_NAME_JSON_PARAMETER];
            
            if ([countryName hasPrefix:[searchText capitalizedString]] )
            {
                [tempArrForCountry addObject:Country];
            }
        }
        
    }
    
    [_tblForCountryCode reloadData];
}
#pragma mark
#pragma mark - VALIDATION Methods
-(BOOL) checkValidation
{
    
    if(_txtFirstName.text.length<1 || _txtLastName.text.length<1 || _txtEmail.text.length<1 || _txtNumber.text.length<10 )
    {
        if(_txtFirstName.text.length<1)
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_FIRST_NAME", nil)];
            // [_txtFirstName becomeFirstResponder];
        }
        else if(_txtLastName.text.length<1)
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_LAST_NAME", nil)];
            //[_txtLastName becomeFirstResponder];
        }
        else if(_txtEmail.text.length<1)
        {
            
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_EMAIL", nil)];
            
            //[_txtEmail becomeFirstResponder];
        }
        else if(_txtNumber.text.length<10)
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_PHONE", nil)];
            //[_txtNumber becomeFirstResponder];
        }
        return false;
    }
    else
    {
        
        if([[UtilityClass sharedObject]isValidEmailAddress:_txtEmail.text])
        {
            if ([strForRegistrationType isEqualToString:MANUAL] && _txtPassword.text.length<6)
            {
                [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_PASSWORD", nil)];
                // [_txtPassword becomeFirstResponder];
                return FALSE;
            }
            return true;
        }
        
        else
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_EMAIL", nil)];
            return false;
        }
    }
    
}
-(void)buildParameter
{
    [dictParam setValue:_txtEmail.text forKey:PARAM_EMAIL];
    [dictParam setValue:_txtFirstName.text forKey:PARAM_FIRST_NAME];
    [dictParam setValue:_txtLastName.text forKey:PARAM_LAST_NAME];
    [dictParam setValue:_txtNumber.text forKey:PARAM_PHONE];
    [dictParam setValue:_btnSelectCountry.titleLabel.text forKey:PARAM_COUNTRY_CODE];
    [dictParam setValue:DEVICE_TOKEN forKey:PARAM_DEVICE_TOKEN];
    [dictParam setValue:DEVICE_TYPE forKey:PARAM_DEVICE_TYPE];
    [dictParam setValue:_txtBio.text forKey:PARAM_BIO];
    [dictParam setValue:_txtAddress.text forKey:PARAM_ADDRESS];
    
    [dictParam setValue:_txtZipCode.text forKey:PARAM_ZIPCODE];
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    [dictParam setValue:tzName forKey:PARAM_DEVICE_TIMEZONE];
    [dictParam setValue:strForCountry forKey:PARAM_COUNTRY];
    [dictParam setValue:strForCity forKey:PARAM_CITY];
	[dictParam setValue:_txtCompanyCode.text forKey:PARAM_COMPANY_ID];
	if (!isPicAdded)
    {
        [dictParam setValue:@"" forKey:PARAM_PICTURE_DATA];
    }
    
    [dictParam setValue:strForRegistrationType forKey:PARAM_LOGIN_BY];
    if([strForRegistrationType isEqualToString:FACEBOOK])
    {
        [dictParam setValue:@""  forKey:PARAM_PASSWORD];
        [dictParam setValue:strForSocialId forKey:PARAM_SOCIAL_UNIQUE_ID];}
    else if
        ([strForRegistrationType isEqualToString:GOOGLE])
        
    {
        [dictParam setValue:@""  forKey:PARAM_PASSWORD];
        [dictParam setValue:strForSocialId forKey:PARAM_SOCIAL_UNIQUE_ID];
    }
    else
    {
        [dictParam setValue:_txtPassword.text forKey:PARAM_PASSWORD];
        [dictParam setValue:@"" forKey:PARAM_SOCIAL_UNIQUE_ID];
    }
}
#pragma mark
#pragma mark - Text Delegate Methods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeTextField=textField;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    activeTextField=nil;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField==_txtNumber)
    {
        if (string==nil || [string isEqualToString:@""] || [string isKindOfClass:[NSNull class]] || string.length < 1)
        {
            
        }
        
        else if(_txtNumber.text.length >=MAX_NUMBER)
        {
            [_txtBio becomeFirstResponder];
            return NO;
        }
        
    }
    return YES;

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==_txtFirstName)
    {  [_txtLastName becomeFirstResponder];textField.text = [textField.text capitalizedString];
    }
    else if(textField==_txtLastName)
    {[_txtEmail becomeFirstResponder]; textField.text = [textField.text capitalizedString];
    }
    else if(textField==_txtEmail)
    {[_txtPassword becomeFirstResponder];
    }
    else if(textField==_txtPassword){
        [_txtNumber becomeFirstResponder];}
    else if(textField==_txtNumber)
    {
	       [_txtBio becomeFirstResponder];
    }
    else if(textField==_txtBio)
    {
        [_txtAddress becomeFirstResponder];
        textField.text = [textField.text capitalizedString];
    }
    else if(textField==_txtAddress)
    {   [_txtZipCode becomeFirstResponder];
        textField.text = [textField.text capitalizedString];
    }
    else if(textField==_txtZipCode)
    {
        [textField resignFirstResponder];
        if (_btnRegister.enabled) {
        [self onClickBtnRegister:nil];
        }
        else
        {
        
        }
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
}
- (NSString *)encodeToBase64String:(UIImage *)image {
    NSData *data=UIImageJPEGRepresentation(image, 1.0);
    NSString *str=[data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    return str;
}
-(void)SetLocalization
{

    [_viewForRegister setBackgroundColor:[UIColor backGroundColor]];
    
    /*textView SetUp*/
    [_txtFirstName  setPlaceholder:[NSLocalizedString(@"FIRST_NAME", nil) capitalizedString]];
    [_txtLastName  setPlaceholder:[NSLocalizedString(@"LAST_NAME", nil) capitalizedString]];
    [_txtEmail  setPlaceholder:[NSLocalizedString(@"EMAIL", nil) capitalizedString]];
    [_txtPassword  setPlaceholder:[NSLocalizedString(@"PASSWORD", nil) capitalizedString]];
    [_txtBio  setPlaceholder:[NSLocalizedString(@"BIO", nil) capitalizedString]];
    [_txtAddress  setPlaceholder:[NSLocalizedString(@"ADDRESS", nil) capitalizedString]];
    [_txtZipCode  setPlaceholder:[NSLocalizedString(@"ZIPCODE", nil) capitalizedString]];
    [_txtNumber  setPlaceholder:[NSLocalizedString(@"NUMBER", nil) capitalizedString]];

    [_txtFirstName   setTextColor:[UIColor textColor]];
    [_txtLastName    setTextColor:[UIColor textColor]];
    [_txtEmail       setTextColor:[UIColor textColor]];
    [_txtPassword    setTextColor:[UIColor textColor]];
    [_txtBio         setTextColor:[UIColor textColor]];
    [_txtAddress     setTextColor:[UIColor textColor]];
    [_txtZipCode     setTextColor:[UIColor textColor]];
    [_txtNumber    setTextColor:[UIColor textColor]];
	[_txtCompanyCode    setTextColor:[UIColor textColor]];

	
    /*SET TITLE*/
    [_btnSelectCountry setTitleColor:[UIColor labelTextColor] forState:UIControlStateNormal];
    [_btnTerm setTitle:NSLocalizedString(@"I AGREE TO THE TERMS AND CONDITION", nil) forState:UIControlStateNormal];
    _btnTerm.titleLabel.numberOfLines = 1;
    _btnTerm.titleLabel.adjustsFontSizeToFitWidth = YES;
    [_lblSelectCountry setText:NSLocalizedString(@"COUNTRY_CODE", nil) withColor:[UIColor GreenColor]];
    _btnTerm.titleLabel.lineBreakMode = NSLineBreakByClipping;
    [_btnSignIn setTitle:[NSLocalizedString(@"LOGIN", nil) capitalizedString] forState:UIControlStateNormal];
    [_btnSignIn setTitleColor:[UIColor textColor] forState:UIControlStateNormal];
    _titleLabel.text=[NSLocalizedString(@"TITLE_REGISTER", nil) capitalizedString];
    _titleLabel.textColor=[UIColor labelTitleColor];
    [_btnRegister setTitle:NSLocalizedString(@"REGISTER", nil) forState:UIControlStateNormal];
    _lblAllReadyHaveAccount.text=NSLocalizedString(@"ALREADY_HAVE_ACCOUNT?", nil);
    /*VIEW CUTTING AND VISIBILITY*/
    [_txtEmail setBorder];
    [_txtFirstName setBorder];
    [_txtLastName setBorder];
    [_txtPassword setBorder];
    [_txtAddress setBorder];
    [_txtBio setBorder];
    [_txtZipCode setBorder];
    [_txtEmail setBorder];
    [_txtNumber setBorder];
	[_txtCompanyCode setBorder];

    [_btnFb setHidden:YES];
    [_btnFb setBackgroundColor:[UIColor clearColor]];
    [_btnFb setTitleColor:[UIColor labelTitleColor] forState:UIControlStateNormal];
    _btnRegister.alpha = 0.5;
    [_scrollView setContentSize:CGSizeMake(0, _btnCheckBox.frame.origin.y+100)];
    CGPoint offset=CGPointMake(0, 10);
    [_scrollView setContentOffset:offset animated:YES];
    [_btnCheckBox setImage:[UIImage imageNamed:@"cb_glossy_off"] forState:UIControlStateNormal];
    [_btnRegister setEnabled:false];
    
    [[_btnSelectCountry layer] setBorderWidth:2.0f];
    [[_btnSelectCountry layer] setBorderColor:[UIColor borderColor].CGColor];
    _viewForSelectCountryCode.hidden=YES;
    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [_imgProPic setUserInteractionEnabled:YES];
    [_imgProPic addGestureRecognizer:singleTap];
    [_imgProPic setUserInteractionEnabled:YES];
    
    [_btnRegister setBackgroundColor:[UIColor buttonColor]];
    _btnRegister.layer.shadowOpacity=0.8;
    _btnRegister.layer.shadowOffset= CGSizeMake(0, 3.0f);
    _btnRegister.layer.shadowColor = [UIColor blackColor].CGColor;
    [_scrollView accessibilityScroll:UIAccessibilityScrollDirectionDown];
    UIFont *font=[UIFont fontWithName:@"Roboto-Bold" size: 14];
    [_btnSignIn.titleLabel setFont:font];
    [_btnSignIn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_scrollView setShowsVerticalScrollIndicator:NO];
    [_viewForNavigation setBackgroundColor:[UIColor GreenColor]];
}
-(void)tapDetected
{
    [self.view endEditing:YES];
    SelectImage *view=[SelectImage getSelectImageViewwithParent:self];
    [view bringSubviewToFront:self.parentViewController.view];
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [_imgProPic setRoundImageWithColor:[UIColor borderColor]];
    [_imgProPic setCenter:CGPointMake(self.view.center.x, _imgProPic.center.y)];
    CGPoint centerImageView = _imgProPic.center;
    centerImageView.x = self.view.center.x;
    _imgProPic.center = centerImageView;
}
#pragma Location Related Methods
-(void) getCountryAndCity:(CLLocationCoordinate2D )location2d
{
    CLLocation *location = [[CLLocation alloc] initWithLatitude:location2d.latitude longitude:location2d.longitude];
     userDefaultLanguages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en_US", nil] forKey:@"AppleLanguages"];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    
    
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *array, NSError *error){
                       if (error)
                       {
                           NSLog(@"Geocode failed with error: %@", error);
                           
                           return;
                       }
                       CLPlacemark *placemark = [array objectAtIndex:0];
                       if (placemark.locality)
                       {
                           strForCity=placemark.locality;
                       }
                       else
                       {
                           if(placemark.administrativeArea)
                               strForCity=placemark.administrativeArea;
                           else
                               strForCity=placemark.subAdministrativeArea;
                       }
                       strForCountry=placemark.country;
                       
                       [[NSUserDefaults standardUserDefaults] setObject:userDefaultLanguages forKey:@"AppleLanguages"];
                       
                       if (strForCountry==nil)
                       {
                           strForCity=@"";
                           strForCountry=@"United States";
                           [_btnSelectCountry setTitle:@"+1" forState:UIControlStateNormal];
                        }
                       else
                       {
                           [self getCountryCodeList:strForCountry];
                       }
                   }];
    
}
-(void) getLocation
{
    CLLocationCoordinate2D coordinate;
    if ([CLLocationManager locationServicesEnabled])
    {
        if ([APPDELEGATE connected])
        {
            [locationManager startUpdatingLocation];
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            locationManager.distanceFilter = kCLDistanceFilterNone;
            #ifdef __IPHONE_8_0
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8"))
            {[locationManager requestAlwaysAuthorization];}
            #endif
            CLLocation *location = [locationManager location];
            if (location)
            {
                coordinate = [location coordinate];
                current_location=coordinate;
                [self getCountryAndCity:current_location];
            }
        }
        else
        {
            networkDialog=nil;
            networkDialog=[[CustomAlertWithTitle alloc]
                                                 initWithTitle:NSLocalizedString(@"ALERT_TITLE_NETWORK_STATUS", nil)
                                                 message:NSLocalizedString(@"ALERT_MSG_NO_INTERNET", nil)
                                                 delegate:APPDELEGATE
                                                 cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_NO", nil)
                                                 otherButtonTitles:NSLocalizedString(@"ALERT_BTN_YES", nil)];
            
        }
    }
    else
    {
        gpsEnableDialog=nil;
        gpsEnableDialog=[[CustomAlertWithTitle alloc]initWithTitle:NSLocalizedString(@"ALERT_TITLE_GPS_NOT_AVAILABEL", nil) message:NSLocalizedString(@"ALERT_MSG_LOCATION_SERVICE_NOT_AVAILABLE", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_NO", nil) otherButtonTitles:NSLocalizedString(@"ALERT_BTN_YES", nil)];
    }
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    CLLocation *location = [locations objectAtIndex:0];
    strForCurLatitude=[NSString stringWithFormat:@"%f",location.coordinate.latitude];
    strForCurLongitude=[NSString stringWithFormat:@"%f",location.coordinate.longitude];
    if (location)
    {
        current_location=[location coordinate];
        [locationManager stopUpdatingLocation];
        [manager stopUpdatingHeading];
    }
    
    
}
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    NSLog(@"didUpdateToLocation");}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError %@",error);
    if (!error)
    {
        [manager stopUpdatingLocation];
    }
    
}
#pragma -
#pragma mark Image picker delegate methdos
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    _imgProPic.image=image;
    isPicAdded=true;
    NSString *strimage =[self encodeToBase64String:_imgProPic.image];
    strImageData=strimage;
    [dictParam setObject:strimage forKey:PARAM_PICTURE_DATA];
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark
#pragma mark - Segue Methods
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}
// Called when UIKeyboardWillShowNotification is sent
- (void)keyboardWillShow:(NSNotification*)aNotification
{
    if (activeTextField)
    {
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        CGPoint pt;
        CGRect rc = [activeTextField bounds];
        rc = [activeTextField convertRect:rc toView:_scrollView];
        pt = rc.origin;
        pt.x = 0;
        pt.y -= kbSize.height;
        CGRect mainRect=self.scrollView.frame;
        mainRect.size.height-=kbSize.height;
        if (!CGRectContainsRect(mainRect, rc))
        {
            [_scrollView setContentOffset:pt animated:YES];
        }
    }

    
}
// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillHide:(NSNotification*)aNotification
{
    
}
#pragma mark-CUSTOM DIALOG METHODS
-(void)onClickCustomDialogOk:(CustomAlertWithTitle *)view
{
    if (view==gpsEnableDialog)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}
-(void)onClickCustomDialogOtpOk:(CustomOtpDialog *)view
{
    
    [self wsRegister];
}
- (void)hideKeyBoard
{
    [self.view endEditing:YES];
    [_scrollView setContentOffset:CGPointZero];
}

-(void)getCountryCodeList:(NSString*) country
{
    [[Parser sharedObject]parseCountryListToArray:&arrForCountry];
            for (NSDictionary *Country in arrForCountry)
            {
                NSString *countryName=[Country valueForKey:COUNTRY_NAME_JSON_PARAMETER];
                
                if ([countryName isEqualToString:[country capitalizedString]] )
                {
                    [_btnSelectCountry setTitle:[Country valueForKey:PHONE_CODE_JSON_PARAMETER] forState:UIControlStateNormal];
                    strForCountry=[Country valueForKey:COUNTRY_NAME_JSON_PARAMETER];
                }
            }
}
@end
