//
//  LoginVc.h
//    Eber Client
//
//  Created by My Mac on 6/14/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <Google/SignIn.h>
#import "CustomAlertWithTextInput.h"

/*include Delegates*/
@interface LoginVc : UIViewController
<GIDSignInUIDelegate,
GIDSignInDelegate,
CustomAlertWithTextInput>

/*outLets*/
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property(nonatomic,weak)IBOutlet UIScrollView *scrLogin;
@property(nonatomic,weak)IBOutlet UITextField *txtEmail;
@property(nonatomic,weak)IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIView *viewForLogin;
/*To know which TextField is Selected*/

@property(nonatomic,weak)IBOutlet UITextField *activeTextField;
#pragma mark-Button
@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPsw;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
#pragma mark-Action
- (IBAction)onClickLogin:(id)sender;
- (IBAction)onClickMoveToRegister:(id)sender;
- (IBAction)onClickForgotPsw:(id)sender;
- (IBAction)onClickGooglePlus:(id)sender;
- (IBAction)onClickFacebook:(id)sender;
/*View For Navigation*/
@property (weak, nonatomic) IBOutlet UIView *viewForNavigation;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnGoogle;
@property (strong, nonatomic) IBOutlet UIButton *btnFb;
@property (weak, nonatomic) IBOutlet UILabel *lblAlredyHaveAccount;

/**Used to Pass Parameter To The Web Service*/
@property(nonatomic,copy) NSMutableDictionary *dictParam;
/** Used To Identify User*/
@property(nonatomic,copy) NSString
*strLoginBy,
*strSocialUniqueID,
*strEmail;
@end
