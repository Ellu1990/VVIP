//
//  HistoryVc.h
//  Eber Client
//  Created by My Mac on 7/2/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryVc : UIViewController

@property (strong, nonatomic) IBOutlet UIView *SearchView;
@property (strong, nonatomic) IBOutlet UITableView *tblHistoryTrip;
@property (strong, nonatomic) IBOutlet UILabel *lblNoItems;
@property (strong, nonatomic) IBOutlet UIButton *btnSearch;
@property (strong, nonatomic) IBOutlet UIButton *btnToDate;
@property (strong, nonatomic) IBOutlet UIButton *btnFromDate;
@property (strong, nonatomic) IBOutlet UIView *viewForPicker;
@property (strong, nonatomic) IBOutlet UIDatePicker *dtPicker;
@property (strong, nonatomic) IBOutlet UIButton *btnDone;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;


//ACTION
- (IBAction)onClickBtnFromDate:(id)sender;
- (IBAction)onClickBtnToDate:(id)sender;
- (IBAction)onClickBtnDone:(id)sender;
- (IBAction)onClickBtnSearch:(id)sender;
- (IBAction)onClickBtnRefresh:(id)sender;
/*SEarch View*/
/*Search View*/
@property (weak, nonatomic) IBOutlet UILabel *lblFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet UILabel *lblDivider;


@end
