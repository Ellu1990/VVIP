//
//  HistoryDetailVC.m
//  Rider Driver
//
//  Created by My Mac on 7/8/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//
#import "HistoryDetailVC.h"
#import "AFNHelper.h"
#import "AppDelegate.h"
#import "NSObject+Constants.h"
#import "Trip.h"
#import "UtilityClass.h"
#import "UIColor+Colors.h"
#import "UILabel+overrideLabel.h"
#import "UIImageView+image.h"
#import "PreferenceHelper.h"
#import "Parser.h"
#import "Invoice.h"

@interface HistoryDetailVC ()
{
    NSString *strForCancelReason;
    BOOL _Authenticated;
    NSURLRequest *_FailedRequest;
    NSURLConnection * connection;
    NSURLRequest *requestURL;
    NSString* webStringURL;
}
@end

@implementation HistoryDetailVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_viewForInvoice setHidden:YES];
    _webView.scrollView.scrollEnabled = TRUE;
    _webView.scalesPageToFit = TRUE;
    [APPDELEGATE showLoadingWithTitle:@"LOADING_HISTORY_DETAIL"];
    _viewForHistory=[self addShadow:_viewForHistory];
    _btnInvoice.layer.cornerRadius = 5; // this value vary as per your desire
    _btnInvoice.clipsToBounds = YES;
    [self callwebservice];
}
-(void)viewWillAppear:(BOOL)animated
{
    [_viewForInvoice setHidden:YES];
    [_viewForInvoice setBackgroundColor:[UIColor backGroundColor]];
    [_lblTripNo setText:NSLocalizedString(@"TRIP_ID", nil)];
    [_lblTripNo setTextColor:[UIColor labelTextColor]];
    [_lblTripNoValue setTextColor:[UIColor textColor]];
    [_btnInvoice setTitle:[NSLocalizedString(@"INVOICE", nil) uppercaseString] forState:UIControlStateNormal];
    [_btnInvoice setBackgroundColor:[UIColor buttonColor]];
    [_btnInvoice setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [_btnBack setTitle:[NSLocalizedString(@"TITLE_HISTORY_DETAIL", nil) capitalizedString] forState:UIControlStateNormal];
    [_btnBack setTitleColor:[UIColor labelTitleColor] forState:UIControlStateNormal];
    [self addShadow:_viewForDiscounts];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
- (IBAction)onClickBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [_imgProvider setRoundImageWithColor:[UIColor borderColor]];
    
}
-(void)callwebservice
{
    
        NSString * strForUserId=PREF.userId;
        NSString * strForUserToken=PREF.userToken;
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:strForUserId forKey:PARAM_USER_ID];
        [dictParam setObject:strForUserToken forKey:PARAM_TOKEN];
        [dictParam setObject:_trip_id forKey:PARAM_TRIP_ID];
        [APPDELEGATE showLoadingWithTitle:@""];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
        [afn getDataFromPath:USER_TRIP_DETAIL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             
             //NSlog(@"History Data= %@",response);
             
             dispatch_async(dispatch_get_main_queue(), ^
                            {
                                NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                                NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                                             options:kNilOptions
                                                                                               error:nil];
                                if (response)
                                {
                                    if([[jsonResponse valueForKey:SUCCESS] intValue]==1)
                                    {
                                        NSDictionary *tripJson= [jsonResponse objectForKey:PARAM_TRIP];
                                        
                                        
                                        if ([[NSString stringWithFormat:@"%@",[tripJson valueForKey:PARAM_IS_TRIP_COMPLETED] ]boolValue])
                                        {
                                            if ([[Parser sharedObject]parseInvoice:response])
                                            {
                                                [_btnInvoice setHidden:NO];
                                            }
                                        }
                                        else
                                        {
                                            BOOL cancelationCharge=[[NSString stringWithFormat:@"%@",[tripJson valueForKey:PARAM_IS_CANCELLATION_FEE] ]boolValue];
                                            if(cancelationCharge)
                                            {
                                                if ([[Parser sharedObject]parseInvoice:response])
                                                {
                                                    [_btnInvoice setHidden:NO];
                                                }
                                                else
                                                {
                                                    [_btnInvoice setHidden:YES];
                                                }
                                            }
                                            else
                                            {
                                                [_btnInvoice setHidden:YES];
                                            }
                                        }
                                        double tempDistance=[[NSString stringWithFormat:@"%@",[tripJson valueForKey:PARAM_TRIP_DISTANCE] ] doubleValue];
                                        self.lblTripDistance.text=[NSString stringWithFormat:@"%.2f %@",tempDistance,_unit ];
                                        self.lblSrcAddress.text=[tripJson valueForKey:PARAM_TRIP_SOURCE_ADDRESS];
                                        self.lblDestAddress.text=[tripJson valueForKey:PARAM_TRIP_DESTINATION_ADDRESS];
                                        self.lblTripCost.text=[NSString stringWithFormat:@"%@ %.2f",_currency,[[NSString stringWithFormat:@"%@",[tripJson valueForKey:PARAM_TRIP_COST]] doubleValue]];
                                        NSString *strTime=[tripJson valueForKey:PARAM_USER_CREATE_TIME];
                                        NSString *timeZone=[[UtilityClass sharedObject]getTimeZone];
                                        [[UtilityClass sharedObject]setTimeZone:@"UTC"];
                                        _tripDate=[[UtilityClass sharedObject]stringToDate:strTime withFormate:DATE_TIME_FORMAT_WEB];
                                        [[UtilityClass sharedObject]setTimeZone:timeZone];
                                        _lblTripTime.text=[NSString stringWithFormat:@"%@",[[UtilityClass sharedObject] DateToString:_tripDate withFormate:TIME_FORMAT_AM]];
                                        [self setDate];
                                        NSString *strDuration=[tripJson valueForKey:PARAM_TRIP_TIME];
                                        _lblTripDuration.text= [NSString stringWithFormat:@"%@ %@",strDuration,TIME_SUFFIX];
                                        NSDictionary *tripJsonProvider= [jsonResponse objectForKey:PARAM_PROVIDER];
                                        self.lblProviderNAme.text=[tripJsonProvider valueForKey:PARAM_FIRST_NAME];
                                        self.lblTripNoValue.text=[NSString stringWithFormat:@"%@",[tripJsonProvider valueForKey:PARAM_TRIP_NUMBER] ];
                                        [_lblSrcAddress setFontForLabel:_lblSrcAddress withMaximumFontSize:14 andMaximumLines:2];
                                        [_lblDestAddress setFontForLabel:_lblDestAddress withMaximumFontSize:14 andMaximumLines:2];
                                        CGFloat pointSize=MIN(_lblSrcAddress.font.pointSize, _lblDestAddress.font.pointSize);
                                        UIFont *font=[UIFont fontWithName:_lblSrcAddress.font.fontName size:pointSize];
                                        _lblSrcAddress.font=_lblDestAddress.font=font;
                                        [_lblSrcAddress sizeToFit];
                                        [_lblDestAddress sizeToFit];
                                        [APPDELEGATE hideLoadingView];
                                        
                                        NSString *url=[tripJsonProvider valueForKey:PARAM_PICTURE];
                                        [_imgProvider downloadFromURL:url withPlaceholder:[UIImage imageNamed:@"user"]];
                                        url=[jsonResponse valueForKey:PARAM_MAP_IMAGE];
                                        webStringURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                                        NSURL *urlAddress = [NSURL URLWithString:webStringURL];
                                        
                                        requestURL = [NSURLRequest requestWithURL:urlAddress];
                                        [_webView loadRequest:requestURL];
                                        
                                    }
                                    else
                                    {
                                        [[UtilityClass sharedObject]displayAlertWithTitle:@"" andMessage:NSLocalizedString([jsonResponse valueForKey:ERROR_CODE],nil)];
                                    }
                                    [[AppDelegate sharedAppDelegate] hideLoadingView];
                                }
                            });
         }];
}
-(void)onClickBtnInvoice:(id)sender
{
    if(_viewForInvoice.isHidden)
    {
        [_btnInvoice setTitle:NSLocalizedString(@"DETAIL", nil) forState:UIControlStateNormal];
        [self addShadow:self.viewForDiscounts];
        [_viewForInvoice setHidden:NO];
        [self setLocalization];
        [self setInvoiceData];
    }
    else
    {
        [_viewForInvoice setHidden:YES];
        [_btnInvoice setTitle:NSLocalizedString(@"INVOICE", nil) forState:UIControlStateNormal];
    }
}
-(void)setDate
{
    NSString *strDate=[[UtilityClass sharedObject]DateToString:_tripDate];
    strDate=[strDate substringToIndex:10];
    NSString *current=[[UtilityClass sharedObject] DateToString:[NSDate date] ];
    current=[current substringToIndex:10];
    
    ///   YesterDay Date Calulation
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = -1;
    NSDate *yesterday = [gregorian dateByAddingComponents:dayComponent
                                                   toDate:[NSDate date]
                                                  options:0];
    
    NSString *strYesterday=[[UtilityClass sharedObject] DateToString:yesterday];
    strYesterday=[strYesterday substringToIndex:10];
    
    
    
    if([strDate isEqualToString:current])
    {
        [_lblTripDate setText:[NSString stringWithFormat:@"  %@  ",[NSLocalizedString(@"TODAY", nil) capitalizedString]]];
    }
    else if ([strDate isEqualToString:strYesterday])
    {
        
        [_lblTripDate setText:[NSString stringWithFormat:@"  %@  ",[NSLocalizedString(@"YESTER_DAY", nil) capitalizedString]]];
        
    }
    else
    {
        NSString *text=[[UtilityClass sharedObject]DateToString:_tripDate withFormate:@"dd MMMM yyyy"];
        //2nd Jan 2015
        _lblTripDate.text= [NSString stringWithFormat:@"  %@  ",text];
    }
    _lblTripDate.layer.cornerRadius = 10;
    _lblTripDate.layer.masksToBounds = YES;
    
    _lblTripDate.textColor=[UIColor whiteColor];
    _lblTripDate.backgroundColor=[UIColor GreenColor];
    [_lblTripDate sizeToFit];
    [_lblTripDate setFrame:CGRectMake(_lblTripDate.frame.origin.x,_lblTripDate.frame.origin.y, _lblTripDate.frame.size.width,30)];
    
}
#pragma UIWebViewDelegate
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request   navigationType:(UIWebViewNavigationType)navigationType {
    BOOL result = _Authenticated;
    if (!_Authenticated) {
        _FailedRequest = requestURL;
        
        connection = [[NSURLConnection alloc]
                      initWithRequest:request
                      delegate:self startImmediately:NO];
        
        [connection scheduleInRunLoop:[NSRunLoop mainRunLoop]
                              forMode:NSDefaultRunLoopMode];
        [connection start];
        [self.view bringSubviewToFront:_webView];
    }
    return result;
}

#pragma NSURLConnectionDelegate

-(void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        NSURL* baseURL = [NSURL URLWithString:webStringURL];
        if ([challenge.protectionSpace.host isEqualToString:baseURL.host]) {
            NSLog(@"trusting connection to host %@", challenge.protectionSpace.host);
            [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
        }
        else
            NSLog(@"Not trusting connection to host %@", challenge.protectionSpace.host);
    }
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

-(void)connection:(NSURLConnection *)connections didReceiveResponse:(NSURLResponse *)pResponse {
    _Authenticated = YES;
    [connections cancel];
    [self.webView loadRequest:_FailedRequest];
}
-(UIView*)addShadow:(UIView*)view
{
    view.layer.shadowOpacity=0.8;
    view.layer.shadowOffset= CGSizeMake(0, 3.0f);
    view.layer.shadowColor = [UIColor textColor].CGColor;
    view.layer.masksToBounds=NO;
    view.clipsToBounds = NO;
    return view;
}
-(void)setInvoiceData
{
    Invoice *invoice=[Invoice sharedObject];
    [_lblInvoiceId setText:invoice.invoiceNumber];
    
    if ([invoice.paymentMode boolValue])
    {
        [_imgPayment setImage:[UIImage imageNamed:@"invoice_cash"]];
        [_lblPayment setText:NSLocalizedString(@"PAY_BY_CASH", nil)];
        [_lblRemainingAmount setText:[NSString stringWithFormat:NSLocalizedString(@"CASH_REMAINING_AMOUNT", nil), invoice.cashPayment]];
    }
    else
    {
        [_imgPayment setImage:[UIImage imageNamed:@"invoice_cash"]];
        [_lblPayment setText:NSLocalizedString(@"PAY_BY_CARD", nil)];
        if ([invoice.remainingPayment doubleValue]>0.00)
        {
            [_lblRemainingAmount setText:[NSString stringWithFormat:NSLocalizedString(@"REMAINING_AMOUNT", nil), invoice.remainingPayment]];
        }
        else
        {
            [_lblRemainingAmount setText:[NSString stringWithFormat:NSLocalizedString(@"CARD_REMAINING_AMOUNT", nil), invoice.cardPayment]];
            
        }
    }
    [_lblWalletAmount setText:[NSString stringWithFormat:NSLocalizedString(@"WALLET_PAYMENT", nil), invoice.remainingPayment]];
    [_lblTotalValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.total]];
    [_lblTime setText:[NSString stringWithFormat:@"%@ %@", invoice.time,TIME_SUFFIX]];
    [_lblDistance setText:[NSString stringWithFormat:@"%@ %@", invoice.distance,DISTANCE_SUFFIX]];
    [_lblBaseCostValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.basePrice]];
    NSString *strTitle,*strSubValue;
    strTitle=NSLocalizedString(@"BASE_PRICE", nil);
    if ([invoice.basePriceDistance intValue]==1)
    {
        strSubValue=[NSString stringWithFormat:@"%@",invoice.distanceUnit];
    }
    else
    {
        strSubValue=[NSString stringWithFormat:@"%@ / %@",invoice.basePriceDistance,invoice.distanceUnit];
    }                                    [_lblBaseCost setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    strTitle=NSLocalizedString(@"DISTANCE_COST", nil);
    strSubValue=[NSString stringWithFormat:@"%@ %@ / %@",invoice.currency,invoice.pricePerUnitDistance,invoice.distanceUnit];
    [_lblDistanceCost setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    [_lblDistanceCostValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.distanceCost]];
    strTitle=NSLocalizedString(@"TIME_COST", nil);
    strSubValue=[NSString stringWithFormat:@"%@ %@ / %@",invoice.currency,invoice.pricePerUnitTime,TIME_SUFFIX];
    [_lblTimeCost setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    [_lblTimeCostValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.timeCost]];
    
    strTitle=NSLocalizedString(@"TAX_COST", nil);
    strSubValue=[NSString stringWithFormat:@"%@ %%",invoice.tax];
    [_lblTax setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    [_lblTaxCostValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.taxCost]];
    
    strTitle=NSLocalizedString(@"WAIT_TIME_COST", nil);
    strSubValue=[NSString stringWithFormat:@"%@ %@",invoice.pricePerWaitingTime,TIME_SUFFIX];
    [_lblWaitTimeCost setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    [_lblWaitTimeCostValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.waitingTimeCost]];
    
    strTitle=NSLocalizedString(@"SURGE_COST", nil);
    strSubValue=[NSString stringWithFormat:@"x%@",invoice.surgeMultiplier];
    [_lblSurgeCost setAttributedText:[self makeAttributedString:strTitle andSubtitle:strSubValue]];
    [_lblSurgeCostValue setText:[NSString stringWithFormat:@" %@ %@",invoice.currency,invoice.surgeTimeFee]];
    
    [_lblRefferalBonousValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.referralBonus]];
    [_lblPromoBonousValue setText:[NSString stringWithFormat:@"%@ %@",invoice.currency,invoice.promoBonus]];
    
}

-(void)setLocalization
{
    [_lblTotalValue setTextColor:[UIColor buttonColor]];
    [_lblDiscount setTextColor:[UIColor buttonColor]];
    [_lblInvoiceId setTextColor:[UIColor textColor]];
    [_lblInvoiceId setText:@"00"];
    [_lblInvoiceId setBackgroundColor:[UIColor whiteColor]];
    [_lblTime setTextColor:[UIColor buttonTextColor]];
    [_lblDistance setTextColor:[UIColor buttonTextColor]];
    [_lblPayment setTextColor:[UIColor buttonTextColor]];
    [_lblTotal setContentMode:UIViewContentModeCenter];
    [_lblTotalValue setContentMode:UIViewContentModeCenter];
    [_lblTimeCost setTextColor:[UIColor textColor]];
    [_lblTimeCostValue setTextColor:[UIColor textColor]];
    [_lblDistanceCost setTextColor:[UIColor textColor]];
    [_lblDistanceCostValue setTextColor:[UIColor textColor]];
    [_lblBaseCost setTextColor:[UIColor textColor]];
    [_lblBaseCostValue setTextColor:[UIColor textColor]];
    [_lblWaitTimeCost setTextColor:[UIColor textColor]];
    [_lblWaitTimeCostValue setTextColor:[UIColor textColor]];
    [_lblSurgeCost setTextColor:[UIColor textColor]];
    [_lblSurgeCostValue setTextColor:[UIColor textColor]];
    [_lblTotal setTextColor:[UIColor textColor]];
    [_lblPromoBonous setTextColor:[UIColor labelTextColor]];
    [_lblRefferalBonous setTextColor:[UIColor labelTextColor]];
    [_lblRemainingAmount setTextColor:[UIColor textColor]];
    [_lblWalletAmount setTextColor:[UIColor textColor]];
    [_lblTotal setText:[NSLocalizedString(@"TOTAL", nil) uppercaseString]];
    [_lblRefferalBonous setText:[NSLocalizedString(@"REFFEREL_BONOUS", nil) capitalizedString]];
    [_lblPromoBonous setText:[NSLocalizedString(@"PROMO_BONOUS", nil) capitalizedString]];
    [_lblDiscount setText:[NSLocalizedString(@"DISCOUNT", nil) uppercaseString]];
    
    [_lblTax setTextColor:[UIColor textColor]];
    [_lblTaxCostValue setTextColor:[UIColor textColor]];
    _viewForInvoice=[[UtilityClass sharedObject]addShadow:_viewForInvoice];
    [self.view setBackgroundColor:[UIColor backGroundColor]];
    [self.viewForTimeAndDistance setBackgroundColor:[UIColor GreenColor]];
    [self.viewForDiscounts setBackgroundColor:[UIColor whiteColor]];
}
-(NSMutableAttributedString*)makeAttributedString:(NSString*)title andSubtitle:(NSString*)subtitle
{
    UIFont *titleFont = [UIFont fontWithName:_lblBaseCostValue.font.fontName    size:14.0];
    // Define your regular font
    UIFont *subtitleFont = [UIFont fontWithName:_lblBaseCostValue.font.fontName    size:11.0];;
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@",title,subtitle]];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor textColor] range:NSMakeRange(0,title.length)];
    [string addAttribute:NSFontAttributeName value:titleFont range:NSMakeRange(0,title.length)];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor labelTextColor] range:NSMakeRange(title.length+1,subtitle.length)];
    [string addAttribute:NSFontAttributeName value:subtitleFont range:NSMakeRange(title.length+1,subtitle.length)];
    return string;
    
}
@end
