//
//  HistoryVc.m
//  Eber Client
//  Created by My Mac on 7/2/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//

#import "HistoryVc.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "UtilityClass.h"
#import "NSObject+Constants.h"
#import "Trip.h"
#import "HistoryCell.h"
#import "HistoryDetailVC.h"
#import "UIColor+Colors.h"
#import "UILabel+overrideLabel.h"
#import "Parser.h"
#import "PreferenceHelper.h"
@interface HistoryVc ()
{
    NSMutableArray *arrForHistory,*arrForDate,*arrForSection,*tempArray;
    BOOL from;
    NSString *tripid,*strForUserId,*strForUserToken,*tripDate,*unit,*currency;
    
}
@end
@implementation HistoryVc
#pragma mark-View LifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    arrForHistory=[[NSMutableArray alloc]init];
    strForUserId=PREF.userId;
    strForUserToken=PREF.userToken;
    [self setLocalization];
       self.viewForPicker.hidden=YES;
    tempArray=[[NSMutableArray alloc]init];
    [self getHistory];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)setLocalization
{
    [_SearchView setBackgroundColor:[UIColor whiteColor]];
    _SearchView=[self addShadow:_SearchView];
    [self.view setBackgroundColor:[UIColor backGroundColor]];
    
    [_lblTo setText:[NSLocalizedString(@"TO", nil) capitalizedString] withColor:[UIColor GreenColor]];
    [_lblFrom setText:[NSLocalizedString(@"FROM", nil) capitalizedString] withColor:[UIColor GreenColor]];
    
    [_btnToDate setTitle:[NSLocalizedString(@"SELECT_DATE", nil) capitalizedString] forState:UIControlStateNormal];
    [_btnFromDate setTitle:[NSLocalizedString(@"SELECT_DATE", nil) capitalizedString] forState:UIControlStateNormal];
    
    [_lblNoItems setText:NSLocalizedString(@"NO_ITEM_TO_DISPLAY", nil)];
    [_lblNoItems setTextColor:[UIColor labelTextColor]];

    [_btnDone setBackgroundColor:[UIColor buttonColor]];

}
-(void)viewDidLayoutSubviews
{
    [_btnSearch setContentMode:UIViewContentModeCenter];
    [_btnSearch.imageView setContentMode:UIViewContentModeScaleAspectFit];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [_btnBack setTitle:[NSLocalizedString(@"TITLE_HISTORY", nil) capitalizedString] forState:UIControlStateNormal];
    [_btnBack setTitleColor:[UIColor labelTitleColor] forState:UIControlStateNormal];
    [self.navigationController setNavigationBarHidden:NO];
}
#pragma mark-WebServiceCalls
#pragma mark-Get History
-(void)getHistory
{
		NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
		[dictParam setObject:strForUserId forKey:PARAM_USER_ID];
		[dictParam setObject:strForUserToken forKey:PARAM_TOKEN];
        [APPDELEGATE showLoadingWithTitle:@""];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
		[afn getDataFromPath:USER_HISTORY withParamData:dictParam withBlock:^(id response, NSError *error)
		 {
             dispatch_async(dispatch_get_main_queue(), ^
			 {
                 
                 [arrForHistory removeAllObjects];
                 if ([[Parser sharedObject]parseHistory:response toArray:&arrForHistory])
                 {
                     [tempArray addObjectsFromArray:arrForHistory];
                     [self makeSection];
                     [self.tblHistoryTrip reloadData];
                     [self adjustHeightOfTableview];
                     [_tblHistoryTrip setHidden:NO];
                     self.lblNoItems.hidden=YES;
                 }
                 else
                 {
                     _tblHistoryTrip.hidden=YES;
                 }
                 [APPDELEGATE hideLoadingView];
                 

                 
            });
			 
		}];
}
#pragma mark-TABLE VIEW METHODS
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{		return arrForDate.count;}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return   [[arrForSection objectAtIndex:section] count]; ;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0f;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 0.0f)];
    [footerView setBackgroundColor:[UIColor backGroundColor]];
    return footerView;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    CGRect initialFrame = CGRectMake(0, 5, 100, 40);
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(10, 20, 10,20 );
    CGRect paddedFrame = UIEdgeInsetsInsetRect(initialFrame, contentInsets);
    UILabel *lblDate=[[UILabel alloc]initWithFrame:paddedFrame];
    lblDate.textColor=[UIColor	labelTitleColor];
    lblDate.layer.cornerRadius = 10;
    lblDate.layer.masksToBounds = YES;
    [lblDate setContentMode:UIViewContentModeCenter];
    Trip *t=[[arrForSection objectAtIndex:section] objectAtIndex:0];
    NSString *strDate=[[UtilityClass sharedObject]DateToString:t.tripCreateDate];
    strDate=[strDate substringToIndex:10];
    NSString *current=[[UtilityClass sharedObject] DateToString:[NSDate date] ];
    current=[current substringToIndex:10];
    ///   YesterDay Date Calulation
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = -1;
    NSDate *yesterday = [gregorian dateByAddingComponents:dayComponent
                                                   toDate:[NSDate date]
                                                  options:0];
    
    NSString *strYesterday=[[UtilityClass sharedObject] DateToString:yesterday];
    strYesterday=[strYesterday substringToIndex:10];
   
    if([strDate isEqualToString:current])
    {
        NSString *title=[NSLocalizedString(@"TODAY", nil) capitalizedString];
        [lblDate setText:[NSString stringWithFormat:@"  %@  ",title] withColor:[UIColor whiteColor]];
    }
    else if ([strDate isEqualToString:strYesterday])
    {
        NSString *title=[NSLocalizedString(@"YESTER_DAY", nil) capitalizedString];
        [lblDate setText:[NSString stringWithFormat:@"  %@  ",title] withColor:[UIColor whiteColor]];
    }
    else
    {
        NSString *text=[[UtilityClass sharedObject]DateToString:t.tripCreateDate withFormateSufix:@"d MMMM yyyy"];
        lblDate.text= [NSString stringWithFormat:@"  %@  ",text];
    }
    [lblDate setFont:_lblTo.font];
    [lblDate sizeToFit];
    [lblDate setFrame:CGRectMake(10, 10, lblDate.frame.size.width,30)];
    lblDate.textColor=[UIColor whiteColor];
    lblDate.backgroundColor=[UIColor GreenColor];
    [headerView addSubview:lblDate];
    [headerView setBackgroundColor:[UIColor backGroundColor]];
    return headerView;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [arrForDate objectAtIndex:section];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"historycell";
    HistoryCell *cell = [self.tblHistoryTrip dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if (cell==nil)
    {
        cell=[[HistoryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    Trip *t=[[arrForSection objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];;
    [cell setDataForCell:t];
    cell.contentView.backgroundColor=[UIColor backGroundColor];
    CGRect frame=cell.contentView.frame;
    frame.origin.x+=10;
    frame.origin.y+=2;
    frame.size.height-=4;
    frame.size.width-=20;
    
    cell.viewForCell.frame=frame;
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70.0f;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger section = [indexPath section];
    NSInteger row=[indexPath row];
    tripid=[[[arrForSection objectAtIndex:section] objectAtIndex:row]tripId ];
    currency=[[[arrForSection objectAtIndex:section] objectAtIndex:row]currency];
    unit=[[[arrForSection objectAtIndex:section] objectAtIndex:row] distanceUnit];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self performSegueWithIdentifier:SEGUE_TO_HISTORY_DETAIL sender:self];
    
}
#pragma  mark-Actions
- (IBAction)onClickBtnFromDate:(id)sender {
    
    NSDate *now = [NSDate date];
    [self.dtPicker setMaximumDate:now];
    //NSlog(@"Method Called");
    if (![self.btnFromDate.titleLabel.text isEqualToString:@"Select date"])
    {
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate * date = [dateFormatter dateFromString:self.btnFromDate.titleLabel.text];
        self.dtPicker.minimumDate=date;
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDate *currentDate = [NSDate date];
        NSDateComponents *comps = [[NSDateComponents alloc] init];
        [comps setYear:-100];
        NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
        [self.dtPicker setMinimumDate:minDate];
    }
    from=YES;
    [self.viewForPicker setHidden:NO];
    [self.view bringSubviewToFront:self.viewForPicker];
    //[self.lblBackground setHidden:NO];
    
}
- (IBAction)onClickBtnToDate:(id)sender {
    NSDate *now = [NSDate date];
    [self.dtPicker setMaximumDate:now];
    
    if (![self.btnFromDate.titleLabel.text isEqualToString:@"Select date"])
    {
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate * date = [dateFormatter dateFromString:self.btnFromDate.titleLabel.text];
        self.dtPicker.minimumDate=date;
    }
    from=NO;
    [self.viewForPicker setHidden:NO];
}
- (IBAction)onClickBtnDone:(id)sender{
    [self.viewForPicker setHidden:YES];
    
    NSString *date = [NSString stringWithFormat:@"%@",[[UtilityClass sharedObject] DateToString:self.dtPicker.date withFormate:@"yyyy-MM-dd"]];
    if (from)
    {
        [self.btnFromDate setTitle:date forState:UIControlStateNormal];
    }
    else
    {
        [self.btnToDate setTitle:date forState:UIControlStateNormal];
    }
    
}
- (IBAction)onClickBtnSearch:(id)sender {
    //NSlog(@"Btn Clickerd");
    [APPDELEGATE showLoadingWithTitle:@"LOADING"];
    [self dateBetween:self.btnFromDate.titleLabel.text toDate:self.btnToDate.titleLabel.text];
}
- (IBAction)onClickBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)onClickBtnRefresh:(id)sender {
    [tempArray removeAllObjects];
    [tempArray addObjectsFromArray:arrForHistory];
    [self makeSection];
    [self.tblHistoryTrip reloadData];
    [self adjustHeightOfTableview];
    [_btnToDate setTitle:[NSLocalizedString(@"SELECT_DATE", nil) capitalizedString] forState:UIControlStateNormal];
    [_btnFromDate setTitle:[NSLocalizedString(@"SELECT_DATE", nil) capitalizedString] forState:UIControlStateNormal];
    self.tblHistoryTrip.hidden=NO;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"segueToHistoryDetail"])
    {
        HistoryDetailVC *history = [segue destinationViewController];
        history.trip_id = tripid;
        history.currency=currency;
        history.unit=unit;
        
    }
}
#pragma mark-User Define Method
- (void)adjustHeightOfTableview
{
    CGFloat height = self.tblHistoryTrip.contentSize.height;
    CGFloat maxHeight = self.tblHistoryTrip.superview.frame.size.height - self.tblHistoryTrip.frame.origin.y;
    
    // if the height of the content is greater than the maxHeight of
    // total space on the screen, limit the height to the size of the
    // superview.
    
    if (height > maxHeight)
        height = maxHeight;
    
    // now set the frame accordingly
    
    [UIView animateWithDuration:0.25 animations:^{
        CGRect frame = self.tblHistoryTrip.frame;
        frame.size.height = height;
        self.tblHistoryTrip.frame = frame;
        
        // if you have other controls that should be resized/moved to accommodate
        // the resized tableview, do that here, too
    }];
    [_tblHistoryTrip setHidden:NO];
}
-(UIView*)addShadow:(UIView*)view
{
    view.layer.shadowOpacity=0.8;
    view.layer.shadowOffset= CGSizeMake(0, 3.0f);
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.masksToBounds=NO;
    view.clipsToBounds = NO;
    return view;
}
-(void)makeSection
{
    arrForDate=[[NSMutableArray alloc]init];
    arrForSection=[[NSMutableArray alloc]init];
    [tempArray sortUsingComparator:^NSComparisonResult (id a, id b)
     {
         Trip * itemA = (Trip*)a;
         Trip * itemB =(Trip*)b;
         return itemA.tripCreateDate>itemB.tripCreateDate;
     }];
    
    for (Trip *t in tempArray)
    {
        NSDate *date=t.tripCreateDate;
        if(![arrForDate containsObject:date])
        {
            [arrForDate addObject:date];
        }
    }
    
    /* Fill Section ARray*/
    for (NSDate *date1 in arrForDate)
    {
        NSMutableArray *tmp=[[NSMutableArray alloc]init];
        for (Trip *t in tempArray)
        {
            
            NSDate *date2=[t tripCreateDate];
            if ([date1 compare:date2] == NSOrderedSame)
            {
                
                if (t != nil)
                {[tmp addObject:t];}
            }
            
        }
        if (tmp != nil)
        {
            [arrForSection addObject:tmp];
        }
        
        
    }
    
    
    /* Sort Section ARray*/
    [arrForSection sortUsingComparator:^(id a, id b) {
        return [[b[0] tripCreateDate] compare:[a[0] tripCreateDate]];
    }];
}
-(void)dateBetween:(NSString*)s_date toDate:(NSString*)toDate
{
    NSInteger i=0;
    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    [tempArray removeAllObjects];
    [tempArray addObjectsFromArray:arrForHistory];
    
    for (Trip *t in tempArray)
    {
        NSDate *date1=t.tripCreateDate;
        NSDate *st_date=[[UtilityClass sharedObject] stringToDate:s_date];
        NSDate *en_date=[[UtilityClass sharedObject] stringToDate:toDate];
        //NSlog(@"DATE 1:_%@ S_DATE:_%@ E_DATE:%@",date1,st_date,en_date);
        
        if(
           (([date1 compare:st_date]==NSOrderedDescending )
            ||
            ([date1 compare:st_date]==NSOrderedSame) )&&
           
           (([date1 compare:en_date]==NSOrderedAscending )
            ||
            ([date1 compare:en_date]==NSOrderedSame) )
           )
            
            
        {
            
        }
        else
        {
            [indexSet addIndex:i];
        }
        i++;
        
    }
    
    [tempArray removeObjectsAtIndexes:indexSet];
    if (tempArray.count<1)
    {
        self.lblNoItems.hidden=NO;
        self.tblHistoryTrip.hidden=YES;
    }
    else
    {
        [self makeSection];
        self.lblNoItems.hidden=YES;
        [self.tblHistoryTrip reloadData];
        [self adjustHeightOfTableview];
        self.tblHistoryTrip.hidden=NO;
    }
    [tempArray addObjectsFromArray:arrForHistory];
    [APPDELEGATE hideLoadingView];
}

@end
