//
//  HistoryCell.m
//  UberforX Provider
//
//  Created by My Mac on 11/15/14.
//  Copyright (c) 2014 Deep Gami. All rights reserved.
//

#import "HistoryCell.h"
#import "Trip.h"
#import "UIColor+Colors.h"
#import "NSObject+Constants.h"
#import "UtilityClass.h"
#import "UIImageView+image.h"
@implementation HistoryCell
@synthesize imageView;

- (void)awakeFromNib
{
    [super awakeFromNib];
    CGRect frame=_imgProvider.frame;
    frame.origin.y=10;
    frame.size.height=_viewForCell.frame.size.height-20;
    frame.size.width=frame.size.height;
    _imgProvider.frame=frame;
    [_imgProvider setRoundImageWithColor:[UIColor borderColor]];
    
    _viewForCell=[self addShadow:_viewForCell];
    [_viewForCell setBackgroundColor:[UIColor whiteColor]];
    [_lblname setTextColor:[UIColor textColor]];
    [_lblTime setTextColor:[UIColor textColor]];
    [_lblPrice setTextColor:[UIColor textColor]];
    
    
}

-(void)setDataForCell:(Trip *)trip
{
    _lblPrice.text= [NSString stringWithFormat:@"%@ %@",trip.currency,trip.tripTotalCost ];
    _lblTime.text=[NSString stringWithFormat:@"%@",trip.tripCreateTime ];
    _lblname.text=[NSString stringWithFormat:@"%@ %@", trip.userFirstName,trip.userLastName ];
    
    [[UtilityClass sharedObject] loadFromURL:[NSURL URLWithString:trip.userImage] callback:^(UIImage *image) {
        if (image)
        {
            _imgProvider.image=image;
            
        }else
        {
            _imgProvider.image=[UIImage imageNamed:@"user"];
            
            
        }
    }];
    if (trip.isTripCancelled) {
        [_lblTripCancelled setText:NSLocalizedString(@"TRIP_CANCELLED", nil)];
        [_lblTripCancelled setTextColor:[UIColor redColor]];
        [_lblTripCancelled setHidden:NO];
    }
    else
    {
        [_lblTripCancelled setHidden:YES];
    }
    
    
}
-(UIView*)addShadow:(UIView*)view
{
    view.layer.shadowOpacity=0.4;
    view.layer.shadowOffset= CGSizeMake(0, 2.0f);
    view.layer.shadowColor = [UIColor textColor].CGColor;
    view.layer.masksToBounds=NO;
    view.clipsToBounds = NO;
    return view;
}
@end

