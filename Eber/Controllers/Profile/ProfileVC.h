//
//  ProfileVC.h
//  UberNew
//
//  Created by Elluminati - macbook on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNHelper.h"
#import "SelectImage.h"
#import "CustomAlertWithTextInput.h"
#import "CustomOtpDialog.h"

@interface ProfileVC : UIViewController
<UITextFieldDelegate,
UINavigationControllerDelegate,
UIGestureRecognizerDelegate,
CustomAlertWithTextInput,
CustomOtpDialogDelegate
>
{
    
}
@property (weak, nonatomic) IBOutlet UIView *profileView;
//Views
@property (weak, nonatomic) IBOutlet UIView *viewForSelectCountryCode;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *viewForProfile;
@property (weak, nonatomic) IBOutlet UIImageView *imgProPic;
///

/*viewForProfile*/
@property (weak, nonatomic) IBOutlet UILabel *lblFullName;
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtNumber;
@property (weak, nonatomic) IBOutlet UITextField *activeTextField;
@property (weak, nonatomic) IBOutlet UITextField *txtCompanyID;
@property (weak, nonatomic) IBOutlet UIButton *btnRemoveCompanyId;

@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtBio;
@property (weak, nonatomic) IBOutlet UITextField *txtZipCode;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectCountry;


//View For Country Code
@property (weak, nonatomic) IBOutlet UISearchBar *searchbar;
@property (weak, nonatomic) IBOutlet UITableView *tblForCountryCode;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;


//Buttons
@property (weak, nonatomic) IBOutlet UIButton *btnUpdateProfile;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

////// Actions
- (IBAction)onClickBtnUpdate:(id)sender;
- (IBAction)onClickBtnPickerCancel:(id)sender;
- (IBAction)onClickBtnBack:(id)sender;
- (IBAction)onClickBtnselectCountry:(id)sender;
- (IBAction)onClickRemoveCompanyID:(id)sender;

//Other Properties
@property (weak, nonatomic) IBOutlet UILabel *lblChangePassword;

@property(nonatomic,copy) NSString *jsonParameter;
@property(nonatomic,copy)NSMutableArray *arrForCountry;
@property(nonatomic,copy) NSMutableDictionary *dictParam;
@property(nonatomic,copy) NSString *strImageData,
*strForSocialId,
*strForToken,
*strForID;
@property(nonatomic,assign) BOOL isPicAdded;
/*VIEW FOR ASK PASSWORD*/
@end
