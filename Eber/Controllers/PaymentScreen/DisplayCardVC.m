//
//  DisplayCardVC.m
//  UberforXOwner
//
//  Created by Deep Gami on 17/11/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "DisplayCardVC.h"
#import "DispalyCardCell.h"
#import "AppDelegate.h"
#import "NSObject+Constants.h"
#import "Stripe.h"
#import "UtilityClass.h"
#import "STPCard.h"
#import "AFNHelper.h"
#import "UIColor+Colors.h"
#import "MapVc.h"
#import "UITextField+BaseText.h"
#import "Parser.h"
#import "Card.h"
#import "PreferenceHelper.h"


@interface DisplayCardVC ()
{
	//Array Of Cards
	NSMutableArray *arrForCards;
	//CARD Details
    Card *selectedCard;
    CustomAlertWithTitle *deleteCardView;
	NSString *card_id,*strCardType,*strForUserId,*strForUserToken,*walletAmount,*walletCurrencyCode;
	NSInteger card_length,selectedCardPosition;
    BOOL isRemoveSelectedCard,walletStatus;
    UIGestureRecognizer *tapGesture;
    
}
@end

@implementation DisplayCardVC
@synthesize comeFromRegister;
#pragma mark - View Life Cycle :
- (void)viewDidLoad
{
	[super viewDidLoad];
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    
    [self SetLocalization];
    @try {
        if(StripePublishableKey)
        {
            
            [Stripe setDefaultPublishableKey:StripePublishableKey];
        }
        
    } @catch (NSException *exception)
    {
        [[UtilityClass sharedObject]displayAlertWithTitle:NSLocalizedString(@"ALERT_TITLE_STRIPE_KEY",nil) andMessage:NSLocalizedString(@"ALERT_MSG_STRIPE_KEY",nil)];
    }
    @finally
    {
        selectedCard=[[Card alloc]init];
        arrForCards=[[NSMutableArray alloc]init];
        card_id=@"0";
        card_length=16;
        strForUserId=PREF.userId;
        strForUserToken=PREF.userToken;
        
    }
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_GETTING_CARD", nil)];
    [self getAllMyCards];
    if(!IS_PARENT_IS_REGISTER)
    {

        CGRect frame=[APPDELEGATE.window frame];
        CGRect newFrame=_btnAddCardManually.frame;
        newFrame.origin.x=frame.origin.x+10;
        newFrame.size.width=frame.size.width-20;
        newFrame.origin.y=frame.size.height-(10+newFrame.size.height);
        [_btnAddCardManually setFrame:newFrame];
        [self.view bringSubviewToFront:_btnAddCardManually];
        [_btnSkip setHidden:YES];
        [_btnBack setTitle:[NSLocalizedString(@"TITLE_PAYMENT", nil) capitalizedString] forState:UIControlStateNormal];
        
    }
    else
    {
    [_btnBack setTitle:[NSLocalizedString(@"TITLE_PAYMENT", nil) capitalizedString] forState:UIControlStateNormal];
    }
    [_btnBack setTitleColor:[UIColor labelTitleColor] forState:UIControlStateNormal];
    
  }
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UINavigationBar appearance].translucent = NO;
    [self.navigationController.navigationBar setBackgroundColor:[UIColor GreenColor]];
    _btnBackWallet.layer.cornerRadius = 5; // this value vary as per your desire
    _btnBackWallet.clipsToBounds = YES;

    [self.navigationController setNavigationBarHidden:NO];
	
}
- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}
-(void)SetLocalization
{
    self.pvew.hidden=YES;
    _pvew= [self addShadow:_pvew];
    /*button setUp*/
    [_btnBackWallet setHidden:YES];
    [_btnBackWallet setTitle:[NSLocalizedString(@"ALERT_BTN_CANCEL", nil) uppercaseString] forState:UIControlStateNormal];
    [_btnAddWalletAmount setTitle:[[NSLocalizedString(@"ADD", nil) capitalizedString] capitalizedString] forState:UIControlStateNormal];
    [_btnSelectedDelete setTitle:[NSLocalizedString(@"REMOVE", nil) capitalizedString] forState:UIControlStateNormal];
    [_btnSkip setTitle:[NSLocalizedString(@"SKIP", nil) uppercaseString] forState:UIControlStateNormal];
    [_btnAddCardManually setTitle:[NSLocalizedString(@"ADD_NEW_CARD", nil) uppercaseString] forState:UIControlStateNormal];
    [_btnBackWallet setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [_btnBackWallet setBackgroundColor:[UIColor buttonColor]];
    [_btnAddWalletAmount setTitleColor:[UIColor textColor] forState:UIControlStateNormal];
    [self addButtonShadow:_btnAddCardManually];
    [self addButtonShadow:_btnSkip];
    [_btnSubmitCard setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    [_btnSubmitCard setBackgroundColor:[UIColor buttonColor]];
    [_btnSubmitCard setTitle:[NSLocalizedString(@"SUBMIT", nil) uppercaseString] forState:UIControlStateNormal];
    [self.btnAddCardManually setBackgroundColor:[UIColor buttonColor]];
    [self.btnSkip setBackgroundColor:[UIColor buttonColor]];
    
    
    [_lblNoItemToDisplay setText:NSLocalizedString(@"NO_ITEM_TO_DISPLAY", nil)];
    [_lblNoItemsToDisplay setText:NSLocalizedString(@"NO_ITEM_TO_DISPLAY", nil)];
    [_lblNoItemToDisplay setHidden:NO];
    [_lblNoItemsToDisplay setHidden:NO];
    [_lblNoItemToDisplay setTextColor:[UIColor labelTextColor]];
    [_lblNoItemsToDisplay setTextColor:[UIColor labelTextColor]];
    
    _viewForSelectedCard=[self addShadow:_viewForSelectedCard];
    [_viewForSelectedCard setHidden:YES];
    [_tableView setHidden:YES];
    _txtWalletAmount.hidden=YES;
    _viewForWallet=[self addShadow:_viewForWallet];
    [_txtmm setBorder];
    [_txtyy setBorder];
    [_txtCvv setBorder];
    [_txtCreditCard setBorder];
    [_txtmm setPlaceholder:NSLocalizedString(@"MM", nil)];
    [_txtyy setPlaceholder:NSLocalizedString(@"YY", nil)];
    [_txtCvv setPlaceholder:NSLocalizedString(@"CVV", nil)];
    [_txtCreditCard setPlaceholder:NSLocalizedString(@"CREDIT_CARD_NUMBER", nil)];
    [_txtmm setTextColor:[UIColor textColor]];
    [_txtyy setTextColor:[UIColor textColor]];
    [_txtCvv setTextColor:[UIColor textColor]];
    [_txtCreditCard setTextColor:[UIColor textColor]];
    
    [_lblWallet setText:NSLocalizedString(@"WALLET", nil)];
    [_lblSelCard setText:NSLocalizedString(@"SELECTED_CARD", nil)];
    [_lblOtherCard setText:NSLocalizedString(@"OTHER_CARDS", nil)];
    [_lblWallet setTextColor:[UIColor textColor]];
    [_lblSelectedCard setTextColor:[UIColor labelTextColor]];
    [_lblOtherCard setTextColor:[UIColor labelTextColor]];
    [_txtWalletAmount setPlaceholder:NSLocalizedString(@"WALLET_AMOUNT", nil)];
    [self.view setBackgroundColor:[UIColor backGroundColor]];
}
#pragma mark - textfield Delegate Methods :
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
	if([textField isEqual:self.txtCreditCard])
	{
		[self.txtmm becomeFirstResponder];
		
	}else if([textField isEqual:self.txtCvv])
	{
		[self.txtCvv becomeFirstResponder];
	}
	else if([textField isEqual:self.txtmm])
	{
        if ([self.txtmm.text intValue] > 12)
        {
            self.txtmm.text=@"12";
        }
		[self.txtyy becomeFirstResponder];
		
	}else if([textField isEqual:self.txtyy])
	{
		[self.txtCvv resignFirstResponder];
	}
    else
    {
        [textField resignFirstResponder];
    }
	
	return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self.txtCreditCard resignFirstResponder];
	[self.txtCvv resignFirstResponder];
	[self.txtmm resignFirstResponder];
	[self.txtyy resignFirstResponder];
}
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	
	[self touchesBegan:touches withEvent:event];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == _txtCreditCard)
    {
        if (string==nil || [string isEqualToString:@""] || [string isKindOfClass:[NSNull class]] || string.length < 1)
        {
           if (self.txtCreditCard.text.length <=1)
           {
                [_txtCreditCard.leftView setContentMode:UIViewContentModeRight];
                _txtCreditCard.leftView= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
              
            }
            
        }
        else if (self.txtCreditCard.text.length == 2 && range.length == 0)
        {
            strCardType=[NSString stringWithFormat:@"%@",[self cardBrandFromNumber:self.txtCreditCard.text]];
            card_length=[self lengthForCardType:strCardType];
            if ([strCardType isEqualToString:@"Unknown"] || [UtilityClass isEmpty:strCardType])
            {
                
            }
            else
            {
                [_txtCreditCard.leftView setContentMode:UIViewContentModeCenter];
                 _txtCreditCard.leftView= [[UIImageView alloc] initWithImage:[UIImage imageNamed:strCardType]];
            }
            
        }

        else if (self.txtCreditCard.text.length >= card_length && range.length == 0)
        {
            [self.txtmm becomeFirstResponder];
            return NO; // return NO to not change text
        }

        else if(_txtCreditCard.text.length==4 || _txtCreditCard.text.length==9 || _txtCreditCard.text.length==14)
        {
            NSString *str=_txtCreditCard.text;
            _txtCreditCard.text=[NSString stringWithFormat:@"%@-",str];
        }
        if (_txtCreditCard.text.length == Card_Length && range.length == 0)
        {
            if(self.txtmm.text.length >= Card_Month)
            {
                return NO;
            }
            else
            {
                [self.txtmm becomeFirstResponder];
            }
        }
    }
    else if (textField == self.txtmm)
    {
        if (self.txtmm.text.length >= Card_Month && range.length == 0)
        {
            if(self.txtyy.text.length >= Card_Year)
            {
                return NO;
            }
            else
            {
                [self.txtyy becomeFirstResponder];
            }
        }
        
    }
    else if (textField == self.txtyy)
    {
        if (self.txtyy.text.length >= Card_Year && range.length == 0)
        {
            if(self.txtCvv.text.length >= Card_CVC_CVV)
            {
                return NO;
            }
            else
            {
                [self.txtCvv becomeFirstResponder];
            }
        }
    }
    else
    {
        if (self.txtCvv.text.length >= Card_CVC_CVV && range.length == 0)
        {
            [self.txtCvv resignFirstResponder];
            [UIView animateWithDuration:0.5 animations:^{
                
                [self.view endEditing:YES];
                
            } completion:^(BOOL finished)
             {
             }];
        }
    }
    
    return YES;
}
#pragma mark -
#pragma mark - UITableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return arrForCards.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	DispalyCardCell *cell=(DispalyCardCell *)[self.tableView dequeueReusableCellWithIdentifier:@"cardcell"];
	if (cell==nil) {
		cell=[[DispalyCardCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellSlider"];
	}
	if(arrForCards.count>0)
	{
        Card *card=[arrForCards objectAtIndex:indexPath.row];
        [cell setCellData:card.lastFour];
		[cell.btnDelete setTag:indexPath.row];
		[cell.btnDelete addTarget:self action:@selector(deleteCard:) forControlEvents:UIControlEventTouchUpInside];
	}
	return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	 
    Card *card=[arrForCards objectAtIndex:indexPath.row];
    card_id=card.cardId;
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self selectCard];
    selectedCardPosition=[indexPath row];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
	return 0.0f;
}

#pragma mark -Actions :
- (IBAction)onClickBtnHidePview:(id)sender
{
	// hide payment view :
	self.pvew.hidden=YES;
    [self.txtCreditCard resignFirstResponder];
	[self.txtCvv resignFirstResponder];
	[self.txtmm resignFirstResponder];
	[self.txtyy resignFirstResponder];
}
- (IBAction)onClickBtnBackWallet:(id)sender
{
    [_txtWalletAmount resignFirstResponder];
    [_pvew setHidden:YES];
    [_btnBackWallet setHidden:YES];
    _txtWalletAmount.text=@"";
    [_txtWalletAmount setHidden:YES];
    [_lblWalletAmount setHidden:NO];
    [_btnAddWalletAmount setTitle:[NSLocalizedString(@"ADD", nil) capitalizedString] forState:UIControlStateNormal];
    [self.view removeGestureRecognizer:tapGesture];
}
- (IBAction)onClickBtnaddPayment:(id)sender
{
    if(self.txtCreditCard.text.length<1 || self.txtmm.text.length<1 || self.txtyy.text.length<1 || self.txtCvv.text.length<1)
	{
		if(self.txtCreditCard.text.length<1)
		{
			[[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_PLEASE_CREDIT_CARD_NUMBER", nil)];
		}
		else if(self.txtmm.text.length<1)
		{
			[[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_PLEASE_MONTH", nil)];
		}
		else if(self.txtyy.text.length<1)
		{
			[[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_PLEASE_YEAR", nil)];
		}
		else if(self.txtCvv.text.length<1)
		{
			[[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_PLESE_CVV", nil)];
		}
	}
	else
	{
        if ([_txtmm.text integerValue]>12)
        {
        			[[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_PLEASE_MONTH", nil)];
        }
        else
        {
        
		[self onClickBtnHidePview:nil];
		[[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOADING_ADD_CARD", nil) ];
	
		if (![Stripe defaultPublishableKey])
		{
			[[UtilityClass sharedObject]displayAlertWithTitle:NSLocalizedString(@"ALERT_TITLE_STRIPE_KEY",nil) andMessage:NSLocalizedString(@"ALERT_MSG_STRIPE_KEY",nil)];
			return;
		}
		
            NSString *strCard=[self.txtCreditCard.text stringByReplacingOccurrencesOfString:@" " withString:@""];
            STPCardParams *obj=[[STPCardParams alloc] init];
            obj.number=strCard;
            obj.expMonth=[self.txtmm.text integerValue];
            obj.expYear=[self.txtyy.text integerValue];
            obj.cvc=self.txtCvv.text;
            
            STPAPIClient *StaObj=[[STPAPIClient alloc] init];
            
            [StaObj createTokenWithCard:obj completion:^(STPToken * _Nullable token, NSError * _Nullable error) {
                if (error)
                {
                    [[AppDelegate sharedAppDelegate] hideLoadingView];
                    [self hasError:error];
                }
                else
                {
                    [self hasToken:token];
                    
                }
                
            }];
		
        }
	}
	
}
-(IBAction)onClickBtnAddCardManually:(id)sender
{
   self.pvew.hidden=NO;
	self.txtCreditCard.text=@"";
	self.txtCvv.text=@"";
	self.txtmm.text=@"";
	self.txtyy.text=@"";
	[self.txtCreditCard becomeFirstResponder];
}
-(IBAction)deleteCard:(UIButton*)sender
{
	cardTag = sender.tag;
    Card *card=[arrForCards objectAtIndex:cardTag];
    card_id=card.cardId;
    isRemoveSelectedCard=NO;
    deleteCardView=
    [[CustomAlertWithTitle alloc]
     initWithTitle:NSLocalizedString(@"ALERT_TITLE_DELETE_CARD", nil)
     message:NSLocalizedString(@"ALERT_MSG_ARE_YOU_SURE",nil)
     delegate:self
     cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_NO", nil)
     otherButtonTitles:NSLocalizedString(@"ALERT_BTN_YES", nil)];
    
    
}
- (IBAction)onClickBtnBack:(id)sender
{
    if (IS_PARENT_IS_REGISTER)
    {
        [APPDELEGATE goToMap];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(IBAction)onClickBtnSkip:(id)sender
{
    [APPDELEGATE goToMap];
}
- (IBAction)onClickBtnSelectedDelete:(id)sender
{
    card_id=selectedCard.cardId;
    isRemoveSelectedCard=YES;
    deleteCardView=[[CustomAlertWithTitle alloc]
     initWithTitle:NSLocalizedString(@"ALERT_TITLE_DELETE_CARD", nil)
     message:NSLocalizedString(@"ALERT_MSG_ARE_YOU_SURE",nil)
     delegate:self
     cancelButtonTitle:NSLocalizedString(@"ALERT_BTN_NO", nil)
     otherButtonTitles:NSLocalizedString(@"ALERT_BTN_YES", nil)];

}
- (void)adjustHeightOfTableview
{
    CGFloat height = self.tableView.contentSize.height;
    CGFloat maxHeight = self.tableView.superview.frame.size.height - self.tableView.frame.origin.y;
    
    // if the height of the content is greater than the maxHeight of
    // total space on the screen, limit the height to the size of the
    // superview.
    
    if (height > maxHeight)
        height = maxHeight;
    
    // now set the frame accordingly
    
    [UIView animateWithDuration:0.25 animations:^{
        CGRect frame = self.tableView.frame;
        frame.size.height = height;
        self.tableView.frame = frame;
     }];
    if (arrForCards.count)
    {
        [_tableView setHidden:NO];
        [_lblNoItemsToDisplay setHidden:YES];
    }
    else
    {
        [_tableView setHidden:YES];
        [_lblNoItemsToDisplay setHidden:NO];
    }
    
}
#pragma mark -
#pragma mark - WS Stripe Methods
- (void)hasToken:(STPToken *)token
{
	strForLastFour=token.card.last4;
	strForStripeToken=token.tokenId;
    if (_txtWalletAmount.isHidden)
    {
        [self addCardOnServer];
    }
    else
    {
        
        [self addWalletAmount:[_txtWalletAmount.text doubleValue] andPaymentToken:strForStripeToken andCardId:@""];
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
}
- (NSInteger)lengthForCardType:(NSString *)type
{
	NSInteger length;
	if ([type  isEqualToString:@"American Express"])
	{
		length = 18;
	}
	else if ([type  isEqualToString:@"DinersClub"] )
	{
		length = 17;
	}
	else if([type  isEqualToString:@"JCB"]  || [type isEqualToString:@"MasterCard"]
		   || [type isEqualToString:@"Visa"] || [type isEqualToString:@"Discover"]
		   )
	{
		length = 19;
	}
	else
	{
		length=19;
	}
	
	return length;
}
- (void)hasError:(NSError *)error
{
	//NSlog(@"Error:-----%@",error);
	
	[[UtilityClass sharedObject] displayAlertWithTitle:NSLocalizedString(@"Error", @"Error") andMessage:[error localizedDescription]];
	[[AppDelegate sharedAppDelegate] hideLoadingView];
}
#pragma mark -
#pragma mark - WS Methods
- (void)wsDeleteCard
{
   
        NSString *url=[NSString stringWithFormat:@"%@%@",USER_DELETECARD,card_id ];
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setObject:strForUserId forKey:PARAM_USER_ID];
        [dict setObject:card_id forKey:PARAM_CARD_ID];
    
    
        
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString( @"LOADING_DELETE_CARD", nil)];
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:DELETE];
        [helper getDataFromPath:url withParamData:dict withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                 NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                                error:&error];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if([[jsonResponse valueForKey:SUCCESS] boolValue])
                     {
                         if (isRemoveSelectedCard)
                         {
                             if (arrForCards.count >= 1)
                             {
                                 Card *card=[arrForCards objectAtIndex:0];
                                 card_id=card.cardId;
                                 [self selectCard];
                             }
                             else
                             {
                                 selectedCard.cardId=nil;
                                 [self upDateUIOfCard:NO];
                             }
                         }
                         else
                         {
                             for (NSInteger i=0;i<arrForCards.count; i++)
                             {
                                 if (i==cardTag)
                                 {
                                     [arrForCards removeObjectAtIndex:cardTag];
                                 }
                                 
                             }
                             
                             if (arrForCards.count != 0)
                             {
                                 [self upDateUI:YES];
                             } else
                             {
                                 [self upDateUI:NO];
                             }
                         }
                         [_tableView reloadData];
                     }
                     else
                     {
                         [[UtilityClass sharedObject]displayAlertWithTitle:@"" andMessage:NSLocalizedString([jsonResponse valueForKey:ERROR_CODE], nil)];
                     }
                     [[AppDelegate sharedAppDelegate] hideLoadingView];
                 });
                 
             }
             else
             {
			 }
         }];
}
-(void)addCardOnServer
{
		//BUILD PARAMETER
		NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
		[dictParam setValue:strForUserId forKey:PARAM_USER_ID];
		[dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
		[dictParam setValue:strCardType forKey:PARAM_CARD_TYPE];
		[dictParam setValue:strForLastFour forKey:PARAM_LAST_FOUR];
		[dictParam setValue:strForStripeToken forKey:PARAM_PAYMENT_TOKEN];
        [APPDELEGATE showLoadingWithTitle:@""];
    //CALL WEBSERVICE
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
		[afn getDataFromPath:USER_ADD_CARD withParamData:dictParam withBlock:^(id response, NSError *error) {
			if (response)
			{
                if ([[Parser sharedObject]parseAddCard:response toArray:&arrForCards])
                {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSInteger cardListSize = arrForCards.count;
                        
                        switch (cardListSize)
                        {
                            case 1:
                                if ([[arrForCards objectAtIndex:0] isDefault])
                                {
                                    [self upDateUI:NO];
                                    [self upDateUIOfCard:YES];
                                    selectedCard=[arrForCards objectAtIndex:0];
                                    [_lblSelectedCard setText:[NSString stringWithFormat:@"************%@",selectedCard.lastFour ]];
                                    card_id = selectedCard.cardId;
                                    [arrForCards removeObjectAtIndex:0];
                                }
                                else
                                [self upDateUI:YES];
                                break;
                            case 0:
                                [self upDateUI:NO];
                                [self upDateUIOfCard:NO];
                                selectedCard.cardId=nil;
                                break;
                        }
                        [_tableView reloadData];
                        [[AppDelegate sharedAppDelegate] hideLoadingView];
                    });
                    
                    
                }
			}
			dispatch_async(dispatch_get_main_queue()
                           , ^{
                               [[AppDelegate sharedAppDelegate] hideLoadingView];
                           });
			
		}];
}
-(void)addWalletAmount:(double) walletAmt andPaymentToken:(NSString*)paymentToken andCardId:(NSString*)cardId
{
    if (walletAmt>0 || (selectedCard.cardId && paymentToken))
    {
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:strForUserId forKey:PARAM_USER_ID];
    [dictParam setValue:paymentToken forKey:PARAM_PAYMENT_TOKEN];
    [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
    [dictParam setValue:cardId forKey:PARAM_CARD_ID];
    [dictParam setValue:[NSNumber numberWithDouble:walletAmt] forKey:PARAM_WALLET];
    [APPDELEGATE showLoadingWithTitle:@""];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
    [afn getDataFromPath:WS_ADD_WALLET_AMOUNT withParamData:dictParam withBlock:^(id response, NSError *error)
    {
        if (response)
        {
            NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data	options:kNilOptions error:nil];
            
            if ([[Parser sharedObject]parseSuccess:response])
            {
					dispatch_async(dispatch_get_main_queue(), ^{
                    [_pvew setHidden:YES];
                    [_btnBackWallet setHidden:YES];
                    _txtWalletAmount.text=@"";
                    [_txtWalletAmount setHidden:YES];
                    [_lblWalletAmount setHidden:NO];
                    [_lblWalletAmount setText:[NSString stringWithFormat:@"%@ %@",[jsonResponse valueForKey:PARAM_WALLET],[jsonResponse valueForKey:PARAM_WALLET_CURRENCY_CODE]]];
                      
                    [_btnAddWalletAmount setTitle:[NSLocalizedString(@"ADD", nil) capitalizedString] forState:UIControlStateNormal];
                   [[AppDelegate sharedAppDelegate] hideLoadingView];
                });
            }
            else
            {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[AppDelegate sharedAppDelegate] hideLoadingView];
                     [[UtilityClass sharedObject]showToast:NSLocalizedString([jsonResponse valueForKey:ERROR_CODE],nil)];
                 });
            }
        }
        
        
    }];
    
    }
    else
    {
        if ([UtilityClass isEmpty:selectedCard.cardId])
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_SELECT_CARD_FIRST", nil)];
        }
        else
        {
        [[UtilityClass sharedObject]showToast:NSLocalizedString(@"TOAST_ENTER_VALID_AMOUNT", nil)];
        }
    }
 
}
-(void)getAllMyCards
{
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
		NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
		[dictParam setObject:strForUserId forKey:PARAM_USER_ID];
        [APPDELEGATE showLoadingWithTitle:@""];
		[afn getDataFromPath:WS_USER_GET_CARDS withParamData:dictParam withBlock:^(id response, NSError *error)
		 {
             if ([[Parser sharedObject]parseCards:response toArray:&arrForCards andWalletAmount:&walletAmount andCurrencyCode:&walletCurrencyCode andWalletStatus:&walletStatus])
             {
                 dispatch_async(dispatch_get_main_queue()
                                , ^{
                                    [[AppDelegate sharedAppDelegate] hideLoadingView];
                                    [_lblWalletAmount setText:[NSString stringWithFormat:@"%@ %@",walletAmount,walletCurrencyCode]];
                                    [_swtchWalletPayment setOn:walletStatus];
                                    if (arrForCards.count==0)
                                    {
                                        self.tableView.hidden=YES;
                                        _viewForSelectedCard.hidden=YES;
                                        _lblNoItemToDisplay.hidden=NO;
                                        _lblNoItemsToDisplay.hidden=NO;
                                    }
                                    else
                                    {
                                        self.tableView.hidden=NO;
                                        _lblNoItemsToDisplay.hidden=YES;
                                        NSInteger length=arrForCards.count;
                                        for (NSInteger i=0;i<length;i++)
                                        {
                                            Card *card=[arrForCards objectAtIndex:i];
                                            if(card.isDefault)
                                            {
                                                selectedCard=card;
                                                [_lblSelectedCard setText:[NSString stringWithFormat:@"************%@",selectedCard.lastFour ]];
                                                [_viewForSelectedCard setHidden:NO];
                                                [_lblSelectedCard setHidden:NO];
                                                [_lblNoItemToDisplay setHidden:YES];
                                                [arrForCards removeObjectAtIndex:i];
                                                length--;
                                            }
                                            
                                        }
                                        if (arrForCards.count)
                                        {
                                            [self.tableView reloadData];
                                            [self adjustHeightOfTableview];
                                            
                                        }
                                        
                                    }
                                    
                                    [APPDELEGATE hideLoadingView];
                                });
                 
                 
             }
		 }];
	
}
-(void)selectCard
{
    
       [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOADING_SELECT_CARD", nil)];
        NSString *url=[NSString stringWithFormat:@"%@",USER_SELECTCARD];
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setObject:card_id forKey:PARAM_CARD_ID];
        [dict setObject:strForUserId forKey:PARAM_USER_ID];
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST];
        [helper getDataFromPath:url withParamData:dict withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                 NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                                error:&error];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if([[jsonResponse valueForKey:SUCCESS] boolValue])
                     {
                         [self upDateUIOfCard:YES];
                         if (isRemoveSelectedCard)
                         {
                             [self selectCardDataModify:0];
                         }
                         else
                         {
                             [arrForCards addObject:selectedCard];
                             [self selectCardDataModify:selectedCardPosition];
                         }
                        }
                     else
                     {
                         [[UtilityClass sharedObject]showToast:NSLocalizedString([jsonResponse valueForKey:ERROR_CODE],nil)];
                     }
                     [[AppDelegate sharedAppDelegate] hideLoadingView];
                     
                 });
                 
             }
             else
             {
                 //NSlog(@"Errror");
                 
             }
             
         }];
   
}
-(void)setWalletStatus:(BOOL)isUseWallet
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:strForUserId forKey:PARAM_USER_ID];
    [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
    [dictParam setValue:[NSNumber numberWithBool:isUseWallet] forKey:PARAM_IS_USE_WALLET];
    [APPDELEGATE showLoadingWithTitle:@""];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
    [afn getDataFromPath:WS_CHANGE_WALLET_STATUS withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if (response)
         {
             if ([[Parser sharedObject]parseSuccess:response])
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [_swtchWalletPayment setOn:isUseWallet];
                     [[AppDelegate sharedAppDelegate] hideLoadingView];
                 });
                 
                 
             }
             else
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[AppDelegate sharedAppDelegate] hideLoadingView];});
             }
         }
         
         
     }];
    
}
#pragma mark-User Define Method
-(void)selectCardDataModify:(NSInteger)position
{
    selectedCard = [arrForCards objectAtIndex:position];
    for (NSInteger i=0; i<arrForCards.count; i++)
    {
        if (i==position)
        {
            [arrForCards removeObjectAtIndex:position];
        }
    }
    [_lblSelectedCard setText:[NSString stringWithFormat:@"************%@",selectedCard.lastFour ]];
    card_id = selectedCard.cardId;
    [_tableView reloadData];
}
-(void) upDateUIOfCard:(BOOL) isUpdate
{
    if (isUpdate)
    {
        [_viewForSelectedCard setHidden:NO];
        [_lblNoItemToDisplay setHidden:YES];
    }
    else
    {
        [_viewForSelectedCard setHidden:YES];
        [_lblNoItemToDisplay setHidden:NO];
    }
}
-(void) upDateUI:(BOOL) isUpdate
{
    if (isUpdate)
    {
        [_lblNoItemsToDisplay setHidden:YES];
        [_tableView setHidden:NO];
        
    } else
    {
        [_lblNoItemsToDisplay setHidden:NO];
        [_tableView setHidden:YES];
    }
}
-(UIView*)addShadow:(UIView*)view
{
    view.layer.shadowOpacity=0.5;
    view.layer.shadowOffset= CGSizeMake(0, 2.0f);
    view.layer.shadowColor = [UIColor textColor].CGColor;
    view.layer.masksToBounds=NO;
    view.clipsToBounds = NO;
    return view;
    
}
-(void)addButtonShadow:(UIButton*)button
{
    button.layer.shadowOpacity=0.8;
    button.layer.shadowOffset= CGSizeMake(0, 3.0f);
    button.layer.shadowColor = [UIColor textColor].CGColor;
    [button setBackgroundColor:[UIColor buttonColor]];
}
-(void)onClickCustomDialogOk:(CustomAlertWithTitle *)view
{
   
    if (view==deleteCardView) {
        [self wsDeleteCard];
    }
    
}
- (IBAction)onClickBtnAddWalletAmount:(id)sender
{
    if(_txtWalletAmount.isHidden)
    {
        _txtWalletAmount.text=@"";
        _txtWalletAmount.hidden=NO;
        [_txtWalletAmount becomeFirstResponder];
        [_lblWalletAmount setHidden:YES];
        [_btnBackWallet setHidden:NO];
        [_btnAddWalletAmount setTitle:[NSLocalizedString(@"SUBMIT", nil) capitalizedString] forState:UIControlStateNormal];
        [self.view addGestureRecognizer:tapGesture];
    }
    else
    {
        [self.view removeGestureRecognizer:tapGesture];
        [self addWalletAmount:[_txtWalletAmount.text doubleValue] andPaymentToken:@"" andCardId:selectedCard.cardId];
        
    }
}
- (IBAction)onChangeSwtchStatus:(id)sender
{
    UISwitch *switchObject = (UISwitch *)sender;
    if(switchObject.isOn)
    {
        [self setWalletStatus:YES];
    }
    else
    {
        [self setWalletStatus:NO];
    }

}

-(NSString*)cardBrandFromNumber:(NSString *)number
{     if ([number hasPrefix:@"34"] || [number hasPrefix:@"37"]) {         return @"amex";     } else if ([number hasPrefix:@"60"] ||                [number hasPrefix:@"62"] ||                [number hasPrefix:@"64"] ||                [number hasPrefix:@"65"]) {         return @"discover";     } else if ([number hasPrefix:@"35"]) {         return @"JCB";     } else if ([number hasPrefix:@"30"] ||                [number hasPrefix:@"36"] ||                [number hasPrefix:@"38"] ||                [number hasPrefix:@"39"]) {         return @"Diners Club";     } else if ([number hasPrefix:@"4"]) {         return @"visa";     } else if ([number hasPrefix:@"5"]) {         return @"MasterCard";     } else {         return @"Unknown";     }
}
-(void)hideKeyboard
{
    [self.view endEditing:YES];
    [self.view removeGestureRecognizer:tapGesture];
}
@end
