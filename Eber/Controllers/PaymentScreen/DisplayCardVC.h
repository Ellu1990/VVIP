
#import "Stripe.h"
#import "Navigation.h"
#import "CustomAlertWithTitle.h"
@interface DisplayCardVC :Navigation
<UITableViewDataSource,
UITableViewDelegate,
UITextFieldDelegate,
CustomAlertDelegate>
{
	NSString *strForStripeToken,*strForLastFour;
	NSInteger cardTag;
    
}
//OutLets
@property(assign,nonatomic) BOOL comeFromRegister;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *pvew;
@property (weak, nonatomic) IBOutlet UITextField *txtCreditCard;
@property (weak, nonatomic) IBOutlet UITextField *txtCvv;
@property (weak, nonatomic) IBOutlet UITextField *txtmm;
@property (weak, nonatomic) IBOutlet UITextField *txtyy;
@property (strong, nonatomic) IBOutlet UIButton *btnAddCardManually;
@property (strong, nonatomic) IBOutlet UIButton *btnSkip;
@property (weak, nonatomic) IBOutlet UILabel *lblNoItemToDisplay;
@property (weak, nonatomic) IBOutlet UILabel *lblNoItemsToDisplay;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (strong, nonatomic) STPCard* stripeCard; //USed To get Token


//Actions--
- (IBAction)onChangeSwtchStatus:(id)sender;
- (IBAction)onClickBtnAddCardManually:(id)sender;
- (IBAction)onClickBtnBack:(id)sender;
- (IBAction)onClickBtnHidePview:(id)sender;
- (IBAction)onClickBtnaddPayment:(id)sender;
-(IBAction)onClickBtnSkip:(id)sender;
- (IBAction)onClickBtnSelectedDelete:(id)sender;
- (IBAction)onClickBtnAddWalletAmount:(id)sender;
/*Views*/
@property (weak, nonatomic) IBOutlet UIView *mainViewForSelectPaymentCard;
@property (weak, nonatomic) IBOutlet UIView *viewForSelectedCard;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedCard;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectedDelete;
@property (weak, nonatomic) IBOutlet UILabel *lblOtherCard;
@property (weak, nonatomic) IBOutlet UILabel *lblSelCard;
/*View for Wallet*/
@property (weak, nonatomic) IBOutlet UIView *viewForWallet;
@property (weak, nonatomic) IBOutlet UIImageView *imgWallet;
@property (weak, nonatomic) IBOutlet UIButton *btnAddWalletAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblWalletAmount;
@property (weak, nonatomic) IBOutlet UITextField *txtWalletAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblWallet;
@property (weak, nonatomic) IBOutlet UIButton *btnBackWallet;
@property (weak, nonatomic) IBOutlet UISwitch *swtchWalletPayment;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmitCard;

@end
