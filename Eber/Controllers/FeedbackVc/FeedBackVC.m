//
//  InvoiceVC.m
//    Eber ClientProvider
//
//  Created by Elluminati Mini Mac 5 on 29/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import "FeedbackVC.h"
#import "invoice.h"
#import "RatingBar.h"
#import "PreferenceHelper.h"
#import "Parser.h"
#import "MapVc.h"
#import "NSObject+Constants.h"
#import "UtilityClass.h"
#import "AFNHelper.h"
#import "Invoice.h"
#import "AppDelegate.h"
#import "SWRevealViewController.h"
#import "MapVc.h"
#import "UILabel+overrideLabel.h"
#import "UIColor+Colors.h"
#import "UIImageView+image.h"
#import "FavouriteDriver.h"

@interface FeedbackVC ()
{
    UILabel *placeholderLabel;
}
@end

@implementation FeedbackVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setLocalization];
    _imgUser.image=[UIImage imageNamed:@"user"];
    [_lblProviderName setText:[NSString stringWithFormat:@"%@ %@",[CurrentTrip sharedObject].providerFirstName,[CurrentTrip sharedObject].providerLastName]];
    Invoice *i=[Invoice sharedObject];
    [_lblfTime setText:[NSString stringWithFormat:@"%@ %@", i.time,TIME_SUFFIX] withColor:[UIColor textColor]];
    [_lblCost setText:[NSString stringWithFormat:@"%@ %@", i.distance,DISTANCE_SUFFIX ] withColor:[UIColor textColor]];
    [APPDELEGATE hideLoadingView];
	[self getFavDriver];
	
}
-(void)getFavDriver
{
	[APPDELEGATE showLoadingWithTitle:nil];
	
	NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
	[dictParam setObject:strUserId forKey:PARAM_USER_ID];
	[dictParam setObject:[CurrentTrip sharedObject].serviceTypeID forKey:PARAM_SERVICE_TYPE_ID];
 // AUTOCOMPLETE API
	
	AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST];
	[afn getDataFromPath:WS_GET_FAV_DRIVER withParamData:dictParam withBlock:^(id response, NSError *error)
	 {
		if ([[Parser sharedObject]parseGetFavouriteDriver:response])
		 {
			dispatch_async(dispatch_get_main_queue(), ^
						   {
						   FavouriteDriver *obj = [FavouriteDriver sharedObject];
						   
							NSArray *arrFavProvider = [NSArray arrayWithArray:obj.arrFavDriver];
//						   
						   for (int i=0; i<arrFavProvider.count; i++)
						   {
							 NSDictionary *dictData = [arrFavProvider objectAtIndex:i];
							 NSString *strProviderid = [dictData valueForKey:@"provider_id"];
						   
						       if ([strProviderid isEqualToString:[CurrentTrip sharedObject].providerId])
							   {
							   [_btnFavourite setImage:[UIImage imageNamed:@"cb_glossy_on"] forState:UIControlStateNormal];
							   [_btnFavourite setTitle:@"  Select driver as your favourite driver." forState:UIControlStateNormal];
							   _btnFavourite.selected = YES;
						       }
						       else
							   {
							   [_btnFavourite setImage:[UIImage imageNamed:@"cb_glossy_off"] forState:UIControlStateNormal];
							   [_btnFavourite setTitle:@"  Select driver as your favourite driver." forState:UIControlStateNormal];
							   _btnFavourite.selected = NO;
							   }
							}
						   [APPDELEGATE hideLoadingView];
						   });
		 }
	 else
		 {
			dispatch_async(dispatch_get_main_queue(), ^{
				[APPDELEGATE hideLoadingView];
			});
		 }
	 }];
}


-(void)setLocalization
{
    [self.view setBackgroundColor:[UIColor backGroundColor]];
    [_viewForFeedBack setBackgroundColor:[UIColor backGroundColor]];
    
    [_btnRate setBackgroundColor:[UIColor buttonColor]];
    [_btnRate setTitle:[NSLocalizedString(@"SUBMIT", nil) uppercaseString] forState:UIControlStateNormal];
    [_btnRate setTitleColor:[UIColor buttonTextColor] forState:UIControlStateNormal];
    
    [_commentView setBackgroundColor:[UIColor whiteColor]];
    
    [_txtComment setBackgroundColor:[UIColor whiteColor]];
    placeholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0, _txtComment.frame.size.width - 20.0, 34.0)];
    [placeholderLabel setText:NSLocalizedString(@"ENTER_REVIEW", nil)];
    [placeholderLabel setBackgroundColor:[UIColor clearColor]];
    [placeholderLabel setFont:[_txtComment font]];
    [placeholderLabel setTextColor:[UIColor lightGrayColor]];
    [placeholderLabel setNumberOfLines:2];
    [_txtComment setText:@""];
    [_txtComment setTextColor:[UIColor textColor]];
    [_txtComment addSubview:placeholderLabel];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self customSetup];
    [_btnNavigation setTitle:[NSLocalizedString(@"TITLE_FEEDBACK", nil) capitalizedString] forState:UIControlStateNormal];
    [_btnNavigation setTitleColor:[UIColor labelTitleColor] forState:UIControlStateNormal];
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [_imgUser downloadFromURL:[CurrentTrip sharedObject].providerProfileImage withPlaceholder:[UIImage imageNamed:@"user"]];
}
-(void)viewDidLayoutSubviews
{
    [_imgUser setRoundImageWithColor:[UIColor borderColor]];
    _imgUser.center=CGPointMake(self.view.center.x,_imgUser.center.y);
    ratingView=[[RatingBar alloc] initWithSize:CGSizeMake(200, 40) AndPosition:CGPointMake(self.view.center.x-100,_lblProviderName.frame.origin.y+_lblProviderName.frame.size.height+10)];
    ratingView.backgroundColor=[UIColor clearColor];
    [self.viewForFeedBack addSubview:ratingView];
    [self.viewForFeedBack bringSubviewToFront:ratingView];
    
}
-(void)wsRateProvider:(double)rate andReview:(NSString*)review
{
    [APPDELEGATE showLoadingWithTitle:@""];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    [dictParam setValue:PREF.userId forKey:PARAM_USER_ID];
    [dictParam setValue:PREF.tripId forKey:PARAM_TRIP_ID];
    [dictParam setValue:[NSNumber numberWithDouble:rate] forKey:PARAM_RATING];
    [dictParam setValue:review forKey:PARAM_REVIEW];
	[dictParam setValue:[NSString stringWithFormat:@"%d",_btnFavourite.selected] forKey:PARAM_IS_FAVOURITE];
    NSString *strUrl=[NSString stringWithFormat:@"%@%@",USER_RATE_PROVIDER,PREF.tripId];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:PUT];
        [afn getDataFromPath:strUrl withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
             NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data							options:kNilOptions error:nil];
                dispatch_async(dispatch_get_main_queue(), ^
                            {
            
                 if([[jsonResponse valueForKey:SUCCESS]boolValue])
                 {
						IS_TRIP_EXSIST=NO;
						IS_TRIP_ACCEPTED=NO;
						[[CurrentTrip sharedObject] resetObject];
						[APPDELEGATE goToMap];
						return;
                 }
                else
                {
                    NSString *errorCode=[jsonResponse valueForKey:ERROR_CODE];
                    [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode,nil)];
                }
                                
                                [APPDELEGATE hideLoadingView];
                });
             
         }];
    
}
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}

- (IBAction)onClickBtnRate:(id)sender
{
    RBRatings rating=[ratingView getcurrentRatings];
    double rate=rating/2.0;
    if (rating%2 != 0)
    {
        rate += 0.5;
    }
    if(rate==0)
    {   [[AppDelegate sharedAppDelegate] hideLoadingView];
        [[UtilityClass sharedObject] showToast:NSLocalizedString(@"TOAST_GIVE_RATE",nil)];}
    else
    {
        [self wsRateProvider:rate andReview:[NSString stringWithFormat:@"%@",_txtComment.text]];
    }

}

- (IBAction)onClickFavourite:(id)sender
{
	if (_btnFavourite.isSelected)
	{
	[_btnFavourite setImage:[UIImage imageNamed:@"cb_glossy_off"] forState:UIControlStateNormal];
	[_btnFavourite setTitle:@"  Select driver as your favourite driver." forState:UIControlStateNormal];
	_btnFavourite.selected = NO;
	}
	else
	{
	[_btnFavourite setImage:[UIImage imageNamed:@"cb_glossy_on"] forState:UIControlStateNormal];
	[_btnFavourite setTitle:@"  Select driver as your favourite driver." forState:UIControlStateNormal];
	_btnFavourite.selected = YES;
	}
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtComment resignFirstResponder];
}
- (void)customSetup
{
    [self.btnNavigation addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    SWRevealViewController *reveal = self.revealViewController;
    reveal.panGestureRecognizer.enabled = NO;
}
-(void)dealloc
{
    
}
#pragma mark-TEXTVIEW DELEGATE
- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    [txtView resignFirstResponder];
    return NO;
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    CGPoint pt;
    CGRect rc = [textView bounds];
    rc = [textView convertRect:rc toView:_scrollView];
    pt = rc.origin;
    pt.x = 0;
    pt.y -= 100;
    [_scrollView setContentOffset:pt animated:YES];
    return YES;
}


- (void) textViewDidChange:(UITextView *)theTextView
{
    if(![_txtComment hasText]) {
        [_txtComment addSubview:placeholderLabel];
        [UIView animateWithDuration:0.15 animations:^{
            placeholderLabel.alpha = 1.0;
        }];
    } else if ([[_txtComment subviews] containsObject:placeholderLabel]) {
        
        [UIView animateWithDuration:0.15 animations:^{
            placeholderLabel.alpha = 0.0;
        } completion:^(BOOL finished) {
            [placeholderLabel removeFromSuperview];
        }];
    }
}


- (void)textViewDidEndEditing:(UITextView *)theTextView
{
    if (![_txtComment hasText])
    {
        [_txtComment addSubview:placeholderLabel];
        [UIView animateWithDuration:0.15 animations:^{
            placeholderLabel.alpha = 1.0;
        }];
    }
}
@end

