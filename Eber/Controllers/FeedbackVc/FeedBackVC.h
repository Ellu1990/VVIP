//
//  InvoiceVC.h
//    Eber ClientProvider
//
//  Created by Elluminati Mini Mac 5 on 29/08/16.
//  Copyright © 2016 Elluminati Mini Mac 5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingBar.h"

@interface FeedbackVC : UIViewController
<UITextViewDelegate>

{
    NSString *strTime,*strDistance;
    RatingBar *ratingView;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnNavigation;
/*view For FeedBack*/

@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UILabel *lblCost;
@property (weak, nonatomic) IBOutlet UITextView *txtComment;
@property (weak, nonatomic) IBOutlet UIView *viewForFeedBack;
@property (weak, nonatomic) IBOutlet UILabel *lblfTime;
@property (weak, nonatomic) IBOutlet UILabel *lblProviderName;
@property (weak, nonatomic) IBOutlet UIButton *btnRate;
@property (weak, nonatomic) IBOutlet UIButton *btnFavourite;
@property (weak, nonatomic) IBOutlet UIView *commentView;
- (IBAction)onClickBtnRate:(id)sender;
- (IBAction)onClickFavourite:(id)sender;
@end
