//
//  Parser.m
//  
//
//  Created by Elluminati Mini Mac 5 on 12/09/16.
//
//

#import "Parser.h"
#import "UtilityClass.h"
#import "NSObject+Constants.h"
#import "CurrentTrip.h"
#import "Invoice.h"
#import "Trip.h"
#import "card.h"
#import "CarTypeDataModal.h"
#import "PreferenceHelper.h"
#import "Contact.h"
#import "AppDelegate.h"
#import "PaymentGateway.h"
#import "FavouriteDriver.h"

@implementation Parser


#pragma mark - SingleTon Methods
+(Parser *) sharedObject
{
    static Parser *objParser = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        objParser = [[Parser alloc] init];
    });
    return objParser;
}
#pragma mark- Convert Response To Json
-(BOOL)stringToJson:(NSString*)response To:(id*)json
{
    @try
    {
        NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
        NSError *jsonError;
        id parsedObject= [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
        if (parsedObject == nil)
        {
            NSLog(@"Given String Is Not Valid");
            return NO;
        }
        else if ([parsedObject isKindOfClass: [NSArray class]] )
        {
            *json=parsedObject;
            return YES;
        }
        else if ([parsedObject isKindOfClass: [NSDictionary class]] )
        {
            *json=parsedObject;
            return YES;
        }
        else
        {
            NSLog(@"Given Type And Json Type Is Not Compitable");
            return NO;
        }
        
    } @catch (NSException *exception)
    {
        NSLog(@"Json Exception:-- %@",exception);
    }
}
#pragma mark-TRIP RELATED PARSING
- (BOOL)parseBooking:(NSString*)response toArray:(NSMutableArray * __strong *)arrForBooking
{
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                for(NSDictionary *tripDict in [jsonResponse valueForKey:PARAM_SCHEDULE_TRIP])
                {
                    
                    Trip *trip=[[Trip alloc]init];
                    trip.srcAddress=[tripDict valueForKey:PARAM_TRIP_SOURCE_ADDRESS];
                    NSString *strServerDate=[NSString stringWithFormat:@"%@",[tripDict valueForKey:PARAM_SERVER_START_TIME] ];
                    NSString *strDate=[strServerDate substringToIndex:10];
                    NSString *timeZone=[[UtilityClass sharedObject]getTimeZone];
                    [[UtilityClass sharedObject]setTimeZone:@"UTC"];
                    NSDate *date=[[UtilityClass sharedObject]stringToDate:strServerDate withFormate:DATE_TIME_FORMAT_WEB];
                    [[UtilityClass sharedObject]setTimeZone:timeZone];
                    trip.tripTime=[[UtilityClass sharedObject]DateToString:date withFormate:TIME_FORMAT_AM];
                    trip.tripCreateDate=[[UtilityClass sharedObject]stringToDate:strDate];
                    trip.tripId=[tripDict valueForKey:PARAM_ID];
                    [*arrForBooking addObject:trip];
                }
                return YES;
            }
            else
            {
                NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                               });
                
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Booking Response Not Valid Generate Exception %@",e);
        return NO;
    }
    
    return NO;
}
- (BOOL)parseTrip:(NSString*)response
{
    @try
    {
        
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                

                CurrentTrip *trip=[CurrentTrip sharedObject];
                NSString *strTripId=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_TRIP_ID] ];
                PREF.tripId=strTripId;
                /*Place Provider Detail*/
                trip.trip_id=[jsonResponse valueForKey:PARAM_PROVIDER_ID];
                trip.providerFirstName=[jsonResponse valueForKey:PARAM_FIRST_NAME];
                trip.providerLastName=[jsonResponse valueForKey:PARAM_LAST_NAME];
                trip.providerCarNumber=[jsonResponse valueForKey:PARAM_CAR_NUMBER];
                trip.providerCarModal=[jsonResponse valueForKey:PARAM_CAR_MODEL];
                trip.providerProfileImage=[NSString stringWithFormat:@"%@%@",BASE_URL,[jsonResponse valueForKey:PARAM_PICTURE]];
                NSArray *geoProviderLatLong =[jsonResponse valueForKey:PARAM_SOURCE_LOCATION ];
                trip.providerLatitude=[NSString stringWithFormat:@"%@",[geoProviderLatLong objectAtIndex:0] ];
                trip.providerLongitude=[NSString stringWithFormat:@"%@",[geoProviderLatLong objectAtIndex:1] ];
                trip.providerPhone=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_PHONE] ];
                trip.phoneCountryCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_COUNTRY_CODE]];
                
                return YES;
            }
            else
            {
                NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                               });

                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Trip Response Not Valid Generate Exception %@",e);
        return NO;
    }
    
}
- (BOOL)parseProvider:(NSString*)response
{
    
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                NSDictionary *providerJson=[jsonResponse valueForKey:PARAM_PROVIDER];
                CurrentTrip *currentTripProvider=[CurrentTrip sharedObject];
                currentTripProvider.providerFirstName=[NSString stringWithFormat:@"%@",[providerJson valueForKey:PARAM_FIRST_NAME]];
                currentTripProvider.providerLastName = [NSString stringWithFormat:@"%@",[providerJson valueForKey:PARAM_LAST_NAME]];
                currentTripProvider.providerId=[NSString stringWithFormat:@"%@",[providerJson valueForKey:PARAM_ID]];
                currentTripProvider.providerCarModal=[NSString stringWithFormat:@"%@",[providerJson valueForKey:PARAM_CAR_MODEL]];
                currentTripProvider.providerCarNumber=[NSString stringWithFormat:@"%@",[providerJson valueForKey:PARAM_CAR_NUMBER]];
                currentTripProvider.providerProfileImage=[NSString stringWithFormat:@"%@%@",BASE_URL,[providerJson valueForKey:PARAM_PICTURE]];
                currentTripProvider.providerPhone=[NSString stringWithFormat:@"%@",[providerJson valueForKey:PARAM_PHONE]];
                currentTripProvider.phoneCountryCode=[NSString stringWithFormat:@"%@",[providerJson valueForKey:PARAM_COUNTRY_CODE]];
                
                
                currentTripProvider.providerLatitude=[NSString stringWithFormat:@"%@",[[jsonResponse valueForKey:PARAM_SOURCE_LOCATION] objectAtIndex:0]];
                currentTripProvider.providerLongitude=[NSString stringWithFormat:@"%@",[[jsonResponse valueForKey:PARAM_SOURCE_LOCATION] objectAtIndex:1]];
                return YES;
            }
            else
            {
                NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                               });

                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"GET Provider Detail Response Not Valid Generate Exception %@",e);
        return NO;
    }
    
}
-(BOOL) parseTripStatus:(NSString*)response
{
    
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
             CurrentTrip *currentTrip = [CurrentTrip sharedObject];
             NSDictionary *tripJson = [jsonResponse valueForKey:PARAM_TRIP];
             NSString *strTripId=[NSString stringWithFormat:@"%@",[tripJson valueForKey:PARAM_ID]];
             PREF.tripId=strTripId;
             currentTrip.trip_id=[tripJson valueForKey:PARAM_ID];
             currentTrip.providerId = [NSString stringWithFormat:@"%@",[tripJson valueForKey:PARAM_CURRENT_PROVIDER]];
             currentTrip.srcAddress = [NSString stringWithFormat:@"%@",[tripJson valueForKey:PARAM_TRIP_SOURCE_ADDRESS]];
             currentTrip.destAddress = [NSString stringWithFormat:@"%@",[tripJson valueForKey:PARAM_TRIP_DESTINATION_ADDRESS]];
             currentTrip.isProviderAccepted = [NSString stringWithFormat:@"%@",[tripJson valueForKey:PARAM_IS_PROVIDER_ACCEPTED]];
             currentTrip.providerStatus=[NSString stringWithFormat:@"%@",[tripJson valueForKey:PARAM_PROVIDER_STATUS]];
             currentTrip.isProviderRated=[NSString stringWithFormat:@"%@",[tripJson valueForKey:PARAM_IS_PROVIDER_RATED]];
			currentTrip.serviceTypeID=[NSString stringWithFormat:@"%@",[tripJson valueForKey:@"service_type_id"]];
             NSArray *geoSrcArray =[tripJson valueForKey:PARAM_SOURCE_LOCATION ];
             currentTrip.srcLatitude=[NSString stringWithFormat:@"%@",[geoSrcArray objectAtIndex:0] ];
             currentTrip.srcLongitude=[NSString stringWithFormat:@"%@",[geoSrcArray objectAtIndex:1] ];
             NSArray *geoDestArray = [tripJson valueForKey:PARAM_DESTINATION_LOCATION];;
             currentTrip.destLatitude=[NSString stringWithFormat:@"%@",[geoDestArray objectAtIndex:0] ];
             currentTrip.destLongitude=[NSString stringWithFormat:@"%@",[geoDestArray objectAtIndex:1] ];
             currentTrip.paymentMode=[[NSString stringWithFormat:@"%@",[tripJson valueForKey:PARAM_PAYMENT_MODE]] boolValue] ;
             currentTrip.cancelTripCharge=[NSString stringWithFormat:@"%.2f",[[jsonResponse valueForKey:PARAM_CANCELLATION_FEE] doubleValue]];
             currentTrip.isPromoUsed=[[jsonResponse valueForKey:PARAM_IS_PROMO_USED] boolValue];
             currentTrip.totalTime=[NSString stringWithFormat:@"%.2f",[[tripJson valueForKey:PARAM_TOTAL_TIME] doubleValue]];
             currentTrip.totalDistance=[NSString stringWithFormat:@"%.2f",[[tripJson valueForKey:PARAM_TOTAL_DISTANCE] doubleValue]];
             currentTrip.tripNumber=[NSString stringWithFormat:@"%@",[tripJson valueForKey:PARAM_TRIP_NUMBER]];
             currentTrip.waitingPrice=[[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_FOR_WAITING_TIME]] doubleValue];
             WAITING_SECONDS_REMAIN=[[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_TOTAL_WAIT_TIME] ] integerValue];
             CurrencySign=[NSString stringWithFormat:@"%@",[tripJson valueForKey:PARAM_CURRENCY] ];
             NSDictionary *city_detail=[jsonResponse valueForKey:PARAM_CITY_DETAIL];
             PREF.CashAvailable=[[city_detail valueForKey:PARAM_IS_PAYMENT_MODE_CASH] boolValue];
             PREF.CardAvailable=[[city_detail valueForKey:PARAM_IS_PAYMENT_MODE_CARD] boolValue];
             PREF.PromoForCash=[[city_detail valueForKey:PARAM_IS_PROMO_APPLY_FOR_CASH] boolValue];
             PREF.PromoForCard=[[city_detail valueForKey:PARAM_IS_PROMO_APPLY_FOR_CARD] boolValue];
			currentTrip.cityDetailID = [city_detail valueForKey:@"_id"];
                return YES;
            }
            else
            {
                return NO;
            }
        }
        else
        {
            NSLog(@"PARSING PROBLEM RESPONSE IS NOT VALID");
        }
    }
    @catch(NSException *e)
    {
            NSLog(@"TRIP STATUS RESPONSE IS NOT VALID %@",e);
            return NO;
    }
}
- (BOOL)parseInvoice:(NSString*)response
{
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                Invoice *invoice=[Invoice sharedObject];
                NSDictionary *jsonTripService=[jsonResponse valueForKey:PARAM_TRIP_SERVICE];
                NSDictionary *jsonTrip=[jsonResponse valueForKey:PARAM_TRIP];
                invoice.basePrice=[NSString stringWithFormat:@"%.2f",[[jsonTripService valueForKey:PARAM_BASEPRICE] doubleValue] ];
                invoice.total=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_TOTAL] doubleValue]];
                invoice.time=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_TOTAL_TIME] doubleValue]];
                invoice.timeCost=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_TIME_COST] doubleValue]];
                invoice.pricePerUnitTime=[NSString stringWithFormat:@"%.2f",[[jsonTripService valueForKey:PARAM_PRICE_FOR_TOTAL_TIME] doubleValue]];
                invoice.distance=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_TOTAL_DISTANCE] doubleValue]];
                invoice.distanceCost=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_DISTANCE_COST] doubleValue]];
                invoice.paymentMode=[NSString stringWithFormat:@"%@",[jsonTrip valueForKey:PARAM_PAYMENT_MODE]];
                invoice.referralBonus=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_REFFERAL_BONOUS] doubleValue]];
                invoice.promoBonus=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_PROMO_BONOUS] doubleValue]];
                invoice.pricePerUnitDistance=[NSString stringWithFormat:@"%.2f",[[jsonTripService valueForKey:PARAM_PRICE_PER_UNIT_DISTANCE] doubleValue]];
                invoice.currency=[NSString stringWithFormat:@"%@",[jsonTrip valueForKey:PARAM_CURRENCY]];
                invoice.tax=[NSString stringWithFormat:@"%.2f",[[jsonTripService valueForKey:PARAM_TAX] doubleValue]];
                invoice.taxCost=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_TAX_FEE] doubleValue]];
                invoice.basePriceDistance=[NSString stringWithFormat:@"%.2f",[[jsonTripService valueForKey:PARAM_BASE_PRICE_DISTANCE] doubleValue]];
                invoice.surgeTimeFee=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_SURGE_FEE] doubleValue]];
                invoice.pricePerWaitingTime=[NSString stringWithFormat:@"%.2f",[[jsonTripService valueForKey:PARAM_FOR_WAITING_TIME] doubleValue]];
                invoice.totalWaitingTime=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_TOTAL_WAITING_TIME] doubleValue]];
                invoice.surgeMultiplier=[NSString stringWithFormat:@"%.2f",[[jsonTripService valueForKey:PARAM_SURGE_MULTIPLIEER] doubleValue]];
                invoice.waitingTimeCost=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_WAITING_TIME_COST] doubleValue]];
                invoice.walletPayment=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_WALLET_PAYMENT] doubleValue]];
                invoice.remainingPayment=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_REMAINING_PAYMENT] doubleValue]];
                invoice.invoiceNumber=[NSString stringWithFormat:@"%@",[jsonTrip valueForKey:PARAM_INVOICE_NUMBER]];
                invoice.cardPayment=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_CARD_PAYMENT] doubleValue]];
                
                invoice.cashPayment=[NSString stringWithFormat:@"%.2f",[[jsonTrip valueForKey:PARAM_CASH_PAYMENT] doubleValue]];
                
                NSString *DistanceUnit=[NSString stringWithFormat:@"%@",[jsonTrip valueForKey:PARAM_DISTANCE_UNIT]];
                if ([DistanceUnit isEqualToString:@"0"])
                {
                    invoice.distanceUnit=NSLocalizedString(@"MILE", nil);
                    DISTANCE_SUFFIX=NSLocalizedString(@"MILE", nil);
                }
                else
                {
                    invoice.distanceUnit=NSLocalizedString(@"KILOMETER", nil);
                    DISTANCE_SUFFIX=NSLocalizedString(@"KILOMETER", nil);
                }
                
                return YES;
            }
            else
            {
                NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [APPDELEGATE hideLoadingView];
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                               });

                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Parse Invoice Response Not Valid Generate Exception %@",e);
        return NO;
    }
}
- (BOOL)parseHistory:(NSString*)response toArray:(NSMutableArray * __strong *)arrForHistory
{
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                for(NSDictionary *tripDict in [jsonResponse valueForKey:PARAM_TRIPS])
                {
                    Trip *trip=[[Trip alloc]init];
                    trip.tripId=[tripDict valueForKey:PARAM_TRIP_ID] ;
                    trip.userImage=[NSString stringWithFormat:@"%@%@",BASE_URL,[tripDict valueForKey:PARAM_PICTURE]];
                    trip.userFirstName=[tripDict valueForKey:PARAM_FIRST_NAME];
                    trip.userLastName=[tripDict valueForKey:PARAM_LAST_NAME];
                    trip.tripTotalCost=[tripDict valueForKey:PARAM_TOTAL];
                    trip.tripTime=[tripDict valueForKey:PARAM_TIME];
                    NSString *strTime=[tripDict valueForKey:PARAM_TRIP_CREATE_TIME];
                    NSString *timeZone=[[UtilityClass sharedObject]getTimeZone];
                    [[UtilityClass sharedObject]setTimeZone:@"UTC"];
                    NSDate *date=[[UtilityClass sharedObject]stringToDate:strTime withFormate:DATE_TIME_FORMAT_WEB];
                    [[UtilityClass sharedObject]setTimeZone:timeZone];
                    trip.tripCreateTime=[[UtilityClass sharedObject]DateToString:date withFormate:TIME_FORMAT_AM];
                   
                    NSString *strDate=[[UtilityClass sharedObject]DateToString:date withFormate:DATE_FORMAT];
                   [[UtilityClass sharedObject]setTimeZone:@"UTC"];
                    trip.tripCreateDate=[[UtilityClass sharedObject]stringToDate:strDate withFormate:DATE_FORMAT];
                    [[UtilityClass sharedObject]setTimeZone:timeZone];
                    trip.currency=[NSString stringWithFormat:@"%@",[tripDict valueForKey:PARAM_CURRENCY]];
                    trip.distanceUnit=[NSString stringWithFormat:@"%@",[tripDict valueForKey:PARAM_DISTANCE_UNIT]];
                    trip.isTripCancelled=[[NSString stringWithFormat:@"%@",[tripDict valueForKey:PARAM_IS_TRIP_CANCELLED_BY_USER]] boolValue];
                    if ([trip.distanceUnit isEqualToString:@"0"])
                    {
                        trip.distanceUnit=NSLocalizedString(@"MILE", nil);
                    }
                    else
                    {
                        trip.distanceUnit=NSLocalizedString(@"KILOMETER", nil);;
                    }
                        [*arrForHistory addObject:trip];
                }
                return YES;
            }
            else
            {
                NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                               });

                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"History Response Not Valid Generate Exception %@",e);
        return NO;
    }
    
    return NO;
}

#pragma mark-REGISTER AND LOGIN
- (BOOL)parseLogin:(NSString*)response
{
    
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                PREF.UserFirstName= [NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_FIRST_NAME] ];
                PREF.UserLastName=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_LAST_NAME] ];
                PREF.UserPicture= [NSString stringWithFormat:@"%@%@",BASE_URL,[jsonResponse valueForKey:PARAM_PICTURE] ];
                PREF.UserEmail=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_EMAIL] ];
                PREF.UserSocialId=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_SOCIAL_UNIQUE_ID] ];
                PREF.UserLoginBy=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_LOGIN_BY] ];
                PREF.UserCountryCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_COUNTRY_CODE] ];
                PREF.UserPhone=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_PHONE] ];
                PREF.UserAddress=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_ADDRESS] ];
                PREF.UserBio=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_BIO] ];
                PREF.UserZipcode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_ZIPCODE] ];
                PREF.UserLogin=YES;
                PREF.UserApproved=[[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_USER_APPROVED]] boolValue];
                PREF.UserId=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_USER_ID]];
                PREF.UserToken=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_TOKEN]];
                PREF.RefferalCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_REFERRAL_CODE]];
				PREF.UserCompanyId=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_COMPANY_ID]];
                strUserId=PREF.userId;
                strUserToken=PREF.userToken;
                NSString *messageCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:MESSAGE_CODE]];
                dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(messageCode, nil)];
                               });
                return YES;
            }
            else
            {
                NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                                    dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                               });

                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Login Response Not Valid Generate Exception %@",e);
        return NO;
    }
}
- (BOOL)isProfileUpdated:(NSString*)response
{
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                PREF.UserFirstName= [NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_FIRST_NAME] ];
                PREF.UserLastName=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_LAST_NAME] ];
                PREF.UserPicture= [NSString stringWithFormat:@"%@%@",BASE_URL,[jsonResponse valueForKey:PARAM_PICTURE] ];
                PREF.UserSocialId=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_SOCIAL_UNIQUE_ID] ];
                PREF.UserCountryCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_COUNTRY_CODE] ];
                PREF.UserPhone=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_PHONE] ];
                PREF.UserAddress=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_ADDRESS] ];
                PREF.UserBio=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_BIO] ];
                PREF.UserZipcode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_ZIPCODE] ];
                NSString *messageCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:MESSAGE_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(messageCode, nil)];
                               });

                return YES;
            }
            else
            {dispatch_async(dispatch_get_main_queue(), ^{
                NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                               });

                
            });
                 return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Login Response Not Valid Generate Exception %@",e);
        return NO;
    }
}
#pragma mark-Payment Related Parsing
- (BOOL)parseCards:(NSString*)response toArray:(NSMutableArray * __strong *)arrForCards andWalletAmount:(NSString * __strong *)walletAmount andCurrencyCode:(NSString * __strong *)walletCurrencyCode andWalletStatus:(BOOL *)walletStatus
{
    @try
    {
        NSLog(@"Card List %@",response);
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                [*arrForCards removeAllObjects];
                *walletCurrencyCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_WALLET_CURRENCY_CODE]];
                *walletAmount=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_WALLET]];
                *walletStatus=[[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_IS_USE_WALLET]] boolValue];
			
				for (NSDictionary *cardJson in [jsonResponse valueForKey:PARAM_CARD])
                {
                    Card *card=[[Card alloc]init];
                    card.cardId=[NSString stringWithFormat:@"%@",[cardJson valueForKey:PARAM_ID]];
                    card.cardType=[NSString stringWithFormat:@"%@",[cardJson valueForKey:PARAM_CARD_TYPE]];
                    card.lastFour=[NSString stringWithFormat:@"%@",[cardJson valueForKey:PARAM_LAST_FOUR]];
                    card.createdAt=[NSString stringWithFormat:@"%@",[cardJson valueForKey:PARAM_CREATED_AT]];
                    card.updatedAt=[NSString stringWithFormat:@"%@",[cardJson valueForKey:PARAM_UPDATED_AT]];
                    card.paymentToken=[NSString stringWithFormat:@"%@",[cardJson valueForKey:PARAM_PAYMENT_TOKEN]];
                    card.customerId=[NSString stringWithFormat:@"%@",[cardJson valueForKey:PARAM_CUSTOMER_ID]];
                    card.isDefault=[[NSString stringWithFormat:@"%@",[cardJson valueForKey:PARAM_IS_DEFAULT]] boolValue];
                    [*arrForCards addObject:card];
                }
			
				return YES;
            }
            else
            {
                NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                 dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                               });

                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Login Response Not Valid Generate Exception %@",e);
        return NO;
    }
}
- (BOOL)parseAddCard:(NSString*)response toArray:(NSMutableArray * __strong *)arrForCards
{
    
   if ([UtilityClass isEmpty:response])
        return false;
    
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                Card *card=[[Card alloc]init];
                card.cardId=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_ID]];
                card.cardType=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_CARD_TYPE]];
                card.lastFour=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_LAST_FOUR]];
                card.createdAt=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_CREATED_AT]];
                card.updatedAt=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_UPDATED_AT]];
                card.paymentToken=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_PAYMENT_TOKEN]];
                card.customerId=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_CUSTOMER_ID]];
                card.isDefault=[[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_IS_DEFAULT]] boolValue];
                [*arrForCards addObject:card];
                return YES;
            }
            else
            {
                NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                               });
                
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Login Response Not Valid Generate Exception %@",e);
        return NO;
    }
    return false;
}
#pragma mark-AUTHENTICATION RELATED PARSING
- (BOOL)parseApiKeys:(NSString*)response
{
    NSLog(@"%@",response);
    @try
    {
        if (response)
        {
            NSDictionary *jsonResponse;
            if([self stringToJson:response To:&jsonResponse])
            {
                @try
                {
                    GoogleServerKey=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_GOOGLE_SERVER_KEY]];
                    HotlineAppKey=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_HOTLINE_KEY]];
                    HotlineAppId=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PARAM_HOTLINE_APP_ID]];
                    StripePublishableKey=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:PREF_STRIPE_KEY] ];
                    if (GoogleServerKey && HotlineAppId && HotlineAppId)
                    {
                        PREF.ServerKey=GoogleServerKey;
                        PREF.HotlineAppId=HotlineAppId;
                        PREF.HotlineAppKey=HotlineAppKey;
                        PREF.StripeKey=StripePublishableKey;
                        
                    }
                    else
                    {
                        GoogleServerKey=[NSString stringWithFormat:@"%@",PREF.serverKey ];
                        HotlineAppKey=[NSString stringWithFormat:@"%@",PREF.hotLineAppKey ];
                        HotlineAppId=[NSString stringWithFormat:@"%@",PREF.hotLineAppId ];
                        StripePublishableKey=[NSString stringWithFormat:@"%@",PREF.stripeKey];
                    }
                    
                    
                    return true;
                }
                @catch (NSException *exception)
                {
                    GoogleServerKey=[NSString stringWithFormat:@"%@",PREF.serverKey ];
                    HotlineAppKey=[NSString stringWithFormat:@"%@",PREF.hotLineAppKey ];
                    HotlineAppId=[NSString stringWithFormat:@"%@",PREF.hotLineAppId ];
                    StripePublishableKey=[NSString stringWithFormat:@"%@",PREF.stripeKey];
                    
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        else
            return false;
        
    }
    @catch(NSException *e)
    {
        NSLog(@"GET APIKEYS  Not Valid Generate Exception %@",e);
        return NO;
    }
}
- (BOOL)parseVerificationTypes:(NSString*)response
{
    @try
    {
        if (response)
        {
            NSLog(@"Verification Response %@",response);
            NSDictionary *jsonResponse;
            if([self stringToJson:response To:&jsonResponse])
            {
                    PREF.EmailOtpOn=[[jsonResponse valueForKey:PARAM_EMAIL_VERIFICATION_ON] boolValue];
                    PREF.SmsOtpOn=[[jsonResponse valueForKey:PARAM_SMS_VERIFICATION_ON] boolValue];
                    PREF.preTripTime=[jsonResponse valueForKey:PARAM_SCHEDULED_REQUEST_PRE_START_TIME];
                    PREF.ContactEmail=[jsonResponse valueForKey:PARAM_CONTACT_EMAIL];
                    PREF.IsPathDraw=[[jsonResponse valueForKey:PARAM_USER_PATH_DRAW] boolValue];
                    return true;
            }
            else
            {
                return false;
            }
        }
        else
            return false;
        
    }
    @catch(NSException *e)
    {
        NSLog(@"GET Application Settings Not Valid Generate Exception %@",e);
        return NO;
    }
}
#pragma mark- OTHER-PARSING METHODS
- (BOOL)parseFareEstimate:(NSString*)response
{
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                CurrentTrip *currentTrip=[CurrentTrip sharedObject];
                currentTrip.estimateFareTotal=[NSString stringWithFormat:@"%.2f",[[jsonResponse valueForKey:PARAM_ESTIMATE_FARE] doubleValue]];
                currentTrip.estimateFareTime=[NSString stringWithFormat:@"%.2f",[[jsonResponse valueForKey:PARAM_PRICE_PER_UNIT_TIME] doubleValue]];
                currentTrip.estimateFareDistance=[NSString stringWithFormat:@"%.2f",[[jsonResponse valueForKey:PARAM_DISTANCE]doubleValue]];
                return YES;
            }
            else
            {
                NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                               });
                
                return NO;
            }
        }
        else
        {
            return NO;
        }
    }
    @catch(NSException *e)
    {
        NSLog(@"GET FareEstimate Response Not Valid Generate Exception %@",e);
        return NO;
    }
    
}
- (BOOL)parseGetFavouriteDriver:(NSString*)response
{
	@try
	{
	NSDictionary *jsonResponse;
	if([self stringToJson:response To:&jsonResponse])
		{
		if([[jsonResponse valueForKey:SUCCESS]boolValue])
			{
			 FavouriteDriver *favDriver=[FavouriteDriver sharedObject];
			favDriver.arrFavDriver = [jsonResponse valueForKey:@"favourite_provider"];
			favDriver.defaultRadious = [jsonResponse valueForKey:@"default_Search_radious"];
			
			
			return YES;
			}
		else
			{
			NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
			dispatch_async(dispatch_get_main_queue(), ^
						   {
						   [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
						   });
			
			return NO;
			}
		}
	else
		{
		return NO;
		}
	}
	@catch(NSException *e)
	{
	NSLog(@"GET Favoruite deiver error %@",e);
	return NO;
	}
	
}
-(BOOL)parseVehicalTypeList:(NSString*)response toArray:(NSMutableArray * __strong *)arrForTypeList
{
    NSLog(@"%@",response);
    @try
    {
        NSDictionary *jsonResponse;
	
        if([self stringToJson:response To:&jsonResponse])
        {
            [*arrForTypeList removeAllObjects];
		
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                NSDictionary *cityDetail= [jsonResponse valueForKey:PARAM_CITY_DETAIL];
                NSArray *arr= [jsonResponse valueForKey:PARAM_CITY_TYPE];
                
                [CurrentTrip sharedObject].serverTime=[jsonResponse valueForKey:PARAM_SERVER_TIME];
                [CurrentTrip sharedObject].timeZone=[cityDetail valueForKey:PARAM_TIME_ZONE];
                
                for (NSDictionary *dict in arr)
                {
                    CarTypeDataModal *obj=[[CarTypeDataModal alloc]init];
                    NSDictionary *type_detail=[dict objectForKey:PARAM_TYPE_DETAILS];
                    obj.description=[type_detail valueForKey:PARAM_DESCRIPTION];
                    obj.id_=[dict valueForKey:PARAM_ID];
                    
                    obj.picture=[NSString stringWithFormat:@"%@%@",BASE_URL,[type_detail valueForKey:PARAM_TYPE_IMAGE_URL]];
                    
                    obj.name=[type_detail valueForKey:PARAM_TYPE_NAME];
                    obj.base_price_distance=[NSString stringWithFormat:@"%.2f",[[dict valueForKey:PARAM_BASE_PRICE_DISTANCE] doubleValue]];
                    obj.price_for_total_time=[NSString stringWithFormat:@"%.2f",[[dict valueForKey:PARAM_PRICE_FOR_TOTAL_TIME]doubleValue]];
                    obj.price_per_unit_distance=[NSString stringWithFormat:@"%.2f",[[dict valueForKey:PARAM_PRICE_PER_UNIT_DISTANCE]doubleValue]];
                    obj.base_price=[NSString stringWithFormat:@"%.2f",[[dict valueForKey:PARAM_BASEPRICE]doubleValue] ];
                    obj.cancellation_charge=[NSString stringWithFormat:@"%.2f",[[dict valueForKey:PARAM_CANCELLATION_FEE]doubleValue] ];
                    obj.max_space=[NSString stringWithFormat:@"%.0f",[[dict valueForKey:PARAM_MAX_SPACE]doubleValue] ];
                    obj.tax=[NSString stringWithFormat:@"%.2f",[[dict valueForKey:PARAM_TAX] doubleValue]];
                     obj.minFare=[NSString stringWithFormat:@"%.2f",[[dict valueForKey:PARAM_MIN_FARE]doubleValue ] ];
                    obj.surgeStartHour=[NSString stringWithFormat:@"%@",[dict valueForKey:PARAM_SURGE_START_HOUR]];
                    obj.surgeEndHour=[NSString stringWithFormat:@"%@",[dict valueForKey:PARAM_SURGE_END_HOUR]];
                    obj.sugrgeMultiplier=[NSString stringWithFormat:@"%@",[dict valueForKey:PARAM_SURGE_MULTIPLIEER]];
                    obj.isSurgeHour=[[dict valueForKey:PARAM_IS_SURGE_HOUR] boolValue ];
                    obj.isSelected=NO;

                    [*arrForTypeList addObject:obj];
                }
                NSDictionary *city_detail=[jsonResponse valueForKey:PARAM_CITY_DETAIL];
                PREF.CashAvailable=[[city_detail valueForKey:PARAM_IS_PAYMENT_MODE_CASH] boolValue];
                PREF.CardAvailable=[[city_detail valueForKey:PARAM_IS_PAYMENT_MODE_CARD] boolValue];
                
                CurrencySign=[jsonResponse valueForKey:PARAM_CURRENCY];
                NSString *DistanceUnit=[NSString stringWithFormat:@"%@",[city_detail valueForKey:PARAM_DISTANCE_UNIT]];
                if ([DistanceUnit isEqualToString:@"0"])
                {
                    DISTANCE_SUFFIX=NSLocalizedString(@"MILE", nil);
                }
                else
                {
                    DISTANCE_SUFFIX=NSLocalizedString(@"KILOMETER", nil);
                }
                return YES;
            }
            else
            {
                NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                               });
                
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"History Response Not Valid Generate Exception %@",e);
        return NO;
    }
    
    return NO;
    
}
#pragma mark
#pragma mark-Contact Parsing
- (BOOL)parseEmergencyContactList:(NSString*)response toArray:(NSMutableArray * __strong *)arrForContactList
{
    NSLog(@"%@",response);
    @try
    {
        NSDictionary *jsonResponse;
        
        if([self stringToJson:response To:&jsonResponse])
        {
            [*arrForContactList removeAllObjects];
            
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                NSArray *arrContact= [jsonResponse valueForKey:PARAM_EMERGENCY_CONTACT_DATA];
                
                for (NSDictionary *contactDetail in arrContact)
                {
                    Contact *contact=[[Contact alloc]init];
                    contact.contactId=[NSString stringWithFormat:@"%@",[contactDetail valueForKey:PARAM_ID] ];
                    contact.contactName=[NSString stringWithFormat:@"%@",[contactDetail valueForKey:PARAM_EMERGENCY_CONTACT_NAME]];
                    contact.contactNumber=[NSString stringWithFormat:@"%@",[contactDetail valueForKey:PARAM_PHONE]];
                    contact.isAlwaysShareRideDetail=[[contactDetail valueForKey:PARAM_IS_ALWAYS_SHARE_DETAIL] boolValue];
                    [*arrForContactList addObject:contact];
                }
                return YES;
            }
            else
            {
                NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                               });
                
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Contact Response Not Valid Generate Exception %@",e);
        return NO;
    }
    
    return NO;

}

- (BOOL)parseAddContact:(NSString*)response toArray:(NSMutableArray * __strong *)arrForContactsList
{
    
    if ([UtilityClass isEmpty:response])
        return false;
    
    @try
    {
        NSDictionary *jsonResponse;
        if([self stringToJson:response To:&jsonResponse])
        {
            if([[jsonResponse valueForKey:SUCCESS]boolValue])
            {
                NSDictionary *contactDetail =[jsonResponse valueForKey:PARAM_EMERGENCY_CONTACT_DATA];
                Contact *contact=[[Contact alloc]init];
                contact.contactId=[NSString stringWithFormat:@"%@",[contactDetail valueForKey:PARAM_ID] ];
                contact.contactName=[NSString stringWithFormat:@"%@",[contactDetail valueForKey:PARAM_EMERGENCY_CONTACT_NAME]];
                contact.contactNumber=[NSString stringWithFormat:@"%@",[contactDetail valueForKey:PARAM_PHONE]];
                contact.isAlwaysShareRideDetail=[[contactDetail valueForKey:PARAM_IS_ALWAYS_SHARE_DETAIL] boolValue];
                [*arrForContactsList addObject:contact];
                
                return YES;
            }
            else
            {
                NSString *errorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                dispatch_async(dispatch_get_main_queue(), ^
                               {
                                   [[UtilityClass sharedObject] showToast:NSLocalizedString(errorCode, nil)];
                               });
                
                return NO;
            }
        }
        else
        {
            return NO;
        }
        
    }
    @catch(NSException *e)
    {
        NSLog(@"Contact Response Not Valid Generate Exception %@",e);
        return NO;
    }
    return false;
}

#pragma mark-Parse Invalid Token
-(BOOL)parseSessionToken:(NSString*)response
{
    @try
    {
        if (response)
        {
            NSDictionary *jsonResponse;
            if([self stringToJson:response To:&jsonResponse])
            {
                if(![[jsonResponse valueForKey:SUCCESS]boolValue])
                {
                    NSString *strErrorCode=[NSString stringWithFormat:@"%@",[jsonResponse valueForKey:ERROR_CODE]];
                    
                    if ([strErrorCode isEqualToString:invalidTokenId])
                    {
                        NSLog(@"Logout %@",response);
                        [PREF clearPreference];
                        dispatch_async(dispatch_get_main_queue()
                                       , ^{
                                           [APPDELEGATE hideLoadingView];
                                           [APPDELEGATE goToMain];
                                           
                                       });
                        return false;
                        
                    }
                    return true;
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
        
    }
    @catch(NSException *e)
    {
        return true;
        
    }
    
    return true;

}
-(BOOL)parseSuccess:(NSString *)response
{
    @try
    {
        if (response)
        {
            NSDictionary *jsonResponse;
            if([self stringToJson:response To:&jsonResponse])
            {
                if([[jsonResponse valueForKey:SUCCESS]boolValue])
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
        else
        {
            return false;
        }
    }
    @catch(NSException *e)
    {
        return false;
    }
}
- (void)parseCountryListToArray:(NSMutableArray * __strong *)arrForCountry
{
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"countrycodes" ofType:@"json"];
        NSData *data = [NSData dataWithContentsOfFile:filePath];
        *arrForCountry = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        
}
@end
