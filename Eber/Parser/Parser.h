//
//  Parser.h
//  
//
//  Created by Elluminati Mini Mac 5 on 12/09/16.
//
//

#import <Foundation/Foundation.h>

@interface Parser : NSObject
- (BOOL)stringToJson:(NSString*)response To:(id*)json;
- (BOOL)parseTrip:(NSString*)response;
- (BOOL)parseProvider:(NSString*)response;
- (BOOL)parseTripStatus:(NSString*)response;
- (BOOL)parseInvoice:(NSString*)response;
- (BOOL)parseLogin:(NSString*)response;
- (BOOL)parseHistory:(NSString*)response toArray:(NSMutableArray * __strong *)arrForHistory;
- (BOOL)parseBooking:(NSString*)response toArray:(NSMutableArray * __strong *)arrForBooking;
- (BOOL)parseCards:(NSString*)response toArray:(NSMutableArray * __strong *)arrForCards andWalletAmount:(NSString * __strong *)walletAmount andCurrencyCode:(NSString * __strong *)walletCurrencyCode andWalletStatus:(BOOL *)walletStatus;
- (BOOL)parseAddCard:(NSString*)response toArray:(NSMutableArray * __strong *)arrForCards;
- (BOOL)parseVehicalTypeList:(NSString*)response toArray:(NSMutableArray * __strong *)arrForTypeList;
- (BOOL)isProfileUpdated:(NSString*)response;
- (BOOL)parseFareEstimate:(NSString*)response;
- (BOOL)parseApiKeys:(NSString*)response;
- (BOOL)parseVerificationTypes:(NSString*)response;
- (BOOL)parseEmergencyContactList:(NSString*)response toArray:(NSMutableArray * __strong *)arrForContactList;
- (BOOL)parseAddContact:(NSString*)response toArray:(NSMutableArray * __strong *)arrForContactsList;
- (BOOL)parseSessionToken:(NSString*)response;
- (BOOL)parseSuccess:(NSString*)response;
- (void)parseCountryListToArray:(NSMutableArray * __strong *)arrForCountry;
- (BOOL)parseGetFavouriteDriver:(NSString*)response;
+(Parser *) sharedObject;

@end
