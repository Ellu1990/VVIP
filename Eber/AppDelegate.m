//
//  AppDelegate.m
//    Eber Client
//
//  Created by My Mac on 6/14/16.
//  Copyright © 2016 Jaydeep. All rights reserved.
//
#import <GoogleMaps/GoogleMaps.h>
#import "AppDelegate.h"
#import "Reachability.h"
#import "NSObject+Constants.h"
#import "Stripe.h"
#import "UIColor+Colors.h"
#import "UtilityClass.h"
#import "AFNHelper.h"
#import "MapVc.h"
#import "TripVc.h"
#import "SWRevealViewController.h"
#import "Parser.h"
#import "PreferenceHelper.h"
#import "Hotline.h"


#define push_alert @"alert"
#define push_id  @"id"
#define push_aps @"aps"

#define push_provider_coming @"200"
#define push_provider_accept_request @"203"
#define push_provider_arrived @"204"
#define push_provider_start_trip @"206"
#define push_provider_end_trip @"209"
#define push_no_provider_arround_you @"213"
#define push_provider_cancel_trip @"214"
#define push_approved @"215"
#define push_decline @"216"
#define push_login_at_another_device @"220"

@import Firebase;

@interface AppDelegate ()
@end

@implementation AppDelegate
{
    
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	
	// Override point for customization after application launch.
	[FIRApp configure];
    [GIDSignIn sharedInstance].clientID = [FIRApp defaultApp].options.clientID;
	[GIDSignIn sharedInstance].delegate = self;
	[[FBSDKApplicationDelegate sharedInstance] application:application
					didFinishLaunchingWithOptions:launchOptions];
	
   
    /*Navigation Controller*/
   [[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init]
                                      forBarPosition:UIBarPositionAny
                                          barMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [[UINavigationBar appearance] setBarTintColor:[UIColor GreenColor]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [GMSServices provideAPIKey:GoogleKey];
    
    
    /*Hotline Integration*/
    /* Initialize Hotline*/
    
    HotlineConfig *config = [[HotlineConfig alloc]initWithAppID:@"e812e6d0-3a60-4f66-8efc-dd597d631f34"  andAppKey:@"397faeba-604f-4b0c-98ae-f5f043394c6e"];
    
    [[Hotline sharedInstance] initWithConfig:config];
   
    [self.window makeKeyAndVisible]; // or similar code to set a visible view
    
    /*  Set your view before the following snippet executes */
    
    /* Handle remote notifications */
    if ([[Hotline sharedInstance]isHotlineNotification:launchOptions]) {
        [[Hotline sharedInstance]handleRemoteNotification:launchOptions
                                              andAppstate:application.applicationState];
    }
    
    /* Any other code to be executed on app launch */
    
    /* Reset badge app count if so desired */
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];/* Initialize Hotline*/
    
   
    /* Enable remote notifications */

    
    /*Push Notification*/
	//-- Set Notification
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        
        UIUserNotificationType types = UIUserNotificationTypeBadge |
        UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        
        UIUserNotificationSettings *mySettings =
        [UIUserNotificationSettings settingsForTypes:types categories:nil];
        
        [application registerUserNotificationSettings:mySettings];
        [application registerForRemoteNotifications];
        
        
    }
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	  [FBSDKAppEvents activateApp];
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -
#pragma mark - Loading View

-(void)showLoadingWithTitle:(NSString *)title
{
    if (view==nil)
    {
        view=[[UIView alloc]initWithFrame:self.window.bounds];
        [view setBackgroundColor:[UIColor clearColor]];
        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake(15,15,45,45)];
        img.backgroundColor=[UIColor clearColor];
        img.contentMode = UIViewContentModeScaleToFill;
        img.clipsToBounds = YES;
        img.image=[UIImage imageNamed:@"progress"];
        img.center=view.center;
        img.transform =CGAffineTransformRotate(img.transform, 1.0);
        CABasicAnimation* rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        rotationAnimation.fromValue = @(0.0);
        rotationAnimation.toValue = @(M_PI * 2.0);
        rotationAnimation.duration = 1.0;
        rotationAnimation.cumulative = YES;
        rotationAnimation.repeatCount = HUGE_VALF;
        [img.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
        [view addSubview:img];
        [self.window addSubview:view];
        [self.window bringSubviewToFront:view];
    }

}

-(void)hideLoadingView
{
    
	if (view)
    {
		[view removeFromSuperview];
        view=nil;
        
	}
}

#pragma mark -
#pragma mark - sharedAppDelegate

+(AppDelegate *)sharedAppDelegate
{
	return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (BOOL)application: (UIApplication *)application openURL: (NSURL *)url sourceApplication: (NSString *)sourceApplication annotation: (id)annotation
{
	
	if([[NSUserDefaults standardUserDefaults]boolForKey:GOOGLE]==YES)
	{
		return  [[GIDSignIn sharedInstance] handleURL:url
								    sourceApplication:sourceApplication
										 annotation:annotation];

	}
	else
	{
		return [[FBSDKApplicationDelegate sharedInstance] application:application
												    openURL:url
										    sourceApplication:sourceApplication
												 annotation:annotation];
	}
}


- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
	withError:(NSError *)error {
	if (error == nil)
    {	NSLog(@"%@", user.profile.name);	}
    else {NSLog(@"%@", error.localizedDescription);}
}
- (BOOL)connected
{
	Reachability *reachability = [Reachability reachabilityForInternetConnection];
	NetworkStatus networkStatus = [reachability currentReachabilityStatus];
	return networkStatus != NotReachable;
}

#pragma  Handle Push
/*
 *Register For Retrive Remote Notification
 */
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    if (notificationSettings.types != UIUserNotificationTypeNone)
    {[application registerForRemoteNotifications];}
}
/*
 *Register For Retrive Device Token Notification
 */
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{

    [[Hotline sharedInstance] updateDeviceToken:deviceToken];
    NSString* devToken = [[[[deviceToken description]
                            stringByReplacingOccurrencesOfString:@"<"withString:@""]
                           stringByReplacingOccurrencesOfString:@">" withString:@""]
                          stringByReplacingOccurrencesOfString: @" " withString: @""];
    NSLog(@"%@",devToken);
    if(![UtilityClass isEmpty:devToken])
    {
        DEVICE_TOKEN=devToken;
        
    }
    else
    {
        DEVICE_TOKEN=@"";
    }
    if (PREF.isUserLogin)
    {
        PREF.deviceToken=DEVICE_TOKEN;
        [self wsRefreshToken];
    }

}


- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"error is : %@", error);
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    if ([[Hotline sharedInstance]isHotlineNotification:userInfo])
    {
        [[Hotline sharedInstance]handleRemoteNotification:userInfo andAppstate:[[UIApplication sharedApplication]applicationState]];
    }
    NSMutableDictionary *aps=[userInfo valueForKey:push_aps];
    NSString *msg=[aps valueForKey:push_alert];
    NSString *pushId=[msg valueForKey:push_id];
    if ([pushId isEqualToString:push_provider_accept_request])
    {
        UINavigationController *myVc=(UINavigationController*) self.window.rootViewController;
        SWRevealViewController *v=(SWRevealViewController*)myVc;
            UINavigationController *nv=(UINavigationController*)[v frontViewController];
            if ([nv.visibleViewController isKindOfClass:[MapVc class]])
            {
                MapVc *map=(MapVc*)nv.visibleViewController;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [map.timerForCheckReqStatus invalidate];
                    map.timerForCheckReqStatus=nil;
                    map.viewForDriver.hidden=YES;
                    [[AppDelegate sharedAppDelegate]hideLoadingView];
                    [map performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:self];
                    return; });
                
            }
            else
            {
                [[UtilityClass sharedObject]displayAlertWithTitle:NSLocalizedString(@"ALERT_TITLE_TRIP_CANCEL", nil) andMessage:NSLocalizedString(pushId, nil)];
            }
       }
    else if ([pushId isEqualToString:push_provider_cancel_trip])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self goToMap];
            [[CurrentTrip sharedObject]resetObject];
        });
        
    }
    else if ([pushId isEqualToString:push_approved])
    {
        checkApprove=true;
        PREF.UserApproved=1;
        SWRevealViewController *v=(SWRevealViewController*)self.window.rootViewController;
        UINavigationController *nv=(UINavigationController*)[v frontViewController];
            if ([nv.visibleViewController isKindOfClass:[MapVc class]])
            {
                MapVc *map=(MapVc*)nv.visibleViewController;
                [map checkPush];
            }
            else
            {
                [self goToMap];
            }
     }
    else if ([pushId isEqualToString:push_decline])
    {
        checkDecline=true;
        PREF.UserApproved=0;
        SWRevealViewController *v=(SWRevealViewController*)self.window.rootViewController;
        UINavigationController *nv=(UINavigationController*)[v frontViewController];
        if ([nv.visibleViewController isKindOfClass:[MapVc class]])
        {
                MapVc *map=(MapVc*)nv.visibleViewController;
                [map checkPush];
            }
        else
        {
                [self goToMap];
            }
        
    }
    else if([pushId isEqualToString:push_login_at_another_device])
    {
        PREF.UserLogin=NO;
        [self goToMain];
    }
    else if ([pushId isEqualToString:push_provider_coming]||[pushId isEqualToString:push_provider_arrived]||[pushId isEqualToString:push_provider_start_trip]||[pushId isEqualToString:push_provider_end_trip])
    {
            UINavigationController *myVc=(UINavigationController*) self.window.rootViewController;
            SWRevealViewController *v=(SWRevealViewController*)myVc;
            UINavigationController *nv=(UINavigationController*)[v frontViewController];
            if ([nv.visibleViewController isKindOfClass:[TripVc class]])
            {
                TripVc *trip=(TripVc*)nv.visibleViewController;
                [trip checkForTripStatus];
            }
            else
            {
                [[UtilityClass sharedObject]showToast:NSLocalizedString(pushId, nil)];
            }
    }
    else
    {
    }
}
-(void)handleRemoteNitification:(UIApplication *)application userInfo:(NSDictionary *)userInfo
{
    if ([[Hotline sharedInstance]isHotlineNotification:userInfo])
    {
        [[Hotline sharedInstance]handleRemoteNotification:userInfo andAppstate:[[UIApplication sharedApplication]applicationState]];
    }
    NSMutableDictionary *aps=[userInfo valueForKey:push_aps];
    NSString *msg=[aps valueForKey:push_alert];
    NSString *pushId=[msg valueForKey:push_id];
    if ([pushId isEqualToString:push_provider_accept_request])
    {
        UINavigationController *myVc=(UINavigationController*) self.window.rootViewController;
        SWRevealViewController *v=(SWRevealViewController*)myVc;
        UINavigationController *nv=(UINavigationController*)[v frontViewController];
        if ([nv.visibleViewController isKindOfClass:[MapVc class]])
        {
            MapVc *map=(MapVc*)nv.visibleViewController;
            dispatch_async(dispatch_get_main_queue(), ^{
                [map.timerForCheckReqStatus invalidate];
                map.timerForCheckReqStatus=nil;
                map.viewForDriver.hidden=YES;
                [[AppDelegate sharedAppDelegate]hideLoadingView];
                [map performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:self];
                return; });
            
        }
        else
        {
            [[UtilityClass sharedObject]displayAlertWithTitle:NSLocalizedString(@"ALERT_TITLE_TRIP_CANCEL", nil) andMessage:NSLocalizedString(push_id, nil)];
            
        }
    }
    else if ([pushId isEqualToString:push_provider_cancel_trip])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self goToMap];
            [[CurrentTrip sharedObject]resetObject];
        });
        
    }
    else if ([pushId isEqualToString:push_approved])
    {
        checkApprove=true;
        PREF.UserApproved=1;
        SWRevealViewController *v=(SWRevealViewController*)self.window.rootViewController;
        UINavigationController *nv=(UINavigationController*)[v frontViewController];
        if ([nv.visibleViewController isKindOfClass:[MapVc class]])
        {
            MapVc *map=(MapVc*)nv.visibleViewController;
            [map checkPush];
        }
        else
        {
            [self goToMap];
        }
    }
    else if ([pushId isEqualToString:push_decline])
    {
        checkDecline=true;
        PREF.UserApproved=0;
        SWRevealViewController *v=(SWRevealViewController*)self.window.rootViewController;
        UINavigationController *nv=(UINavigationController*)[v frontViewController];
        if ([nv.visibleViewController isKindOfClass:[MapVc class]])
        {
            MapVc *map=(MapVc*)nv.visibleViewController;
            [map checkPush];
        }
        else
        {
            [self goToMap];
        }
        
    }
    else if([pushId isEqualToString:push_login_at_another_device])
    {
        PREF.UserLogin=NO;
        [self goToMain];
    }
    else if ([pushId isEqualToString:push_provider_coming]||[pushId isEqualToString:push_provider_arrived]||[pushId isEqualToString:push_provider_start_trip]||[pushId isEqualToString:push_provider_end_trip])
    {
        UINavigationController *myVc=(UINavigationController*) self.window.rootViewController;
        SWRevealViewController *v=(SWRevealViewController*)myVc;
        UINavigationController *nv=(UINavigationController*)[v frontViewController];
        if ([nv.visibleViewController isKindOfClass:[TripVc class]])
        {
            TripVc *trip=(TripVc*)nv.visibleViewController;
            [trip checkForTripStatus];
        }
        else
        {
            [[UtilityClass sharedObject]showToast:NSLocalizedString(pushId, nil)];
        }
    }
    else
    {
    }
}
-(void)wsRefreshToken
{
        NSString *userId,*userToken,*deviceToken;
        userId=PREF.userId;
        userToken=PREF.userToken;
        deviceToken=DEVICE_TOKEN;
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        [dictparam setObject:userId forKey:PARAM_USER_ID];
        [dictparam setObject:userToken forKey:PARAM_TOKEN];
        [dictparam setObject:deviceToken forKey:PARAM_DEVICE_TOKEN];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:PUT];
        NSString *strUrl=[NSString stringWithFormat:@"%@%@",USER_REFRESH_TOKEN,PREF.userId];
        [afn getDataFromPath:strUrl withParamData:dictparam withBlock:^(id response, NSError *error) {
        }];
}
#pragma mark-Orientation ManageMent
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (void)goToMain
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nv = [sb instantiateInitialViewController];
    self.window.rootViewController = nv;
    [self.window makeKeyAndVisible];
    return;
}
-(void)goToMap
{
    HotlineUser *user=[HotlineUser sharedInstance];
    user.name=[NSString stringWithFormat:@"%@ %@",PREF.userFirstName,PREF.userLastName];
    user.email=[NSString stringWithFormat:@"%@",PREF.userEmail];
    user.phoneNumber=[NSString stringWithFormat:@"%@",PREF.userPhone];
    user.phoneCountryCode=[NSString stringWithFormat:@"%@",PREF.userCountryCode];
    user.externalID=PREF.userFirstName;
    [[Hotline sharedInstance] updateUser:user];

    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nv = [sb instantiateViewControllerWithIdentifier:@"map"];
    self.window.rootViewController = nv;
    [self.window makeKeyAndVisible];
    return;
}
/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}
#pragma mark -
#pragma mark - Directory Path Methods

- (NSString *)applicationCacheDirectoryString
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cacheDirectory = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return cacheDirectory;
}

#pragma  method automatically called when Internet is Not Available
-(void)onClickCustomDialogOk:(CustomAlertWithTitle *)view
{
      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}
@end
